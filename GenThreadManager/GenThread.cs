﻿/*
* Copyright (c) 2011 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace GenThreadManagement
{
    public interface IGenThread
    {
        bool DoWork();
        void Finalise();
        void Initialise();
        TimeSpan InitialPause { get; }
        TimeSpan Interval { get; }
        DateTime NextRunTime();
        TimeSpan? StartHourOffset { get; }
        string ThreadName { get; }
        int ThreadId { get; }
        void RunThread();
        ManualResetEvent ExitEvent { get; }
        bool IsRunning { get; set; }
        GenThreadInfo ThreadInfo { get; set; }
        void LogMessage(String component, String message, PVBCInterfaces.LogEntryType logEntryType = PVBCInterfaces.LogEntryType.Trace);
        void SetRefreshThreadTraceRequired();
    }

    public enum ErrorLimitAction
    {
        Continue,
        Pause,
        StopThread
    }

    public abstract class GenThread : IGenThread
    {
        private static int nextThreadId = 0;
        protected DateTime? PrevRunTime = null;
        protected bool InitialRun = true;

        public int ThreadId { get; internal set; }

        public ManualResetEvent ExitEvent { get; private set; }
        public PVBCInterfaces.IUtilityLog SystemServices;
        
        public bool IsRunning { get; set; }

        public GenThreadInfo ThreadInfo { get; set; }

        public PVBCInterfaces.IThreadControl ThreadControl;

        private bool RefreshThreadTraceRequired = true;

        public GenThread(PVBCInterfaces.IUtilityLog systemServices, PVBCInterfaces.IThreadControl threadControl = null)
        {
            ThreadControl = threadControl;
            SystemServices = systemServices;
            ConstructorCommon(systemServices);
            
            IsRunning = false;
            ThreadInfo = null;
        }

        public void SetRefreshThreadTraceRequired()
        {
            RefreshThreadTraceRequired = true;
        }

        private void RefreshThreadTrace()
        {           
            RefreshThreadTraceRequired = false;

            if (ThreadControl != null) // null indicates threadtrace not configured and not in use
            {
                // Place log entries to inform of changes in Thread Trace State
                bool old = SystemServices.ThreadTraceEnabled;
                SystemServices.ThreadTraceEnabled = ThreadControl.TraceEnabled;
                LogMessage("RefreshThreadTrace", "This Thread - ThreadTrace was " + old.ToString() + " is now " + SystemServices.ThreadTraceEnabled.ToString()
                    + " Thread Level Trace is " + SystemServices.EnableThreadTrace.ToString(), PVBCInterfaces.LogEntryType.Information);
            }
        }

        private void ConstructorCommon(PVBCInterfaces.IUtilityLog systemServices)
        {
            //SystemServices = systemServices;
            ExitEvent = new ManualResetEvent(false);
            ThreadId = nextThreadId++;
        }

        protected DateTime NextRunTimeStamp { get; private set; }

        public virtual DateTime NextRunTime()
        {
            DateTime now = DateTime.Now;
            DateTime nextTime;

            if (InitialRun)
                return now + InitialPause;

            // If StartHourOffset is not null - lock onto interval increments
            // otherwise just increment prev run time by interval
            if (StartHourOffset == null)
                if (PrevRunTime == null)
                    return now + Interval;
                else
                {
                    DateTime next =  PrevRunTime.Value + Interval;
                    if (next < now)
                        return now;
                    else
                        return next;
                }
                    
            // ensure hour offset is less than Inverval
            TimeSpan hourOffset = TimeSpan.FromSeconds((int)(StartHourOffset.Value.TotalSeconds % Interval.TotalSeconds));
            // sync interval number calc with offset time for that interval
            now -= hourOffset;
            int intervalNum = ((int)now.TimeOfDay.TotalSeconds) / ((int)Interval.TotalSeconds);

            nextTime = now.Date + TimeSpan.FromSeconds((intervalNum + 1) * Interval.TotalSeconds) + hourOffset;

            return nextTime;
        }

        public abstract TimeSpan Interval { get; }

        public virtual TimeSpan? StartHourOffset
        {
            get
            {
                return null;
            }
        }

        public virtual TimeSpan InitialPause
        {
            get
            {
                return TimeSpan.FromMinutes(0.0);
            }
        }

        public virtual void Initialise()
        {
        }

        public abstract bool DoWork();  // returns false if thread requires shutdown

        public abstract String ThreadName { get; }

        public virtual void Finalise()
        {
        }

        public void LogMessage(String component, String message, PVBCInterfaces.LogEntryType logEntryType = PVBCInterfaces.LogEntryType.Trace)
        {
            SystemServices.LogMessage(component, message, logEntryType);
        }

        public void RunThread()
        {
            RefreshThreadTrace();

            GenThreadManager.ThisGenThreadManager.ThreadHasStarted();

            LogMessage("GenThread.RunThread", "Thread starting - " + ThreadName, PVBCInterfaces.LogEntryType.Trace);

            try
            {
                Initialise();
            }
            catch (Exception e)
            {
                LogMessage("GenThread.RunThread", "Thread: " + ThreadName + " - Initialise Exception: " + e.Message, PVBCInterfaces.LogEntryType.ErrorMessage);
                return;
            }  

            IsRunning = true;

            LogMessage("GenThread.RunThread", "Thread started - " + ThreadName, PVBCInterfaces.LogEntryType.Information);

            while (IsRunning)
            {
                try
                {
                    if (RefreshThreadTraceRequired)
                        RefreshThreadTrace();

                    DateTime nextTime = NextRunTime();
                    NextRunTimeStamp = nextTime;

                    InitialRun = false;
                    int wait = 0;

                    if (ThreadInfo.CurrentErrorCount >= GenThreadInfo.ErrorLimit)
                    {
                        ErrorLimitAction action = StopOnErrorLimitReached();
                        if (action == ErrorLimitAction.StopThread)
                        {
                            LogMessage("GenThread.RunThread", "Thread error limit reached - stopping - " + ThreadName, PVBCInterfaces.LogEntryType.Information);
                            IsRunning = false;
                            ThreadInfo.NoRestartBefore = DateTime.Now.AddMinutes(10.0);
                            break;
                        }
                        else if (action == ErrorLimitAction.Pause)
                        {
                            wait = 10 * 60 * 1000;  // pause for 10 minutes if error limit reached
                            LogMessage("GenThread.RunThread", "Thread error limit reached - pausing - " + ThreadName, PVBCInterfaces.LogEntryType.Information);
                        }
                    }
                    else
                    {
                        if (wait == 0)
                            if (nextTime == DateTime.MaxValue)
                                wait = 60000;
                            else
                                wait = (int)(nextTime - DateTime.Now).TotalMilliseconds;

                        if (wait < 0)
                            wait = 0;
#if (DEBUG)
                        if (wait > 60000) // ensure time check every 60 seconds - respond to system time change in debug mode
                            wait = 60000;
#endif
                    }

                    // exit if ExitEvent is signaled
                    if (ExitEvent.WaitOne(wait))
                    {
                        IsRunning = false;
                        break;
                    }

                    if (DateTime.Now >= nextTime)
                    {
                        try
                        {
                            GenThreadManager.ThisGenThreadManager.ThreadOperationsLock.AcquireReaderLock(3000);
                        }
                        catch (ApplicationException)
                        {
                            continue;
                        }
                        IsRunning &= DoWork();
                        PrevRunTime = nextTime;
                        GenThreadManager.ThisGenThreadManager.ThreadOperationsLock.ReleaseReaderLock();
                    }                    
                }
                catch (ThreadInterruptedException)
                {
                    IsRunning = false;
                }
                catch (Exception e)
                {
                    LogMessage("GenThread.RunThread", "Thread - " + ThreadName + " - Exception: " + e.Message, PVBCInterfaces.LogEntryType.ErrorMessage);
                    IsRunning = false;
                }
                finally
                {
                    if (GenThreadManager.ThisGenThreadManager.ThreadOperationsLock.IsReaderLockHeld)
                        GenThreadManager.ThisGenThreadManager.ThreadOperationsLock.ReleaseReaderLock();
                }
            }

            try
            {
                LogMessage("GenThread.RunThread", "Thread stopping - " + ThreadName, PVBCInterfaces.LogEntryType.Trace);
                Finalise();
                LogMessage("GenThread.RunThread", "Thread stopped - " + ThreadName, PVBCInterfaces.LogEntryType.Information);
            }
            catch (Exception e)
            {
                LogMessage("GenThread.RunThread", "Thread: " + ThreadName + " - Finalise Exception: " + e.Message, PVBCInterfaces.LogEntryType.ErrorMessage);
            }

            GenThreadManager.ThisGenThreadManager.ThreadHasStopped();
        }

        // request stop thread at error limit - default is to stop
        // the thread can override to obtain a pause rather than a stop
        public virtual ErrorLimitAction StopOnErrorLimitReached()
        {
            return ErrorLimitAction.StopThread;
        }
    }  
}
