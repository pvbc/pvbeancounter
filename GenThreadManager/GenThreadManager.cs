﻿/*
* Copyright (c) 2011 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace GenThreadManagement
{
    public struct ThreadError
    {
        public DateTime ErrorTime;
        public String Message;

        public ThreadError(DateTime time, String message)
        {
            ErrorTime = time;
            Message = message;
        }
    }

    public class GenThreadInfo
    {
        public String Name = "";
        public IGenThread GenThread;
        public GenThreadInfo ParentGenThreadInfo;
        public Thread OS_Thread = null;
        public DateTime? StartTime = null;
        public int StartCount = 0;
        public bool AutoRestart;
        public DateTime? NoRestartBefore = null;

        public static readonly int ErrorLimit = 40;

        private List<ThreadError> ThreadErrors;
        private DateTime LastPurge = DateTime.MinValue;

        public GenThreadInfo(IGenThread thisThread, GenThreadInfo parentThreadInfo, bool autoRestart)
        {
            ThreadErrors = new List<ThreadError>();
            GenThread = thisThread;
            ParentGenThreadInfo =parentThreadInfo;
            AutoRestart = autoRestart;
        }

        private void PurgeOldErrors()
        {
            for (int i = 0; i < ThreadErrors.Count; )
            {
                if (ThreadErrors[i].ErrorTime.AddHours(1.0) < DateTime.Now)
                    ThreadErrors.RemoveAt(i);
                else
                    i++;
            }
            LastPurge = DateTime.Now;
        }

        public int CurrentErrorCount 
        {
            get
            {
                if (LastPurge.AddMinutes(10.0) < DateTime.Now)
                    PurgeOldErrors();  // avoid excessive purge calls
                return ThreadErrors.Count;
            }
        }

        public void RegisterError(DateTime time, String message)
        {
            PurgeOldErrors();
            ThreadError error = new ThreadError(time, message);
            ThreadErrors.Add(error);
        }
    }

    public class GenThreadManager
    {
        public static GenThreadManager ThisGenThreadManager = null;

        public List<GenThreadInfo> Threads;
        private PVBCInterfaces.IUtilityLog SystemServices;

        private int RunningCount = 0;
        private ManualResetEvent AllStopped;

        public ReaderWriterLock ThreadOperationsLock;

        private Object thisLock = new Object();

        public GenThreadManager(PVBCInterfaces.IUtilityLog systemServices)
        {
            ThisGenThreadManager = this;
            Threads = new List<GenThreadInfo>();
            SystemServices = systemServices;
            AllStopped = new ManualResetEvent(true);
            ThreadOperationsLock = new ReaderWriterLock();
        }

        public int AddThread(IGenThread genThread, GenThreadInfo parentThreadInfo, bool autoRestart)
        {
            // add thread to thread list
            GenThreadInfo info = new GenThreadInfo(genThread, parentThreadInfo, autoRestart);
            genThread.ThreadInfo = info;
            info.Name = genThread.ThreadName;
            info.OS_Thread = new Thread(info.GenThread.RunThread);
            info.OS_Thread.Name = info.Name;
            info.StartTime = null;
            info.StartCount = 0;

            Threads.Add(info);

            return info.GenThread.ThreadId;
        }

        public GenThreadInfo GetCurrentThreadInfo()
        {
            foreach (GenThreadInfo info in Threads)
                if (info.OS_Thread == Thread.CurrentThread)
                    return info;            
            return null;
        }

        public void RestartThread(GenThreadInfo info)
        {
            info.OS_Thread = new Thread(info.GenThread.RunThread);
            info.OS_Thread.Name = info.Name;
            StartThread(info);
            SystemServices.LogMessage("GenThreadManager.RestartThread", info.Name + " restarted as thread: " + info.GenThread.ThreadId, PVBCInterfaces.LogEntryType.Trace);
        }

        public void DoAutoRestart()
        {
            foreach (GenThreadInfo info in Threads)
            {
                if (info.AutoRestart && info.OS_Thread != null
                    && info.CurrentErrorCount < GenThreadInfo.ErrorLimit 
                    && (info.NoRestartBefore.HasValue ? (info.NoRestartBefore.Value <= DateTime.Now) : true)
                    && (info.OS_Thread.ThreadState == ThreadState.Stopped || info.OS_Thread.ThreadState == ThreadState.Aborted || info.OS_Thread.ThreadState == ThreadState.Unstarted))
                    RestartThread(info);
            }
        }

        private GenThreadInfo FindThread(IGenThread genThread)
        {
            foreach (GenThreadInfo info in Threads)
                if (info.GenThread == genThread)
                    return info;
            return null;
        }

        private GenThreadInfo FindThread(int threadNo)
        {
            foreach (GenThreadInfo info in Threads)
                if (info.GenThread.ThreadId == threadNo)
                    return info;
            return null;
        }

        public void StartThread(int id)
        {
            // start one thread
            GenThreadInfo info = FindThread(id);
            StartThread(info);
        }

        private void StartThread(GenThreadInfo info)
        {
            // GenThread will exit when ExitEvent is Set
            info.GenThread.ExitEvent.Reset();
            info.GenThread.IsRunning = false;
            info.OS_Thread.Start();
            info.StartTime = DateTime.Now;
            info.StartCount++;
        }

        private void StopThread(GenThreadInfo info, bool forceStop = false, bool deleteThread = false)
        {
            // stop one thread if still marked as running
            if (info.OS_Thread != null )
            {
                // tell GenThread to exit - polite request to stop (including child threads)
                info.AutoRestart = false;
                info.GenThread.IsRunning = false;
                info.GenThread.ExitEvent.Set();              

                // force the thread to stop - used to clean up stopped threads and threads with bad behaviour
                if (forceStop)
                {
                    if (!info.OS_Thread.Join(7000))
                    {
                        SystemServices.LogMessage("GenThreadManager.StopThread", "Killing thread: " + info.Name, PVBCInterfaces.LogEntryType.ErrorMessage);
                        info.OS_Thread.Interrupt();
                    }
                }
            }
            if (deleteThread && forceStop)
                Threads.Remove(info);
        }

        public void StopThread(int id, bool forceStop = false, bool deleteThread = false)
        {
            // stop one thread
            GenThreadInfo info = FindThread(id);
            if (info != null)
                StopThread(info, forceStop, deleteThread);
        }

        // Following is called as thread starts
        internal void ThreadHasStarted()
        {
            lock (thisLock)
            {
                // signal that at least 1 thread is running
                RunningCount++;
                AllStopped.Reset();
            }
            SystemServices.LogMessage("GenThreadManager", "Thread Started", PVBCInterfaces.LogEntryType.Trace);
        }

        // following is called as thread stops
        internal void ThreadHasStopped()
        {
            SystemServices.LogMessage("GenThreadManager", "Thread Stopped", PVBCInterfaces.LogEntryType.Trace);
            lock (thisLock)
            {
                // decrement running thread count
                RunningCount--;
                if (RunningCount < 1)
                {
                    // signal that all threads have stopped
                    RunningCount = 0;
                    AllStopped.Set();
                }
            }
        }

        public void StopThreads()
        {
            // stop any threads that are still running

            //First pass - request stop for each thread that has no parent thread
            //Threads with parent threads should be stopped by the parent
            foreach (GenThreadInfo info in Threads)
            {
                if ( info.ParentGenThreadInfo == null && info.OS_Thread != null)
                    StopThread(info);
            }

            // Wait for up to 15 seconds for all threads to stop
            AllStopped.WaitOne(15000);
            
            // Remove all threads forcing any threads still running to stop
            while (Threads.Count > 0)
            {
                GenThreadInfo info = Threads[0];
                if (info.OS_Thread != null)
                    StopThread(info, true);
                Threads.Remove(info);
            }

            lock (thisLock)
            {
                AllStopped.Set();
                RunningCount = 0;
            }
        }

        public void RefreshThreadTrace()
        {
            foreach (GenThreadInfo info in Threads)
            {
                info.GenThread.SetRefreshThreadTraceRequired();
            }
        }
    }
}
