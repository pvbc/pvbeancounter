﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.IO.Ports;
using System.Net.Sockets;
using System.Threading;
using Buffers;
using PVSettings;
using PVBCInterfaces;
using GenThreadManagement;

namespace DeviceStream
{
    public class DataChunk
    {
        public int ChunkMaxSize;
        public int ChunkSize;
        public byte[] ChunkBytes;

        public DataChunk(int size)
        {
            ChunkBytes = new byte[size];
            ChunkSize = 0;
            ChunkMaxSize = size;
        }
    }

    public interface IDeviceStream
    {
        //void ConfigurePort();
    }

    public abstract class DeviceStream 
    {
        protected DeviceManagerSettings DeviceManagerSettings;
        public Stream Stream { get; protected set; }
        protected bool UseStaticBuffer;
        protected IByteBuffer ByteBuffer;
        
        public abstract String DeviceName { get; }

        public bool DeviceError { get; set; }

        public DeviceStream(DeviceManagerSettings deviceManagerSettings, bool useStaticBuffer)
        {
            DeviceManagerSettings = deviceManagerSettings;
            Stream = null;
            UseStaticBuffer = useStaticBuffer;
            DeviceError = false;
            StartBuffer();
            // use original threaded byte buffer
            
        }

        public abstract void ConfigurePort();

        public DeviceStream(bool useStaticBuffer)
        {
            Stream = null;
            ByteBuffer = null;
            UseStaticBuffer = useStaticBuffer;
            DeviceError = false;
            // use new synchronous byte buffer
            
        }

        protected void LogMessage(String routine, String message, LogEntryType logEntryType = LogEntryType.DetailTrace)
        {
            GlobalSettings.LogMessage(StreamType + ": "+ routine, message, logEntryType);
        }

        protected virtual String StreamType { get { return "DeviceStream"; } }

        public virtual void PurgeStreamBuffers()
        {
            // do nothing is the default
        }

        //public abstract Stream GetStream();

        public static String BytesToString(byte[] input, long size)
        {
            long outSize = size;
            if (size > input.Length)
                size = input.Length;
            char[] output = new char[outSize];
            int i;
            for (i = 0; i < outSize; i++)
                output[i] = (char)input[i];

            StringBuilder sb = new StringBuilder(output.Length);
            sb.Append(output);

            return sb.ToString();
        }

        public bool Write(byte[] bytes, int start, int length)
        {
            if (DeviceError)
            {
                if (Reset())
                    DeviceError = false;
                else
                    return false;
            }
            
            try
            {
                if (GlobalSettings.SystemServices.LogMessageContent)
                    Buffers.ByteBuffer.FormatDump(bytes, start, length);

                //Stream stream = GetStream();
                Stream.Write(bytes, start, length);
                return true;
            }
            catch (Exception e)
            {                
                LogMessage("Write", "Exception writing to stream: " + e.Message, LogEntryType.Trace);
                DeviceError = true;               
                return false;
            }
        }

        protected virtual void StartBuffer()
        {
            ByteBufferBase byteBuffer;

            if (UseStaticBuffer)
                byteBuffer = new FixedByteBuffer();
            else
                byteBuffer = new ByteBuffer(this);
            
            ByteBuffer = byteBuffer;
        }

        public virtual void StopBuffer()
        {
        }

        public MatchInfo ReadFromBuffer(int length, int minLength, out byte[] bytes, TimeSpan? wait = null, bool consume = true)
        {
            bytes = new byte[length];
            MatchInfo info = ByteBuffer.ReadFromBuffer(bytes, length, minLength, wait, consume);
            return info;
        }

        public MatchInfo FindInBuffer(byte[] pattern, int maxSkip, bool extractSkipped, out DataChunk skipped, TimeSpan? wait = null, bool consume = true)
        {
            DataChunk chunk;
            
            MatchInfo info = ByteBuffer.FindInBuffer(pattern, maxSkip, consume, wait, extractSkipped, out chunk);
                
            skipped = chunk;
            return info;    
        }

        public void SetFixedBuffer(byte[] buffer, int firstByte, int bytesUsed)
        {
            if (!UseStaticBuffer)
                throw new Exception("DeviceStream - Attempt to set a non-static buffer");

            ((FixedByteBuffer)ByteBuffer).SetFixedBuffer(buffer, firstByte, bytesUsed);
        }

        public void ResetFixedBuffer()
        {
            if (!UseStaticBuffer)
                throw new Exception("DeviceStream - Attempt to reset a non-static buffer");

            ((FixedByteBuffer)ByteBuffer).ResetFixedBuffer();
        }

        public abstract bool Reset();
        
    }
}
