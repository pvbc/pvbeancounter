﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.IO.Ports;
using System.Net.Sockets;
using System.Threading;
using Buffers;
using PVSettings;
using PVBCInterfaces;
using GenThreadManagement;

namespace DeviceStream
{
    public class SerialStream : DeviceStream
    {
        private SerialPort SerialPort;
        //private bool IsOpen;

        private String PortName;
        private int BaudRate;
        private Parity Parity;
        private int DataBits;
        private StopBits StopBits;
        private Handshake Handshake;
        private int ReadTimeout;

        public SerialStream(DeviceManagerSettings deviceManagerSettings, bool useStaticBuffer)
            : base(deviceManagerSettings, useStaticBuffer)
        {
            if (UseStaticBuffer)
                SerialPort = null;
            else
                SerialPort = new SerialPort();

            //IsOpen = false;

            PortName = deviceManagerSettings.PortName;
            BaudRate = deviceManagerSettings.BaudRate == "" ? 9600 : Convert.ToInt32(deviceManagerSettings.BaudRate);
            Parity = SerialPortSettings.ToParity(deviceManagerSettings.Parity) == null ? System.IO.Ports.Parity.None :
                SerialPortSettings.ToParity(deviceManagerSettings.Parity).Value;
            DataBits = deviceManagerSettings.DataBits == "" ? 8 : Convert.ToInt32(deviceManagerSettings.DataBits);
            StopBits = SerialPortSettings.ToStopBits(deviceManagerSettings.StopBits) == null ? System.IO.Ports.StopBits.One :
                SerialPortSettings.ToStopBits(deviceManagerSettings.StopBits).Value;
            Handshake = SerialPortSettings.ToHandshake(deviceManagerSettings.Handshake) == null ? System.IO.Ports.Handshake.None :
                SerialPortSettings.ToHandshake(deviceManagerSettings.Handshake).Value;
            ReadTimeout = deviceManagerSettings.TimeOut.HasValue ? ((int)deviceManagerSettings.TimeOut.Value.TotalMilliseconds) : (deviceManagerSettings.MessageIntervalInt * 1000);

            if (!UseStaticBuffer)
                ConfigurePort();
        }

        public SerialStream(bool useStaticBuffer, 
            String portName, int baudRate, Parity parity, int dataBits, StopBits stopBits, Handshake handshake, int readTimeout)
            : base(null, useStaticBuffer)
        {
            if (UseStaticBuffer)
                SerialPort = null;
            else
                SerialPort = new SerialPort();

            //IsOpen = false;
            PortName = portName;
            BaudRate = baudRate;
            Parity = parity;
            DataBits = dataBits;
            StopBits = stopBits;
            Handshake = handshake;
            ReadTimeout = readTimeout;

            if (!UseStaticBuffer)
                ConfigurePort();
        }

        public override void ConfigurePort()
        {
            SerialPort.PortName = PortName;
            SerialPort.BaudRate = BaudRate;
            SerialPort.Parity = Parity;
            SerialPort.DataBits = DataBits;
            SerialPort.StopBits = StopBits;
            SerialPort.Handshake = Handshake;
            SerialPort.ReadTimeout = ReadTimeout;
            SerialPort.WriteTimeout = 20000;
        }

        public override string DeviceName
        {
            get 
            {
                if (UseStaticBuffer)
                    return "FixedBuffer_NoPort";
                return DeviceManagerSettings.PortName; 
            }
        }

        protected override String StreamType { get { return "SerialStream"; } }

        private void OpenStream()
        {
            if (UseStaticBuffer)
            {
                ((FixedByteBuffer)ByteBuffer).ResetFixedBuffer();
                return;
            }
            LogMessage("DeviceStream.GetStream", "Fixing stream");
            SerialPortRepair.Execute(PortName);
            LogMessage("DeviceStream.GetStream", "Opening stream");
            SerialPort.Open();

            //IsOpen = true;
            Stream = SerialPort.BaseStream;

            LogMessage("DeviceStream.GetStream", "Stream open");
        }

        public void Open()
        {
            if (UseStaticBuffer)
            {
                ((FixedByteBuffer)ByteBuffer).ResetFixedBuffer();
                return;
            }
            //DMF Simplify
            //if (!IsOpen)
            //    OpenStream();
            OpenStream();
            //Stream = SerialPort.BaseStream;
        }

        public override void PurgeStreamBuffers()
        {
            if (UseStaticBuffer)
            {
                ((FixedByteBuffer)ByteBuffer).ResetFixedBuffer();
                return;
            }
            SerialPort.DiscardOutBuffer();
            // pause to allow all pending data to arrive
            System.Threading.Thread.Sleep(300);
            SerialPort.DiscardInBuffer();
            StartBuffer();
            LogMessage("PurgeStreamBuffers", "SerialPort buffers purged: " + PortName, LogEntryType.Trace);
        }

        public void Close()
        {
            if (UseStaticBuffer)
                return;
            //if (IsOpen)
            //{
                LogMessage("DeviceStream.Close", "Closing stream", LogEntryType.Trace);

                base.StopBuffer();
                SerialPort.Close();
                SerialPort.Dispose();
                //SerialPort = null;
                Stream = null;
                //IsOpen = false;
                LogMessage("DeviceStream.Close", "Stream closed", LogEntryType.Trace);
            //}
        }

        public override bool Reset()
        {
            if (UseStaticBuffer)
                return true;
            ResetToClosed();
            try
            {
                // pause to allow port to settle!!!
                System.Threading.Thread.Sleep(15000);
                //IsOpen = false;
                SerialPort = new System.IO.Ports.SerialPort();
                ConfigurePort();
                Open();
                //IsOpen = true;
            }
            catch (Exception e)
            {
                LogMessage("Reset", "Exception opening SerialPort: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }
            return true;
        }

        public void ResetToClosed()
        {
            if (UseStaticBuffer)
                return;
            //if (IsOpen)
            {
                try
                {
                    Stream = null;
                    SerialPort.Close();
                }
                catch (Exception e)
                {
                    LogMessage("Reset", "Exception closing SerialPort: " + e.Message, LogEntryType.ErrorMessage);
                }                            
            }
        }
    }
}
