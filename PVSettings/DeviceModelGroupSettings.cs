﻿/*
* Copyright (c) 2013 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ComponentModel;
using System.Text;
using System.Xml;

namespace PVSettings
{
    public enum TranslateOptionType
    {
        Exchange,
        Algorithm
    }

    public class TranslateOption
    {
        public TranslateOptionType OptionType { get; private set; }
        public String Name { get; private set; }

        public TranslateOption(TranslateOptionType optionType, String name)
        {
            OptionType = optionType;
            Name = name;
        }
    }

    public class TranslateOptionList
    {
        public String Name { get; private set; }
        public List<TranslateOption> OptionList;

        public TranslateOptionList(String name, List<TranslateOption> optionList)
        {
            OptionList = optionList;
            Name = name;
        }
    }

    public class TranslateItem
    {
        public String ItemId { get; private set; }
        public byte[] ItemIdBytes;
        public String Name { get; private set; }
        public TranslateOptionList TranslateOptionList { get; private set; }

        public TranslateItem(String itemId, String name, TranslateOptionList translateOptionList )
        {
            ItemId = itemId;
            ItemIdBytes = null;
            Name = name;
            TranslateOptionList = translateOptionList;
        }
    }

    public class DeviceTranslateGroupSettings : SettingsBase, INotifyPropertyChanged
    {
        private DeviceManagementSettings DeviceManagementSettings;     
        public String Name { get; private set; }
        public List<TranslateOptionList> TranslateOptionLists;
        public List<TranslateItem> DeviceTranslateList;
        public RegisterSettings.RegisterValueType ItemIdType { get; private set; }
        public bool TranslateBytesLoaded = false;

        public DeviceTranslateGroupSettings(DeviceManagementSettings root, XmlElement element)
            : base(root, element)
        {
            DeviceManagementSettings = root;
            Name = element.GetAttribute("name");
            TranslateOptionLists = new List<TranslateOptionList>();
            DeviceTranslateList = new List<TranslateItem>();

            LoadLists(element);
        }

        private void LoadLists(XmlElement dmgElement)
        {
            foreach (XmlNode n in dmgElement.ChildNodes)
                if (n.NodeType == XmlNodeType.Element && n.Name == "translateoptionlists")
                {
                    foreach (XmlNode mol in n.ChildNodes)
                        if (mol.NodeType == XmlNodeType.Element && mol.Name == "translateoptionlist")
                        {
                            List<TranslateOption> translateOptions = new List<TranslateOption>();
                            foreach (XmlNode mon in mol.ChildNodes)
                            {
                                if (mon.NodeType == XmlNodeType.Element && mon.Name == "translateoption")
                                {
                                    string typeString = ((XmlElement)mon).GetAttribute("type");
                                    string name = ((XmlElement)mon).GetAttribute("name");
                                    TranslateOptionType mot;
                                    if (typeString == "algorithm")
                                        mot = TranslateOptionType.Algorithm;
                                    else
                                        mot = TranslateOptionType.Exchange;
                                    TranslateOption mo = new TranslateOption(mot, name);
                                    translateOptions.Add(mo);
                                }
                            }
                            TranslateOptionList translateOptionList = new TranslateOptionList(((XmlElement)mol).GetAttribute("name"), translateOptions);
                            TranslateOptionLists.Add(translateOptionList);
                        }
                }
                else if (n.NodeType == XmlNodeType.Element && n.Name == "devicetranslatelist")
                {
                    foreach (XmlNode dmn in n.ChildNodes)
                    {
                        if (dmn.NodeType == XmlNodeType.Element && dmn.Name == "translateitem")
                        {
                            string idString = ((XmlElement)dmn).GetAttribute("id");
                            string name = ((XmlElement)dmn).GetAttribute("name");
                            string listName = ((XmlElement)dmn).GetAttribute("listname");

                            TranslateItem dm = new TranslateItem(idString, name, FindTranslateOptionList(listName));

                            DeviceTranslateList.Add(dm);
                        }
                    }
                }
                else if (n.NodeType == XmlNodeType.Element && n.Name == "idtype")
                    ItemIdType = RegisterSettings.GetTypeFromName(((XmlElement)n).GetAttribute("value"));
        }

        private void LoadDeviceTranslateList(XmlElement dmgElement)
        {
             foreach (XmlNode n in dmgElement.ChildNodes)
                if (n.NodeType == XmlNodeType.Element && n.Name == "devicetranslatelist")
                    foreach (XmlNode dmn in n.ChildNodes)
                    {
                        if (dmn.NodeType == XmlNodeType.Element && dmn.Name == "translateitem")
                        {
                            string idString = ((XmlElement)dmn).GetAttribute("id");
                            string name = ((XmlElement)dmn).GetAttribute("name");
                            string listName = ((XmlElement)dmn).GetAttribute("listname");
                            
                            TranslateItem dm = new TranslateItem(idString, name, FindTranslateOptionList(listName));
                            DeviceTranslateList.Add(dm);
                        }
                    }     
        }

        public TranslateOptionList FindTranslateOptionList(String listName)
        {
            foreach (TranslateOptionList l in TranslateOptionLists)
                if (listName == l.Name)
                    return l;
            return null;
        }

        public TranslateItem FindTranslateItem(String name)
        {
            foreach(TranslateItem dm in  DeviceTranslateList)
                if (name == dm.Name)
                    return dm;
            return null;
        }

    }
}
