﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ComponentModel;
using System.Text;
using System.Xml;

namespace PVSettings
{
    public enum DeviceType
    {
        Unknown, // this marker must be first
        Inverter,
        EnergyMeter,
        GasMeter,
        WeatherStation,
        Consolidation,
        _TypeCount  // this marker must be last
    }

    public enum MeasureType
    {
        Unknown, // this marker must be first
        Energy,
        Quantity,
        _TypeCount   // This marker must be last
    }

    public enum FeatureType
    {
        // Note - the order and int values must not change - they are stored in the DB (bad/lazy design!!)
        Unknown = 0,    // this marker must be first
        EnergyAC,        
        YieldAC,        
        YieldDC,        
        ConsumptionAC,
        ImportAC,
        ExportAC,
        NetExportAC,
        GasConsumed,
        _TypeCount    // This marker must be last
    }

    public enum DeviceUsage
    {
        Manager = 0,
        Both,
        Device
    }

    public class FieldSettings
    {
        public FieldSettings(XmlElement element)
        {
            String val = element.GetAttribute("name");
            if (val == "")
                Name = "Error - Unknown";
            else
                Name = val;

            val = element.GetAttribute("description");
            if (val == "")
                Description = Name;
            else
                Description = val;
        }

        public FieldSettings()
        {
            Name = "";
            Description = "";
        }

        public string Name { get; private set; }
        public string Description { get; private set; }

        public static FieldSettings FindFieldByName(ObservableCollection<FieldSettings> list, String name)
        {
            foreach (FieldSettings fs in list)
                if (fs == null)
                    if (name == "")
                        return null;
                    else
                        continue;
                else if (fs.Name == name)
                    return fs;
            return null;
        }
    }

    public class FeatureSettings
    {
        public FeatureSettings(XmlElement element, ObservableCollection<FieldSettings> defaultFields)
        {
            MappableFields = null;

            String val = element.GetAttribute("type");
            if (val == "")
                FeatureType = FeatureType.Unknown;
            else
                FeatureType = FeatureTypeFromString(val);
            
            _FeatureId = 0;
            val = element.GetAttribute("id");
            if (val != "")
            {
                uint.TryParse(val, out _FeatureId);
            }
            val = element.GetAttribute("description");
            if (val == "")
                Name = FeatureType.ToString() + "_" + _FeatureId.ToString();
            else
                Name = val.Trim();

            val = element.GetAttribute("defaultsave");
            if (val == "")
                DefaultSave = (FeatureType == PVSettings.FeatureType.YieldAC || FeatureType == PVSettings.FeatureType.EnergyAC);
            else
                DefaultSave = val == "true";

            foreach (XmlNode e in SettingsBase.GetElementsByName(element, "field"))
            {
                if (MappableFields == null)
                {
                    MappableFields = new ObservableCollection<FieldSettings>();
                    MappableFields.Add(new FieldSettings()); // the null selection
                }
                MappableFields.Add(new FieldSettings((XmlElement)e));
            }
            if (MappableFields == null)
                MappableFields = defaultFields;
            if (MappableFields == null)
                MappableFields = new ObservableCollection<FieldSettings>();
        }

        private uint _FeatureId;
        public uint FeatureId { get { return _FeatureId; } private set { _FeatureId = value; } }
        public FeatureType FeatureType { get; private set; }
        public String Name { get; private set; }
        public bool DefaultSave { get; private set; }

        public ObservableCollection<FieldSettings> MappableFields;

        public static FeatureType FeatureTypeFromString(String featureType)
        {
            for (FeatureType ft = (FeatureType)0; ft < FeatureType._TypeCount; ft++)
                if (ft.ToString() == featureType)
                    return ft;
            return FeatureType.Unknown;
        }

        public static MeasureType MeasureTypeFromFeatureType(FeatureType featureType)
        {
            if (featureType >= FeatureType.EnergyAC && featureType <= FeatureType.NetExportAC)
                return MeasureType.Energy;
            else if (featureType == FeatureType.GasConsumed)
                return MeasureType.Quantity;
            else
                return MeasureType.Unknown;
        }

        public MeasureType MeasureType
        {
            get
            {
                return MeasureTypeFromFeatureType(FeatureType);
            }
        }

        public bool? IsAC
        {
            get
            {
                string val = FeatureType.ToString();
                if (val.EndsWith("AC"))
                    return true;
                if (val.EndsWith("DC"))
                    return false;
                return null;
            }
        }
    }

    public struct NumberFormat
    {
        public String Name;
        public System.Globalization.NumberFormatInfo NumberFormatInfo;
    }

    public class DeviceSettings : SettingsBase, INotifyPropertyChanged
    {
        private DeviceManagementSettings DeviceManagementSettings;
        private ObservableCollection<FeatureSettings> _FeatureList;
        private ObservableCollection<FieldSettings> _CustomFields;
        private ObservableCollection<ExchangeSettings> _ExchangeList;
        private ObservableCollection<ActionSettings> _AlgorithmList;
        private ProtocolSettings _ProtocolSettings = null;

        private ObservableCollection<NumberFormat> _NumberFormatList = null;
        public ObservableCollection<NumberFormat> NumberFormatList { get { return _NumberFormatList; } }

        public static System.Globalization.NumberFormatInfo SettingsNumberFormat;

        public bool HasSettingsUsage { get; private set; }
        public SettingsUsage UseConsolidationType { get; private set; }
        public SettingsUsage UsePVOutputSystem { get; private set; }
        public SettingsUsage UseAddress { get; private set; }
        public SettingsUsage UseSerialNo { get; private set; }
        public SettingsUsage UseMake { get; private set; }
        public SettingsUsage UseModel { get; private set; }
        public SettingsUsage UseQueryInterval { get; private set; }
        public SettingsUsage UseDBInterval { get; private set; }
        public SettingsUsage UseCalibrate { get; private set; }
        public SettingsUsage UseThreshold { get; private set; }
        public SettingsUsage UseHistoryAdjust { get; private set; }

        static DeviceSettings()
        {
            SettingsNumberFormat = new System.Globalization.NumberFormatInfo();
            SettingsNumberFormat.CurrencyDecimalSeparator = ".";
            SettingsNumberFormat.CurrencyGroupSeparator = ",";
            SettingsNumberFormat.NumberDecimalSeparator = ".";
            SettingsNumberFormat.NumberGroupSeparator = ",";
        }

        public DeviceSettings(DeviceManagementSettings root, XmlElement element, String typeName = null)
            : base(root, element)
        {
            DeviceManagementSettings = root;
            LoadNumberFormats();
            LoadCustomFields();
            LoadFeatures();
            LoadExchanges();
            LoadAlgorithms();
            _ProtocolSettings = root.GetProtocol(Protocol);
            LoadSettingsUsage();
        }

        private void LoadNumberFormats()
        {
            _NumberFormatList = new ObservableCollection<NumberFormat>();
            foreach (XmlNode e in Settings.ChildNodes)
            {
                if (e.NodeType == XmlNodeType.Element && e.Name == "numberformat")
                {
                    NumberFormat nf;
                    nf.Name = ((XmlElement)e).GetAttribute("name");
                    nf.NumberFormatInfo = new System.Globalization.NumberFormatInfo();

                    String val = ((XmlElement)e).GetAttribute("numberdecimalseparator");
                    if (val != null && val != "")
                    {
                        nf.NumberFormatInfo.NumberDecimalSeparator = val;
                        nf.NumberFormatInfo.CurrencyDecimalSeparator = val;
                    }

                    val = ((XmlElement)e).GetAttribute("currencydecimalseparator");
                    if (val != null && val != "")
                        nf.NumberFormatInfo.CurrencyDecimalSeparator = val;

                    val = ((XmlElement)e).GetAttribute("numbergroupseparator");
                    if (val != null && val != "")
                    {
                        nf.NumberFormatInfo.NumberGroupSeparator = val;
                        nf.NumberFormatInfo.CurrencyGroupSeparator = val;
                    }

                    val = ((XmlElement)e).GetAttribute("currencygroupseparator");
                    if (val != null && val != "")
                        nf.NumberFormatInfo.CurrencyGroupSeparator = val;

                    val = ((XmlElement)e).GetAttribute("currencysymbol");
                    if (val != null && val != "")
                        nf.NumberFormatInfo.CurrencySymbol = val;

                    val = ((XmlElement)e).GetAttribute("negativesign");
                    if (val != null && val != "")
                        nf.NumberFormatInfo.NegativeSign = val;

                    val = ((XmlElement)e).GetAttribute("positivesign");
                    if (val != null && val != "")
                        nf.NumberFormatInfo.PositiveSign = val;

                    _NumberFormatList.Add(nf);
                }
            }
        }

        public System.Globalization.NumberFormatInfo FindNumberFormat(string name)
        {
            foreach(NumberFormat nf in _NumberFormatList)
            {
                if (nf.Name == name)
                    return nf.NumberFormatInfo;
            }
            return null;
        }

        public System.Globalization.NumberFormatInfo DeviceNumberFormat
        {
            get
            {
                String val = GetValue("devicenumberformat");
                if (val == "")               
                    return System.Globalization.CultureInfo.CurrentCulture.NumberFormat;

                System.Globalization.NumberFormatInfo nfi = FindNumberFormat(val);

                if (nfi == null)
                    return System.Globalization.CultureInfo.CurrentCulture.NumberFormat;
                else
                    return nfi;                    
            }
        }

        public FeatureSettings FindFeature(FeatureType featureType, uint featureId)
        {
            foreach (FeatureSettings fs in FeatureList)
                if (fs.FeatureType == featureType && fs.FeatureId == featureId)
                    return fs;
            return null;
        }

        public List<FeatureSettings> FindFeatures(FeatureType featureType)
        {
            List<FeatureSettings> list = new List<FeatureSettings>();
            foreach (FeatureSettings fs in FeatureList)
                if (fs.FeatureType == featureType)
                    list.Add(fs);

            return list;
        }

        public ObservableCollection<FeatureSettings> FeatureList { get { return _FeatureList; } }
        public ObservableCollection<ExchangeSettings> ExchangeList { get { return _ExchangeList; } }
        public ObservableCollection<ActionSettings> AlgorithmList { get { return _AlgorithmList; } }
        public ProtocolSettings ProtocolSettings { get { return _ProtocolSettings; } }

        public static DeviceType StringToDeviceType(string deviceType)
        {
            for (int i = 1; i < (int)DeviceType._TypeCount; i++ )
                if (((DeviceType)i).ToString() == deviceType)
                    return (DeviceType)i;

            return DeviceType.Unknown;
        }

        public static MeasureType StringToMeasureType(string measureType)
        {
            for (int i = 1; i < (int)MeasureType._TypeCount; i++)
                if (((MeasureType)i).ToString() == measureType)
                    return (MeasureType)i;

            return MeasureType.Unknown;
        }

        private void LoadCustomFields()
        {
            _CustomFields = new ObservableCollection<FieldSettings>();
            _CustomFields.Add(new FieldSettings());  // the null selection entry
            XmlElement cf = SettingsBase.GetElementByName(Settings, "customfields");
            if (cf != null)
                foreach (XmlElement e in SettingsBase.GetElementsByName(cf, "field"))
                {
                    FieldSettings f = new FieldSettings(e);
                    _CustomFields.Add(f);
                }
        }

        private void LoadFeatures()
        {
            _FeatureList = new ObservableCollection<FeatureSettings>();

            XmlElement fe = SettingsBase.GetElementByName(Settings, "features");
            if (fe != null)            
                foreach (XmlElement eFeature in SettingsBase.GetElementsByName(fe, "feature"))
                {
                    if (eFeature.NodeType == XmlNodeType.Element && eFeature.Name == "feature")
                    {
                        FeatureSettings feature = new FeatureSettings((XmlElement)eFeature, _CustomFields);
                        _FeatureList.Add(feature);
                    }
                }                    
        }

        private static SettingsUsage GetSettingUsage(XmlElement elem)
        {
            string val = elem.GetAttribute("usage");
            if (val == null || val == "")
                return SettingsUsage.Hidden;
            if (val.ToLower() == "enabled")
                return SettingsUsage.Enabled;
            if (val.ToLower() == "disabled")
                return SettingsUsage.Disabled;
            return SettingsUsage.Hidden;
        }


        private void LoadSettingsUsage()
        {
            if (DeviceType == PVSettings.DeviceType.Consolidation)
            {
                UseConsolidationType = SettingsUsage.Disabled;
                UsePVOutputSystem =  SettingsUsage.Disabled;
            }
            else
            {
                UseConsolidationType = SettingsUsage.Hidden;
                UsePVOutputSystem = SettingsUsage.Hidden;
            }

            UseAddress = SettingsUsage.Hidden;
            UseSerialNo = SettingsUsage.Disabled;
            UseMake = SettingsUsage.Disabled;
            UseModel = SettingsUsage.Disabled;
            UseQueryInterval = SettingsUsage.Hidden;
            UseDBInterval = SettingsUsage.Hidden;
            UseCalibrate = SettingsUsage.Hidden;
            UseThreshold = SettingsUsage.Hidden;
            UseHistoryAdjust = SettingsUsage.Hidden;

            XmlElement settings = GetElement("settingsusage");
            if (settings == null)
            {
                HasSettingsUsage = false;
                return;
            }

            HasSettingsUsage = true;
            foreach (XmlNode node in settings.ChildNodes)
            {
                if (node.NodeType != XmlNodeType.Element)
                    continue;
                XmlElement elem = (XmlElement)node;
                if (elem.Name == "address")
                    UseAddress = GetSettingUsage(elem);
                else if (elem.Name == "serialno")
                    UseSerialNo = GetSettingUsage(elem);
                else if (elem.Name == "make")
                    UseMake = GetSettingUsage(elem);
                else if (elem.Name == "model")
                    UseModel = GetSettingUsage(elem);
                else if (elem.Name == "queryinterval")
                    UseQueryInterval = GetSettingUsage(elem);
                else if (elem.Name == "dbinterval")
                    UseDBInterval = GetSettingUsage(elem);
                else if (elem.Name == "calibrate")
                    UseCalibrate = GetSettingUsage(elem);
                else if (elem.Name == "threshold")
                    UseThreshold = GetSettingUsage(elem);
                else if (elem.Name == "histadjust")
                    UseHistoryAdjust = GetSettingUsage(elem);
            }
        }

        private void LoadExchanges()
        {
            _ExchangeList = new ObservableCollection<ExchangeSettings>();
            foreach (XmlNode e in Settings.ChildNodes)
            {
                if (e.NodeType == XmlNodeType.Element && e.Name == "exchange")
                {
                    ExchangeSettings command = new ExchangeSettings(DeviceManagementSettings, (XmlElement)e);
                    _ExchangeList.Add(command);
                }
            }
        }

        private void LoadAlgorithms()
        {
            _AlgorithmList = new ObservableCollection<ActionSettings>();
            foreach (XmlNode e in Settings.ChildNodes)
            {
                if (e.NodeType == XmlNodeType.Element && e.Name == "algorithm")
                {
                    ActionSettings algorithm = new ActionSettings(DeviceManagementSettings, (XmlElement)e);
                    _AlgorithmList.Add(algorithm);
                }
            }
        }

        public override string ToString()
        {
            return Id;
        }

        public FeatureSettings GetFeatureSettings(FeatureType featureType, uint featureId)
        { 
            foreach (FeatureSettings fs in _FeatureList)
                if (fs.FeatureType == featureType && fs.FeatureId == featureId)
                    return fs;
            return null;
        }

        public String DeviceAlgorithm { get { return GetValue("devicealgorithm"); } }

        public DeviceUsage Usage
        {
            get
            {
                String val = GetValue("usage");
                if (val == "" || val == "Both")
                    return DeviceUsage.Both;
                if (val == "Manager")
                    return DeviceUsage.Manager;
                else
                    return DeviceUsage.Device;
            }
        }

        public String Name
        {
            get
            {
                String val = Settings.GetAttribute("name");
                if (val == null || val == "") // legacy style
                    val = GetValue("specification");
                return val;
            }
        }

        public String Manufacturer { get { return GetValue("manufacturer"); }}

        public String Model { get { return GetValue("model"); } }

        public UInt64 Address 
        { 
            get 
            { 
                string val = GetValue("address");
                if (val == null || val == "")
                    return 1;
                return UInt64.Parse(val);
            } 
        }

        public String Id { get { return DeviceTypeName + ": " + Name + " - " + Version; } }

        public String Description 
        { 
            get 
            {
                String val = GetValue("description");
                string status = Status;
                if (val == "")
                    if (status == "")
                        return DeviceTypeName + ": " + Name + " - " + Version; 
                    else
                        return DeviceTypeName + ": " + Name + " - " + Version + " - " + status; 
                else
                    if (status == "")
                        return DeviceTypeName + ": " + val;
                    else
                        return DeviceTypeName + ": " + val + " - " + status; 
            } 
        }

        public struct DeviceName
        {
            public string Description;
            public string Id;
            public string Name;
        }

        public bool NamesContains(String id)
        {
            List<DeviceName> names = Names;
            foreach (DeviceName dn in names)
                if (dn.Id == id)
                    return true;
            return false;
        }

        public string NamesNameOfId(String id)
        {
            List<DeviceName> names = Names;
            foreach (DeviceName dn in names)
                if (dn.Id == id)
                    return dn.Name;
            return "My Device";
        }

        public List<DeviceName> Names
        {
            get
            {
                List<DeviceName> names = new List<DeviceName>();
                DeviceName baseName;
                baseName.Description = Description;
                baseName.Id = Id;
                baseName.Name = Name;
                names.Add(baseName);
                
                foreach (XmlNode e in Settings.ChildNodes)
                {
                    if (e.NodeType == XmlNodeType.Element && e.Name == "synonym")
                    {
                        XmlAttribute valueAttrib = (XmlAttribute)e.Attributes.GetNamedItem("value");
                        XmlAttribute idAttrib = (XmlAttribute)e.Attributes.GetNamedItem("id");
                        XmlAttribute idName = (XmlAttribute)e.Attributes.GetNamedItem("name");
                        if (valueAttrib != null && idAttrib != null)
                        {
                            DeviceName name;
                            
                            if (Status == "")
                                name.Description = DeviceTypeName + ": " + valueAttrib.Value;
                            else
                                name.Description = DeviceTypeName + ": " + valueAttrib.Value +" - " + Status; 
                            
                            name.Id = idAttrib.Value;
                            if (idName == null)
                                name.Name = Id;
                            else
                                name.Name = idName.Value;

                            names.Add(name);
                        }
                    }
                }
                return names;
            }
        }

        public List<DeviceName> LegacyNames
        {
            get
            {
                List<DeviceName> names = new List<DeviceName>();
                DeviceName baseName;
                baseName.Description = Description;
                baseName.Id = Id;

                foreach (XmlNode e in Settings.ChildNodes)
                {
                    if (e.NodeType == XmlNodeType.Element && e.Name == "legacyid")
                    {
                        XmlAttribute idAttrib = (XmlAttribute)e.Attributes.GetNamedItem("id");
                        if (idAttrib != null)
                        {
                            DeviceName name;
                            
                            name.Description = Description;
                            name.Id = idAttrib.Value;
                            name.Name = name.Id;
                            names.Add(name);
                        }
                    }
                }
                return names;
            }
        }

        public DeviceType DeviceType
        {
            get
            {
                return StringToDeviceType(GetValue("devicetype"));
            }
        }

        public String Protocol
        {
            get
            {
                return GetValue("protocol");
            }
        }

        public struct GroupName
        {
            public string Name;
            public string Id;
        }

        public List<GroupName> DeviceGroups
        {
            get
            {
                List<GroupName> names = new List<GroupName>();                
                foreach (XmlNode e in Settings.ChildNodes)
                {
                    if (e.NodeType == XmlNodeType.Element && e.Name == "groupname")
                    {
                        XmlAttribute valueAttrib = (XmlAttribute)e.Attributes.GetNamedItem("value");
                        XmlAttribute idAttrib = (XmlAttribute)e.Attributes.GetNamedItem("id");
                        
                        if (valueAttrib != null)
                        {
                            GroupName name;
                            name.Name = valueAttrib.Value;
                            if (idAttrib != null)
                                name.Id = idAttrib.Value;
                            else
                                name.Id = name.Name;
                            names.Add(name);
                        }
                    }
                }
                List<GroupName> protocolGroups = ProtocolSettings.DeviceGroups;
                foreach (GroupName g in protocolGroups)
                {
                    names.Add(g);
                }
                if (names.Count == 0)
                {
                    GroupName name;
                    name.Name = Protocol;
                    name.Id = name.Name;
                    names.Add(name);
                }
                return names;
            }
        }

        public bool StaticReset
        {
            get
            {
                String val = GetValue("staticreset");
                if (val == "true")
                    return true;
                else
                    return false;
            }
        }

        public bool ThreePhase
        {
            get
            {
                String val = GetValue("threephase");
                if (val == "true")
                    return true;
                else
                    return false;
            }
        }

        public String DeviceTypeName
        {
            get
            {
                String val = GetValue("devicetype");
                return val;
            }
        }

        public String Status
        {
            get
            {
                String val = GetValue("status");
                return val;
            }
        }

        public String Version
        {
            get
            {
                String val = GetValue("version");
                return val;
            }
        }

        public bool HasStartOfDayEnergyDefect
        {
            get
            {
                String val = GetValue("hasstartofdayenergydefect");
                return val == "true";
            }
        }

        public int CrazyDayStartMinutes
        {
            get
            {
                String val = GetValue("crazydaystartminutes");
                if (val == "")
                    return 90;
                return Int32.Parse(val);
            }
        }

        public bool UseHistory
        {
            get
            {
                return GetValue("usehistory") == "true";
            }
        }

    }
}

