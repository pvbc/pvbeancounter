﻿/*
* Copyright (c) 2013 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Permissions;
using System.Security;
using System.IO;
using System.Security.AccessControl;
using PVSettings;
using System.ServiceProcess;
using MackayFisher.Utilities;

namespace PVSettings
{
    public class AutoConfigPVOutput
    {
        private ApplicationSettings ApplicationSettings;
        private bool SettingsUpdated;
        const String NewLine = " \r\n";

        public AutoConfigPVOutput()
        {
            ApplicationSettings = GlobalSettings.ApplicationSettings;
            SettingsUpdated = false;
        }

        private string CheckConsolidationPVOutputReference()
        {
            string message = "";

            foreach (DeviceManagerDeviceSettings cd in ApplicationSettings.AllConsolidationDevicesList)
            {
                if (cd.ConsolidationType == ConsolidationType.PVOutput)
                {
                    if (cd.PVOutputSystem == "")
                        message += "Found PVOutput consolidation without a PVOutput System Id" + NewLine;
                    else
                    {
                        PvOutputSiteSettings site = ApplicationSettings.FindPVOutputBySystemId(cd.PVOutputSystem);
                        if (site == null)
                        {
                            message += "Consolidation PVOutput System Id: " + cd.PVOutputSystem + " - Does not match a configured PVOutput System - PVOutput System Id cleared" + NewLine;
                            cd.PVOutputSystem = "";
                            SettingsUpdated = true;
                        }
                    }
                }
            }
            return message;
        }

        private List<DeviceManagerDeviceSettings> GetPVOutputConsolidation(string systemId)
        {
            List<DeviceManagerDeviceSettings> deviceList = new List<DeviceManagerDeviceSettings>();

            foreach (DeviceManagerDeviceSettings cd in ApplicationSettings.AllConsolidationDevicesList)
            {
                if (cd.ConsolidationType == ConsolidationType.PVOutput && systemId == cd.PVOutputSystem)
                    deviceList.Add(cd);                
            }
            return deviceList;
        }

        private string CreateConsolidationDevice(string systemId)
        {
            string message = "";

            int cdmCount = 0;
            DeviceManagerSettings cdms = null;

            foreach (DeviceManagerSettings dms in ApplicationSettings.DeviceManagerListCollection)
            {
                if (dms.ManagerType == DeviceManagerType.Consolidation)
                {
                    cdmCount++;
                    cdms = dms;
                }
            }

            if (cdmCount == 0)
            {
                cdms = ApplicationSettings.AddDeviceManager();
                cdms.SetDeviceGroupName("Consolidation");
                cdmCount++;
                SettingsUpdated = true;
                message += "Added one Consolidation Device Manager" + NewLine;
            }
            else if (cdmCount > 1)
                message += "Found " + cdmCount + " Consolidation Device Managers - only one is required" + NewLine;

            DeviceManagerDeviceSettings cds = cdms.AddDevice();
            cds.ConsolidationType = ConsolidationType.PVOutput;
            cds.PVOutputSystem = systemId;
            SettingsUpdated = true;

            message += "Added a Consolidation Device for PVOutput System Id: " + systemId + NewLine;

            return message;
        }

        private string LinkConsolidationDevice(string systemId)
        {
            String message = "";

            List<DeviceManagerDeviceSettings> deviceList = GetPVOutputConsolidation("");
            if (deviceList.Count > 0)
            {
                deviceList[0].ConsolidationType = ConsolidationType.PVOutput;
                deviceList[0].PVOutputSystem = systemId;
                SettingsUpdated = true;
                message += "Modified a Consolidation Device to reference PVOutput System Id: " + systemId + NewLine;
            }
            else
                message += CreateConsolidationDevice(systemId);

            return message;
        }

        private bool AddConsolidation(FeatureType fromFeatureType, FeatureType toFeatureType, uint featureId, DeviceManagerDeviceSettings dsFrom, 
            DeviceManagerDeviceSettings dsTo, ConsolidateDeviceSettings.OperationType operation)
        {
            // PVOutput does not support 3 phase - add phases together
            // Convert EnergyAC to ConsumptionAC (used with EnergyMeters)
            FeatureSettings ftTo = dsTo.DeviceSettings.FindFeature(toFeatureType, 0);
            if (ftTo == null)
                return false;
            ConsolidateDeviceSettings cds;
            cds = dsFrom.AddDeviceConsolidation(operation);
            
            cds.ConsolidateFromFeature = dsFrom.DeviceSettings.FindFeature(fromFeatureType, featureId);
            cds.ConsolidateFromDevice = dsFrom;

            cds.ConsolidateToDevice = dsTo;
            
            cds.ConsolidateToFeature = ftTo;  
            SettingsUpdated = true;
            return true;
        }

        private bool HasFieldConsolidations(String systemId, DeviceManagerDeviceSettings ds)
        {
            foreach (ConsolidateDeviceSettings c in ds.ConsolidateToDevices.Collection)
                if (c.ConsolidateToDevice != null
                && c.ConsolidateToDevice.ConsolidationType == ConsolidationType.PVOutput
                && c.ConsolidateToDevice.PVOutputSystem == systemId
                && c.ConsolidateToField != null)
                    return true;

            return false;
        }

        private string RemoveConsolidations(String systemId, FeatureType fromFeatureType, DeviceManagerDeviceSettings ds)
        {
            string message = "";

            // build list of PVOutput consolidations - ignore others
            List<ConsolidateDeviceSettings> consols = new List<ConsolidateDeviceSettings>();
            foreach (ConsolidateDeviceSettings c in ds.ConsolidateToDevices.Collection)
                if (c.ConsolidateToDevice == null
                || c.ConsolidateToDevice.ConsolidationType == ConsolidationType.PVOutput 
                && c.ConsolidateToDevice.PVOutputSystem == systemId
                && c.ConsolidateFromFeatureType == fromFeatureType)
                    consols.Add(c);

            if (consols.Count > 0)
            {
                message += "Removing PVOutput: " + systemId + " - Feature: " + fromFeatureType + " consolidations from Device: " + ds.Name + NewLine;
                foreach (ConsolidateDeviceSettings c in consols)
                    ds.DeleteDeviceConsolidation(c);
            }
            return message;
        }

        private string ConsolidateDeviceToSystem(FeatureType fromFeatureType, FeatureType toFeatureType, DeviceManagerDeviceSettings ds, string systemId, 
            ConsolidateDeviceSettings.OperationType operation = ConsolidateDeviceSettings.OperationType.Add)
        {
            string message = "";

            FeatureSettings ftFrom = ds.DeviceSettings.FindFeature(fromFeatureType , 0);
            if (ftFrom == null)
                return message;

            List<DeviceManagerDeviceSettings> list = GetPVOutputConsolidation(systemId);
            
            bool added = AddConsolidation(fromFeatureType, toFeatureType, 0, ds, list[0], operation);
            if (ds.DeviceSettings.ThreePhase == true)
            {
                AddConsolidation(fromFeatureType, toFeatureType, 1, ds, list[0], operation);
                AddConsolidation(fromFeatureType, toFeatureType, 2, ds, list[0], operation);
            }

            if (added)
                if (operation == ConsolidateDeviceSettings.OperationType.Add)
                    message += "Add Device: " + ds.Name + " - " + fromFeatureType + " to " + toFeatureType + NewLine;    
                else
                    message += "Subtract Device: " + ds.Name + " - " + fromFeatureType + " from " + toFeatureType + NewLine;
            
            return message;
        }

        private void GetDeviceTypeCounts(out int inverters, out int consumption, out int netExport)
        {
            inverters = 0;
            consumption = 0;
            netExport = 0;

            // detect inverters first - important for NetExport coniderations
            foreach (DeviceManagerDeviceSettings ds in ApplicationSettings.AllDevicesList)
            {
                if (ds.DeviceType == DeviceType.Inverter)
                {
                    FeatureSettings fs = ds.DeviceSettings.GetFeatureSettings(FeatureType.YieldAC, 0);
                    if (fs != null)
                        inverters++;
                }
            }

            foreach (DeviceManagerDeviceSettings ds in ApplicationSettings.AllDevicesList)
            {
                if (ds.DeviceType == DeviceType.EnergyMeter)
                {
                    uint address;
 
                    if (ds.DeviceSettings.Name == "EW4009")
                        address = 1;
                    else
                        address = 0;        // this is correct for CC128 and DSM

                    FeatureSettings fs = ds.DeviceSettings.GetFeatureSettings(FeatureType.NetExportAC, address);
                    // only auto configure net export if inverters present
                    if (fs != null && inverters > 0)
                        netExport++;
                    else  // no auto consumption if net export found
                    {
                        fs = ds.DeviceSettings.GetFeatureSettings(FeatureType.EnergyAC, address);
                        if (fs != null)
                            consumption++;
                    }
                }
            }
        }

        public String ConfigurePVOutput(String selectedSystemId = "")
        {
            String message = "";

            message += CheckConsolidationPVOutputReference();

            int pvoCount = 0;
            String systemId = selectedSystemId;

            if (ApplicationSettings.PvOutputSystemList.Collection.Count > 1 && systemId == "")
            {
                message += NewLine + "Multiple PVOutput systems found. You must select a specific system to configure." + NewLine + NewLine;
                return message;
            }

            foreach (PvOutputSiteSettings pvo in ApplicationSettings.PvOutputSystemList.Collection)
            {
                pvoCount++;
                if (pvo.SystemId == "")
                    message += "Found PVOutput system without a PVOutput System Id" + NewLine;
                else
                {
                    if (systemId == "")
                        systemId = pvo.SystemId;
                    List<DeviceManagerDeviceSettings> deviceList = GetPVOutputConsolidation(pvo.SystemId);
                    if (deviceList.Count == 0)
                        message += LinkConsolidationDevice(pvo.SystemId);
                    else if (deviceList.Count > 1)
                        message += NewLine + "PVOutput System Id: " + pvo.SystemId + " - referenced from "
                            + deviceList.Count + " Consolidation Devices - YOU MUST REMOVE ONE" + NewLine + NewLine;
                }
            }

            if (pvoCount == 0)
            {
                message += NewLine + "No PVOutput systems are defined - Device readings cannot be sent to PVOutput" + NewLine;
                return message;
            }
                              
            if (systemId == "")
            {
                message += NewLine + "The PVOutput system has no System Id - Device readings cannot be sent to PVOutput" + NewLine;
                return message;
            }

            foreach(DeviceManagerDeviceSettings ds in ApplicationSettings.AllDevicesList)
                if (HasFieldConsolidations(systemId, ds))
                    return NewLine + 
                        "Field level PVOutput consolidation entries detected - cannot auto-configure as these field level consolidations will be lost" + 
                        NewLine + "Remove the field level consolidation entries if you wish to auto-configure PVOutput" + NewLine + NewLine;

            if (message == "")
                message = NewLine + "PVOutput and Consolidation Devices were correctly configured" + NewLine + NewLine;

            if (pvoCount > 1)
                message += NewLine + pvoCount + " PVOutput systems are defined - Auto configuration is using system: " + systemId + NewLine;

            int inverters;
            int consumption;
            int netExport;

            GetDeviceTypeCounts(out inverters, out consumption, out netExport);

            foreach (DeviceManagerDeviceSettings ds in ApplicationSettings.AllDevicesList)
            {
                if (ds.DeviceSettings.DeviceType == DeviceType.Inverter) 
                    message += RemoveConsolidations(systemId, FeatureType.YieldAC, ds);
                else if (ds.DeviceSettings.DeviceType == DeviceType.EnergyMeter)
                {
                    message += RemoveConsolidations(systemId, FeatureType.EnergyAC, ds);      
                    message += RemoveConsolidations(systemId, FeatureType.NetExportAC, ds);
                }
            }

            foreach (DeviceManagerDeviceSettings ds in ApplicationSettings.AllDevicesList)
            {
                if (ds.DeviceSettings.DeviceType == DeviceType.Inverter)
                {
                    if (inverters > 0)
                    {
                        message += ConsolidateDeviceToSystem(FeatureType.YieldAC, FeatureType.YieldAC, ds, systemId);
                        if (netExport > 0)
                            message += ConsolidateDeviceToSystem(FeatureType.YieldAC, FeatureType.ConsumptionAC, ds, systemId);
                    }
                }
                else if (ds.DeviceSettings.DeviceType == DeviceType.EnergyMeter)
                {
                    uint consumeAddress;
                    uint inverterAddress;
                    if (ds.DeviceSettings.Name == "EW4009")
                    {
                        consumeAddress = 1;
                        inverterAddress = 0;
                    }
                    else
                    {
                        consumeAddress = 0; // on CC128 and DSM address 0 is normally household consumption
                        inverterAddress = 1;
                    }
                                    
                    if (ds.Address == consumeAddress)
                    {                  
                        if (netExport > 0)
                            message += ConsolidateDeviceToSystem(FeatureType.NetExportAC, FeatureType.ConsumptionAC, ds, systemId, ConsolidateDeviceSettings.OperationType.Subtract);
                        else if (consumption > 0)
                            message += ConsolidateDeviceToSystem(FeatureType.EnergyAC, FeatureType.ConsumptionAC, ds, systemId);
                    }                    
                    else if (ds.Address == inverterAddress && inverters == 0)
                    {
                        message += ConsolidateDeviceToSystem(FeatureType.EnergyAC, FeatureType.YieldAC, ds, systemId);
                        if (netExport > 0)
                            message += ConsolidateDeviceToSystem(FeatureType.EnergyAC, FeatureType.ConsumptionAC, ds, systemId, ConsolidateDeviceSettings.OperationType.Add);
                    }               
                }
            }

            if (SettingsUpdated)
                message += NewLine + "You must 'Save Settings' to keep these changes";

            return message;
        }
    }
}
