﻿/*
* Copyright (c) 2013 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml;

namespace PVSettings
{
    public class SettingsCollection<TSettingType, TParentSettingType> 
        where TSettingType : PVSettings.SettingsBase
        where TParentSettingType : PVSettings.SettingsBase
    {
        public String CollectionName { get; private set; }  // the xml element name of the parent collection element
        public String ItemName { get; private set; }        // the xml element name of the items in the collection

        public XmlElement CollectionElement { get; private set; }

        public ObservableCollection<TSettingType> Collection { get; private set; }

        // used to create a new setting
        private Func<SettingsBase, XmlElement, TParentSettingType, String, TSettingType> NewSetting;

        private TParentSettingType Parent;

        public SettingsCollection(TParentSettingType parent, string collectionName, string itemName, 
            Func<SettingsBase, XmlElement, TParentSettingType, String, TSettingType> newSetting)
        {
            Collection = new ObservableCollection<TSettingType>();

            Parent = parent;
            CollectionName = collectionName;
            ItemName = itemName;
            NewSetting = newSetting;

            if (LoadCollectionElement() == 1)
                LoadExisting();
        }

        private int LoadCollectionElement()
        {
            List<XmlElement> list = SettingsBase.GetElementsByName(Parent.Settings, CollectionName);
            if (list.Count == 1)
            {
                CollectionElement = list[0];
                return 1;
            }
            else if (list.Count == 0)
            {
                CollectionElement = Parent.Settings.OwnerDocument.CreateElement(CollectionName);
                Parent.Settings.AppendChild(CollectionElement);
                return 0;
            }
            else
                throw new Exception("LoadCollectionElement - SettingsCollection - " + typeof(TSettingType).ToString() + " - multiple collection elements found - CollectionElement: " + CollectionName);
        }

        // The following is used when settings are reloaded on a live system
        // it plugs the new setting XmlElement into each settings class in an existing settings collection
        public void RefreshCollection()
        {
            // find possibly new collection element
            LoadCollectionElement();

            foreach (TSettingType s in Collection)
            {
                bool found = false;
                foreach (XmlElement item in SettingsBase.GetElementsByName(CollectionElement, ItemName))
                {
                    if (s.IsThisMyElement(item))
                    {
                        s.Settings = item;
                        found = true;
                        break;
                    }                    
                }
                if (!found)
                    GlobalSettings.SystemServices.LogMessage("SettingsCollection", "Collection: " +
                        CollectionName + " - Item: " + ItemName + 
                        " - Cannot locate settings in new settings colection", PVBCInterfaces.LogEntryType.Information);
            }
        }

        private void LoadExisting()
        {
            foreach (XmlElement item in SettingsBase.GetElementsByName(CollectionElement, ItemName))
            {
                TSettingType existing = NewSetting(Parent.RootSettings == null ? Parent : Parent.RootSettings, item, Parent, null);
                Collection.Add(existing);
            }
        }

        public void SortCollection(Func<TSettingType, String> orderByKey)
        {
            int i = 0;
            foreach (TSettingType setting in Collection.OrderBy<TSettingType, String>(orderByKey))
            {
                int j = Collection.IndexOf(setting);
                if (i != j)
                {
                    TSettingType temp = Collection[i];
                    Collection.RemoveAt(i);
                    Collection.Insert(i, setting);
                    Collection.RemoveAt(j);
                    Collection.Insert(j, temp);
                }
                i++;
            }
        }

        public void Add(TSettingType item)
        {
            CollectionElement.AppendChild(item.Settings);
            Collection.Add(item);
        }

        public TSettingType Add(XmlElement itemElement)
        {
            TSettingType item = NewSetting(Parent.RootSettings, itemElement, Parent, null);
            Add(item);
            // do not call Parent.SettingChangedEventHandler(""); 
            // this is used when loading the settings file
            return item;
        }

        public TSettingType Add()
        {
            XmlElement itemElement = Parent.Settings.OwnerDocument.CreateElement(ItemName);
            TSettingType item = NewSetting(Parent.RootSettings, itemElement, Parent, null);
            Add(item);
            Parent.SettingChangedEventHandler("");
            return item;
        }

        public TSettingType Add(String id)
        {
            XmlElement itemElement = Parent.Settings.OwnerDocument.CreateElement(ItemName);
            TSettingType item = NewSetting(Parent.RootSettings, itemElement, Parent, id);
            Add(item);
            Parent.SettingChangedEventHandler("");
            return item;
        }

        public void Remove(TSettingType item)
        {
            item.UnRegisterSettings();
            Collection.Remove(item);
            CollectionElement.RemoveChild(item.Settings);
            Parent.SettingChangedEventHandler("");
        }
    }
}
