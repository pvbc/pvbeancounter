﻿/*
* Copyright (c) 2010 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.IO;
using System.Threading;
using Microsoft.Win32;
using PVBCInterfaces;
using MackayFisher.Utilities;
using GenThreadManagement; 

namespace PVSettings
{
    public class GlobalSettings
    {
        public static SystemServices SystemServices;
        public static ApplicationSettings ApplicationSettings;
        public static GenericConnector.GenDatabase TheDB;

        public static void LogMessage(String component, String message, LogEntryType logEntryType = LogEntryType.Trace)
        {
            if (SystemServices != null)
            {
                GenThreadInfo tInfo = null;
                if (GenThreadManager.ThisGenThreadManager != null)
                    tInfo = GenThreadManager.ThisGenThreadManager.GetCurrentThreadInfo();

                if (tInfo == null)
                    SystemServices.LogMessage(component, message, logEntryType);
                else
                    tInfo.GenThread.LogMessage(component, message, logEntryType);
            }
        }
    }

    public delegate void SettingsNotification();

    public enum AutoDBTypes
    {
        Jet_2003,
        Jet_2007,
        SQLite,
        MySQL,
        SQLServer
    }

    public interface IGenericStrings
    {
        string ToString(object genericValue);
        object FromString(string stringValue);
    }

    public class DateStrings : IGenericStrings
    {
        String DateFormat;
        String LegacyDateFormat;

        public DateStrings(String dateFormat, String legacyDateFormat)
        {
            DateFormat = dateFormat;
            LegacyDateFormat = legacyDateFormat;
        }

        String IGenericStrings.ToString(object inDate)
        {
            if (inDate == null)
                return "";
            else
                return ((DateTime?)inDate).Value.Date.ToString(DateFormat, System.Globalization.CultureInfo.InvariantCulture);
        }

        object IGenericStrings.FromString(String inDateStr)
        {
            if (inDateStr == "")
                return null;

            DateTime? date = null;
            try
            {
                date = DateTime.ParseExact(inDateStr, DateFormat, System.Globalization.CultureInfo.InvariantCulture);
                return date;
            }
            catch (FormatException)
            {
            }

            if (LegacyDateFormat != null && LegacyDateFormat != "")
            {
                try
                {
                    date = DateTime.ParseExact(inDateStr, LegacyDateFormat, System.Globalization.CultureInfo.InvariantCulture);
                    return date;
                }
                catch (FormatException)
                {
                }

                try
                {
                    date = DateTime.ParseExact(inDateStr, LegacyDateFormat, System.Globalization.CultureInfo.CurrentCulture);
                    return date;
                }
                catch (FormatException)
                {
                }
            }

            try
            {
                date = DateTime.Parse(inDateStr, System.Globalization.CultureInfo.InvariantCulture);
                return date;
            }
            catch (FormatException)
            {
            }

            try
            {
                date = DateTime.Parse(inDateStr, System.Globalization.CultureInfo.CurrentCulture);
                return date;
            }
            catch (FormatException)
            {
            }

            return date;
        }
    }

    public class DateTimeStrings : IGenericStrings
    {
        String DateTimeFormat;
        String LegacyDateTimeFormat;

        public DateTimeStrings(String dateTimeFormat, String legacyDateTimeFormat)
        {
            DateTimeFormat = dateTimeFormat;
            LegacyDateTimeFormat = legacyDateTimeFormat;
        }

        String IGenericStrings.ToString(object inDateTime)
        {
            if (inDateTime == null)
                return "";
            else
                return ((DateTime?)inDateTime).Value.ToString(DateTimeFormat, System.Globalization.CultureInfo.InvariantCulture);
        }

        object IGenericStrings.FromString(String inDateStr)
        {
            if (inDateStr == "")
                return null;

            DateTime? date = null;
            try
            {
                date = DateTime.ParseExact(inDateStr, DateTimeFormat, System.Globalization.CultureInfo.InvariantCulture);
                return date;
            }
            catch (FormatException)
            {
            }

            if (LegacyDateTimeFormat != null && LegacyDateTimeFormat != "")
            {
                try
                {
                    date = DateTime.ParseExact(inDateStr, LegacyDateTimeFormat, System.Globalization.CultureInfo.InvariantCulture);
                    return date;
                }
                catch (FormatException)
                {
                }

                try
                {
                    date = DateTime.ParseExact(inDateStr, LegacyDateTimeFormat, System.Globalization.CultureInfo.CurrentCulture);
                    return date;
                }
                catch (FormatException)
                {
                }
            }

            try
            {
                date = DateTime.Parse(inDateStr, System.Globalization.CultureInfo.InvariantCulture);
                return date;
            }
            catch (FormatException)
            {
            }

            try
            {
                date = DateTime.Parse(inDateStr, System.Globalization.CultureInfo.CurrentCulture);
                return date;
            }
            catch (FormatException)
            {
            }

            return date;
        }
    }  
  
    public abstract class GenericSettingBase
    {
        private static List<GenericSettingBase> AllSettings = new List<GenericSettingBase>();

        private bool _haveValue = false;
        protected bool haveValue
        {
            get { return _haveValue; }
            set
            {
#if DEBUG
                if (_elementName == "traceenabled")
                    Thread.Sleep(2);
#endif
                _haveValue = value;
            }
        }

        protected string _elementName;
        protected string _propertyName;

        protected GenericSettingBase()
        {
            AllSettings.Add(this);
        }

        public void ClearCachedValue()
        {
            haveValue = false;
        }

        public static void ClearCache()
        {
            if (AllSettings == null)
                return;
            foreach (GenericSettingBase s in AllSettings)
            {
                s.ClearCachedValue();
#if DEBUG
                if (s._elementName == "traceenabled")
                    Thread.Sleep(2);
#endif
            }
        }
    }

    public class GenericSetting<T> : GenericSettingBase 
    {
        private SettingsBase _settings;
        private T _Value;
        private T _defaultValue;
        private Type coreType;
        private Type outerType;
        private IGenericStrings GenericStrings;

        // Prevent recursion using default anon function  
        private bool IsEvaluatingDefault = false;

        public String ElementName { get { return _elementName; } }

        private bool GetBoolean(string val)
        {
            val = val.ToLower();
            if (val == "true")         
                return true;
            else if (val == "false")           
                return false;
            else
            {
                if (DefaultValue == null)
                    return (bool)Convert.ChangeType(_defaultValue, coreType);
                else
                    return (bool)Convert.ChangeType(DefaultValue(), coreType);
            }
        }

        public void SetValue(T value, bool suppressChange)
        {
            _Value = value;
            
            if (value == null || value.ToString() == "")
            {
                _settings.SetValue(_elementName, "", _propertyName);
                haveValue = false;
            }
            else
            {
                if (coreType == typeof(bool))
                    _settings.SetValue(_elementName, value.ToString().ToLower(), _propertyName);
                else if (coreType == typeof(DateTime))
                {
                    string temp;
                    TypeConverter conv = TypeDescriptor.GetConverter(typeof(DateTime?));

                    DateTime? date = (DateTime?)conv.ConvertFrom(value);  // conv needed if T is DateTime rather than DateTime?
                    temp = GenericStrings.ToString(date);

                    _settings.SetValue(_elementName, temp, _propertyName, suppressChange);
                }
                else if (GenericStrings != null)
                    _settings.SetValue(_elementName, GenericStrings.ToString(value), _propertyName, suppressChange);
                else
                    _settings.SetValue(_elementName, value.ToString(), _propertyName, suppressChange);

                haveValue = true;
            }
        }

        public T Value
        {
            get
            {
                try{
                    if (haveValue)
                        return _Value;
                    else
                    {
                        string val = _settings.GetValue(_elementName);
                        if (val == "" || val == null)
                        {
                            if (DefaultValue == null || IsEvaluatingDefault)
                                return _defaultValue;
                            else
                            {
                                IsEvaluatingDefault = true;
                                T defValue = DefaultValue();
                                if (!DefaultIsDynamic)
                                {
                                    // after initial evaluation - store as a non default value
                                    _Value = defValue;
                                    haveValue = true;
                                    SetValue(defValue, false);
                                }
                                IsEvaluatingDefault = false;
                                return defValue;
                            }
                        }

                        if (coreType == typeof(string))
                            _Value = (T)Convert.ChangeType(val, outerType);
                        else if (coreType == typeof(bool))
                            _Value = (T)Convert.ChangeType(GetBoolean(val), outerType);
                        else if (coreType == typeof(Int32) || coreType == typeof(int))
                        {
                            Int32 temp = Int32.Parse(val, DeviceSettings.SettingsNumberFormat);
                            if (coreType == outerType)
                                _Value = (T)Convert.ChangeType(val, outerType);
                            else
                            {
                                TypeConverter conv = TypeDescriptor.GetConverter(outerType);
                                _Value = (T)conv.ConvertFrom(temp);
                            }
                        }
                        else if (coreType == typeof(TimeSpan))
                        {
                            TimeSpan temp = TimeSpan.Parse(val);
                            if (coreType == outerType)
                                _Value = (T)Convert.ChangeType(val, outerType);
                            else
                            {
                                TypeConverter conv = TypeDescriptor.GetConverter(outerType);
                                _Value = (T)conv.ConvertFrom(temp);
                            }
                        }
                        else if (coreType == typeof(DateTime))
                        {
                            DateTime? temp = (DateTime?)GenericStrings.FromString(val);
                            if (!temp.HasValue)
                                _Value = _defaultValue;
                            else if (coreType == outerType)
                                _Value = (T)Convert.ChangeType(val, outerType);
                            else
                            {
                                TypeConverter conv = TypeDescriptor.GetConverter(outerType);
                                _Value = (T)conv.ConvertFrom(temp);
                            }
                        }
                        else if (GenericStrings != null)
                        {
                            object temp = GenericStrings.FromString(val);
                            if (temp == null)
                                _Value = _defaultValue;
                            else if (coreType == outerType)
                                _Value = (T)Convert.ChangeType(val, outerType);
                            else
                            {
                                TypeConverter conv = TypeDescriptor.GetConverter(outerType);
                                _Value = (T)conv.ConvertFrom(temp);
                            }
                        }
                        else
                        {
                            TypeConverter conv = TypeDescriptor.GetConverter(coreType);
                            if (coreType == outerType)
                                _Value = (T)conv.ConvertFromInvariantString(val);
                            else
                            {
                                TypeConverter outerConv = TypeDescriptor.GetConverter(outerType);
                                object obj = conv.ConvertFromInvariantString(val);
                                _Value = (T)outerConv.ConvertFrom(obj);
                            }
                        }

                        haveValue = true;
                        return _Value;
                    }                    
                }
                catch (Exception e)
                {
                    throw e;
                }
            }
            set
            {
                SetValue(value, false);
            }
        }

        public delegate T GetDefaultValue();

        private GetDefaultValue DefaultValue = null;
        private bool DefaultIsDynamic = false;          // when false DefaultValue is called once to provide a real value then not treated as default unless forced

        public GenericSetting(GetDefaultValue getDefaultValue, SettingsBase settings, string propertyName, bool defaultIsDynamic = true, IGenericStrings genericStrings = null)
        {
            DefaultValue = getDefaultValue;
            T defaultValue = default(T);
            DefaultIsDynamic = defaultIsDynamic;
            outerType = typeof(T);
            if (outerType.IsGenericType && outerType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                // drill down to natural type
                coreType = outerType.GenericTypeArguments[0];
                _defaultValue = defaultValue;
            }
            else
            {
                coreType = outerType;
                _defaultValue = defaultValue;
            }

            if (genericStrings == null)
            {
                if (coreType == typeof(DateTime))
                    GenericStrings = settings.DateTimeStrings;
                else
                    GenericStrings = null;
            }
            else
                GenericStrings = genericStrings;

            haveValue = false;
            _settings = settings;
            _propertyName = propertyName;
            _elementName = propertyName.ToLower();
            _Value = defaultValue;
        }

        public GenericSetting(T defaultValue, SettingsBase settings, string propertyName, IGenericStrings genericStrings = null)
        {
            outerType = typeof(T);
            if (outerType.IsGenericType && outerType.GetGenericTypeDefinition() == typeof(Nullable<>))
            {
                // drill down to natural type
                coreType = outerType.GenericTypeArguments[0];
                _defaultValue = defaultValue;
            }
            else
            {
                coreType = outerType;
                _defaultValue = defaultValue;
            }

            if (genericStrings == null)
            {
                if (coreType == typeof(DateTime))
                    GenericStrings = settings.DateTimeStrings;
                else
                    GenericStrings = null;
            }
            else
                GenericStrings = genericStrings;

            haveValue = false;
            _settings = settings;
            _propertyName = propertyName;
            _elementName = propertyName.ToLower();
            _Value = defaultValue;
        }

        public GenericSetting(SettingsBase settings, string propertyName, IGenericStrings genericStrings = null) 
        {
            outerType = typeof(T);
            if (outerType.IsGenericType && outerType.GetGenericTypeDefinition() == typeof(Nullable<>))
                coreType = outerType.GenericTypeArguments[0];                
            else
                coreType = outerType;

            if (genericStrings == null)
            {
                if (coreType == typeof(DateTime))
                    GenericStrings = settings.DateTimeStrings;
                else
                    GenericStrings = null;
            }
            else
                GenericStrings = genericStrings;

            _defaultValue = default(T);
            haveValue = false;
           
            _settings = settings;
            _propertyName = propertyName;
            _elementName = propertyName.ToLower();
            _Value = _defaultValue;
        }
    }

    public class SettingsBase : INotifyPropertyChanged
    {
#if DEBUG
        // help with tracking the version of a settings object currently in use during debug
        public DateTime SettingsLoadedTime = DateTime.MinValue;
#endif
        public static System.Threading.Mutex ApplicationSettingsMutex = new Mutex();

        internal SettingsBase RootSettings;

        public event PropertyChangedEventHandler PropertyChanged;

        protected XmlDocument document { get { return Settings.OwnerDocument; } }

#if DEBUG
        private XmlElement m_Settings = null;
        internal XmlElement Settings
        {
            get { return m_Settings; }
            set
            {
                m_Settings = value;
                SettingsLoadedTime = DateTime.Now;
            }
        }
#else
        internal XmlElement Settings { get; set; }
#endif

        protected const String NotDefined = "Not Defined";

        private DateStrings _DateStrings = null;

        public virtual DateStrings DateStrings 
        { 
            get
            {
                if (RootSettings == null)
                {
                    if (_DateStrings == null)
                        _DateStrings = new DateStrings("yyyy-MM-dd", "");
                    return _DateStrings;
                }
                else
                    return RootSettings.DateStrings;
            }
        }

        private DateTimeStrings _DateTimeStrings = null;

        public virtual DateTimeStrings DateTimeStrings
        {
            get
            {
                if (RootSettings == null)
                {
                    if (_DateTimeStrings == null)
                        _DateTimeStrings = new DateTimeStrings("yyyy'-'MM'-'dd'T'HH':'mm':'ss", "");
                    return _DateTimeStrings;
                }
                else
                    return RootSettings.DateTimeStrings;
            }
        }

        public static XmlElement GetElementByName(XmlElement parent, string name)
        {
            foreach (XmlNode n in parent.ChildNodes)
                if (n.NodeType == XmlNodeType.Element && ((XmlElement)n).Name == name)
                    return (XmlElement)n;

            return null;
        }

        public static List<XmlElement> GetElementsByName(XmlElement parent, string name)
        {
            List<XmlElement> list = new List<XmlElement>();
            foreach (XmlNode n in parent.ChildNodes)
                if (n.NodeType == XmlNodeType.Element && ((XmlElement)n).Name == name)
                    list.Add((XmlElement)n);

            return list;
        }

        // Used to construct ApplicationsSettingsBase only 
        // All others must use - 
        // public SettingsBase(ApplicationSettingsBase rootSettings, XmlElement element)
        public SettingsBase()
        {
            Settings = null;
        }

        public SettingsBase(SettingsBase rootSettings, XmlElement element)
        {
            RootSettings = rootSettings;
            Settings = element;
        }

        public virtual bool IsThisMyElement(XmlElement newElem)
        {
            throw new NotImplementedException("This setting cannot perform dynamic setting update");
        }

        public virtual void UnRegisterSettings()
        {
        }

        public void DeleteSettings()
        {
            if (Settings != null)
                Settings.ParentNode.RemoveChild(Settings);
        }

        public void OnPropertyChanged(SettingsBase obj, PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(obj, e);
        }

        public virtual void OnPropertyChanged(PropertyChangedEventArgs e)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, e);
        }

        protected XmlElement GetElement(String nodeName)
        {
            foreach (XmlNode e in Settings.ChildNodes)
            {
                if (e.NodeType == XmlNodeType.Element && e.Name == nodeName)
                    return (XmlElement)e;
            }

            return null;
        }

        protected bool DeleteElement(String nodeName)
        {

            foreach (XmlNode e in Settings.ChildNodes)
            {
                if (e.NodeType == XmlNodeType.Element && e.Name == nodeName)
                {
                    Settings.RemoveChild(e);
                    return true;
                }
            }

            return false;
        }

        internal String GetValue(String nodeName)
        {
            ApplicationSettingsMutex.WaitOne();
            try
            {
                XmlElement elem = GetElement(nodeName);
                String tmp;
                if (elem == null)
                    tmp = "";
                else
                    tmp = elem.Attributes["value"].Value;
                ApplicationSettingsMutex.ReleaseMutex();
                return tmp;
            }
            catch (Exception e)
            {
                GlobalSettings.SystemServices.LogMessage("SettingsBase", 
                    "GetValue probably called on element with no 'value' attribute - Exception: " 
                    + e.Message, LogEntryType.ErrorMessage);
            }
            ApplicationSettingsMutex.ReleaseMutex();
            return "";
        }

        protected String GetNullableValue(String nodeName)
        {
            ApplicationSettingsMutex.WaitOne();
            try
            {
                XmlElement elem = GetElement(nodeName);
                String tmp;
                if (elem == null)
                    tmp = null;
                else
                    tmp = elem.Attributes["value"].Value;
                ApplicationSettingsMutex.ReleaseMutex();
                return tmp;
            }
            catch
            {
            }
            ApplicationSettingsMutex.ReleaseMutex();
            return "";
        }

        public virtual void SettingChangedEventHandler(String name)
        {
            RootSettings.SettingChangedEventHandler(name);
        }

        protected void SetValueInternal(String nodeName, String value)
        {
            ApplicationSettingsMutex.WaitOne();
            try
            {
                GetElement(nodeName).Attributes["value"].Value = value;
            }
            catch
            {
                AddElement(nodeName, value);
            }
            ApplicationSettingsMutex.ReleaseMutex();
        }

        protected void SetPropertyChanged(String propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
            SettingChangedEventHandler("");
        }

        protected void DoPropertyChanged(String propertyName)
        {
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
        }

        internal void SetValue(String nodeName, String value, String propertyName, bool suppressChange = false)
        {
            ApplicationSettingsMutex.WaitOne();
            try
            {
                XmlElement elem = GetElement(nodeName);
                if (elem == null)
                    AddElement(nodeName, value);
                else
                    elem.Attributes["value"].Value = value;
                // OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
            }
            catch
            {
            }
            // moved here to run after Add
            OnPropertyChanged(new PropertyChangedEventArgs(propertyName));
            ApplicationSettingsMutex.ReleaseMutex();
            if (!suppressChange)
                SettingChangedEventHandler(nodeName);
        }

        internal void DeleteValue(String nodeName)
        {
            ApplicationSettingsMutex.WaitOne();
            try
            {
                DeleteElement(nodeName);
            }
            catch
            {
            }
            ApplicationSettingsMutex.ReleaseMutex();
        }

        protected XmlElement AddElement(String elementName, String value)
        {
            XmlElement e;
            XmlAttribute a;

            e = document.CreateElement(elementName);
            a = document.CreateAttribute("value");

            a.Value = value;
            e.Attributes.Append(a);

            Settings.AppendChild(e);
            return e;
        }

        protected XmlElement AddElement(XmlElement parent, String elementName, String value = null)
        {
            XmlElement e;

            e = document.CreateElement(elementName);
            if (value != null)
            {
                XmlAttribute a = document.CreateAttribute("value");
                a.Value = value;
                e.Attributes.Append(a);
            }

            parent.AppendChild(e);
            return e;
        }

        protected XmlElement AddElement(XmlElement parent, String elementName, XmlElement beforeElement)
        {
            XmlElement e;

            e = document.CreateElement(elementName);

            parent.InsertBefore(e, beforeElement);
            return e;
        }

        internal static bool ElementHasChild(XmlNode target, String childName, String childValue)
        {
            foreach (XmlNode child in target.ChildNodes)
            {
                if (child.Name == childName)
                {
                    foreach (XmlNode childChild in child.Attributes)
                    {
                        if (childChild.Name == "value")
                            return childChild.Value == childValue;
                    }
                }
            }
            return false;
        }

        protected static void SetMachineRegistryValue(String author, String application, String name, String value)
        {
            Microsoft.Win32.RegistryKey macFishKey;
            Microsoft.Win32.RegistryKey software = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE", true);
            macFishKey = software.CreateSubKey(author);
            Microsoft.Win32.RegistryKey pvbcKey;
            pvbcKey = macFishKey.CreateSubKey(application);
            pvbcKey.SetValue(name, value);
            pvbcKey.Close();
            macFishKey.Close();
            software.Close();
        }

        protected static String GetMachineRegistryValue(String author, String application, String name)
        {
            Microsoft.Win32.RegistryKey macFishKey;
            Microsoft.Win32.RegistryKey software = Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE");
            macFishKey = software.OpenSubKey(author);
            if (macFishKey == null)
                return NotDefined;
            Microsoft.Win32.RegistryKey pvbcKey;
            pvbcKey = macFishKey.OpenSubKey(application);
            if (pvbcKey == null)
                return NotDefined;
            String value = (String)pvbcKey.GetValue(name, NotDefined);
            pvbcKey.Close();
            macFishKey.Close();
            software.Close();
            return value;
        }

        public String TimeToString(TimeSpan? inTime)
        {
            if (inTime == null)
                return "";
            else
                return inTime.Value.ToString();
            
        }

        public TimeSpan? StringToTime(String inTimeStr)
        {
            if (inTimeStr == "")
                return null;

            TimeSpan? time = null;
            try
            {
                time = TimeSpan.Parse(inTimeStr);
                return time;
            }
            catch (FormatException e)
            {
                GlobalSettings.LogMessage("StringToTime", "value: " + inTimeStr + " - Exception: " + e.Message, LogEntryType.Information);
            }

            return null;
        }


        public String DateToString(DateTime? inDate, String dateFormat = null)
        {
            if (inDate == null)
                return "";
            else if (dateFormat == null)
                return ((IGenericStrings)DateStrings).ToString(inDate.Value);
            else
                return inDate.Value.Date.ToString(dateFormat, System.Globalization.CultureInfo.InvariantCulture);
        }

        public DateTime? StringToDate(String inDateStr)
        {
            if (inDateStr == "")
                return null;

            DateTime? date = null;
            try
            {
                date = (DateTime?)((IGenericStrings)DateStrings).FromString(inDateStr);
                return date;
            }
            catch (FormatException)
            {
            }

            return date;
        }

        public String DateTimeToString(DateTime? inDateTime, String dateTimeFormat = null)
        {
            if (inDateTime == null)
                return "";
            else if (dateTimeFormat == null)
                return ((IGenericStrings)DateTimeStrings).ToString(inDateTime.Value);
            else
                return inDateTime.Value.ToString(dateTimeFormat, System.Globalization.CultureInfo.InvariantCulture);
        }

        public DateTime? StringToDateTime(String inDateStr)
        {
            if (inDateStr == "")
                return null;

            DateTime? date = null;
            try
            {
                date = (DateTime?)((IGenericStrings)DateTimeStrings).FromString(inDateStr);
                return date;
            }
            catch (FormatException)
            {
            }

            return date;
        }
    }
}

