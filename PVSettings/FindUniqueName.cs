﻿/*
* Copyright (c) 2013 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PVSettings
{
    public class FindUniqueName
    {
        public String BaseName { get; private set; }

        private List<String> AvoidList;

        public FindUniqueName(String baseName)
        {
            AvoidList = new List<string>();
            BaseName = baseName;
        }

        public void Add(String name)
        {
            if (name.StartsWith(BaseName))
                AvoidList.Add(name);
        }

        private bool NameAvailable(String candidate)
        {
            foreach (String name in AvoidList)
                if (name == candidate)
                    return false;
            return true;
        }

        public String UniqueName
        {
            get
            {
                String tryName = BaseName;
                int id = 1;
                while (!NameAvailable(tryName))
                {
                    id++;
                    tryName = BaseName + "_" + id.ToString("00");
                }
                return tryName;
            }
        }
    }
}
