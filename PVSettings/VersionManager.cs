﻿/*
* Copyright (c) 2010 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GenericConnector;
using System.Data.Common;
using PVBCInterfaces;

namespace PVSettings
{
    // DDL is the abstract class representing DDL commands used to bring a database from an earlier version to a later version
    // Must be implemented for each target database type as the DDL is similar but not generic
    internal abstract class DDL
    {
        public abstract String Table_version { get; }
        public abstract String Table_pvoutputlog { get; } // original tab;e
        public abstract String Table_pvoutputlog_2000 { get; }  // version  2000

        public virtual String View_pvoutput_v
        { get { return ""; } }

        public virtual String View_pvoutput5min_v
        { get { return ""; } }

        public virtual String View_pvoutput_sub_v
        { get { return ""; } }

        public virtual String Alter_pvoutputlog_1400a
        { get { return ""; } }

        public virtual String Alter_pvoutputlog_1400b
        { get { return ""; } }

        public abstract string Alter_pvoutputlog_1421
        { get; }

        public abstract String View_pvoutput_sub_v_1500 { get; }

        public abstract String View_pvoutput5min_v_1500 { get; }

        public abstract String View_pvoutput_v_1500 { get; }

        public abstract String View_pvoutput_sub_v_1700 { get; }

        public abstract String View_pvoutput5min_v_1700 { get; }

        public abstract String View_pvoutput_v_1700 { get; }

        public abstract String View_pvoutput_sub_v_1836 { get; }

        public abstract String View_pvoutput5min_v_1836 { get; }

        public abstract String View_pvoutput_v_1836 { get; }

        public abstract string Alter_pvoutputlog_2_0_1_0 { get; }

        public abstract string Table_devicetype_2000 { get; }
        
        public abstract string Table_devicefeature_2000 { get; }
        
        public abstract string Table_device_2000 { get; }        

        public abstract string Table_devicereading_energy_2_0_0_0 { get; }
        
        public abstract String View_devicedayoutput_v_2000 { get; }        

        public abstract string Table_pvoutputlog_2_0_1_0 { get; }

        public abstract string Table_devicereading_quantity_2_0_1_1 { get; }

        public virtual String CurrentTable_devicetype { get { return Table_devicetype_2000; } }
        public virtual String CurrentTable_device { get { return Table_device_2000; } }
        public virtual String CurrentTable_devicefeature { get { return Table_devicefeature_2000; } }
        public virtual String CurrentTable_devicereading_energy { get { return Table_devicereading_energy_2_0_0_0; } }
        public virtual String CurrentTable_devicereading_quantity { get { return Table_devicereading_quantity_2_0_1_1; } }
        public virtual String CurrentTable_pvoutputlog { get { return Table_pvoutputlog_2_0_1_0; } }
        public virtual String CurrentTable_version { get { return Table_version; } }
        public virtual String CurrentView_devicedayoutput_v { get { return View_devicedayoutput_v_2000; } }
    }

    internal class MySql_DDL : DDL
    {
        // required for 1.3.0.0 and later
        public override string Table_pvoutputlog
        {
            get
            {
                return
                    "create table `pvoutputlog` " +
                    "( " +
                        "`SiteId` varchar(10) NOT NULL, " +
                        "`OutputDay` date NOT NULL, " +
                        "`OutputTime` mediumint(9) NOT NULL, " +
                        "`Loaded` tinyint(1) NOT NULL DEFAULT '0', " +
                        "`ImportEnergy` double NULL, " +
                        "`ImportPower` double NULL, " +
                        "PRIMARY KEY (`SiteId`,`OutputDay`,`OutputTime`) " +
                     ") ENGINE=InnoDB DEFAULT CHARSET=latin1";
            }
        }

        // required for 1.3.0.0 and later
        public override String Table_version
        {
            get
            {
                return
                    "create table `version` " +
                    "( " +
                        "`Major` varchar(4) NOT NULL, " +
                        "`Minor` varchar(4) NOT NULL, " +
                        "`Release` varchar(4) NOT NULL, " +
                        "`Patch` varchar(4) NOT NULL " +
                     ") ENGINE=InnoDB DEFAULT CHARSET=latin1";
            }
        }

        // required for 1.3.0.0 and later
        public override string View_pvoutput_v
        {
            get
            {
                return
                    "CREATE VIEW `pvoutput_v` AS " +
                    "select OutputDay, (OutputTime DIV 600) * 600 as OutputTime, sum(Energy)*1000 as Energy, " +
                    "max(Power)*1000 as Power, min(Power)*1000 as MinPower " +
                    "from pvoutput_sub_v " +
                    "group by OutputDay, (OutputTime DIV 600) * 600";
            }
        }

        // required for 1.3.3.0 and later
        public override string View_pvoutput5min_v
        {
            get
            {
                return
                    "CREATE VIEW `pvoutput5min_v` AS " +
                    "select OutputDay, (OutputTime DIV 300) * 300 as OutputTime, sum(Energy)*1000 as Energy, " +
                    "max(Power)*1000 as Power, min(Power)*1000 as MinPower " +
                    "from pvoutput_sub_v " +
                    "group by OutputDay, (OutputTime DIV 300) * 300";
            }
        }

        public override string View_pvoutput_sub_v
        {
            get
            {
                return
                    "CREATE VIEW `pvoutput_sub_v` AS " +
                    "select DATE(oh.OutputTime) as OutputDay, TIME_TO_SEC(TIME(oh.OutputTime)) as OutputTime, " +
                    "sum(oh.OutputKwh) as Energy, sum(oh.OutputKwh*3600/oh.Duration) as Power " +
                    "from outputhistory as oh " +
                    "where oh.OutputKwh > 0 " +
                    "and oh.OutputTime >= (curdate() - 20) " +
                    "group by DATE(oh.OutputTime), TIME_TO_SEC(TIME(oh.OutputTime))";
            }
        }

        // required for 1.4.0.0 and later
        public override string Alter_pvoutputlog_1400a
        {
            get
            {
                return
                    "alter table `pvoutputlog` " +
                    "add `ImportEnergy` double NULL, " +
                    "add `ImportPower` double NULL ";
            }
        }

        public override string Alter_pvoutputlog_1400b
        {
            get
            {
                return
                    "alter table `pvoutputlog` " +
                    "modify `Energy` double NULL, " +
                    "modify `Power` double NULL ";
            }
        }

        // required for 1.4.2.1 and later
        public override string Alter_pvoutputlog_1421
        {
            get
            {
                return
                    "alter table `pvoutputlog` " +
                    "add `Temperature` double NULL ";
            }
        }

        public override String View_pvoutput_sub_v_1500
        {
            get
            {
                return
                    "CREATE VIEW `pvoutput_sub_v` AS " +
                    "select DATE(oh.OutputTime) as OutputDay, TIME_TO_SEC(TIME(oh.OutputTime)) as OutputTime, " +
                    "sum(oh.OutputKwh) as Energy, sum(oh.OutputKwh*3600/oh.Duration) as Power, " +
                    "MIN(MinPower) AS Min_Power, " +
                    "MAX(MaxPower) AS Max_Power " +
                    "from outputhistory as oh " +
                    "where oh.OutputKwh > 0 " +
                    "and oh.OutputTime >= (curdate() - 20) " +
                    "group by DATE(oh.OutputTime), TIME_TO_SEC(TIME(oh.OutputTime))";
            }
        }

        public override String View_pvoutput5min_v_1500
        {
            get
            {
                return
                    "CREATE VIEW `pvoutput5min_v` AS " +
                    "select OutputDay, (OutputTime DIV 300) * 300 as OutputTime, sum(Energy)*1000 as Energy, " +
                    "max(Power)*1000 as Power, min(Power)*1000 as MinPower, " +
                    "MAX(Max_Power)*1000 AS RealMaxPower, " +
                    "MIN(Min_Power)*1000 AS RealMinPower " +
                    "from pvoutput_sub_v " +
                    "group by OutputDay, (OutputTime DIV 300) * 300";
            }
        }

        public override String View_pvoutput_v_1500
        {
            get
            {
                return
                    "CREATE VIEW `pvoutput_v` AS " +
                    "select OutputDay, (OutputTime DIV 600) * 600 as OutputTime, sum(Energy)*1000 as Energy, " +
                    "max(Power)*1000 as Power, min(Power)*1000 as MinPower, " +
                    "MAX(Max_Power)*1000 AS RealMaxPower, " +
                    "MIN(Min_Power)*1000 AS RealMinPower " +
                    "from pvoutput_sub_v " +
                    "group by OutputDay, (OutputTime DIV 600) * 600";
            }
        }

        public override String View_pvoutput_sub_v_1700
        {
            get
            {
                return
                    "CREATE VIEW `pvoutput_sub_v` AS " +
                    "select i.SiteId as SiteId, DATE(oh.OutputTime) as OutputDay, TIME_TO_SEC(TIME(oh.OutputTime)) as OutputTime, " +
                    "sum(oh.OutputKwh) as Energy, sum(oh.OutputKwh*3600/oh.Duration) as Power, " +
                    "MIN(MinPower) AS Min_Power, " +
                    "MAX(MaxPower) AS Max_Power " +
                    "from outputhistory as oh, inverter as i " +
                    "where oh.Inverter_Id = i.Id " +
                    "and oh.OutputKwh > 0 " +
                    "and oh.OutputTime >= (curdate() - 20) " +
                    "group by i.SiteId, DATE(oh.OutputTime), TIME_TO_SEC(TIME(oh.OutputTime))";
            }
        }

        public override String View_pvoutput5min_v_1700
        {
            get
            {
                return
                    "CREATE VIEW `pvoutput5min_v` AS " +
                    "select SiteId, OutputDay, (OutputTime DIV 300) * 300 as OutputTime, sum(Energy)*1000 as Energy, " +
                    "max(Power)*1000 as Power, min(Power)*1000 as MinPower, " +
                    "MAX(Max_Power)*1000 AS RealMaxPower, " +
                    "MIN(Min_Power)*1000 AS RealMinPower " +
                    "from pvoutput_sub_v " +
                    "group by SiteId, OutputDay, (OutputTime DIV 300) * 300";
            }
        }

        public override String View_pvoutput_v_1700
        {
            get
            {
                return
                    "CREATE VIEW `pvoutput_v` AS " +
                    "select SiteId, OutputDay, (OutputTime DIV 600) * 600 as OutputTime, sum(Energy)*1000 as Energy, " +
                    "max(Power)*1000 as Power, min(Power)*1000 as MinPower, " +
                    "MAX(Max_Power)*1000 AS RealMaxPower, " +
                    "MIN(Min_Power)*1000 AS RealMinPower " +
                    "from pvoutput_sub_v " +
                    "group by SiteId, OutputDay, (OutputTime DIV 600) * 600";
            }
        }

        public override String View_pvoutput_sub_v_1836
        {
            get
            {
                return
                    "CREATE VIEW `pvoutput_sub_v` AS " +
                    "select i.SiteId as SiteId, DATE(oh.OutputTime) as OutputDay, TIME_TO_SEC(TIME(oh.OutputTime)) as OutputTime, " +
                    "sum(oh.OutputKwh) as Energy, sum(oh.OutputKwh*3600/oh.Duration) as Power, " +
                    "MIN(MinPower) AS Min_Power, " +
                    "MAX(MaxPower) AS Max_Power, " +
                    "MAX(oh.Temperature) AS Max_Temp " +
                    "from outputhistory as oh, inverter as i " +
                    "where oh.Inverter_Id = i.Id " +
                    "and oh.OutputKwh > 0 " +
                    "and oh.OutputTime >= (curdate() - 20) " +
                    "group by i.SiteId, DATE(oh.OutputTime), TIME_TO_SEC(TIME(oh.OutputTime))";
            }
        }

        public override String View_pvoutput5min_v_1836
        {
            get
            {
                return
                    "CREATE VIEW `pvoutput5min_v` AS " +
                    "select SiteId, OutputDay, (OutputTime DIV 300) * 300 as OutputTime, sum(Energy)*1000 as Energy, " +
                    "max(Power)*1000 as Power, min(Power)*1000 as MinPower, " +
                    "MAX(Max_Power)*1000 AS RealMaxPower, " +
                    "MIN(Min_Power)*1000 AS RealMinPower, " +
                    "MAX(Max_Temp) AS MaxTemp " +
                    "from pvoutput_sub_v " +
                    "group by SiteId, OutputDay, (OutputTime DIV 300) * 300";
            }
        }

        public override String View_pvoutput_v_1836
        {
            get
            {
                return
                    "CREATE VIEW `pvoutput_v` AS " +
                    "select SiteId, OutputDay, (OutputTime DIV 600) * 600 as OutputTime, sum(Energy)*1000 as Energy, " +
                    "max(Power)*1000 as Power, min(Power)*1000 as MinPower, " +
                    "MAX(Max_Power)*1000 AS RealMaxPower, " +
                    "MIN(Min_Power)*1000 AS RealMinPower, " +
                    "MAX(Max_Temp) AS MaxTemp " +
                    "from pvoutput_sub_v " +
                    "group by SiteId, OutputDay, (OutputTime DIV 600) * 600";
            }
        }

        public override string Table_pvoutputlog_2000
        {
            get
            {
                return
                    "create table `pvoutputlog` " +
                    "( " +
                        "`SiteId` varchar(10) NOT NULL, " +
                        "`OutputDay` date NOT NULL, " +
                        "`OutputTime` mediumint(9) NOT NULL, " +
                        "`Energy` double NULL, " +
                        "`Power` double NULL, " +
                        "`Loaded` tinyint(1) NOT NULL DEFAULT '0', " +
                        "`ImportEnergy` double NULL, " +
                        "`ImportPower` double NULL, " +
                        "`Temperature` double NULL, " +
                        "PRIMARY KEY (`SiteId`,`OutputDay`,`OutputTime`) " +
                     ") ENGINE=InnoDB DEFAULT CHARSET=latin1";
            }
        }

        public override string Table_devicetype_2000
        {
            get
            {
                return
                    "CREATE TABLE if not exists `devicetype` " +
                    "( " +
                        "`Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT, " +
                        "`Manufacturer` VARCHAR(60) NOT NULL, " +
                        "`Model` VARCHAR(50) NOT NULL, " +
                        "`MaxPower` MEDIUMINT NULL, " +
                        "`DeviceType` VARCHAR(20) NOT NULL, " +
                        "PRIMARY KEY (`Id` ), " +
                        "CONSTRAINT `uk1_devicetype` UNIQUE (`Manufacturer` ASC, `Model` ASC) " +
                    ") ENGINE=InnoDB DEFAULT CHARSET=latin1 ";
            }
        }

        public override string Table_device_2000
        {
            get
            {
                return
                    "CREATE TABLE `device` " +
                    "( " +
                        "`Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT, " +
                        "`SerialNumber` VARCHAR(45) NOT NULL, " +
                        "`DeviceType_Id` MEDIUMINT UNSIGNED NOT NULL, " +
                        "PRIMARY KEY (`Id` ), " +
                        "CONSTRAINT `uk1_device` UNIQUE (`SerialNumber` ASC, `DeviceType_Id` ASC), " +
                        "CONSTRAINT `fk_device_devicetype` FOREIGN KEY (`DeviceType_Id`) REFERENCES `devicetype` (`Id`) " +
                    ") ENGINE=InnoDB DEFAULT CHARSET=latin1 ";
            }
        }

        public override string Table_devicefeature_2000
        {
            get
            {
                return
                    "CREATE TABLE `devicefeature` " +
                    "( " +
                        "`Id` MEDIUMINT UNSIGNED NOT NULL AUTO_INCREMENT, " +
                        "`Device_Id` MEDIUMINT UNSIGNED NOT NULL, " +
                        "`FeatureType` SMALLINT NOT NULL, " +
                        "`FeatureId` TINYINT NOT NULL, " +
                        "`MeasureType` VARCHAR(20) NOT NULL, " +
                        "`IsConsumption` CHAR(1) NULL, " +
                        "`IsAC` CHAR(1) NULL, " +
                        "`IsThreePhase` CHAR(1) NULL, " +
                        "`StringNumber` MEDIUMINT NULL, " +
                        "`PhaseNumber` MEDIUMINT NULL, " +
                        "PRIMARY KEY ( `Id` ), " +
                        "CONSTRAINT `uk1_devicefeature` UNIQUE (`Device_Id` ASC, `FeatureType` ASC, `FeatureId` ASC), " +
                        "CONSTRAINT `fk_devicefeature_device` FOREIGN KEY (`Device_Id`) REFERENCES `device` (`Id`) " +
                    ") ENGINE=InnoDB DEFAULT CHARSET=latin1 ";
            }
        }

        public override string Table_devicereading_energy_2_0_0_0
        {
            get
            {
                return
                    "CREATE TABLE `devicereading_energy` " +
                    "( " +
                        "`ReadingEnd` DATETIME NOT NULL, " +
                        "`DeviceFeature_Id` MEDIUMINT UNSIGNED NOT NULL, " +
                        "`ReadingStart` DATETIME NOT NULL, " +
                        "`EnergyTotal` DOUBLE NULL, " +
                        "`EnergyToday` DOUBLE NULL, " +
                        "`EnergyDelta` FLOAT NULL, " +
                        "`CalcEnergyDelta` FLOAT NULL, " +
                        "`HistEnergyDelta` FLOAT NULL, " +
                        "`Mode` MEDIUMINT NULL, " +
                        "`ErrorCode` MEDIUMINT NULL, " +
                        "`Power` MEDIUMINT NULL, " +
                        "`Volts` FLOAT NULL, " +
                        "`Amps` FLOAT NULL, " +
                        "`Frequency` FLOAT NULL, " +
                        "`Temperature` FLOAT NULL, " +
                        "`MinPower` MEDIUMINT NULL, " +
                        "`MaxPower` MEDIUMINT NULL, " +                        
                        "PRIMARY KEY (`ReadingEnd`, `DeviceFeature_Id` ) , " +
                        "CONSTRAINT `uk_devicereading_energy` UNIQUE (`DeviceFeature_Id`, `ReadingEnd`) , " +
                        "CONSTRAINT `fk_devicereadingenergy_devicefeature` FOREIGN KEY (DeviceFeature_Id) REFERENCES `devicefeature` (Id) " +
                    ") ENGINE=InnoDB DEFAULT CHARSET=latin1 ";
            }
        }

        public override String View_devicedayoutput_v_2000
        {
            get
            {
                return
                    "CREATE VIEW devicedayoutput_v " +
                    "AS SELECT f.Device_Id, DATE(r.ReadingEnd) OutputDay, SUM(r.EnergyDelta) OutputKwh " +
                    "from devicereading_energy r, devicefeature f " +
                    "where r.DeviceFeature_Id = f.Id " +
                    "group by f.Device_Id, DATE(r.ReadingEnd) ";
            }
        }

        public override string Alter_pvoutputlog_2_0_1_0
        {
            get
            {
                return
                    "alter table `pvoutputlog` " +
                    "add `Volts` double NULL, " +
                    "add `ExtendedData1` double NULL, " +
                    "add `ExtendedData2` double NULL, " +
                    "add `ExtendedData3` double NULL, " +
                    "add `ExtendedData4` double NULL, " +
                    "add `ExtendedData5` double NULL, " +
                    "add `ExtendedData6` double NULL ";
            }
        }

        public override string Table_pvoutputlog_2_0_1_0
        {
            get
            {
                return
                    "create table `pvoutputlog` " +
                    "( " +
                        "`SiteId` varchar(10) NOT NULL, " +
                        "`OutputDay` date NOT NULL, " +
                        "`OutputTime` mediumint(9) NOT NULL, " +
                        "`Energy` double NULL, " +
                        "`Power` double NULL, " +
                        "`Loaded` tinyint(1) NOT NULL DEFAULT '0', " +
                        "`ImportEnergy` double NULL, " +
                        "`ImportPower` double NULL, " +
                        "`Temperature` double NULL, " +
                        "`Volts` double NULL, " +
                        "`ExtendedData1` double NULL, " +
                        "`ExtendedData2` double NULL, " +
                        "`ExtendedData3` double NULL, " +
                        "`ExtendedData4` double NULL, " +
                        "`ExtendedData5` double NULL, " +
                        "`ExtendedData6` double NULL, " +
                        "PRIMARY KEY (`SiteId`,`OutputDay`,`OutputTime`) " +
                     ") ENGINE=InnoDB DEFAULT CHARSET=latin1";
            }
        }

        public override string Table_devicereading_quantity_2_0_1_1
        {
            get
            {
                return
                    "CREATE TABLE `devicereading_quantity` " +
                    "( " +
                        "`ReadingEnd` DATETIME NOT NULL, " +
                        "`DeviceFeature_Id` MEDIUMINT UNSIGNED NOT NULL, " +
                        "`ReadingStart` DATETIME NOT NULL, " +
                        "`QuantityTotal` DOUBLE NULL, " +
                        "`QuantityToday` DOUBLE NULL, " +
                        "`QuantityDelta` FLOAT NULL, " +
                        "`Mode` MEDIUMINT NULL, " +
                        "`ErrorCode` MEDIUMINT NULL, " +
                        "`Rate` FLOAT NULL, " +
                        "PRIMARY KEY (`ReadingEnd`, `DeviceFeature_Id` ) , " +
                        "CONSTRAINT `uk_devicereading_quantity` UNIQUE (`DeviceFeature_Id`, `ReadingEnd`) , " +
                        "CONSTRAINT `fk_devicereadingquantity_devicefeature` FOREIGN KEY (DeviceFeature_Id) REFERENCES `devicefeature` (Id) " +
                    ") ENGINE=InnoDB DEFAULT CHARSET=latin1 ";
            }
        }
    }

    internal class MySql_5_6_DDL : MySql_DDL
    {
        public override string Table_devicereading_energy_2_0_0_0
        {
            get
            {
                return
                    "CREATE TABLE `devicereading_energy` " +
                    "( " +
                        "`ReadingEnd` DATETIME(3) NOT NULL, " +
                        "`DeviceFeature_Id` MEDIUMINT UNSIGNED NOT NULL, " +
                        "`ReadingStart` DATETIME(3) NOT NULL, " +
                        "`EnergyTotal` DOUBLE NULL, " +
                        "`EnergyToday` DOUBLE NULL, " +
                        "`EnergyDelta` FLOAT NULL, " +
                        "`CalcEnergyDelta` FLOAT NULL, " +
                        "`HistEnergyDelta` FLOAT NULL, " +
                        "`Mode` MEDIUMINT NULL, " +
                        "`ErrorCode` MEDIUMINT NULL, " +
                        "`Power` MEDIUMINT NULL, " +
                        "`Volts` FLOAT NULL, " +
                        "`Amps` FLOAT NULL, " +
                        "`Frequency` FLOAT NULL, " +
                        "`Temperature` FLOAT NULL, " +
                        "`MinPower` MEDIUMINT NULL, " +
                        "`MaxPower` MEDIUMINT NULL, " +
                        "PRIMARY KEY (`ReadingEnd`, `DeviceFeature_Id` ) , " +
                        "CONSTRAINT `uk_devicereading_energy` UNIQUE (`DeviceFeature_Id`, `ReadingEnd`) , " +
                        "CONSTRAINT `fk_devicereadingenergy_devicefeature` FOREIGN KEY (DeviceFeature_Id) REFERENCES `devicefeature` (Id) " +
                    ") ENGINE=InnoDB DEFAULT CHARSET=latin1 ";
            }
        }

        public override string Table_devicereading_quantity_2_0_1_1
        {
            get
            {
                return
                    "CREATE TABLE `devicereading_quantity` " +
                    "( " +
                        "`ReadingEnd` DATETIME(3) NOT NULL, " +
                        "`DeviceFeature_Id` MEDIUMINT UNSIGNED NOT NULL, " +
                        "`ReadingStart` DATETIME(3) NOT NULL, " +
                        "`QuantityTotal` DOUBLE NULL, " +
                        "`QuantityToday` DOUBLE NULL, " +
                        "`QuantityDelta` FLOAT NULL, " +
                        "`Mode` MEDIUMINT NULL, " +
                        "`ErrorCode` MEDIUMINT NULL, " +
                        "`Rate` FLOAT NULL, " +
                        "PRIMARY KEY (`ReadingEnd`, `DeviceFeature_Id` ) , " +
                        "CONSTRAINT `uk_devicereading_quantity` UNIQUE (`DeviceFeature_Id`, `ReadingEnd`) , " +
                        "CONSTRAINT `fk_devicereadingquantity_devicefeature` FOREIGN KEY (DeviceFeature_Id) REFERENCES `devicefeature` (Id) " +
                    ") ENGINE=InnoDB DEFAULT CHARSET=latin1 ";
            }
        }
    }

    internal class SQLite_DDL : DDL
    {
        // required for 1.3.0.0 and later
        public override string Table_pvoutputlog
        {
            get
            {
                return
                    "create table `pvoutputlog` " +
                    "( " +
                        "`SiteId` TEXT NOT NULL, " +
                        "`OutputDay` date NOT NULL, " +
                        "`OutputTime` INTEGER NOT NULL, " +
                        "`Energy` REAL NULL, " +
                        "`Power` REAL NULL, " +
                        "`Loaded` INTEGER NOT NULL DEFAULT '0', " +
                        "`ImportEnergy` REAL NULL, " +
                        "`ImportPower` REAL NULL, " +
                        "PRIMARY KEY (`SiteId`,`OutputDay`,`OutputTime`) " +
                     ")";
            }
        }

        // required for 1.3.0.0 and later
        public override String Table_version
        {
            get
            {
                return
                    "create table `version` " +
                    "( " +
                        "`Major` TEXT NOT NULL, " +
                        "`Minor` TEXT NOT NULL, " +
                        "`Release` TEXT NOT NULL, " +
                        "`Patch` TEXT NOT NULL " +
                     ")";
            }
        }

        // required for 1.3.0.0 and later
        public override string View_pvoutput_v
        {
            get
            {
                return
                    "CREATE VIEW [pvoutput_v] AS " +
                    "select OutputDay, (OutputTime / 600) * 600 as OutputTime, sum(Energy)*1000 as Energy, " +
                    "max(Power)*1000 as Power, min(Power)*1000 as MinPower " +
                    "from pvoutput_sub_v " +
                    "group by OutputDay, (OutputTime / 600) * 600";
            }
        }

        public override string View_pvoutput5min_v
        {
            get
            {
                return
                    "CREATE VIEW [pvoutput5min_v] AS " +
                    "select OutputDay, (OutputTime / 300) * 300 as OutputTime, sum(Energy)*1000 as Energy, " +
                    "max(Power)*1000 as Power, min(Power)*1000 as MinPower " +
                    "from pvoutput_sub_v " +                    
                    "group by OutputDay, (OutputTime / 300) * 300";
            }
        }

        public override string View_pvoutput_sub_v
        {
            get
            {
                return
                    "CREATE VIEW [pvoutput_sub_v] AS " +
                    "select DATE(oh.OutputTime) as OutputDay, " +
                    "(strftime('%s', oh.OutputTime) - strftime('%s', date(oh.OutputTime))) as OutputTime, " +
                    "sum(oh.OutputKwh) as Energy, sum(oh.OutputKwh*3600/oh.Duration) as Power " +
                    "from outputhistory as oh " +
                    "where oh.OutputKwh > 0 " +
                    "and oh.OutputTime >= date('now', '-20 day') " + 
                    "group by DATE(oh.OutputTime), (strftime('%s', oh.OutputTime) - strftime('%s', date(oh.OutputTime)))";
            }
        }

        // required for 1.4.0.0 and later
        public override string Alter_pvoutputlog_1400a
        {
            get
            {
                return
                    "alter table `pvoutputlog` add " +
                    "`ImportEnergy` REAL ";
            }
        }

        public override string Alter_pvoutputlog_1400b
        {
            get
            {
                return
                    "alter table `pvoutputlog` add " +
                    "`ImportPower` REAL ";
            }
        }

        // required for 1.4.2.1 and later
        public override string Alter_pvoutputlog_1421
        {
            get
            {
                return
                    "alter table `pvoutputlog` " +
                    "add `Temperature` double NULL ";
            }
        }

        public override String View_pvoutput_sub_v_1500
        {
            get
            {
                return
                    "CREATE VIEW [pvoutput_sub_v] AS " +
                    "select DATE(oh.OutputTime) as OutputDay, " +
                    "(strftime('%s', oh.OutputTime) - strftime('%s', date(oh.OutputTime))) as OutputTime, " +
                    "sum(oh.OutputKwh) as Energy, sum(oh.OutputKwh*3600/oh.Duration) as Power, " +
                    "MIN(MinPower) AS Min_Power, " +
                    "MAX(MaxPower) AS Max_Power " +
                    "from outputhistory as oh " +
                    "where oh.OutputKwh > 0 " +
                    "and oh.OutputTime >= date('now', '-20 day') " +
                    "group by DATE(oh.OutputTime), (strftime('%s', oh.OutputTime) - strftime('%s', date(oh.OutputTime)))";
            }
        }

        public override String View_pvoutput5min_v_1500
        {
            get
            {
                return
                    "CREATE VIEW [pvoutput5min_v] AS " +
                    "select OutputDay, (OutputTime / 300) * 300 as OutputTime, sum(Energy)*1000 as Energy, " +
                    "max(Power)*1000 as Power, min(Power)*1000 as MinPower, " +
                    "MAX(Max_Power) * 1000 AS RealMaxPower, " +
                    "MIN(Min_Power) * 1000 AS RealMinPower " +
                    "from pvoutput_sub_v " +
                    "group by OutputDay, (OutputTime / 300) * 300";
            }
        }

        public override String View_pvoutput_v_1500
        {
            get
            {
                return
                    "CREATE VIEW [pvoutput_v] AS " +
                    "select OutputDay, (OutputTime / 600) * 600 as OutputTime, sum(Energy)*1000 as Energy, " +
                    "max(Power)*1000 as Power, min(Power)*1000 as MinPower, " +
                    "MAX(Max_Power) * 1000 AS RealMaxPower, " +
                    "MIN(Min_Power) * 1000 AS RealMinPower " +
                    "from pvoutput_sub_v " +
                    "group by OutputDay, (OutputTime / 600) * 600";
            }
        }

        public override String View_pvoutput_sub_v_1700
        {
            get
            {
                return
                    "CREATE VIEW [pvoutput_sub_v] AS " +
                    "select i.SiteId as SiteId, DATE(oh.OutputTime) as OutputDay, " +
                    "(strftime('%s', oh.OutputTime) - strftime('%s', date(oh.OutputTime))) as OutputTime, " +
                    "sum(oh.OutputKwh) as Energy, sum(oh.OutputKwh*3600/oh.Duration) as Power, " +
                    "MIN(MinPower) AS Min_Power, " +
                    "MAX(MaxPower) AS Max_Power " +
                    "from outputhistory as oh, inverter as i " +
                    "where oh.Inverter_Id = i.Id " +
                    "and oh.OutputKwh > 0 " +
                    "and oh.OutputTime >= date('now', '-20 day') " +
                    "group by i.SiteId, DATE(oh.OutputTime), (strftime('%s', oh.OutputTime) - strftime('%s', date(oh.OutputTime)))";
            }
        }

        public override String View_pvoutput5min_v_1700
        {
            get
            {
                return
                    "CREATE VIEW [pvoutput5min_v] AS " +
                    "select SiteId, OutputDay, (OutputTime / 300) * 300 as OutputTime, sum(Energy)*1000 as Energy, " +
                    "max(Power)*1000 as Power, min(Power)*1000 as MinPower, " +
                    "MAX(Max_Power) * 1000 AS RealMaxPower, " +
                    "MIN(Min_Power) * 1000 AS RealMinPower " +
                    "from pvoutput_sub_v " +
                    "group by SiteId, OutputDay, (OutputTime / 300) * 300";
            }
        }

        public override String View_pvoutput_v_1700
        {
            get
            {
                return
                    "CREATE VIEW [pvoutput_v] AS " +
                    "select SiteId, OutputDay, (OutputTime / 600) * 600 as OutputTime, sum(Energy)*1000 as Energy, " +
                    "max(Power)*1000 as Power, min(Power)*1000 as MinPower, " +
                    "MAX(Max_Power) * 1000 AS RealMaxPower, " +
                    "MIN(Min_Power) * 1000 AS RealMinPower " +
                    "from pvoutput_sub_v " +
                    "group by SiteId, OutputDay, (OutputTime / 600) * 600";
            }
        }

        public override String View_pvoutput_sub_v_1836
        {
            get
            {
                return
                    "CREATE VIEW [pvoutput_sub_v] AS " +
                    "select i.SiteId as SiteId, DATE(oh.OutputTime) as OutputDay, " +
                    "(strftime('%s', oh.OutputTime) - strftime('%s', date(oh.OutputTime))) as OutputTime, " +
                    "sum(oh.OutputKwh) as Energy, sum(oh.OutputKwh*3600/oh.Duration) as Power, " +
                    "MIN(MinPower) AS Min_Power, " +
                    "MAX(MaxPower) AS Max_Power, " +
                    "MAX(oh.Temperature) AS Max_Temp " +
                    "from outputhistory as oh, inverter as i " +
                    "where oh.Inverter_Id = i.Id " +
                    "and oh.OutputKwh > 0 " +
                    "and oh.OutputTime >= date('now', '-20 day') " +
                    "group by i.SiteId, DATE(oh.OutputTime), (strftime('%s', oh.OutputTime) - strftime('%s', date(oh.OutputTime)))";
            }
        }

        public override String View_pvoutput5min_v_1836
        {
            get
            {
                return
                    "CREATE VIEW [pvoutput5min_v] AS " +
                    "select SiteId, OutputDay, (OutputTime / 300) * 300 as OutputTime, sum(Energy)*1000 as Energy, " +
                    "max(Power)*1000 as Power, min(Power)*1000 as MinPower, " +
                    "MAX(Max_Power) * 1000 AS RealMaxPower, " +
                    "MIN(Min_Power) * 1000 AS RealMinPower, " +
                    "MAX(Max_Temp) AS MaxTemp " +
                    "from pvoutput_sub_v " +
                    "group by SiteId, OutputDay, (OutputTime / 300) * 300";
            }
        }

        public override String View_pvoutput_v_1836
        {
            get
            {
                return
                    "CREATE VIEW [pvoutput_v] AS " +
                    "select SiteId, OutputDay, (OutputTime / 600) * 600 as OutputTime, sum(Energy)*1000 as Energy, " +
                    "max(Power)*1000 as Power, min(Power)*1000 as MinPower, " +
                    "MAX(Max_Power) * 1000 AS RealMaxPower, " +
                    "MIN(Min_Power) * 1000 AS RealMinPower, " +
                    "MAX(Max_Temp) AS MaxTemp " +
                    "from pvoutput_sub_v " +
                    "group by SiteId, OutputDay, (OutputTime / 600) * 600";
            }
        }

        public override string Table_pvoutputlog_2000
        {
            get
            {
                return
                    "create table `pvoutputlog` " +
                    "( " +
                        "`SiteId` TEXT NOT NULL, " +
                        "`OutputDay` date NOT NULL, " +
                        "`OutputTime` INTEGER NOT NULL, " +
                        "`Energy` REAL NULL, " +
                        "`Power` REAL NULL, " +
                        "`Loaded` INTEGER NOT NULL DEFAULT '0', " +
                        "`ImportEnergy` REAL NULL, " +
                        "`ImportPower` REAL NULL, " +
                        "`Temperature` REAL NULL, " +
                        "PRIMARY KEY (`SiteId`,`OutputDay`,`OutputTime`) " +
                     ")";
            }
        }


        public override string Table_devicetype_2000
        {
            get
            {
                return
                    "CREATE TABLE devicetype " +
                    "( " +
                        "Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "Manufacturer TEXT NOT NULL, " +
                        "Model TEXT NOT NULL, " +
                        "MaxPower INTEGER NULL, " +
                        "DeviceType TEXT NOT NULL, " +
                        "CONSTRAINT DeviceType_UK1 UNIQUE (Manufacturer, Model)  " +
                    ") ";
            }
        }

        public override string Table_device_2000
        {
            get
            {
                return
                    "CREATE TABLE device " +
                    "( " +
                        "Id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "SerialNumber TEXT NOT NULL, " +
                        "DeviceType_Id INTEGER NOT NULL, " +
                        "FOREIGN KEY (DeviceType_Id) REFERENCES devicetype (Id), " +
                        "CONSTRAINT uk_device UNIQUE (SerialNumber, DeviceType_Id)  " +
                    ") ";
            }
        }

        public override string Table_devicefeature_2000
        {
            get
            {
                return
                    "CREATE TABLE devicefeature " +
                    "( " +
                        "`Id` INTEGER PRIMARY KEY AUTOINCREMENT, " +
                        "`Device_Id` INTEGER NOT NULL, " +
                        "`FeatureType` INTEGER NOT NULL, " +
                        "`FeatureId` INTEGER NOT NULL, " +
                        "`MeasureType` TEXT NOT NULL, " +
                        "`IsConsumption` TEXT NULL, " +
                        "`IsAC` TEXT NULL, " +
                        "`IsThreePhase` TEXT NULL, " +
                        "`StringNumber` INTEGER NULL, " +
                        "`PhaseNumber` INTEGER NULL, " +
                        "CONSTRAINT uk_devicefeature UNIQUE (Device_Id, FeatureType, FeatureId)  " +
                        "CONSTRAINT `fk_devicefeature_device` FOREIGN KEY (`Device_Id`) REFERENCES `device` (`Id`) " +
                    ") ";
            }
        }

        public override string Table_devicereading_energy_2_0_0_0
        {
            get
            {
                return
                    "CREATE TABLE devicereading_energy " +
                    "( " +
                        "ReadingEnd DATETIME NOT NULL, " +
                        "DeviceFeature_Id INTEGER NOT NULL, " +
                        "ReadingStart DATETIME NOT NULL, " +
                        "EnergyTotal REAL NULL, " +
                        "EnergyToday REAL NULL, " +
                        "EnergyDelta REAL NULL, " +
                        "CalcEnergyDelta REAL NULL, " +
                        "HistEnergyDelta REAL NULL, " +
                        "Mode INTEGER NULL, " +
                        "ErrorCode INTEGER NULL, " +
                        "Power INTEGER NULL, " +
                        "Volts REAL NULL, " +
                        "Amps REAL NULL, " +
                        "Frequency REAL NULL, " +
                        "Temperature REAL NULL, " +
                        "MinPower INTEGER NULL, " +
                        "MaxPower INTEGER NULL, " +                       
                        "CONSTRAINT PK_devicereading_ac_1 PRIMARY KEY (ReadingEnd, DeviceFeature_Id ), " +
                        "CONSTRAINT uk_devicereading_ac UNIQUE (DeviceFeature_Id, ReadingEnd),  " +
                        "FOREIGN KEY (DeviceFeature_Id) REFERENCES devicefeature (Id) " +
                    ") ";
            }
        }

        public override String View_devicedayoutput_v_2000
        {
            get
            {
                return
                    "CREATE VIEW devicedayoutput_v " +
                    "AS SELECT f.Device_Id, Date(r.ReadingEnd) OutputDay, SUM(r.EnergyDelta) OutputKwh " +
                    "from devicereading_energy r, devicefeature f " +
                    "where r.DeviceFeature_Id = f.Id " +
                    "group by f.Device_Id, Date(r.ReadingEnd) ";
            }
        }

        public string Drop_devicedayoutput_v
        {
            get
            {
                return
                    "drop view `devicedayoutput_v` ";
            }
        }

        public String View_devicedayoutput_v_20017
        {
            get
            {
                return
                    "CREATE VIEW devicedayoutput_v " +
                    "AS SELECT f.Device_Id Device_Id, Date(r.ReadingEnd) OutputDay, SUM(r.EnergyDelta) OutputKwh " +
                    "from devicereading_energy r, devicefeature f " +
                    "where r.DeviceFeature_Id = f.Id " +
                    "group by f.Device_Id, Date(r.ReadingEnd) ";
            }
        }

        public override string Alter_pvoutputlog_2_0_1_0
        {
            get
            {
                return
                    "alter table `pvoutputlog` add " +
                    "`Volts` REAL ";
            }
        }

        public string Alter_pvoutputlog_2_0_1_0a
        {
            get
            {
                return
                    "alter table `pvoutputlog` add " +
                    "`ExtendedData1` REAL ";
            }
        }

        public string Alter_pvoutputlog_2_0_1_0b
        {
            get
            {
                return
                    "alter table `pvoutputlog` add " +
                    "`ExtendedData2` REAL ";
            }
        }

        public string Alter_pvoutputlog_2_0_1_0c
        {
            get
            {
                return
                    "alter table `pvoutputlog` add " +
                    "`ExtendedData3` REAL ";
            }
        }

        public string Alter_pvoutputlog_2_0_1_0d
        {
            get
            {
                return
                    "alter table `pvoutputlog` add " +
                    "`ExtendedData4` REAL ";
            }
        }

        public string Alter_pvoutputlog_2_0_1_0e
        {
            get
            {
                return
                    "alter table `pvoutputlog` add " +
                    "`ExtendedData5` REAL ";
            }
        }

        public string Alter_pvoutputlog_2_0_1_0f
        {
            get
            {
                return
                    "alter table `pvoutputlog` add " +
                    "`ExtendedData6` REAL ";
            }
        }

        public override string Table_pvoutputlog_2_0_1_0
        {
            get
            {
                return
                    "create table `pvoutputlog` " +
                    "( " +
                        "`SiteId` TEXT NOT NULL, " +
                        "`OutputDay` date NOT NULL, " +
                        "`OutputTime` INTEGER NOT NULL, " +
                        "`Energy` REAL NULL, " +
                        "`Power` REAL NULL, " +
                        "`Loaded` INTEGER NOT NULL DEFAULT '0', " +
                        "`ImportEnergy` REAL NULL, " +
                        "`ImportPower` REAL NULL, " +
                        "`Temperature` REAL NULL, " +
                        "`Volts` REAL NULL, " +
                        "`ExtendedData1` REAL NULL, " +
                        "`ExtendedData2` REAL NULL, " +
                        "`ExtendedData3` REAL NULL, " +
                        "`ExtendedData4` REAL NULL, " +
                        "`ExtendedData5` REAL NULL, " +
                        "`ExtendedData6` REAL NULL, " +
                        "PRIMARY KEY (`SiteId`,`OutputDay`,`OutputTime`) " +
                     ")";
            }
        }

        public override string Table_devicereading_quantity_2_0_1_1
        {
            get
            {
                return
                    "CREATE TABLE devicereading_quantity " +
                    "( " +
                        "ReadingEnd DATETIME NOT NULL, " +
                        "DeviceFeature_Id INTEGER NOT NULL, " +
                        "ReadingStart DATETIME NOT NULL, " +
                        "QuantityTotal REAL NULL, " +
                        "QuantityToday REAL NULL, " +
                        "QuantityDelta REAL NULL, " +
                        "Mode INTEGER NULL, " +
                        "ErrorCode INTEGER NULL, " +
                        "Rate REAL NULL, " +
                        "CONSTRAINT PK_devicereading_qty_1 PRIMARY KEY (ReadingEnd, DeviceFeature_Id ), " +
                        "CONSTRAINT uk_devicereading_qty UNIQUE (DeviceFeature_Id, ReadingEnd),  " +
                        "FOREIGN KEY (DeviceFeature_Id) REFERENCES devicefeature (Id) " +
                    ") ";
            }
        }
    }

    internal class SQLServer_DDL : DDL
    {
        public override string Table_pvoutputlog
        {
            get
            {
                return Table_pvoutputlog_2000;
            }
        }

        public override String Table_version
        {
            get
            {
                return
                    "create table version " +
                    "( " +
                        "Major [varchar](4) NOT NULL, " +
                        "Minor [varchar](4) NOT NULL, " +
                        "Release [varchar](4) NOT NULL, " +
                        "Patch [varchar](4) NOT NULL " +
                     ") ";
            }
        }

        // required for 1.4.2.1 and later
        public override string Alter_pvoutputlog_1421
        {
            get
            {
                return
                    "alter table pvoutputlog " +
                    "add Temperature float NULL ";
            }
        }

        public override String View_pvoutput_sub_v_1500
        {
            get
            {
                return
                    "CREATE VIEW pvoutput_sub_v AS " +
                    "SELECT CAST(FLOOR(CAST(OutputTime AS float)) AS DATETIME) AS OutputDay, " +
                        "DATEDIFF(second, CAST(FLOOR(CAST(OutputTime AS float)) AS DATETIME), OutputTime) AS OutputTime, " +
                        "SUM(OutputKwh) AS Energy, " +
                        "SUM(OutputKwh * 3600 / Duration) AS Power, " +
                        "MIN(MinPower) AS Min_Power, " +
                        "MAX(MaxPower) AS Max_Power " +
                    "FROM pvhistory.outputhistory AS oh " +
                    "WHERE (OutputTime >= GETDATE() - 20) " +
                    "AND (OutputKwh > 0) " +
                    "GROUP BY CAST(FLOOR(CAST(OutputTime AS float)) AS DATETIME), " +
                        "DATEDIFF(second, CAST(FLOOR(CAST(OutputTime AS float)) AS DATETIME), OutputTime)";
            }
        }

        public override String View_pvoutput5min_v_1500
        {
            get
            {
                return
                    "CREATE VIEW pvoutput5min_v AS " +
                    "SELECT OutputDay, FLOOR(OutputTime / 300) * 300 AS OutputTime, " +
                        "SUM(Energy) * 1000 AS Energy, " +
                        "MAX(Power) * 1000 AS Power, " +
                        "MIN(Power) * 1000 AS MinPower, " +
                        "MAX(Max_Power) * 1000 AS RealMaxPower, " +
                        "MIN(Min_Power) * 1000 AS RealMinPower " +
                    "FROM pvhistory.pvoutput_sub_v " +
                    "GROUP BY OutputDay, FLOOR(OutputTime / 300) * 300 ";
            }
        }

        public override String View_pvoutput_v_1500
        {
            get
            {
                return
                    "CREATE VIEW pvoutput_v AS " +
                    "SELECT OutputDay, FLOOR(OutputTime / 600) * 600 AS OutputTime, " +
                        "SUM(Energy) * 1000 AS Energy, " +
                        "MAX(Power) * 1000 AS Power, " +
                        "MIN(Power) * 1000 AS MinPower, " +
                        "MAX(Max_Power) * 1000 AS RealMaxPower, " +
                        "MIN(Min_Power) * 1000 AS RealMinPower " +
                    "FROM pvhistory.pvoutput_sub_v " +
                    "GROUP BY OutputDay, FLOOR(OutputTime / 600) * 600 ";
            }
        }

        public override String View_pvoutput_sub_v_1700
        {
            get
            {
                return
                    "CREATE VIEW pvoutput_sub_v AS " +
                    "SELECT i.SiteId AS SiteId, CAST(FLOOR(CAST(OutputTime AS float)) AS DATETIME) AS OutputDay, " +
                        "DATEDIFF(second, CAST(FLOOR(CAST(OutputTime AS float)) AS DATETIME), OutputTime) AS OutputTime, " +
                        "SUM(OutputKwh) AS Energy, " +
                        "SUM(OutputKwh * 3600 / Duration) AS Power, " +
                        "MIN(MinPower) AS Min_Power, " +
                        "MAX(MaxPower) AS Max_Power " +
                    "FROM outputhistory AS oh INNER JOIN inverter AS i ON oh.Inverter_Id = i.Id " +
                    "WHERE (OutputTime >= GETDATE() - 20) " +
                    "AND (OutputKwh > 0) " +
                    "GROUP BY i.SiteId, CAST(FLOOR(CAST(OutputTime AS float)) AS DATETIME), " +
                        "DATEDIFF(second, CAST(FLOOR(CAST(OutputTime AS float)) AS DATETIME), OutputTime)";
            }
        }

        public override String View_pvoutput5min_v_1700
        {
            get
            {
                return
                    "CREATE VIEW pvoutput5min_v AS " +
                    "SELECT SiteId, OutputDay, FLOOR(OutputTime / 300) * 300 AS OutputTime, " +
                        "SUM(Energy) * 1000 AS Energy, " +
                        "MAX(Power) * 1000 AS Power, " +
                        "MIN(Power) * 1000 AS MinPower, " +
                        "MAX(Max_Power) * 1000 AS RealMaxPower, " +
                        "MIN(Min_Power) * 1000 AS RealMinPower " +
                    "FROM pvoutput_sub_v " +
                    "GROUP BY SiteId, OutputDay, FLOOR(OutputTime / 300) * 300 ";
            }
        }

        public override String View_pvoutput_v_1700
        {
            get
            {
                return
                    "CREATE VIEW pvoutput_v AS " +
                    "SELECT SiteId, OutputDay, FLOOR(OutputTime / 600) * 600 AS OutputTime, " +
                        "SUM(Energy) * 1000 AS Energy, " +
                        "MAX(Power) * 1000 AS Power, " +
                        "MIN(Power) * 1000 AS MinPower, " +
                        "MAX(Max_Power) * 1000 AS RealMaxPower, " +
                        "MIN(Min_Power) * 1000 AS RealMinPower " +
                    "FROM pvoutput_sub_v " +
                    "GROUP BY SiteId, OutputDay, FLOOR(OutputTime / 600) * 600 ";
            }
        }

        public override String View_pvoutput_sub_v_1836
        {
            get
            {
                return
                    "CREATE VIEW pvoutput_sub_v AS " +
                    "SELECT i.SiteId AS SiteId, CAST(FLOOR(CAST(OutputTime AS float)) AS DATETIME) AS OutputDay, " +
                        "DATEDIFF(second, CAST(FLOOR(CAST(OutputTime AS float)) AS DATETIME), OutputTime) AS OutputTime, " +
                        "SUM(OutputKwh) AS Energy, " +
                        "SUM(OutputKwh * 3600 / Duration) AS Power, " +
                        "MIN(MinPower) AS Min_Power, " +
                        "MAX(MaxPower) AS Max_Power, " +
                        "MAX(oh.Temperature) AS Max_Temp " +
                    "FROM outputhistory AS oh INNER JOIN inverter AS i ON oh.Inverter_Id = i.Id " +
                    "WHERE (OutputTime >= GETDATE() - 20) " +
                    "AND (OutputKwh > 0) " +
                    "GROUP BY i.SiteId, CAST(FLOOR(CAST(OutputTime AS float)) AS DATETIME), " +
                        "DATEDIFF(second, CAST(FLOOR(CAST(OutputTime AS float)) AS DATETIME), OutputTime)";
            }
        }

        public override String View_pvoutput5min_v_1836
        {
            get
            {
                return
                    "CREATE VIEW pvoutput5min_v AS " +
                    "SELECT SiteId, OutputDay, FLOOR(OutputTime / 300) * 300 AS OutputTime, " +
                        "SUM(Energy) * 1000 AS Energy, " +
                        "MAX(Power) * 1000 AS Power, " +
                        "MIN(Power) * 1000 AS MinPower, " +
                        "MAX(Max_Power) * 1000 AS RealMaxPower, " +
                        "MIN(Min_Power) * 1000 AS RealMinPower, " +
                        "MAX(Max_Temp) AS MaxTemp " +
                    "FROM pvoutput_sub_v " +
                    "GROUP BY SiteId, OutputDay, FLOOR(OutputTime / 300) * 300 ";
            }
        }

        public override String View_pvoutput_v_1836
        {
            get
            {
                return
                    "CREATE VIEW pvoutput_v AS " +
                    "SELECT SiteId, OutputDay, FLOOR(OutputTime / 600) * 600 AS OutputTime, " +
                        "SUM(Energy) * 1000 AS Energy, " +
                        "MAX(Power) * 1000 AS Power, " +
                        "MIN(Power) * 1000 AS MinPower, " +
                        "MAX(Max_Power) * 1000 AS RealMaxPower, " +
                        "MIN(Min_Power) * 1000 AS RealMinPower, " +
                        "MAX(Max_Temp) AS MaxTemp " +
                    "FROM pvoutput_sub_v " +
                    "GROUP BY SiteId, OutputDay, FLOOR(OutputTime / 600) * 600 ";
            }
        }

        public override string Table_pvoutputlog_2000
        {
            get
            {
                return
                    "create table pvoutputlog " +
                    "( " +
                        "SiteId [varchar](10) NOT NULL, " +
                        "OutputDay [date] NOT NULL, " +
                        "OutputTime [int] NOT NULL, " +
                        "Energy [float] NULL, " +
                        "Power [float] NULL, " +
                        "Loaded int NOT NULL DEFAULT 0, " +
                        "ImportEnergy [float] NULL, " +
                        "ImportPower [float] NULL, " +
                        "Temperature [float] NULL, " +
                        "CONSTRAINT PK_pvoutputlog PRIMARY KEY CLUSTERED ([SiteId] ASC, [OutputDay] ASC,[OutputTime] ASC) " +
                     ") ";
            }
        }

        public override string Table_pvoutputlog_2_0_1_0
        {
            get
            {
                return
                    "create table pvoutputlog " +
                    "( " +
                        "SiteId [varchar](10) NOT NULL, " +
                        "OutputDay [date] NOT NULL, " +
                        "OutputTime [int] NOT NULL, " +
                        "Energy [float] NULL, " +
                        "Power [float] NULL, " +
                        "Loaded int NOT NULL DEFAULT 0, " +
                        "ImportEnergy [float] NULL, " +
                        "ImportPower [float] NULL, " +
                        "Temperature [float] NULL, " +
                        "Volts [float] NULL, " +
                        "ExtendedData1 [float] NULL, " +
                        "ExtendedData2 [float] NULL, " +
                        "ExtendedData3 [float] NULL, " +
                        "ExtendedData4 [float] NULL, " +
                        "ExtendedData5 [float] NULL, " +
                        "ExtendedData6 [float] NULL, " +
                        "CONSTRAINT PK_pvoutputlog PRIMARY KEY CLUSTERED ([SiteId] ASC, [OutputDay] ASC,[OutputTime] ASC) " +
                     ") ";
            }
        }

        public override string Table_devicetype_2000
        {
            get
            {
                return 
                    "CREATE TABLE devicetype " +
                    "( " +
                        "Id [int] IDENTITY(1,1) NOT NULL, " +
                        "Manufacturer [varchar](60) NOT NULL, " +
                        "Model [varchar](50) NOT NULL, " +
                        "MaxPower [int] NULL, " +
                        "DeviceType [varchar](20) NOT NULL, " +
                        "CONSTRAINT PK_DeviceType PRIMARY KEY CLUSTERED ([Id] ASC ) " +
                        "WITH " +
                        "( " +
                            "PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, " +
                            "ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON " +
                        "), " +
                        "CONSTRAINT DeviceType_UK1 UNIQUE (Manufacturer, Model)  " +
                        "WITH " +
                        "( " +
                            "PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, " +
                            "ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON " +
                        ") " +                        
                    ") ";
            }
        }

        public override string Table_device_2000
        {
            get
            {
                return 
                    "CREATE TABLE device " +
                    "( " +
	                    "Id [int] IDENTITY(1,1) NOT NULL, " +
	                    "SerialNumber [varchar](45) NOT NULL, " +
	                    "DeviceType_Id [int] NOT NULL, " +
                        "CONSTRAINT PK_device PRIMARY KEY CLUSTERED " +
                        "( " +
	                        "Id ASC " +
                        ") " +
                        "WITH " +
                        "( " +
                            "PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, " +
                            "ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON " +
                        "), " +
                        "CONSTRAINT fk_device_devicetype FOREIGN KEY (DeviceType_Id) REFERENCES devicetype (Id), " +
                        "CONSTRAINT uk_device UNIQUE (SerialNumber, DeviceType_Id)  " +
                        "WITH " +
                        "( " +
                            "PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, " +
                            "ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON " +
                        ") " +
                    ") "; 
            }
        }

        public override string Table_devicefeature_2000
        {
            get
            {
                return
                    "CREATE TABLE devicefeature " +
                    "( " +
                        "Id [int] IDENTITY(1,1) NOT NULL, " +
                        "Device_Id [int] NOT NULL, " +
                        "FeatureType [smallint] NOT NULL, " +
                        "FeatureId [tinyint] NOT NULL, " +
                        "MeasureType [varchar](20) NOT NULL, " +
                        "IsConsumption [char](1) NULL, " +
                        "IsAC [char](1) NULL, " +
                        "IsThreePhase [char](1) NULL, " +
                        "StringNumber [int] NULL, " +
                        "PhaseNumber [int] NULL, " +
                        "CONSTRAINT PK_devicefeature PRIMARY KEY CLUSTERED " +
                        "( " +
                            "Id ASC " +
                        ") " +
                        "WITH " +
                        "( " +
                            "PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, " +
                            "ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON " +
                        "), " +
                        "CONSTRAINT uk_devicefeature UNIQUE (Device_Id, FeatureType, FeatureId), " +
                        "CONSTRAINT FK_devicefeature_device FOREIGN KEY(Device_Id) REFERENCES device (Id) " +
                    ") ";
            }
        }

        public override string Table_devicereading_energy_2_0_0_0
        {
            get
            {
                return
                    "CREATE TABLE devicereading_energy " +
                    "( " +
                        "ReadingEnd DATETIMEOFFSET(3) NOT NULL, " +
                        "DeviceFeature_Id INT NOT NULL, " +
                        "ReadingStart DATETIMEOFFSET(3) NOT NULL, " +
                        "EnergyTotal FLOAT NULL, " +
                        "EnergyToday FLOAT NULL, " +
                        "EnergyDelta REAL NULL, " +
                        "CalcEnergyDelta REAL NULL, " +
                        "HistEnergyDelta REAL NULL, " +
                        "Mode INT NULL, " +
                        "ErrorCode INT NULL, " +
                        "Power INT NULL, " +
                        "Volts REAL NULL, " +
                        "Amps REAL NULL, " +
                        "Frequency REAL NULL, " +
                        "Temperature REAL NULL, " +
                        "MinPower INT NULL, " +
                        "MaxPower INT NULL, " +                        
                        "CONSTRAINT [PK_devicereading_energy_1] PRIMARY KEY CLUSTERED (ReadingEnd, DeviceFeature_Id ) " +
                        "WITH " +
                        "( " +
                            "PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, " +
                            "ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON " +
                        "), " +
                        "CONSTRAINT uk_devicereading_energy UNIQUE (DeviceFeature_Id, ReadingEnd)  " +
                        "WITH " +
                        "( " +
                            "PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, " +
                            "ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON " +
                        "), " +
                        "CONSTRAINT fk_devicereading_energy_devicefeature FOREIGN KEY (DeviceFeature_Id) REFERENCES devicefeature (Id) " +
                    ") ";
            }
        }

        public override String View_devicedayoutput_v_2000
        {
            get
            {
                return
                    "CREATE VIEW devicedayoutput_v " +
                    "AS SELECT f.Device_Id, CAST(r.ReadingEnd AS DATE) OutputDay, SUM(r.EnergyDelta) OutputKwh " +
                    "from devicereading_energy r, devicefeature f " +
                    "where r.DeviceFeature_Id = f.Id " +
                    "group by f.Device_Id, CAST(r.ReadingEnd AS DATE) ";
            }
        }

        public override string Alter_pvoutputlog_2_0_1_0
        {
            get
            {
                return
                    "alter table pvoutputlog add " +
                    "Volts float NULL, " +
                    "ExtendedData1 float NULL, " +
                    "ExtendedData2 float NULL, " +
                    "ExtendedData3 float NULL, " +
                    "ExtendedData4 float NULL, " +
                    "ExtendedData5 float NULL, " +
                    "ExtendedData6 float NULL ";                   
            }
        }

        public override string Table_devicereading_quantity_2_0_1_1
        {
            get
            {
                return
                    "CREATE TABLE devicereading_quantity " +
                    "( " +
                        "ReadingEnd DATETIMEOFFSET(3) NOT NULL, " +
                        "DeviceFeature_Id INT NOT NULL, " +
                        "ReadingStart DATETIMEOFFSET(3) NOT NULL, " +
                        "QuantityTotal FLOAT NULL, " +
                        "QuantityToday FLOAT NULL, " +
                        "QuantityDelta REAL NULL, " +
                        "Mode INT NULL, " +
                        "ErrorCode INT NULL, " +
                        "Rate REAL NULL, " +
                        "CONSTRAINT [PK_devicereading_quantity_1] PRIMARY KEY CLUSTERED (ReadingEnd, DeviceFeature_Id ) " +
                        "WITH " +
                        "( " +
                            "PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, " +
                            "ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON " +
                        "), " +
                        "CONSTRAINT uk_devicereading_quantity UNIQUE (DeviceFeature_Id, ReadingEnd)  " +
                        "WITH " +
                        "( " +
                            "PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, " +
                            "ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON " +
                        "), " +
                        "CONSTRAINT fk_devicereading_quantity_devicefeature FOREIGN KEY (DeviceFeature_Id) REFERENCES devicefeature (Id) " +
                    ") ";
            }
        }
    }

    public class VersionManager
    {
        private DBVersion Version;

        private bool CopyToTable(String fromRelation, String toTable, List<String> columns, GenConnection con)
        {
            if (columns == null || columns.Count == 0)
            {
                GlobalSettings.LogMessage("VersionManager", "CopyToTable - Empty column list", LogEntryType.ErrorMessage);
                return false;
            }

            String columnList = "";

            foreach (String column in columns)
            {
                if (columnList == "")
                    columnList = column;
                else
                    columnList += ", " + column;
            }



            String fromSelect = "select " + columnList + " from " + fromRelation;
            String toInsert = "insert into " + toTable + " ( " + columnList + " ) " + fromSelect;

            GenCommand cmd = null;

            bool ret = true;

            try
            {
                cmd = new GenCommand(toInsert, con);
                int res = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                ret = false;
                GlobalSettings.LogMessage("VersionManager", "CopyToTable - Exception: " + e.Message, LogEntryType.ErrorMessage);
            }
            finally
            {
                if (cmd != null)
                    cmd.Dispose();
            }

            return ret;
        }

        private bool Update_pvoutput_v(DDL ddl, GenConnection con)
        {
            try
            {
                GenCommand cmd = new GenCommand("drop view pvoutput_v", con);
                int res = cmd.ExecuteNonQuery();
            }
            catch(Exception)
            {
                //utilityLog.LogMessage("VersionManager", "Update_pvoutput_v: exception dropping pvoutput_v: " + e.Message, LogEntryType.ErrorMessage);
            }

            try
            {
                GenCommand cmd = new GenCommand("drop view pvoutput5min_v", con);
                int res = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                //utilityLog.LogMessage("VersionManager", "Update_pvoutput_v: exception dropping pvoutput5min_v: " + e.Message, LogEntryType.ErrorMessage);
            }

            try
            {
                GenCommand cmd = new GenCommand("drop view pvoutput_sub_v", con);
                int res = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                //utilityLog.LogMessage("VersionManager", "Update_pvoutput_v: exception dropping pvoutput_sub_v: " + e.Message, LogEntryType.ErrorMessage);
            }

            try
            {
                if (CreateRelation(ddl.View_pvoutput_sub_v, con))
                    if (CreateRelation(ddl.View_pvoutput_v, con))
                        if (CreateRelation(ddl.View_pvoutput5min_v, con))
                            return true;
            }
            catch (Exception e)
            {
                GlobalSettings.LogMessage("VersionManager", "Update_pvoutput_v: exception creating view: " + e.Message, LogEntryType.ErrorMessage);
            }
            
            return false;
        }

        private bool Update_pvoutput_v_1500(DDL ddl, GenConnection con)
        {
            try
            {
                GenCommand cmd = new GenCommand("drop view pvoutput_v", con);
                int res = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                //utilityLog.LogMessage("VersionManager", "Update_pvoutput_v_1500: exception dropping pvoutput_v: " + e.Message, LogEntryType.ErrorMessage);
            }

            try
            {
                GenCommand cmd = new GenCommand("drop view pvoutput5min_v", con);
                int res = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                //utilityLog.LogMessage("VersionManager", "Update_pvoutput_v_1500: exception dropping pvoutput5min_v: " + e.Message, LogEntryType.ErrorMessage);
            }

            try
            {
                GenCommand cmd = new GenCommand("drop view pvoutput_sub_v", con);
                int res = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                //utilityLog.LogMessage("VersionManager", "Update_pvoutput_v_1500: exception dropping pvoutput_sub_v: " + e.Message, LogEntryType.ErrorMessage);
            }

            try
            {
                if (CreateRelation(ddl.View_pvoutput_sub_v_1500, con))
                    if (CreateRelation(ddl.View_pvoutput_v_1500, con))
                        if (CreateRelation(ddl.View_pvoutput5min_v_1500, con))
                            return true;
            }
            catch (Exception e)
            {
                GlobalSettings.LogMessage("VersionManager", "Update_pvoutput_v_1500: exception creating view: " + e.Message, LogEntryType.ErrorMessage);
            }

            return false;
        }

        private bool Update_pvoutput_v_1700(DDL ddl, GenConnection con)
        {
            try
            {
                GenCommand cmd = new GenCommand("drop view pvoutput_v", con);
                int res = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                //utilityLog.LogMessage("VersionManager", "Update_pvoutput_v_1500: exception dropping pvoutput_v: " + e.Message, LogEntryType.ErrorMessage);
            }

            try
            {
                GenCommand cmd = new GenCommand("drop view pvoutput5min_v", con);
                int res = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                //utilityLog.LogMessage("VersionManager", "Update_pvoutput_v_1500: exception dropping pvoutput5min_v: " + e.Message, LogEntryType.ErrorMessage);
            }

            try
            {
                GenCommand cmd = new GenCommand("drop view pvoutput_sub_v", con);
                int res = cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                //utilityLog.LogMessage("VersionManager", "Update_pvoutput_v_1500: exception dropping pvoutput_sub_v: " + e.Message, LogEntryType.ErrorMessage);
            }

            try
            {
                if (CreateRelation(ddl.View_pvoutput_sub_v_1700, con))
                    if (CreateRelation(ddl.View_pvoutput_v_1700, con))
                        if (CreateRelation(ddl.View_pvoutput5min_v_1700, con))
                            return true;
            }
            catch (Exception e)
            {
                GlobalSettings.LogMessage("VersionManager", "Update_pvoutput_v_1700: exception creating view: " + e.Message, LogEntryType.ErrorMessage);
            }

            return false;
        }

        private bool Update_pvoutput_v_1836(DDL ddl, GenConnection con)
        {
            try
            {
                GenCommand cmd = new GenCommand("drop view pvoutput_v", con);
                int res = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                GlobalSettings.LogMessage("VersionManager", "Update_pvoutput_v_1500: exception dropping pvoutput_v: " + e.Message, LogEntryType.ErrorMessage);
            }

            try
            {
                GenCommand cmd = new GenCommand("drop view pvoutput5min_v", con);
                int res = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                GlobalSettings.LogMessage("VersionManager", "Update_pvoutput_v_1500: exception dropping pvoutput5min_v: " + e.Message, LogEntryType.ErrorMessage);
            }

            try
            {
                GenCommand cmd = new GenCommand("drop view pvoutput_sub_v", con);
                int res = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                GlobalSettings.LogMessage("VersionManager", "Update_pvoutput_v_1500: exception dropping pvoutput_sub_v: " + e.Message, LogEntryType.ErrorMessage);
            }

            try
            {
                if (CreateRelation(ddl.View_pvoutput_sub_v_1836, con))
                    if (CreateRelation(ddl.View_pvoutput_v_1836, con))
                        if (CreateRelation(ddl.View_pvoutput5min_v_1836, con))
                            return true;
            }
            catch (Exception e)
            {
                GlobalSettings.LogMessage("VersionManager", "Update_pvoutput_v_1836: exception creating views: " + e.Message, LogEntryType.ErrorMessage);
            }

            return false;
        }

        private bool CreateRelation(String tableCommand, GenConnection con)
        {
            try
            {
                GenCommand cmd = new GenCommand(tableCommand, con);
                int res = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                GlobalSettings.LogMessage("CreateRelation", "Error creating relation " + tableCommand + " - error: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }
            return true;
        }

        private bool AlterRelation(String tableCommand, GenConnection con)
        {
            try
            {
                GenCommand cmd = new GenCommand(tableCommand, con);
                int res = cmd.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                GlobalSettings.LogMessage("AlterRelation", "Error altering relation " + tableCommand + " - error: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }
            return true;
        }

        private void UpdateVersion(String major, String minor, String release, String patch, GenConnection con)
        {
            try
            {
                GenCommand cmd = new GenCommand("delete from version", con);
                cmd.ExecuteNonQuery();
            }
            catch 
            {
            }

            String cmdStr;

            if (con.DBType == GenDBType.SQLServer)
                cmdStr =
                "insert into version (Major, Minor, Release, Patch) values (@Major, @Minor, @Release, @Patch)";
            else
                cmdStr =
                "insert into version (Major, Minor, `Release`, Patch) values (@Major, @Minor, @Release, @Patch)";

            try
            {
                GenCommand cmd = new GenCommand(cmdStr, con);
                cmd.AddParameterWithValue("@Major", major);
                cmd.AddParameterWithValue("@Minor", minor);
                cmd.AddParameterWithValue("@Release", release);
                cmd.AddParameterWithValue("@Patch", patch);
                cmd.ExecuteNonQuery();
                Version.major = major;
                Version.minor = minor;
                Version.release = release;
                Version.patch = patch;
            }
            catch (Exception e)
            {
                throw new Exception("UpdateVersion: error updating version table: " + e.Message, e);
            }
        }

        public bool GetCurrentVersion(GenConnection con, String databaseType, out DBVersion version)
        {
            GenCommand cmd;
            if (databaseType == "SQL Server")
                cmd = new GenCommand("Select Major, Minor, [Release], Patch from [version]", con);
            else
                cmd = new GenCommand("Select Major, Minor, `Release`, Patch from version", con);

            version.major = "";
            version.minor = "";
            version.release = "";
            version.patch = "";

            try
            {
                GenDataReader dr;

                dr = (GenDataReader)cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    dr.Read();
                    version.major = dr.GetString("Major");
                    version.minor = dr.GetString("Minor");
                    version.release = dr.GetString("Release");
                    version.patch = dr.GetString("Patch");

                    dr.Close();
                    return true;
                }
                else
                {
                    dr.Close();
                    return false;
                }
            }
            catch (Exception e)
            {
                if (GlobalSettings.SystemServices != null) // true when service is running
                    GlobalSettings.LogMessage("GetCurrentVersion", "Exception: " + e.Message);
                return false;
            }
        }

        private DDL SelectDDL(String databaseType)
        {
            DDL DDL;

            if (databaseType == "MySql")
                if (GlobalSettings.ApplicationSettings.DatabaseVersion == "5.6")
                    DDL = new MySql_5_6_DDL();
                else
                    DDL = new MySql_DDL();
            else if (databaseType == "SQLite")
                DDL = new SQLite_DDL();
            else if (databaseType == "SQL Server")
                DDL = new SQLServer_DDL();
            else
                throw new Exception("Unexpected database type: " + databaseType);
            return DDL;
        }

        public bool UpdateTo_1_3_5_4(GenConnection con, String databaseType)
        {
            int res;

            res = Version.CompareVersion( "1", "3", "5", "4");
            if (res >= 0)
                return false;

            DDL DDL = SelectDDL(databaseType);

            GlobalSettings.LogMessage("UpdateTo_1_3_5_4", "Updating database structure to suit version 1.3.5.4", LogEntryType.Information);

            if (res < 0)
            {
                CreateRelation(DDL.Table_version, con);
                CreateRelation(DDL.Table_pvoutputlog, con);
            }

            if (Update_pvoutput_v(DDL, con))
            {
                UpdateVersion("1", "3", "5", "4", con);
                return true;
            }
            else
                return false;
        }

        public bool UpdateTo_1_3_6_0(GenConnection con, String databaseType)
        {            
            int res;
            
            res = Version.CompareVersion("1", "3", "6", "0");
            if (res >= 0)
                return false;

            DDL DDL = SelectDDL(databaseType);

            GlobalSettings.LogMessage("UpdateTo_1_3_6_0", "Updating database structure to suit version 1.3.6.0", LogEntryType.Information);

            bool success = true;

            if (res < 0)
            {
                success &= AlterRelation(DDL.Alter_pvoutputlog_1400a, con);

                if (databaseType == "SQLite")
                    success &= AlterRelation(((SQLite_DDL)DDL).Alter_pvoutputlog_1400b, con);
            }

            // the tables created here are now 1.4.0.0 compliant
            if (success)
                UpdateVersion("1", "4", "0", "0", con);

            return success;
        }

        public bool UpdateTo_1_4_0_3(GenConnection con, String databaseType)
        {
            int res;

            res = Version.CompareVersion("1", "4", "0", "3");
                if (res >= 0)
                    return false;

            DDL DDL = SelectDDL(databaseType);

            GlobalSettings.LogMessage("UpdateTo_1_4_0_3", "Updating database structure to suit version 1.4.0.3", LogEntryType.Information);

            bool success = true;

            if (res < 0)
            {
                if (databaseType == "SQLite")
                {
                    success = AlterRelation("drop table `pvoutputlog` ", con);
                    success &= CreateRelation(DDL.Table_pvoutputlog, con);
                }
                else
                {
                    success = AlterRelation(DDL.Alter_pvoutputlog_1400b, con);

                }
            }

            if (success)
                UpdateVersion("1", "4", "0", "3", con);

            return success;
        }

        public bool UpdateTo_1_4_2_1(GenConnection con, String databaseType)
        {
            int res;

            res = Version.CompareVersion("1", "4", "2", "1");
            if (res >= 0)
                return false;
            
            bool success = true;

            DDL DDL = SelectDDL(databaseType);

            GlobalSettings.LogMessage("UpdateTo_1_4_2_1", "Updating database structure to suit version 1.4.2.1", LogEntryType.Information);

            success = AlterRelation(DDL.Alter_pvoutputlog_1421, con);
            
            if (success)
                UpdateVersion("1", "4", "2", "1", con);

            return success;
        }

        public bool UpdateTo_1_5_0_0(GenConnection con, String databaseType)
        {
            int res;

            res = Version.CompareVersion("1", "5", "0", "0");
            if (res >= 0)
                return false;
            
            bool success = true;

            DDL DDL = SelectDDL(databaseType);

            GlobalSettings.LogMessage("UpdateTo_1_5_0_0", "Updating database structure to suit version 1.5.0.0", LogEntryType.Information);

            success = Update_pvoutput_v_1500(DDL, con);

            if (success)
                UpdateVersion("1", "5", "0", "0", con);

            return success;
        }

        public bool UpdateTo_1_7_0_0(GenConnection con, String databaseType)
        {
            int res;

            res = Version.CompareVersion("1", "7", "0", "0");
            if (res >= 0)
                return false;
            
            bool success = true;

            DDL DDL = SelectDDL(databaseType);

            GlobalSettings.LogMessage("UpdateTo_1_7_0_0", "Updating database structure to suit version 1.7.0.0", LogEntryType.Information);

            success = Update_pvoutput_v_1700(DDL, con);

            if (success)
                UpdateVersion("1", "7", "0", "0", con);

            return success;
        }

        public bool UpdateTo_1_8_3_6(GenConnection con, String databaseType)
        {
            int res;

            res = Version.CompareVersion("1", "8", "3", "6");
            if (res >= 0)
                return false;
            
            bool success = true;

            DDL DDL = SelectDDL(databaseType);

            GlobalSettings.LogMessage("UpdateTo_1_8_3_6", "Updating database structure to suit version 1.8.3.6", LogEntryType.Information);
            
            success &= Update_pvoutput_v_1836(DDL, con);

            if (success)
                UpdateVersion("1", "8", "3", "6", con);

            return success;
        }

        public bool UpdateTo_2_0_0_0(GenConnection con, String databaseType)
        {
            int res;

            res = Version.CompareVersion("2", "0", "0", "0");
            if (res >= 0)
                return false;
            
            bool success = true;

            DDL DDL = SelectDDL(databaseType);

            GlobalSettings.LogMessage("UpdateTo_2_0_0_0", "Updating database structure to suit version 2.0.0.0", LogEntryType.Information);

            success = CreateRelation(DDL.Table_devicetype_2000, con);
            success &= CreateRelation(DDL.Table_device_2000, con);
            success &= CreateRelation(DDL.Table_devicefeature_2000, con);
            success &= CreateRelation(DDL.Table_devicereading_energy_2_0_0_0, con);
            success &= CreateRelation(DDL.View_devicedayoutput_v_2000, con);

            if (success)
                UpdateVersion("2", "0", "0", "0", con);

            return success;
        }

        public bool UpdateTo_2_0_0_17(GenConnection con, String databaseType)
        {
            int res;

            res = Version.CompareVersion("2", "0", "0", "17");
            if (res >= 0)
                return false;

            bool success = true;

            DDL DDL = SelectDDL(databaseType);

            if (databaseType == "SQLite")
            {
                GlobalSettings.LogMessage("UpdateTo_2_0_0_17", "Updating database structure to suit version 2.0.0.17", LogEntryType.Information);
                success &= AlterRelation(((SQLite_DDL)DDL).Drop_devicedayoutput_v, con);
                if (success)
                    success &= CreateRelation(((SQLite_DDL)DDL).View_devicedayoutput_v_20017, con);
            }

            if (success)
                UpdateVersion("2", "0", "0", "17", con);

            return success;
        }

        public bool UpdateTo_2_0_0_26(GenConnection con, String databaseType)
        {
            int res;

            res = Version.CompareVersion("2", "0", "1", "0");
            if (res >= 0)
                return false;

            bool success = true;

            DDL DDL = SelectDDL(databaseType);
           
            GlobalSettings.LogMessage("UpdateTo_2_0_1_0", "Updating database structure to suit version 2.0.0.26", LogEntryType.Information);
            success &= AlterRelation(DDL.Alter_pvoutputlog_2_0_1_0, con);
            
            if (success && databaseType == "SQLite")
            {
                success &= AlterRelation(((SQLite_DDL)DDL).Alter_pvoutputlog_2_0_1_0a, con);
                if (success)
                    success &= AlterRelation(((SQLite_DDL)DDL).Alter_pvoutputlog_2_0_1_0b, con);
                if (success)
                    success &= AlterRelation(((SQLite_DDL)DDL).Alter_pvoutputlog_2_0_1_0c, con);
                if (success)
                    success &= AlterRelation(((SQLite_DDL)DDL).Alter_pvoutputlog_2_0_1_0d, con);
                if (success)
                    success &= AlterRelation(((SQLite_DDL)DDL).Alter_pvoutputlog_2_0_1_0e, con);
                if (success)
                    success &= AlterRelation(((SQLite_DDL)DDL).Alter_pvoutputlog_2_0_1_0f, con);                
            }

            if (success)
                UpdateVersion("2", "0", "1", "0", con);

            return success;
        }

        public bool UpdateTo_2_0_0_31(GenConnection con, String databaseType)
        {
            int res;

            res = Version.CompareVersion("2", "0", "1", "1");
            if (res >= 0)
                return false;

            bool success = true;

            DDL DDL = SelectDDL(databaseType);

            GlobalSettings.LogMessage("UpdateTo_2_0_1_1", "Updating database structure to suit version 2.0.0.31", LogEntryType.Information);

            success &= CreateRelation(DDL.Table_devicereading_quantity_2_0_1_1, con);

            if (success)
                UpdateVersion("2", "0", "1", "1", con);

            return success;
        }

        public struct DBVersion
        {
            public String major;
            public String minor;
            public String release;
            public String patch;

            public int CompareVersion(String majorR, String minorR, String releaseR, String patchR)
            {
                int res;

                if ((res = int.Parse(major, DeviceSettings.SettingsNumberFormat).CompareTo(int.Parse(majorR, DeviceSettings.SettingsNumberFormat))) > 0)
                    return res;
                else if (res == 0)
                {
                    if ((res = int.Parse(minor, DeviceSettings.SettingsNumberFormat).CompareTo(int.Parse(minorR, DeviceSettings.SettingsNumberFormat))) > 0)
                        return res;
                    else if (res == 0)
                    {
                        if ((res = int.Parse(release, DeviceSettings.SettingsNumberFormat).CompareTo(int.Parse(releaseR, DeviceSettings.SettingsNumberFormat))) > 0)
                            return res;
                        else if (res == 0)
                            res = int.Parse(patch, DeviceSettings.SettingsNumberFormat).CompareTo(int.Parse(patchR, DeviceSettings.SettingsNumberFormat));
                    }
                }
                return res;
            }
        }

        private bool RelationExists(GenConnection con, string relationName)
        {
            GenCommand cmd = null;

            try
            {
                cmd = new GenCommand("Select * from " + relationName + " where 0 = 1 ", con);
                GenDataReader dr;
                dr = (GenDataReader)cmd.ExecuteReader();                
                dr.Close();
                return true;
            }
            catch (Exception e)
            {
                if (GlobalSettings.SystemServices != null) // true when service is running
                    GlobalSettings.LogMessage("RelationExists", "Exception: " + e.Message);
                return false;
            }
        }

        private bool PopulateEmptyDatabase(GenConnection con, String databaseType)
        {
            if (GlobalSettings.SystemServices != null) // true when service is running
                GlobalSettings.LogMessage("PopulateEmptyDatabase", "Populating empty database", LogEntryType.Information);
            DDL DDL;
            
            if (databaseType == "MySql")
                if (GlobalSettings.ApplicationSettings.DatabaseVersion == "5.6")
                    DDL = new MySql_5_6_DDL();
                else
                    DDL = new MySql_DDL();
            else if (databaseType == "SQLite")
                DDL = new SQLite_DDL();
            else if (databaseType == "SQL Server")
                DDL = new SQLServer_DDL();
            else
                throw new Exception("Unexpected database type: " + databaseType);

            bool success = true;
            
            if (success && !RelationExists(con, "devicetype"))
                success &= CreateRelation(DDL.CurrentTable_devicetype, con);
            if (success && !RelationExists(con, "device"))
                success &= CreateRelation(DDL.CurrentTable_device, con);
            if (success && !RelationExists(con, "devicefeature"))
                success &= CreateRelation(DDL.CurrentTable_devicefeature, con);
            if (success && !RelationExists(con, "devicereading_energy"))
                success &= CreateRelation(DDL.CurrentTable_devicereading_energy, con);
            if (success && !RelationExists(con, "devicereading_quantity"))
                success &= CreateRelation(DDL.CurrentTable_devicereading_quantity, con);
            if (success && !RelationExists(con, "devicedayoutput_v"))
                success &= CreateRelation(DDL.CurrentView_devicedayoutput_v, con);
            if (success && !RelationExists(con, "pvoutputlog"))
                success &= CreateRelation(DDL.CurrentTable_pvoutputlog, con);

            if (success && !RelationExists(con, "version"))
                success &= CreateRelation(DDL.CurrentTable_version, con);

            if (success)
                UpdateVersion("2", "0", "1", "1", con);

            if (!success && GlobalSettings.SystemServices != null) 
                GlobalSettings.LogMessage("PopulateEmptyDatabase", "Errors when populating empty database", LogEntryType.ErrorMessage);

            return success;
        }

        public void PopulateDatabaseIfEmpty(GenConnection con)
        {
            try
            {
                if (!GetCurrentVersion(con, GlobalSettings.ApplicationSettings.DatabaseType, out Version))
                    PopulateEmptyDatabase(con, GlobalSettings.ApplicationSettings.DatabaseType);
            }
            catch (Exception e)
            {
                throw new Exception("Exception in PopulateDatabaseIfEmpty: " + e.Message, e);
            }
        }

        public void UpdateVersion(GenConnection con)
        {
            

            if (GetCurrentVersion(con, GlobalSettings.ApplicationSettings.DatabaseType, out Version))
            {

                if (GlobalSettings.ApplicationSettings.DatabaseType != "SQL Server")
                {
                    UpdateTo_1_3_5_4(con, GlobalSettings.ApplicationSettings.DatabaseType);
                    UpdateTo_1_3_6_0(con, GlobalSettings.ApplicationSettings.DatabaseType);
                    UpdateTo_1_4_0_3(con, GlobalSettings.ApplicationSettings.DatabaseType);
                }

                UpdateTo_1_4_2_1(con, GlobalSettings.ApplicationSettings.DatabaseType);
                UpdateTo_1_5_0_0(con, GlobalSettings.ApplicationSettings.DatabaseType);

                UpdateTo_1_7_0_0(con, GlobalSettings.ApplicationSettings.DatabaseType);
                UpdateTo_1_8_3_6(con, GlobalSettings.ApplicationSettings.DatabaseType);
                UpdateTo_2_0_0_0(con, GlobalSettings.ApplicationSettings.DatabaseType);
                UpdateTo_2_0_0_17(con, GlobalSettings.ApplicationSettings.DatabaseType);
                UpdateTo_2_0_0_26(con, GlobalSettings.ApplicationSettings.DatabaseType);
                UpdateTo_2_0_0_31(con, GlobalSettings.ApplicationSettings.DatabaseType);
            }
            else
                PopulateEmptyDatabase(con, GlobalSettings.ApplicationSettings.DatabaseType);
        }

    }
}
