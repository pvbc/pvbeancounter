﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml;

namespace PVSettings
{
    public class DeviceEventSettings : PVSettings.SettingsBase
    {
        private ApplicationSettings ApplicationSettings;
        public DeviceManagerDeviceSettings Device { get; private set; }

        private static List<EventType> _EventTypeList = null;
        private static List<String> _EventNameList = null;

        public DeviceEventSettings(SettingsBase root, XmlElement element, DeviceManagerDeviceSettings device)
            : base(root, element)
        {
            ApplicationSettings = (ApplicationSettings)root;
            Device = device;

            _EventFeatureType = new GenericSetting<FeatureType?>((FeatureType?)null, this, "EventFeatureType");
            _EventFeatureId = new GenericSetting<uint?>((uint?)null, this, "EventFeatureId");
            _UseForFeedIn = new GenericSetting<bool>(false, this, "UseForFeedIn");
            _EventName = new GenericSetting<string>("", this, "EventName");
            _EventType = new GenericSetting<EventType?>((EventType?)null, this, "EventType");

            if (_EventTypeList == null)
            {
                LoadEventTypeNameLists();
                DoPropertyChanged("EventTypeList");
            }
        }

        public static void LoadEventTypeNameLists()
        {
            _EventTypeList = new List<EventType>();

            for (PVSettings.EventType i = 0; i < PVSettings.EventType._TypeCount; i++)
                _EventTypeList.Add(i);

            _EventNameList = new List<String>();
            _EventNameList.Add("Inverter Yield");
            _EventNameList.Add("Meter Yield");
            _EventNameList.Add("Consumption");
        }

        public List<EventType> EventTypeList { get { return _EventTypeList; } }

        public List<String> EventNameList { get { return _EventNameList; } }

        public ObservableCollection<FeatureSettings> EventFeatures { get { return Device.DeviceSettings.FeatureList; } }

        public FeatureSettings EventFeature
        {
            get
            {
                FeatureType? fromType = EventFeatureType;
                uint? fromId = EventFeatureId;
                if (fromType == null || fromId == null)
                    return null;
                return Device.DeviceSettings.FindFeature(fromType.Value, fromId.Value);
            }
            set
            {
                if (value == null)
                {
                    EventFeatureType = null;
                    EventFeatureId = null;
                }
                else
                {
                    EventFeatureType = value.FeatureType;
                    EventFeatureId = value.FeatureId;
                }
            }
        }

        private GenericSetting<FeatureType?> _EventFeatureType;
        public FeatureType? EventFeatureType
        {
            get { return _EventFeatureType.Value; }
            set
            {
                FeatureType? old = EventFeatureType;
                uint? oldId = EventFeatureId;
                _EventFeatureType.Value = value;
                
                if (EventFeatureType == FeatureType.YieldAC || EventFeatureType == FeatureType.YieldDC || EventFeatureType == FeatureType.ExportAC || EventFeatureType == FeatureType.NetExportAC)
                    EventType = PVSettings.EventType.Yield;
                else if (EventFeatureType == FeatureType.ConsumptionAC || EventFeatureType == FeatureType.ImportAC)
                    EventType = PVSettings.EventType.Consumption;
                else if (!EventType.HasValue)
                    EventType = PVSettings.EventType.Energy;

                if (old != value)
                {
                    if (ValidateEvent())
                        EventFeatureId = null;
                    else
                    {
                        _EventFeatureType.Value = old;
                        EventFeatureId = oldId;
                    }
                }
            }
        }

        private GenericSetting<uint?> _EventFeatureId;
        public uint? EventFeatureId
        {
            get { return _EventFeatureId.Value; }
            set { _EventFeatureId.Value = value; }
        }

        private GenericSetting<bool> _UseForFeedIn;
        public bool UseForFeedIn
        {
            get { return _UseForFeedIn.Value; }
            set
            {
                if (EventType > PVSettings.EventType.Consumption)
                    value = false;
                _UseForFeedIn.Value = value;                
                if (value)
                    ClearOtherMatchingEvents(true);
            }
        }

        private GenericSetting<String> _EventName;
        public String EventName
        {
            get { return _EventName.Value.Trim(); }
            set { _EventName.Value = value.Trim(); }
        }

        public override bool IsThisMyElement(XmlElement newElem)
        {
            string etName = _EventType.ElementName;
            XmlElement et = GetElementByName(newElem, etName);
            if (et == null)
                return false;
            String etValue;
            try
            {
                etValue = et.Attributes["value"].Value;
            }
            catch (Exception)
            {
                return false;
            }

            string ftName = _EventFeatureType.ElementName;
            XmlElement ft = GetElementByName(newElem, ftName);
            if (ft == null)
                return false;
            String ftValue;
            try
            {
                ftValue = ft.Attributes["value"].Value;
            }
            catch (Exception)
            {
                return false;
            }

            string fidName = _EventFeatureId.ElementName;
            XmlElement fid = GetElementByName(newElem, fidName);
            if (fid == null)
                return false;
            String fidValue;
            try
            {
                fidValue = et.Attributes["value"].Value;
            }
            catch (Exception)
            {
                return false;
            }
            
            return GetValue(etName) == etValue && GetValue(ftName) == ftValue && GetValue(fidName) == fidValue;
        }

        private void ClearOtherMatchingEvents(bool clearOtherFeedIn)
        {
            PVSettings.EventType? eventType = EventType;
            String customEvent = EventName;
            if (eventType > PVSettings.EventType.Consumption || !eventType.HasValue)
                return;

            foreach (DeviceManagerDeviceSettings d in ApplicationSettings.AllDevicesList)
            {
                bool cleared = false;
                for (int i = 0; i < d.DeviceEvents.Collection.Count; )
                {
                    DeviceEventSettings e = d.deviceEvents.Collection[i];
                    if (e != this && e.EventType == eventType)
                        if (e.EventName == customEvent)
                        {
                            d.DeleteEvent(e);
                            cleared = true;
                            continue;
                        }
                        else if (clearOtherFeedIn && e.UseForFeedIn)
                        {
                            e.UseForFeedIn = false;
                            cleared = true;
                        }

                    i++;
                }
                if (cleared)
                    d.AutoEvents = false;
            }
        }

        private bool ValidateEvent()
        {
            if (!EventFeatureType.HasValue)
                return true;
            if (!EventType.HasValue)
                return true;
            if (EventFeatureType == FeatureType.YieldAC && EventType != PVSettings.EventType.Yield)
                return false;
            if (EventFeatureType == FeatureType.ConsumptionAC && EventType != PVSettings.EventType.Consumption)
                return false;

            return true;
        }

        private GenericSetting<EventType?> _EventType;
        public EventType? EventType
        {
            get { return _EventType.Value; }
            set
            {
                EventType? old = EventType;
                Device.AutoEvents = false;
                _EventType.Value = value;

                if (ValidateEvent())
                {
                    if (value.HasValue)
                    {
                        if (EventName == "")
                            EventName = value.Value.ToString();
                        if (value > PVSettings.EventType.Consumption)
                            UseForFeedIn = false;
                        else
                            ClearOtherMatchingEvents(UseForFeedIn);
                    }
                }
                else
                    _EventType.Value = old;
            }
        }
    }
}
