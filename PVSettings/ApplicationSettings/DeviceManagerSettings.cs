﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml;
using PVBCInterfaces;

namespace PVSettings
{
    public enum DeviceManagerType
    {
        ActiveDevice_Generic = 0,
        SMA_SunnyExplorer,
        SMA_WebBox,
        Owl_Meter,
        CC128,
        EW4009,
        DutchSmartMeter,
        RAVEn_USB,
        Consolidation
    }

    public class DeviceManagerSettings : SettingsBase, MackayFisher.Utilities.INamedItem, IThreadControl
    {
        public ApplicationSettings ApplicationSettings;
        public PVSettings.SerialPortSettings SerialPort;
        private ProtocolSettings ProtocolSettings;

        public DeviceGroup DeviceGroup;

        SettingsCollection<DeviceManagerDeviceSettings, DeviceManagerSettings> deviceList = null;

        public bool IsRealDeviceManager { get { return ManagerType != DeviceManagerType.Consolidation; } }

        private bool? _UsesSerialPort = null;
        public bool UsesSerialPort 
        { 
            get 
            {
                if (_UsesSerialPort.HasValue)
                    return _UsesSerialPort.Value;
                //ProtocolSettings protocol = ApplicationSettings.DeviceManagementSettings.GetProtocol(Protocol);
                _UsesSerialPort = ProtocolSettings.UsesSerialPort;
                return _UsesSerialPort.Value;
            } 
        }

        public String PortName
        {
            get { return !UsesSerialPort ? null : SerialPort.PortName; }
            set
            {
                if (SerialPort != null)
                {
                    SerialPort.PortName = value;
                    DoPropertyChanged("Description");
                }
            }
        }

        public String BaudRate
        {
            get { return !UsesSerialPort ? null : SerialPort.BaudRate.ToString(); }
            set
            {
                if (SerialPort != null)
                    SerialPort.BaudRate = value == null || value == "" ? (int?)null : Convert.ToInt32(value);
            }
        }

        public String DataBits
        {
            get { return !UsesSerialPort ? null : SerialPort.DataBits.ToString(); }
            set
            {
                if (SerialPort != null)
                    SerialPort.DataBits = value == null || value == "" ? (int?)null : Convert.ToInt32(value);
            }
        }

        public String StopBits
        {
            get { return !UsesSerialPort ? null : PVSettings.SerialPortSettings.ToString(SerialPort.StopBits); }
            set
            {
                if (SerialPort != null)
                    SerialPort.StopBits = PVSettings.SerialPortSettings.ToStopBits(value);
            }
        }

        public String Parity
        {
            get { return !UsesSerialPort ? null : PVSettings.SerialPortSettings.ToString(SerialPort.Parity); }
            set
            {
                if (SerialPort != null)
                    SerialPort.Parity = PVSettings.SerialPortSettings.ToParity(value);
            }
        }

        public String Handshake
        {
            get { return !UsesSerialPort ? null : PVSettings.SerialPortSettings.ToString(SerialPort.Handshake); }
            set
            {
                if (SerialPort != null)
                    SerialPort.Handshake = PVSettings.SerialPortSettings.ToHandshake(value);
            }
        }

        public TimeSpan? TimeOut
        {
            get { return ProtocolSettings.TimeOut; }
        }

        public DeviceManagerSettings(ApplicationSettings root, XmlElement element)
            : base(root, element)
        {
            ApplicationSettings = (ApplicationSettings)root;
            SerialPort = null;

            _ListenerDeviceId = new GenericSetting<string>("", this, "ListenerDeviceId");

            {
                GenericSetting<String>.GetDefaultValue myDefault = () =>
                {
                    String val = GetValue("protocol");
                    if (val == "")
                        val = "Modbus";
                    else
                    {
                        _DeviceGroupName.SetValue(val, true);
                        DeleteElement("protocol");
                    }
                    return val;
                };
                _DeviceGroupName = new GenericSetting<String>(myDefault, this, "DeviceGroupName");
            }

            String deviceGroupName = DeviceGroupName;
            DeviceGroup = ApplicationSettings.DeviceManagementSettings.GetDeviceGroup(deviceGroupName);
            ProtocolSettings = ApplicationSettings.DeviceManagementSettings.GetProtocol(DeviceGroup.Protocol);

            _Enabled = new GenericSetting<bool>(true, this, "Enabled");
            _TraceEnabled = new GenericSetting<bool>(true, this, "TraceEnabled");

            {
                GenericSetting<String>.GetDefaultValue myDefault = () => { return SetAutoName(); };
                _Name = new GenericSetting<String>(myDefault, this, "Name", false);
            }

            _InstanceNo = new GenericSetting<int>(1, this, "InstanceNo");
            _DiscardStateAtRestart = new GenericSetting<bool>(false, this, "DiscardStateAtRestart");

            {
                GenericSetting<String>.GetDefaultValue myDefault = () =>
                {
                    if (UseQueryInterval)                    
                        return "15";
                    else
                        return "";
                };
                _MessageInterval = new GenericSetting<String>(myDefault, this, "MessageInterval");
            }

            {
                GenericSetting<String>.GetDefaultValue myDefault = () =>
                {
                    if (ManagerType == DeviceManagerType.SMA_SunnyExplorer || ManagerType == DeviceManagerType.SMA_WebBox)
                        return "300";
                    else
                        return "60";
                };
                _DBInterval = new GenericSetting<String>(myDefault, this, "DBInterval");
            }

            _ExecutablePath = new GenericSetting<String>("", this, "ExecutablePath");
            _OwlDatabase = new GenericSetting<String>("C:\\ProgramData\\2SE\\be.db", this, "OwlDatabase");

            {
                GenericSetting<String>.GetDefaultValue myDefault = () =>
                {
                    String name = ApplicationSettings.SunnyExplorerPlantName.Trim();
                    if (name != "")
                        _SunnyExplorerPlantName.Value = name;
                    ApplicationSettings.DeleteValue("sunnyexplorerplantname");

                    if (name == "")
                        return "SunnyExplorer";
                    else
                        return name;
                };
                _SunnyExplorerPlantName = new GenericSetting<String>(myDefault, this, "SunnyExplorerPlantName");
            }

            _SunnyExplorerPassword = new GenericSetting<String>("0000", this, "SunnyExplorerPassword");
            _MaxHistoryDays = new GenericSetting<int>(2, this, "MaxHistoryDays");
            _HistoryHours = new GenericSetting<int?>(24, this, "HistoryHours");

            {
                GenericSetting<DateTime?>.GetDefaultValue myDefault = () => { return ApplicationSettings.FirstFullDay; };
                _FirstFullDay = new GenericSetting<DateTime?>(myDefault, this, "FirstFullDay");
            }

            _ResetFirstFullDay = new GenericSetting<bool>(true, this, "ResetFirstFullDay");
            _ResetFirstFullDaySet = new GenericSetting<DateTime?>((DateTime?)null, this, "ResetFirstFullDaySet");
            _WebBoxUsePush = new GenericSetting<bool>(false, this, "WebBoxUsePush");
            _WebBoxFtpUrl = new GenericSetting<String>("", this, "WebBoxFtpUrl");
            _WebBoxPushDirectory = new GenericSetting<String>("", this, "WebBoxPushDirectory");
            _WebBoxUserName = new GenericSetting<String>("user", this, "WebBoxUserName");
            _WebBoxPassword = new GenericSetting<String>("", this, "WebBoxPassword");
            _WebBoxVersion = new GenericSetting<int>(1, this, "WebBoxVersion");
            _WebBoxFtpLimit = new GenericSetting<Int32?>((Int32?)null, this, "WebBoxFtpLimit");

            {
                GenericSetting<int>.GetDefaultValue myDefault = () => { return ManagerType == DeviceManagerType.SMA_WebBox ? 2 : 0; };
                _IntervalOffset = new GenericSetting<int>(myDefault, this, "IntervalOffset");
            }

            LoadDetails();
        }

        private void LoadSerialPort()
        {
            _UsesSerialPort = null; // forces refer back t0 protocol for this info
            if (UsesSerialPort)
            {
                XmlElement serial = GetElement("serialport");
                if (serial != null)
                    SerialPort = new PVSettings.SerialPortSettings(ApplicationSettings, serial, ProtocolSettings.SerialPort);
                else
                {
                    serial = AddElement(Settings, "serialport");
                    SerialPort = new PVSettings.SerialPortSettings(ApplicationSettings, serial, ProtocolSettings.SerialPort);
                }
            }
            else
                SerialPort = null;
        }

        private void LoadDetails()
        {
            LoadSerialPort();

            deviceList = new SettingsCollection<DeviceManagerDeviceSettings, DeviceManagerSettings>(this, "devices", "device",
                (SettingsBase root, XmlElement e, DeviceManagerSettings dms, String id) => new DeviceManagerDeviceSettings(root, e, dms, id));
        }

        public DeviceManagerDeviceSettings AddDevice()
        {
            DeviceManagerDeviceSettings dev = deviceList.Add(DeviceListItems[0].Id);
            ApplicationSettings.RefreshAllDevices();
            
            return dev;
        }

        public void DeleteDevice(DeviceManagerDeviceSettings delDev)
        {
            deviceList.Remove(delDev);
        }

        public void DeleteAllDevices()
        {
            while(deviceList.Collection.Count > 0)
                deviceList.Remove(deviceList.Collection[0]);
        }

        public ObservableCollection<DeviceListItem> DeviceListItems
        {
            get
            {
                if (DeviceGroup == null)
                    return null;
                else
                    return DeviceGroup.DeviceList;
            }
        }

        public ObservableCollection<DeviceListItem> AllDeviceListItems
        {
            get
            {
                if (DeviceGroup == null)
                    return null;
                else
                    return DeviceGroup.AllDeviceList;
            }
        }

        public ObservableCollection<DeviceListItem> ListenerListItems
        {
            get
            {
                if (DeviceGroup == null)
                    return null;
                else
                    return DeviceGroup.ManagerList;
            }
        }

        public SettingsCollection<DeviceManagerDeviceSettings, DeviceManagerSettings> DeviceList { get { return deviceList; } }
        public ObservableCollection<DeviceManagerDeviceSettings> DeviceListCollection { get { return deviceList.Collection; } }

        public DeviceManagerDeviceSettings GetDevice(uint address)
        {
            foreach (DeviceManagerDeviceSettings dev in deviceList.Collection)
            {
                if (dev.Address == address)
                    return dev;
            }
            return null;
        }

        public DeviceSettings ListenerDeviceSettings
        {
            get
            {
                String listenerDevice = _ListenerDeviceId.Value;
                if (listenerDevice == "")
                    return null;
                
                return ApplicationSettings.DeviceManagementSettings.GetDevice(listenerDevice);               
            }            
        }

        private GenericSetting<String> _ListenerDeviceId;
        public String ListenerDeviceId
        {
            get { return _ListenerDeviceId.Value; }
            set { _ListenerDeviceId.Value = value; }
        }

        public void CheckListenerDeviceId()
        {
            ProtocolSettings.ProtocolType t = ProtocolSettings.Type;
            if (t != ProtocolSettings.ProtocolType.Listener && t != PVSettings.ProtocolSettings.ProtocolType.ManagerQueryResponse)
            {
                ListenerDeviceId = "";
                DoPropertyChanged("ListenerDeviceId");
                return;
            }

            foreach (DeviceListItem item in ListenerListItems)
            {
                if (item.Id == ListenerDeviceId)
                    return;
            }
            if (DeviceListItems.Count > 0)
            {
                ListenerDeviceId = ListenerListItems[0].Id;
                DoPropertyChanged("ListenerDeviceId");
            }
        }

        private GenericSetting<string> _DeviceGroupName;
        public String DeviceGroupName
        {
            get
            {
                return _DeviceGroupName.Value;
            }
            set
            {
                DeviceGroup grp = ApplicationSettings.DeviceManagementSettings.GetDeviceGroup(value);  
                ProtocolSettings ps = ApplicationSettings.DeviceManagementSettings.GetProtocol(grp.Protocol);
                if (GetManagerType(ps.Name) == DeviceManagerType.Consolidation)
                    return;
                SetDeviceGroupName(value);
            }
        }

        public void SetDeviceGroupName(string value)
        {
            DeviceGroup grp = ApplicationSettings.DeviceManagementSettings.GetDeviceGroup(value);
            ProtocolSettings ps = ApplicationSettings.DeviceManagementSettings.GetProtocol(grp.Protocol);
            
            _DeviceGroupName.Value = value;
            DeviceGroup = grp;
            ProtocolSettings = ps;
            DoPropertyChanged("DeviceListItems");
            if (HasAutoName)
            {
                SetAutoName();
                DoPropertyChanged("Name");
            }
            foreach (DeviceManagerDeviceSettings device in DeviceList.Collection)
                device.NotifySelectionChange();
            CheckListenerDeviceId();
            LoadSerialPort();
            DoPropertyChanged("ExecutablePath");
            DoPropertyChanged("ManagerType");
            DoPropertyChanged("ManagerTypeName");
            DoPropertyChanged("AllDeviceListItems");
            DoPropertyChanged("DeviceListItems");
            DoPropertyChanged("ListenerListItems");
            DoPropertyChanged("SunnyExplorerPlantName");
            DoPropertyChanged("SunnyExplorerPassword");
        }

        private bool HasAutoName = false;

        private String SetAutoName()
        {
            String newName = MackayFisher.Utilities.UniqueNameResolver<DeviceManagerSettings>.ResolveUniqueName(
                        DeviceGroupName, ApplicationSettings.DeviceManagerListCollection.GetEnumerator(), this);
            _Name.SetValue(newName, true);
            HasAutoName = true;
            DoPropertyChanged("Description");
            return newName;
        }

        private GenericSetting<string> _Name;
        public String Name
        {
            get
            {
                if (ManagerType == DeviceManagerType.Consolidation)
                {
                    if (_Name.Value != ApplicationSettings.PVOutputConsolidationName)
                    {
                        _Name.Value = ApplicationSettings.PVOutputConsolidationName;
                        DoPropertyChanged("Description");
                    }
                }               
                return _Name.Value;
            }

            set
            {
                if (ManagerType == DeviceManagerType.Consolidation)
                    value = ApplicationSettings.PVOutputConsolidationName;
                else if (value == ApplicationSettings.PVOutputConsolidationName)
                    value = "IllegalName";
                _Name.Value = 
                    MackayFisher.Utilities.UniqueNameResolver<DeviceManagerSettings>.ResolveUniqueName( value, ApplicationSettings.DeviceManagerListCollection.GetEnumerator(), this);
                HasAutoName = (value == null || value == "" || value == DeviceGroupName);
                DoPropertyChanged("Description");
            }
        }

        public override bool IsThisMyElement(XmlElement newElem)
        {
            string eName = _Name.ElementName;
            XmlElement id = GetElementByName(newElem, eName);
            try
            {
                String tmp = id.Attributes["value"].Value;
                return GetValue(eName) == tmp;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public String UniqueName { get { return Name; } }

        public String Description
        {
            get
            {
                if (SerialPort == null)
                    return Name;
                else
                    return Name + ": " + SerialPort.PortName;
            }
        }

        public String ManagerTypeName { get { return ProtocolSettings.Name; } }

        public static DeviceManagerType GetManagerType(String managerTypeName)
        {
            foreach (ManagerSettings ms in ManagerOptions)
                if (ms.TypeName == managerTypeName)
                    return ms.DeviceManagerType;
            
            return DeviceManagerType.ActiveDevice_Generic;    
        }

        public static String GetManagerTypeName(DeviceManagerType managerType)
        {
            return managerType.ToString();            
        }

        public bool UseQueryInterval
        {
            get
            {
                DeviceManagerType managerType = ManagerType;
                foreach (ManagerSettings ms in ManagerOptions)
                    if (ms.DeviceManagerType == managerType)
                        return ms.UsesQueryInterval;
                return false;
            }
        }

        public bool UseDBInterval
        {
            get
            {
                DeviceManagerType managerType = ManagerType;
                foreach (ManagerSettings ms in ManagerOptions)
                    if (ms.DeviceManagerType == managerType)
                        return ms.UsesDBInterval;
                return false;
            }
        }

        private struct ManagerSettings
        {
            public String TypeName;
            public DeviceManagerType DeviceManagerType;
            public bool UsesDBInterval;
            public bool UsesQueryInterval;

            public ManagerSettings(DeviceManagerType deviceManagerType, bool usesQueryInterval, bool usesDBInterval)
            {
                TypeName = deviceManagerType.ToString();
                DeviceManagerType = deviceManagerType;
                UsesQueryInterval = usesQueryInterval;
                UsesDBInterval = usesDBInterval;
            }
        }

        private static readonly ManagerSettings[] ManagerOptions; 
        static DeviceManagerSettings()
        {
            ManagerOptions = new ManagerSettings[]
            { 
                new ManagerSettings( DeviceManagerType.SMA_SunnyExplorer, false, false),
                new ManagerSettings( DeviceManagerType.SMA_WebBox, false, false),
                new ManagerSettings( DeviceManagerType.Owl_Meter, false, false),
                new ManagerSettings( DeviceManagerType.CC128, false, true),
                new ManagerSettings( DeviceManagerType.EW4009, true, true),
                new ManagerSettings( DeviceManagerType.Consolidation, false, false),
                new ManagerSettings( DeviceManagerType.DutchSmartMeter, false, true),
                new ManagerSettings( DeviceManagerType.RAVEn_USB, true, true),
                new ManagerSettings( DeviceManagerType.ActiveDevice_Generic, true, true),
            };
        }

        public DeviceManagerType ManagerType { get { return GetManagerType(ManagerTypeName); } }

        private GenericSetting<int> _InstanceNo;
        public int InstanceNo
        {
            get
            {
                try
                {
                    return _InstanceNo.Value;
                }
                catch (Exception)
                {
                    return 1;
                }
            }

            set
            {
                _InstanceNo.Value = value;
            }
        }

        private GenericSetting<bool> _Enabled;
        public bool Enabled
        {
            get 
            {
                if (ManagerType == DeviceManagerType.Consolidation && _Enabled.Value)
                    _Enabled.Value = false;
                return _Enabled.Value; 
            }
            set { _Enabled.Value = value; } 
        }

        private GenericSetting<bool> _TraceEnabled;
        public bool TraceEnabled
        {
            get 
            { 
                return _TraceEnabled.Value 
                    || !ApplicationSettings.EnableThreadTrace; 
            }
            set { _TraceEnabled.Value = value; }
        }

        private GenericSetting<bool> _DiscardStateAtRestart;
        public bool DiscardStateAtRestart
        {
            get
            {
                return _DiscardStateAtRestart.Value;
            }
            set
            {
                _DiscardStateAtRestart.Value = value;
            }
        }

        private GenericSetting<String> _MessageInterval;
        public String MessageInterval
        {
            get
            {
                return _MessageInterval.Value;
            }

            set
            {
                _MessageInterval.Value = value;                
                if (DBIntervalInt < MessageIntervalInt)
                    DBInterval = MessageInterval;                    
                else if ((DBIntervalInt / MessageIntervalInt) * MessageIntervalInt != DBIntervalInt)
                    DBInterval = MessageInterval;
            }
        }

        
        public UInt16 MessageIntervalInt
        {
            get
            {
                String val = _MessageInterval.Value;
                if (val == "")
                    if (ManagerType == DeviceManagerType.SMA_SunnyExplorer)
                        return 300;
                    else
                        return 6;
                else
                    return UInt16.Parse(val); ;
            }
        }

        private GenericSetting<String> _DBInterval;
        public String DBInterval
        {
            get
            {
                return _DBInterval.Value;
            }

            set
            {
                _DBInterval.Value = value;
                if (DBIntervalInt < MessageIntervalInt)
                    MessageInterval = DBInterval;
                else if ((DBIntervalInt / MessageIntervalInt) * MessageIntervalInt != DBIntervalInt)
                    MessageInterval = DBInterval;              
            }
        }

        public UInt16 DBIntervalInt
        {
            get
            {
                return UInt16.Parse(_DBInterval.Value); ;
            }
        }

        private GenericSetting<String> _ExecutablePath;
        public String ExecutablePath
        {
            get
            {
                if (ProtocolSettings.Type != PVSettings.ProtocolSettings.ProtocolType.Executable)
                    return "";

                String val = _ExecutablePath.Value.Trim();
                if (val == "")
                {
                    val = "C:\\Program Files (x86)\\SMA\\Sunny Explorer\\SunnyExplorer.exe";
                    try
                    {
                        val = System.IO.Path.GetFullPath(val);
                    }
                    catch (ArgumentException)
                    {
                        val = "C:\\Program Files\\SMA\\Sunny Explorer\\SunnyExplorer.exe";
                        try
                        {
                            val = System.IO.Path.GetFullPath(val);
                        }
                        catch (ArgumentException)
                        {
                            val = "Cannot find SunnyExplorer.exe - Is it installed?";
                        }
                    }
                }
                return val;
            }

            set
            {
                if (value != null) value = value.Trim();
                if (ProtocolSettings.Type == PVSettings.ProtocolSettings.ProtocolType.Executable)
                    _ExecutablePath.Value = value;
            }
        }

        private GenericSetting<String> _OwlDatabase;
        public String OwlDatabase
        {
            get { return _OwlDatabase.Value; }
            set { _OwlDatabase.Value = value.Trim(); }
        }

        private GenericSetting<String> _SunnyExplorerPlantName;
        public String SunnyExplorerPlantName
        {
            get { return _SunnyExplorerPlantName.Value; }
            set { _SunnyExplorerPlantName.Value = value.Trim(); }
        }

        private GenericSetting<String> _SunnyExplorerPassword;
        public String SunnyExplorerPassword
        {
            get { return _SunnyExplorerPassword.Value.Trim(); }
            set { _SunnyExplorerPassword.Value = value.Trim(); }
        }

        private GenericSetting<int> _MaxHistoryDays;
        public int MaxHistoryDays
        {
            get
            {
                if (ManagerType != DeviceManagerType.SMA_SunnyExplorer 
                && ManagerType != DeviceManagerType.SMA_WebBox
                && ManagerType != DeviceManagerType.Owl_Meter)
                    return 2;
                else
                    return _MaxHistoryDays.Value;
            }
            set { _MaxHistoryDays.Value = value; }
        }

        private GenericSetting<int?> _HistoryHours;
        public int? HistoryHours
        {
            get { return _HistoryHours.Value; }
            set { _HistoryHours.Value = value; }
        }

        private GenericSetting<DateTime?> _FirstFullDay;
        public DateTime? FirstFullDay
        {
            get { return _FirstFullDay.Value; }
            set { _FirstFullDay.Value = value; }
        }

        private GenericSetting<bool> _ResetFirstFullDay;
        public bool ResetFirstFullDay
        {
            get
            {
                bool val = _ResetFirstFullDay.Value;
                
                // ResetFirstFullDay is only logically set for 10 minutes
                if (val)
                    return ResetFirstFullDaySet >= (DateTime.Now - TimeSpan.FromMinutes(10.0));
                else        
                    return false;
            }

            set
            {
                _ResetFirstFullDay.Value = value;                      
                ResetFirstFullDaySet = value ? DateTime.Now : (DateTime?)null;
            }
        }

        private GenericSetting<DateTime?> _ResetFirstFullDaySet;
        public DateTime? ResetFirstFullDaySet
        {
            get { return _ResetFirstFullDaySet.Value; }
            set { _ResetFirstFullDaySet.Value = value; }
        }

        private GenericSetting<bool> _WebBoxUsePush;
        public bool WebBoxUsePush
        {
            get { return _WebBoxUsePush.Value; }
            set { _WebBoxUsePush.Value = value; }
        }

        private GenericSetting<String> _WebBoxFtpUrl;
        public String WebBoxFtpUrl
        {
            get { return _WebBoxFtpUrl.Value; }
            set { _WebBoxFtpUrl.Value = value.Trim(); }
        }

        private GenericSetting<String> _WebBoxPushDirectory;
        public String WebBoxPushDirectory
        {
            get { return _WebBoxPushDirectory.Value; }
            set { _WebBoxPushDirectory.Value = value.Trim(); }
        }

        public String WebBoxFtpBasePath
        {
            get 
            {
                if (WebBoxVersion == 1)
                    return "DATA";
                else
                    return "XML"; 
            }
        }

        private GenericSetting<String> _WebBoxUserName;
        public String WebBoxUserName
        {
            get { return _WebBoxUserName.Value; }
            set { _WebBoxUserName.Value = value.Trim(); }
        }

        private GenericSetting<String> _WebBoxPassword;
        public String WebBoxPassword
        {
            get { return GetValue("webboxpassword").Trim(); }
            set { SetValue("webboxpassword", value, "WebBoxPassword"); }
        }

        private GenericSetting<int> _WebBoxVersion;
        public int WebBoxVersion
        {
            get { return _WebBoxVersion.Value; }
            set { _WebBoxVersion.Value = value; }
        }

        private GenericSetting<Int32?> _WebBoxFtpLimit;
        public Int32? WebBoxFtpLimit
        {
            get { return _WebBoxFtpLimit.Value; }
            set { _WebBoxFtpLimit.Value = value; }
        }

        private GenericSetting<int> _IntervalOffset;
        public int IntervalOffset
        {
            get { return _IntervalOffset.Value; }
            set { _IntervalOffset.Value = value; }
        }

        private bool _IsSelected = false;
        public bool IsSelected 
        {
            get { return _IsSelected; }
            
            set
            {
                _IsSelected = value;
                DoPropertyChanged("IsSelected");
                foreach (DeviceManagerDeviceSettings device in DeviceList.Collection)
                    device.NotifySelectionChange();
            }
        }        
    }
}

