﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml;

namespace PVSettings
{
    public class DeviceFeatureSettings : PVSettings.SettingsBase
    {
        public DeviceManagerDeviceSettings Device { get; private set; }

        public DeviceFeatureSettings(SettingsBase root, XmlElement element, DeviceManagerDeviceSettings device)
            : base(root, element)
        {
            Device = device;

            _FeatureType = new GenericSetting<FeatureType?>((FeatureType?)null, this, "FeatureType");
            _FeatureId = new GenericSetting<uint?>((uint?)null, this, "FeatureId");
            _SaveFeature = new GenericSetting<bool>(false, this, "SaveFeature");
        }

        public FeatureSettings Feature
        {
            get
            {
                FeatureType? fromType = FeatureType;
                uint? fromId = FeatureId;
                if (fromType == null || fromId == null)
                    return null;
                return Device.DeviceSettings.FindFeature(fromType.Value, fromId.Value);
            }
            set
            {
                if (value == null)
                {
                    FeatureType = null;
                    FeatureId = null;
                }
                else
                {
                    FeatureType = value.FeatureType;
                    FeatureId = value.FeatureId;
                    string save = GetValue("savefeature");
                    if (save == "")
                        SetValue("savefeature", value.DefaultSave ? "true" : "false", "SaveFeature", true);
                }
            }
        }

        public override bool IsThisMyElement(XmlElement newElem)
        {
            string ftName = _FeatureType.ElementName;
            XmlElement ft = GetElementByName(newElem, ftName);
            if (ft == null)
                return false;
            String ftValue;
            try
            {
                ftValue = ft.Attributes["value"].Value;
            }
            catch (Exception)
            {
                return false;
            }

            string fidName = _FeatureId.ElementName;
            XmlElement fid = GetElementByName(newElem, fidName);
            if (fid == null)
                return false;
            String fidValue;
            try
            {
                fidValue = fid.Attributes["value"].Value;
            }
            catch (Exception)
            {
                return false;
            }
            
            return GetValue(ftName) == ftValue && GetValue(fidName) == fidValue;
        }

        private GenericSetting<FeatureType?> _FeatureType;
        public FeatureType? FeatureType
        {
            get { return _FeatureType.Value; }
            set { _FeatureType.Value = value; }
        }

        private GenericSetting<uint?> _FeatureId;
        public uint? FeatureId
        {
            get { return _FeatureId.Value; }
            set { _FeatureId.Value = value; }
        }

        public String Description
        {
            get
            {
                if (Feature == null)
                    return "";
                else
                    return Feature.Name;
            }
        }

        private GenericSetting<bool> _SaveFeature;
        public bool SaveFeature
        {
            get { return _SaveFeature.Value; }
            set { _SaveFeature.Value = value; }
        }
    }
}
