﻿/*
* Copyright (c) 2011 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Xml;
using PVBCInterfaces;

namespace PVSettings
{
    public class PvOutputSiteSettings : SettingsBase, IThreadControl
    {
        public ApplicationSettings ApplicationSettings;
        private ObservableCollection<PVOutputDaySettings> pvOutputDayList;

        public PvOutputSiteSettings(SettingsBase root, XmlElement element)
            : base(root, element)
        {
            ApplicationSettings = (ApplicationSettings)root;

            _HaveSubscription = new GenericSetting<bool>(false, this, "HaveSubscription");

            {
                GenericSetting<String>.GetDefaultValue myDefault = () =>
                {
                    string val = GetValue("siteid");
                    if (val == null || val == "")
                        return "YourSystemIdHere";
                    return val;
                };
                _SystemId = new GenericSetting<string>(myDefault, this, "SystemId");
            }

            {
                GenericSetting<String>.GetDefaultValue myDefault = () => { return SystemId; };
                _Name = new GenericSetting<string>(myDefault, this, "Name");
            }

            _APIKey = new GenericSetting<string>("YourAPIKeyHere", this, "APIKey");
            _DataInterval = new GenericSetting<string>("10", this, "DataInterval");
            _APIVersion = new GenericSetting<string>("", this, "APIVersion");
            _Enable = new GenericSetting<bool>(false, this, "Enable");
            _TraceEnabled = new GenericSetting<bool>(true, this, "TraceEnabled");
            _UploadEnable = new GenericSetting<bool>(true, this, "UploadEnable");
            _AutoBackload = new GenericSetting<bool>(true, this, "AutoBackload");
            _LiveDays = new GenericSetting<string>("2", this, "LiveDays");
            _UploadYield = new GenericSetting<bool>(true, this, "UploadYield");
            _UploadConsumption = new GenericSetting<bool>(true, this, "UploadConsumption");

            {
                GenericSetting<bool>.GetDefaultValue myDefault = () => 
                {
                    string val = GetValue("consumptionpowerminmax");
                    return val != "true"; 
                };
                _PowerMinMax = new GenericSetting<bool>(myDefault, this, "PowerMinMax");
            }

            AdjustPVOutputDayList();
        }

        private void AdjustPVOutputDayList()
        {
            if (LiveDaysChangeIgnore == true)
                return;

            int dayCount = LiveDaysInt;
            if (dayCount < 1)
                dayCount = 1;

            XmlElement days = GetElement("pvoutputdaylist");
            if (days == null)
                days = AddElement(Settings, "pvoutputdaylist");
            
            int i = 0;
            DateTime date = DateTime.Today;

            pvOutputDayList = new ObservableCollection<PVOutputDaySettings>();

            bool matched = true;
            if (days != null)
                while (i < days.ChildNodes.Count && i < dayCount)
                {
                    XmlElement e = (XmlElement)days.ChildNodes[i];
                    if (matched)
                    {
                        date = DateTime.Today - TimeSpan.FromDays(i);
                        PVOutputDaySettings day = new PVOutputDaySettings(ApplicationSettings, e);
                        if (day.Day == date)
                        {
                            pvOutputDayList.Add(day);
                            i++;
                            continue;
                        }
                        else
                            matched = false;
                    }
                    days.RemoveChild(e);                                      
                }

            while (i < dayCount)
            {
                XmlElement e = AddElement(days, "pvoutputday");
                PVOutputDaySettings day = new PVOutputDaySettings(ApplicationSettings, e);
                day.Day = DateTime.Today - TimeSpan.FromDays(i++);
                day.ForceLoad = false;
                pvOutputDayList.Add(day);
            }

            while (days.ChildNodes.Count > dayCount)
            {
                XmlElement e = (XmlElement)days.ChildNodes[days.ChildNodes.Count-1];
                days.RemoveChild(e);
            }
        }

        public override bool IsThisMyElement(XmlElement newElem)
        {
            string eName = _Name.ElementName;
            XmlElement id = GetElementByName(newElem, eName);
            try
            {
                String tmp = id.Attributes["value"].Value;
                return GetValue(eName) == tmp;
            }
            catch (Exception)
            {
                return false;
            }
        }

        private GenericSetting<bool> _HaveSubscription;
        public bool HaveSubscription
        {
            get { return _HaveSubscription.Value; }
            set  { _HaveSubscription.Value = value; }
        }

        public ObservableCollection<PVOutputDaySettings> PvOutputDayList
        {
            get { return pvOutputDayList; }
        }

        public String Description
        {
            get
            {
                if (SystemId == Name)
                    return "System Id: " + SystemId;
                else
                    return Name + " - Id: " + SystemId;
            }
        }

        private GenericSetting<String> _SystemId;
        public String SystemId
        {
            get { return _SystemId.Value; }
            set
            {
                string val = value == null ? "" : value.Trim();
                if (!ApplicationSettings.PVOutputSystemIdAvailable(val))
                    return;
                if (Name == SystemId)
                    Name = val;
                string oldId = SystemId;   
                _SystemId.Value = val;
                ApplicationSettings.PVOutputSystemIdChanged(oldId, val);
                DeleteElement("siteid"); // legacy value no longer required

                OnPropertyChanged(new PropertyChangedEventArgs("PvOutputSystemListCollection"));
            }
        }

        private GenericSetting<String> _Name;
        public String Name
        {
            get
            {
                String val = GetValue("name");
                if (val == null || val == "")
                    return SystemId;
                else
                    return val;
            }

            set
            {
                string val = value == null ? "" : value.Trim();
                string oldName = Name;
                val = ApplicationSettings.PVOutputSystemNameAvailable(val);
                if (val == SystemId)
                    SetValue("name", "", "Name");
                else
                    SetValue("name", val, "Name");
                ApplicationSettings.PVOutputNameChanged(oldName, val == "" ? SystemId : val);
            }
        }

        private GenericSetting<String> _DataInterval;
        public String DataInterval
        {
            get { return _DataInterval.Value; }
            set 
            {
                try
                {
                    int i = int.Parse(_DataInterval.Value);
                    _DataInterval.Value = value;
                }
                catch
                {
                }
            }
        }

        public int DataIntervalSeconds
        {
            get
            {
                try
                {
                    return int.Parse(_DataInterval.Value) * 60;
                }
                catch (Exception)
                {
                    return 600;
                }
            }
        }

        private GenericSetting<String> _APIKey;
        public String APIKey
        {
            get { return _APIKey.Value; }
            set { _APIKey.Value = value; }
        }

        private GenericSetting<String> _APIVersion;
        public String APIVersion
        {
            get { return _APIVersion.Value; }
            set { _APIVersion.Value = value; }
        }

        private GenericSetting<bool> _Enable;
        public bool Enable
        {
            get { return _Enable.Value; }
            set 
            { 
                _Enable.Value = value;
                UploadEnable = value;
            }
        }

        private GenericSetting<bool> _TraceEnabled;
        public bool TraceEnabled
        {
            get { return _TraceEnabled.Value || !ApplicationSettings.EnableThreadTrace; }
            set { _TraceEnabled.Value = value; }
        }

        private GenericSetting<bool> _UploadEnable;
        public bool UploadEnable
        {
            get { return Enable && _UploadEnable.Value; }
            set { _UploadEnable.Value = value && Enable; }
        }

        private GenericSetting<bool> _AutoBackload;
        public bool AutoBackload
        {
            get { return _AutoBackload.Value; }
            set { _AutoBackload.Value = value; }
        }

        public bool LiveDaysChangeIgnore = false;
        private GenericSetting<String> _LiveDays;
        public String LiveDays
        {
            get { return _LiveDays.Value; }
            set
            {
                _LiveDays.SetValue(value, LiveDaysChangeIgnore);
                AdjustPVOutputDayList();
                OnPropertyChanged(new PropertyChangedEventArgs("PvOutputDayList"));
            }
        }

        public int LiveDaysInt { get { return int.Parse(LiveDays); } }

        private GenericSetting<bool> _UploadYield;
        public bool UploadYield
        {
            get { return _UploadYield.Value; }
            set { _UploadYield.Value = value; }
        }

        private GenericSetting<bool> _UploadConsumption;
        public bool UploadConsumption
        {
            get { return _UploadConsumption.Value; }
            set { _UploadConsumption.Value = value; }
        }

        private GenericSetting<bool> _PowerMinMax;
        public bool PowerMinMax
        {
            get { return _PowerMinMax.Value; }
            set
            {
                _PowerMinMax.Value = value;
                DeleteElement("consumptionpowerminmax");
            }
        }
    }
}
