﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml;

namespace PVSettings
{
    public class ConsolidateDeviceSettings : PVSettings.SettingsBase
    {
        public enum OperationType
        {
            Add = 0,
            Subtract,
            Replace,
            Negate,
            Custom
        }

        private DeviceManagerDeviceSettings _ConsolidateToDevice = null;
        private DeviceManagerDeviceSettings _ConsolidateFromDevice = null;
        private ApplicationSettings ApplicationSettings;
        public ConsolidateDeviceSettings(SettingsBase root, XmlElement element, DeviceManagerDeviceSettings fromDevice)
            : base(root, element)
        {
            ApplicationSettings = (ApplicationSettings)root;
            
            _ConsolidateFromFieldName = new GenericSetting<string>("", this, "ConsolidateFromField");
            _ConsolidateFromFeatureType = new GenericSetting<FeatureType?>((FeatureType?)null, this, "ConsolidateFromFeatureType");
            _ConsolidateFromFeatureId = new GenericSetting<uint?>((uint?)null, this, "ConsolidateFromFeatureId");
            _DeviceName = new GenericSetting<string>("", this, "DeviceName");
            _ConsolidateToFieldName = new GenericSetting<string>("", this, "ConsolidateToField");
            _ConsolidateToFeatureType = new GenericSetting<FeatureType>(FeatureType.Unknown, this, "ConsolidateToFeatureType");
            _ConsolidateToFeatureId = new GenericSetting<uint>(0, this, "ConsolidateToFeatureId");
            _Operation = new GenericSetting<OperationType>(OperationType.Subtract, this, "Operation");
            _FieldOperation = new GenericSetting<OperationType?>((OperationType?)null, this, "FieldOperation");

            ConsolidateFromDevice = fromDevice;

            XmlElement temperature = GetElement("usetemperature");
            if (temperature != null)
            {
                if (temperature.GetAttribute("value") == "true")
                {
                    ConsolidateFromField = FieldSettings.FindFieldByName(ConsolidateFromFields, "Temperature");
                    ConsolidateToField = FieldSettings.FindFieldByName(ConsolidateToFields, "TemperaturePVO");
                    FieldOperation = OperationType.Replace;
                }
                DeleteElement("usetemperature");
            }
        }

        public DeviceManagerDeviceSettings ConsolidateFromDevice
        {
            get { return _ConsolidateFromDevice; }
            set
            {
                _ConsolidateFromDevice = value;
                if (value != null && ConsolidateFromFeatureType == FeatureType.Unknown)
                {
                    if (ConsolidateFromFeatures.Count == 1)
                    {
                        ConsolidateFromFeatureType = ConsolidateFromFeatures[0].FeatureType;
                        ConsolidateFromFeatureId = ConsolidateFromFeatures[0].FeatureId;
                    }
                    else
                    {
                        ConsolidateFromFeatureType = FeatureType.Unknown;
                        ConsolidateFromFeatureId = null;
                        foreach (FeatureSettings fs in ConsolidateFromFeatures)
                            if (fs.FeatureType == ConsolidateToFeatureType)
                            {
                                ConsolidateFromFeatureType = ConsolidateToFeatureType;
                                break;
                            }
                    }
                    DoPropertyChanged("ConsolidateFromFeatureType");
                    DoPropertyChanged("ConsolidateFromFeatureId");
                    DoPropertyChanged("ConsolidateFromFeature");
                    DoPropertyChanged("ConsolidateFromFeatures");
                }
            }
        }

        public ObservableCollection<OperationType> Operations
        {
            get
            {
                ObservableCollection<OperationType> list = new ObservableCollection<OperationType>();
                list.Add(OperationType.Add);
                list.Add(OperationType.Subtract);
                list.Add(OperationType.Custom);
                return list;
            }
        }

        public ObservableCollection<OperationType> FieldOperations
        {
            get
            {
                ObservableCollection<OperationType> list = new ObservableCollection<OperationType>();
                list.Add(OperationType.Add);
                list.Add(OperationType.Subtract);
                list.Add(OperationType.Replace);
                list.Add(OperationType.Negate);
                return list;
            }
        }

        public ObservableCollection<FeatureSettings> ConsolidateFromFeatures
        {
            get
            {
                return ConsolidateFromDevice.DeviceSettings.FeatureList;
            }
        }

        public ObservableCollection<FeatureSettings> ConsolidateToFeatures
        {
            get
            {
                if (_ConsolidateToDevice == null)
                    return new ObservableCollection<FeatureSettings>();
                return _ConsolidateToDevice.DeviceSettings.FeatureList;
            }
        }

        public ObservableCollection<FieldSettings> ConsolidateFromFields
        {
            get
            {
                if (ConsolidateFromFeature == null)
                    return new ObservableCollection<FieldSettings>();
                return ConsolidateFromFeature.MappableFields;
            }
        }

        public ObservableCollection<FieldSettings> ConsolidateToFields
        {
            get
            {
                if (ConsolidateToFeature == null)
                    return new ObservableCollection<FieldSettings>();
                return ConsolidateToFeature.MappableFields;
            }
        }

        public FeatureSettings ConsolidateFromFeature
        {
            get
            {
                FeatureType? fromType = ConsolidateFromFeatureType;
                uint? fromId = ConsolidateFromFeatureId;
                if (fromType == null || fromId == null)
                    return null;
                return ConsolidateFromDevice.DeviceSettings.FindFeature(fromType.Value, fromId.Value);
            }
            set
            {
                if (value == null)
                {
                    ConsolidateFromFeatureType = null;
                    ConsolidateFromFeatureId = null;
                }
                else
                {
                    ConsolidateFromFeatureType = value.FeatureType;
                    ConsolidateFromFeatureId = value.FeatureId;
                }
            }
        }

        private GenericSetting<String> _ConsolidateFromFieldName;
        public FieldSettings ConsolidateFromField
        {
            get
            {
                String val = _ConsolidateFromFieldName.Value;
                if (val == "")
                    return null;
                return FieldSettings.FindFieldByName(ConsolidateFromFields, val);
            }
            set
            {
                if (value == null || value.Name == "")
                {
                    _ConsolidateFromFieldName.Value = "";
                    FieldOperation = null;
                }
                else
                {
                    _ConsolidateFromFieldName.Value = value.Name;
                    if (!FieldOperation.HasValue)
                        FieldOperation = OperationType.Replace;
                }
            }
        }

        private GenericSetting<FeatureType?> _ConsolidateFromFeatureType;
        public FeatureType? ConsolidateFromFeatureType
        {
            get { return _ConsolidateFromFeatureType.Value; }
            set 
            {
                FeatureType? old = ConsolidateFromFeatureType;
                _ConsolidateFromFeatureType.Value = value;
                if (old != value)
                {
                    ConsolidateFromFeatureId = null;
                    ConsolidateFromField = null;
                    DoPropertyChanged("ConsolidateFromFeatureId");
                }
            }
        }

        private GenericSetting<uint?> _ConsolidateFromFeatureId;
        public uint? ConsolidateFromFeatureId
        {
            get { return _ConsolidateFromFeatureId.Value; }
            set
            {
                _ConsolidateFromFeatureId.Value = value;
                ConsolidateFromField = null;
            }
        }

        private GenericSetting<String> _DeviceName;
        public DeviceManagerDeviceSettings ConsolidateToDevice
        {
            get
            {
                if (_ConsolidateToDevice != null)
                    return _ConsolidateToDevice;
                string val = _DeviceName.Value;
                _ConsolidateToDevice = ApplicationSettings.GetDeviceByName(val);
                return _ConsolidateToDevice;
            }
            set
            {
                DeviceManagerDeviceSettings old = ConsolidateToDevice;

                if (value != null && value.CheckDeviceRecursion(ConsolidateFromDevice)) // reject change if recursion detected
                    return;
                _DeviceName.Value = value == null ? "" : value.Name;
                    
                _ConsolidateToDevice = value;
                if (old != null)
                    old.ConsolidateFromDevices.Remove(this);
                if (value != null)
                    value.ConsolidateFromDevices.Add(this);

                if (ConsolidateToFeatures.Count == 1)
                {
                    ConsolidateToFeatureType = ConsolidateToFeatures[0].FeatureType;
                    ConsolidateToFeatureId = ConsolidateToFeatures[0].FeatureId;
                }
                else
                {
                    ConsolidateToFeatureType = FeatureType.Unknown;
                    ConsolidateToFeatureId = 0;
                    foreach (FeatureSettings fs in ConsolidateToFeatures)
                        if (fs.FeatureType == ConsolidateFromFeatureType)
                        {
                            ConsolidateToFeatureType = ConsolidateFromFeatureType.Value;
                            break;
                        }
                }
                //DoPropertyChanged("ConsolidateFromFeatures");
                DoPropertyChanged("ConsolidateToFeatureType");
                DoPropertyChanged("ConsolidateToFeatureId");
                DoPropertyChanged("ConsolidateToFeature");
                DoPropertyChanged("ConsolidateToFeatures");
            }
        }

        public FeatureSettings ConsolidateToFeature
        {
            get
            {
                if (ConsolidateToDevice == null)
                    return null;
                FeatureType fromType = ConsolidateToFeatureType;
                uint? fromId = ConsolidateToFeatureId;
                if (fromType == FeatureType.Unknown || fromId == null)
                    return null;
                return ConsolidateToDevice.DeviceSettings.FindFeature(fromType, fromId.Value);
            }
            set
            {
                if (value == null)
                {
                    ConsolidateToFeatureType = FeatureType.Unknown;
                    ConsolidateToFeatureId = 0;
                }
                else
                {
                    ConsolidateToFeatureType = value.FeatureType;
                    ConsolidateToFeatureId = value.FeatureId;
                }
            }
        }

        private GenericSetting<String> _ConsolidateToFieldName;
        public FieldSettings ConsolidateToField
        {
            get
            {
                String val = _ConsolidateToFieldName.Value;
                if (val == "")
                    return null;
                return FieldSettings.FindFieldByName(ConsolidateToFields, val);
            }
            set
            {
                if (value == null || value.Name == "")
                {
                    _ConsolidateToFieldName.Value = "";
                    FieldOperation = null;
                }
                else
                {
                    _ConsolidateToFieldName.Value = value.Name;
                    if (!FieldOperation.HasValue)
                        FieldOperation = OperationType.Replace;
                }
            }
        }

        private GenericSetting<FeatureType> _ConsolidateToFeatureType;
        public FeatureType ConsolidateToFeatureType
        {
            get { return _ConsolidateToFeatureType.Value; }
            set
            {
                FeatureType old = ConsolidateToFeatureType;
                _ConsolidateToFeatureType.Value = value;
                if (old != value)
                {
                    ConsolidateToFeatureId = 0;
                    ConsolidateToField = null;
                    DoPropertyChanged("ConsolidateToFeatureId");
                }
            }
        }

        private GenericSetting<uint> _ConsolidateToFeatureId;
        public uint ConsolidateToFeatureId
        {
            get { return _ConsolidateToFeatureId.Value; }
            set
            {
                _ConsolidateToFeatureId.Value = value;
                ConsolidateToField = null;
            }
        }

        public void RefreshDeviceReference()
        {
            DeviceManagerDeviceSettings dev = ConsolidateToDevice;
            if (dev == null)
                SetValue("devicename", "", "DeviceName");
            else
                SetValue("devicename", dev.Name, "DeviceName");
        }

        public String DeviceName
        {
            get { return _DeviceName.Value; }
        }

        private GenericSetting<OperationType> _Operation;
        public OperationType Operation
        {
            get{ return _Operation.Value; }
            set { SetValue("operation", value.ToString(), "Operation"); }
        }

        private GenericSetting<OperationType?> _FieldOperation;
        public OperationType? FieldOperation
        {
            get { return _FieldOperation.Value; }
            set{ _FieldOperation.Value = value; }
        }
    }
}
