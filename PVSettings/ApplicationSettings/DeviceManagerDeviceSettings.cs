﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Xml;

namespace PVSettings
{
    public enum ConsolidationType
    {
        PVOutput,
        Generic,
        EnumCount
    }

    public enum EventType
    {
        Yield,
        Consumption,
        Energy,
        _TypeCount    // This marker must be last
    }

    public enum SettingsUsage
    {
        Hidden = 0,
        Disabled,
        Enabled
    }

    public class DeviceManagerDeviceSettings : PVSettings.SettingsBase, MackayFisher.Utilities.INamedItem
    {
        private ApplicationSettings ApplicationSettings;
        private DeviceSettings _DeviceSettings = null;

        private DeviceManagerSettings _DeviceManagerSettings;

        public SettingsCollection<ConsolidateDeviceSettings, DeviceManagerDeviceSettings> consolidateToDevices = null;
        public ObservableCollection<ConsolidateDeviceSettings> consolidateFromDevices = null;
        public SettingsCollection<DeviceEventSettings, DeviceManagerDeviceSettings> deviceEvents = null;
        public SettingsCollection<DeviceFeatureSettings, DeviceManagerDeviceSettings> deviceFeatures = null;

        public bool IsRealDevice { get { return DeviceType != DeviceType.Consolidation; } }

        public bool HasSettingsUsage { get { return DeviceSettings.HasSettingsUsage; } }
        public SettingsUsage UseConsolidationType { get { return DeviceSettings.UseConsolidationType; } }
        public SettingsUsage UsePVOutputSystem { get { return DeviceSettings.UsePVOutputSystem; } }
        public SettingsUsage UseAddress { get { return DeviceSettings.UseAddress; } }
        public SettingsUsage UseSerialNo { get { return DeviceSettings.UseSerialNo; } }
        public SettingsUsage UseMake { get { return DeviceSettings.UseMake; } }
        public SettingsUsage UseModel { get { return DeviceSettings.UseModel; } }
        public SettingsUsage UseQueryInterval { get { return DeviceSettings.UseQueryInterval; } }
        public SettingsUsage UseDBInterval { get { return DeviceSettings.UseDBInterval; } }
        public SettingsUsage UseCalibrate { get { return DeviceSettings.UseCalibrate; } }
        public SettingsUsage UseThreshold { get { return DeviceSettings.UseThreshold; } }
        public SettingsUsage UseHistoryAdjust { get { return DeviceSettings.UseHistoryAdjust; } }

        public DeviceManagerDeviceSettings(SettingsBase root, XmlElement element, DeviceManagerSettings deviceManagerSettings, String id = null)
            : base(root, element)
        {
            ApplicationSettings = (ApplicationSettings)root;
            _DeviceManagerSettings = deviceManagerSettings;
            RemoveOldElements();
                       
            LoadDetails(id);

            string name = Name;
            HasAutoName = (name == null || name == "" || name == DeviceSettings.NamesNameOfId(Id));
        }

        private void LoadDetails(String idNew)
        {
            {
                GenericSetting<String>.GetDefaultValue myDefault = () =>
                {
                    return GetAutoName();
                };
                _Name = new GenericSetting<String>(myDefault, this, "Name", false);
            }

            {
                GenericSetting<UInt64>.GetDefaultValue myDefault = () =>
                {
                    UInt64 defaultAddress = DeviceSettings.Address;
                    bool changed;
                    do
                    {
                        changed = false;
                        foreach (DeviceManagerDeviceSettings d in DeviceManagerSettings.DeviceListCollection)
                        {
                            if (d == this)
                                continue;
                            if (d.Address == defaultAddress)
                            {
                                defaultAddress++;
                                changed = true;
                                break;
                            }
                        }
                    } while (changed);
                    return defaultAddress;
                };
                _Address = new GenericSetting<UInt64>(myDefault, this, "Address", false);
            }

            {
                GenericSetting<string>.GetDefaultValue myDefault = () =>
                {
                    if (DeviceSettings.UseSerialNo != SettingsUsage.Enabled)
                        return "";
                    uint id = 1;
                    String serialNo = "AutoSerialNo_" + id;
                    bool changed;
                    do
                    {
                        changed = false;
                        foreach (DeviceManagerDeviceSettings d in ApplicationSettings.AllDevicesList)
                        {
                            if (d == this || d.DeviceType == PVSettings.DeviceType.Consolidation)
                                continue;
                            
                            if (d.SerialNo == serialNo)
                            {
                                changed = true;
                                id++;
                                serialNo = "AutoSerialNo_" + id;
                            }
                        }

                    } while (changed);
                    return serialNo;
                };
                _SerialNo = new GenericSetting<string>(myDefault, this, "SerialNo", false);
            }

            _ConsolidationType = new GenericSetting<PVSettings.ConsolidationType?>(PVSettings.ConsolidationType.PVOutput, this, "ConsolidationType");
            _PVOutputSystem = new GenericSetting<string>("", this, "PVOutputSystem");

            {
                _Id = new GenericSetting<string>("", this, "Id");
                if (idNew != null)
                    Id = idNew;
            }

            _CalibrationFactor = new GenericSetting<float>(1.0F, this, "CalibrationFactor");
            _ZeroThreshold = new GenericSetting<uint>(0, this, "ZeroThreshold");
            _Manufacturer = new GenericSetting<string>(DeviceSettings.Manufacturer, this, "Manufacturer");
            _Model = new GenericSetting<string>(DeviceSettings.Model, this, "Model");

            {
                GenericSetting<string>.GetDefaultValue myDefault = () =>
                {
                    string val = DeviceManagerSettings.MessageInterval;
                    if (val == "")
                        if (DeviceManagerSettings.ManagerType == DeviceManagerType.SMA_SunnyExplorer || DeviceManagerSettings.ManagerType == DeviceManagerType.SMA_WebBox)
                            return "300";
                        else
                            return "15";
                    return val;
                };
                _QueryInterval = new GenericSetting<string>(myDefault, this, "QueryInterval");
            }

            {
                GenericSetting<string>.GetDefaultValue myDefault = () =>
                {
                    string val = DeviceManagerSettings.DBInterval;
                    if (val == "")
                        if (DeviceManagerSettings.ManagerType == DeviceManagerType.SMA_SunnyExplorer || DeviceManagerSettings.ManagerType == DeviceManagerType.SMA_WebBox)
                            return "300";
                        else
                            return "60";
                    return val;
                };
                _DBInterval = new GenericSetting<string>(myDefault, this, "DBInterval");
            }

            {
                GenericSetting<bool>.GetDefaultValue myDefault = () =>
                {
                    string val = GetValue("enable");  // check legacy value
                    DeleteElement("enable");
                    return val == "true";
                };
                _Enabled = new GenericSetting<bool>(myDefault, this, "Enabled", false);
            }

            _AutoEvents = new GenericSetting<bool>(false, this, "AutoEvents");
            _UpdateHistory = new GenericSetting<bool>(false, this, "UpdateHistory");

            {
                GenericSetting<DateTime?>.GetDefaultValue myDefault = () =>
                {
                    return DeviceManagerSettings.FirstFullDay;
                };
                _FirstFullDay = new GenericSetting<DateTime?>(myDefault, this, "FirstFullDay");
            }

            _ResetFirstFullDaySet = new GenericSetting<DateTime?>((DateTime?)null, this, "ResetFirstFullDaySet");
            _ResetFirstFullDay = new GenericSetting<bool>(false, this, "ResetFirstFullDay");

            consolidateToDevices = new SettingsCollection<ConsolidateDeviceSettings, DeviceManagerDeviceSettings>(this, "consolidatetodevices", "device",
                (SettingsBase root, XmlElement e, DeviceManagerDeviceSettings dmds, String id) => new ConsolidateDeviceSettings(root, e, dmds));
            consolidateFromDevices = new ObservableCollection<ConsolidateDeviceSettings>();

            RegisterEvents();
            RegisterFeatures();
        }

        public DeviceFeatureSettings FindFeature(FeatureType featureType, uint featureId)
        {
            foreach (DeviceFeatureSettings dfs in deviceFeatures.Collection) // detect missing features
            {
                if (dfs.FeatureType == featureType && dfs.FeatureId == featureId)
                {
                    return dfs;
                }
            }
            return null;
        }

        public void RegisterFeatures()
        {
            deviceFeatures = new SettingsCollection<DeviceFeatureSettings, DeviceManagerDeviceSettings>(this, "devicefeatures", "feature",
                (SettingsBase root, XmlElement e, DeviceManagerDeviceSettings dmds, String id) => new DeviceFeatureSettings(root, e, dmds));

            List<DeviceFeatureSettings> foundSettings = new List<DeviceFeatureSettings>();
            foreach (FeatureSettings fs in DeviceSettings.FeatureList)
            {
                bool found = false;
                foreach (DeviceFeatureSettings dfs in deviceFeatures.Collection) // detect missing features
                {
                    if (dfs.FeatureType == fs.FeatureType && dfs.FeatureId == fs.FeatureId)
                    {
                        found = true;
                        foundSettings.Add(dfs);
                        break;
                    }
                }
                if (!found) // add missing features
                {                   
                    DeviceFeatureSettings featureSettings = deviceFeatures.Add();
                    featureSettings.Feature = fs;
                    foundSettings.Add(featureSettings);
                }
            }

            List<DeviceFeatureSettings> notFoundSettings = new List<DeviceFeatureSettings>();
            foreach (DeviceFeatureSettings dfs in deviceFeatures.Collection) 
            {
                if (foundSettings.Contains(dfs))
                    continue;
                notFoundSettings.Add(dfs);
            }

            foreach (DeviceFeatureSettings dfs in notFoundSettings)
                DeviceFeatures.Remove(dfs);
        }

        public void RegisterEvents()
        {
            deviceEvents = new SettingsCollection<DeviceEventSettings, DeviceManagerDeviceSettings>(this, "deviceevents", "event", 
                (SettingsBase root, XmlElement e, DeviceManagerDeviceSettings dmds, String id) => new DeviceEventSettings(root, e, dmds));
        }

        public override void UnRegisterSettings()
        {
            while (consolidateToDevices.Collection.Count > 0)
                DeleteDeviceConsolidation(consolidateToDevices.Collection[0]);

            while (consolidateFromDevices.Count > 0)
            {
                ConsolidateDeviceSettings consolidation = consolidateFromDevices[0];
                if (consolidation.ConsolidateFromDevice != null)
                    consolidation.ConsolidateFromDevice.DeleteDeviceConsolidation(consolidation);
                consolidateFromDevices.Remove(consolidation);  // should not be needed but to protect from broken links
            }
            ApplicationSettings.AllDevicesList.Remove(this);
            ApplicationSettings.AllConsolidationDevicesList.Remove(this);
        }

        public void RegisterConsolidations()
        {
            foreach (ConsolidateDeviceSettings consolidation in consolidateToDevices.Collection)
            {
                DeviceManagerDeviceSettings toDevice = consolidation.ConsolidateToDevice;
                if (toDevice != null)
                    toDevice.ConsolidateFromDevices.Add(consolidation);
            }
        }

        private void RemoveOldElements()
        {
            DeleteElement("description");
        }

        public DeviceSettings DeviceSettings
        {
            get
            {
                if (_DeviceSettings == null)
                {
                    _DeviceSettings = ApplicationSettings.DeviceManagementSettings.GetDevice(Id);
                    if (_DeviceSettings != null && Id != _DeviceSettings.Id && !_DeviceSettings.NamesContains(Id))
                        Id = _DeviceSettings.Id;
                }
                return _DeviceSettings;
            }
        }

        public SettingsCollection<ConsolidateDeviceSettings, DeviceManagerDeviceSettings> ConsolidateToDevices { get { return consolidateToDevices; } }
        public ObservableCollection<ConsolidateDeviceSettings> ConsolidateToDevicesCollection { get { return consolidateToDevices.Collection; } }
        public ObservableCollection<ConsolidateDeviceSettings> ConsolidateFromDevices { get { return consolidateFromDevices; } }

        public SettingsCollection<DeviceEventSettings, DeviceManagerDeviceSettings> DeviceEvents { get { return deviceEvents; } }
        public ObservableCollection<DeviceEventSettings> DeviceEventsCollection { get { return deviceEvents.Collection; } }

        public SettingsCollection<DeviceFeatureSettings, DeviceManagerDeviceSettings> DeviceFeatures { get { return deviceFeatures; } }
        public ObservableCollection<DeviceFeatureSettings> DeviceFeaturesCollection { get { return deviceFeatures.Collection; } }

        public bool RecordFeature(FeatureType featureType, uint featureId)
        {
            foreach (DeviceFeatureSettings fs in DeviceFeatures.Collection)
                if (fs.FeatureType == featureType && fs.FeatureId == featureId)
                    return fs.SaveFeature;
            return false;
        }

        internal bool CheckDeviceRecursion(DeviceManagerDeviceSettings startDevice)
        {
            if (startDevice == null)
                return false;
            if (startDevice.Name == Name)    
                return true;

            foreach (ConsolidateDeviceSettings consolSettings in ConsolidateToDevicesCollection)
            {
                DeviceManagerDeviceSettings nextDevice = consolSettings.ConsolidateToDevice;
                if (nextDevice != null && nextDevice.CheckDeviceRecursion(startDevice))
                    return true;
            }

            return false;
        }

        public DeviceEventSettings AddEvent()
        {
            DeviceEventSettings newEvent = deviceEvents.Add();

            DoPropertyChanged("DeviceEvents");
            return newEvent;
        }

        public void DeleteEvent(DeviceEventSettings e)
        {
            deviceEvents.Remove(e);                         
            DoPropertyChanged("DeviceEvents");
        }

        public ConsolidateDeviceSettings AddDeviceConsolidation(ConsolidateDeviceSettings.OperationType operation)
        {
            ConsolidateDeviceSettings newConsol = consolidateToDevices.Add();
            newConsol.ConsolidateToDevice = null;
            newConsol.Operation = operation;
            
            DoPropertyChanged("ConsolidateToDevicesCollection");
            DoPropertyChanged("ConsolidateFromDevices");

            return newConsol;
        }
      

        public void DeleteDeviceConsolidation(ConsolidateDeviceSettings consol)
        {            
            if (consol.ConsolidateToDevice != null)
                consol.ConsolidateToDevice.ConsolidateFromDevices.Remove(consol);
            consolidateToDevices.Remove(consol);

            DoPropertyChanged("ConsolidateToDevicesCollection");
            DoPropertyChanged("ConsolidateFromDevices");
        }

        public void RefreshAfterDefaultChange()
        {
            DoPropertyChanged("QueryInterval");
            DoPropertyChanged("DBInterval");
        }

        public DeviceManagerSettings DeviceManagerSettings
        {
            get { return _DeviceManagerSettings; }
        }

        private bool HasAutoName = false;

        private String GetBaseName()
        {
            return DeviceSettings.NamesNameOfId(Id);
        }

        private int AutoNameCount = 0;
        public String GetAutoName()
        {
            String baseName = GetBaseName();
            
            DeviceEnumerator devices = new DeviceEnumerator(ApplicationSettings.DeviceManagerListCollection);
            String newName = MackayFisher.Utilities.UniqueNameResolver<DeviceManagerDeviceSettings>.ResolveUniqueName(
                baseName, devices, this);
            AutoNameCount++;
            if (AutoNameCount < 2)
                _Name.SetValue(newName, true);
            AutoNameCount--;
            HasAutoName = true;
            return newName;
        }

        private GenericSetting<String> _Name;
        public String Name
        {
            get { return _Name.Value; }            
            set
            {
                HasAutoName = (value == null || value == "" || value == DeviceSettings.NamesNameOfId(Id));
                DeviceEnumerator devices = new DeviceEnumerator(ApplicationSettings.DeviceManagerListCollection);
                _Name.Value = MackayFisher.Utilities.UniqueNameResolver<DeviceManagerDeviceSettings>.ResolveUniqueName(value, devices, this);               
                // the following dorces the name change into the ConsolidateDeviceSettings entries by refreshing the device reference
                foreach (ConsolidateDeviceSettings consol in ConsolidateFromDevices)
                    consol.RefreshDeviceReference();
            }
        }

        public override bool IsThisMyElement(XmlElement newElem)
        {
            string eName = _Name.ElementName;
            XmlElement id = GetElementByName(newElem, eName);
            try
            {
                String tmp = id.Attributes["value"].Value;
                return GetValue(eName) == tmp;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public String UniqueName { get { return Name; } }

        public ObservableCollection<PVSettings.ConsolidationType> AllConsolidationTypes
        {
            get
            {
                ObservableCollection<PVSettings.ConsolidationType> col = new ObservableCollection<ConsolidationType>();
                for (int i = 0; i < (int)PVSettings.ConsolidationType.EnumCount; i++)
                    col.Add((ConsolidationType)i);
                return col;
            }
        }

        public DeviceType DeviceType
        {
            get
            {
                return DeviceSettings.DeviceType;
            }
        }

        private GenericSetting<ConsolidationType?> _ConsolidationType;
        public ConsolidationType? ConsolidationType
        {
            get
            {
                if (DeviceType != PVSettings.DeviceType.Consolidation)
                    return null;
                return _ConsolidationType.Value;
            }
            set
            {
                _ConsolidationType.Value = value;
            }
        }

        public ObservableCollection<PvOutputSiteSettings> PVOutputSystemList 
        { 
            get { return ApplicationSettings.PvOutputSystemListCollection; }
            set { }
        }

        private GenericSetting<String> _PVOutputSystem;
        public String PVOutputSystem
        {
            get { return _PVOutputSystem.Value; }
            set
            {
                _PVOutputSystem.Value = value;
                DoPropertyChanged("PVOutputSystemList");
            }
        }

        private GenericSetting<String> _Id;
        public String Id
        {
            get
            {
                string val = _Id.Value;
                // correct early xml device identity issue
                if (val == "EnergyMeter: DSM-Device-Gas - 01")
                {
                    val = "GasMeter: DSM-Device-Gas - 01";
                    _Id.SetValue(val, true);                    
                }
                return val;
            }
            
            set
            {
                _DeviceSettings = null;  // clear cached reference to device type details                
                _Id.Value = value;
                if (HasAutoName)
                    Name = "";

                _Address.ClearCachedValue();
                _SerialNo.ClearCachedValue();
                DoPropertyChanged("Address");
                DoPropertyChanged("SerialNo");

                RegisterFeatures();
                DoPropertyChanged("DeviceFeatures");
                RegisterEvents();
                DoPropertyChanged("DeviceEvents");
            }             
        }

        private GenericSetting<String> _SerialNo;
        public String SerialNo
        {
            get { return _SerialNo.Value; }
            set { _SerialNo.Value = value;}
        }

        private GenericSetting<UInt64> _Address;
        public UInt64 Address
        {
            get { return _Address.Value; }
            set { _Address.Value = value;  }
        }

        private GenericSetting<float> _CalibrationFactor;
        public float CalibrationFactor
        {
            get { return _CalibrationFactor.Value; }
            set { _CalibrationFactor.Value = value; }
        }

        private GenericSetting<uint> _ZeroThreshold;
        public uint ZeroThreshold
        {
            get { return _ZeroThreshold.Value; }
            set { _ZeroThreshold.Value = value; }
        }

        private GenericSetting<String> _Manufacturer;
        public String Manufacturer
        {
            get { return _Manufacturer.Value.Trim(); }
            set { _Manufacturer.Value = value.Trim(); }
        }

        private GenericSetting<String> _Model;
        public String Model
        {
            get { return _Model.Value.Trim(); }
            set { _Model.Value = value.Trim(); }
        }

        private GenericSetting<String> _QueryInterval;
        public String QueryInterval
        {
            get { return _QueryInterval.Value; }
            set 
            {
                _QueryInterval.Value = value;
                if (DBIntervalInt < QueryIntervalInt)
                    QueryInterval = DBInterval;
                else if ((DBIntervalInt / QueryIntervalInt) * QueryIntervalInt != DBIntervalInt)
                    QueryInterval = DBInterval;
            }
        }

        public UInt16 QueryIntervalInt
        {
            get
            {
                String val = _QueryInterval.Value;
                if (val == "")                   
                    return DeviceManagerSettings.MessageIntervalInt;
                else
                    return UInt16.Parse(val); ;
            }
        }

        private GenericSetting<String> _DBInterval;
        public String DBInterval
        {
            get { return _DBInterval.Value; }
            set
            {
                _DBInterval.Value = value;
                if (DBIntervalInt < QueryIntervalInt)
                    QueryInterval = DBInterval;
                else if ((DBIntervalInt / QueryIntervalInt) * QueryIntervalInt != DBIntervalInt)
                    QueryInterval = DBInterval;
            }
        }

        public UInt16 DBIntervalInt
        {
            get
            {
                String val = _DBInterval.Value;
                if (val == "")
                    return DeviceManagerSettings.DBIntervalInt;
                else
                    return UInt16.Parse(val); ;
            }
        }

        private GenericSetting<bool> _Enabled;
        public bool Enabled
        {
            get
            {
                if (DeviceType == PVSettings.DeviceType.Consolidation)
                    return false;
                return _Enabled.Value;
            }

            set
            {
                if (DeviceType == PVSettings.DeviceType.Consolidation)
                    value = false;
                else if (Id == "")
                    value = false;
                _Enabled.Value = value;
            }
        }

        public bool CheckAdjustAutoEvents()
        {
            int hasYield = 0;
            int hasConsume = 0;

            // detect candidate features on this device
            foreach (FeatureSettings f in DeviceSettings.FeatureList)
            {
                if (f.FeatureType == FeatureType.YieldAC) 
                    hasYield++;
                else if (f.FeatureType == FeatureType.ConsumptionAC) 
                    hasConsume++;
            }

            // a suitable candidate has 1 yield and / or 1 consumption candidate feature
            if (hasYield == 1 && hasConsume <= 1 || hasConsume == 1 && hasYield <= 1)
            {
                // delete existing events on this device
                while (deviceEvents.Collection.Count > 0)
                    DeleteEvent(deviceEvents.Collection[0]);

                // create events for the appropriate features
                foreach (FeatureSettings f in DeviceSettings.FeatureList)
                {
                    if (f.FeatureType == FeatureType.YieldAC)
                    {
                        DeviceEventSettings e = AddEvent();
                        e.EventFeature = f;
                        e.EventType = EventType.Yield;
                        e.EventName = "Inverter Yield";
                        e.UseForFeedIn = true;
                    }
                    else if (f.FeatureType == FeatureType.ConsumptionAC)
                    {
                        DeviceEventSettings e = AddEvent();
                        e.EventFeature = f;
                        e.EventType = EventType.Consumption;
                        e.EventName = "Consumption";
                        e.UseForFeedIn = true;
                    }
                }

                return true; // confirm that auto events for this device are configured
            }
            else
                return false;  // not suitable for auto events
        }

        private GenericSetting<bool> _AutoEvents;
        public bool AutoEvents
        {
            get { return _AutoEvents.Value; }
            set
            {
                if (value)
                    value = CheckAdjustAutoEvents();
                _AutoEvents.Value = value;
                DoPropertyChanged("ManualEvents");
            }
        }

        public bool ManualEvents { get { return !AutoEvents; } }

        private GenericSetting<bool> _UpdateHistory;
        public bool UpdateHistory
        {
            get { return _UpdateHistory.Value; }
            set { _UpdateHistory.Value = value; }
        }

        private GenericSetting<DateTime?> _FirstFullDay;
        public DateTime? FirstFullDay
        {
            get { return _FirstFullDay.Value; }
            set { _FirstFullDay.Value = value; }
        }

        private GenericSetting<DateTime?> _ResetFirstFullDaySet;
        public DateTime? ResetFirstFullDaySet
        {
            get { return _ResetFirstFullDaySet.Value; }
            set { _ResetFirstFullDaySet.Value = value; }
        }

        private GenericSetting<bool> _ResetFirstFullDay;
        public bool ResetFirstFullDay
        {
            get 
            { 
                return _ResetFirstFullDay.Value 
                    && ResetFirstFullDaySet >= (DateTime.Now - TimeSpan.FromMinutes(10.0)); 
            }
            set
            {
                _ResetFirstFullDay.Value = value;
                ResetFirstFullDaySet = value ? DateTime.Now : (DateTime?)null;                
            }
        }

        public ObservableCollection<DeviceListItem> DeviceListItems
        {
            get
            {
                return DeviceManagerSettings.DeviceListItems;
            }
        }

        public bool DeviceManagerSelected
        {
            get { return DeviceManagerSettings.IsSelected; }
        }

        public void NotifySelectionChange()
        {
            DoPropertyChanged("DeviceManagerSelected");
            DoPropertyChanged("DeviceListItems");
        }
    }
}
