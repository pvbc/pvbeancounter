﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PVSettings;
using MackayFisher.Utilities;
using Conversations;
using PVBCInterfaces;
using Algorithms;
using GenericConnector;
using DeviceDataRecorders;

namespace Device
{
    public struct DeviceIdentity
    {
        // require DeviceID or Make, Model and SerialNo
        public int? DeviceId;
        public String Make;       // inverter manufacturer
        public String Model;      // inverter model
        public String SerialNo; // inverter unique id - could be serial number if available

        public DeviceIdentity(String make, String model, String serialNo, int? deviceId = null)
        {
            DeviceId = deviceId;
            Make = make;
            Model = model;
            SerialNo = serialNo;
        }
    }

    public enum FieldType
    {
        AlwaysAccumulate,   // fields like an energy delta must always be accumulated
        AccumulateLast,     // instantaneous values such as power are accumulated using the last value in an interval only
        Replace             // instantaneous values such as temperature never accumulate, always replace
    }

    public struct OutputReadyNotification
    {
        public FeatureType FeatureType;
        public uint FeatureId;
        public DateTime ReadingEnd;
    }

    public class DeviceNotAvailableException : Exception
    {
        public DeviceNotAvailableException(String detail) : base(detail)
        {
        }
    }

    public abstract class DeviceBase 
    {
        public PeriodType PeriodType { get; protected set; }
        public TimeSpan PeriodOffset { get; protected set; }

        public DeviceControl.DeviceManagerBase DeviceManager { get; protected set; }
        public DeviceManagerDeviceSettings DeviceManagerDeviceSettings;
        public DeviceSettings DeviceSettings;
        
        public int? DeviceId { get; protected set; }
        public String Manufacturer { get; set; }
        public String Model { get; set; }

        public abstract DateTime NextRunTime { get; }

        public String SerialNo { get; set; }  // used for serial number on real devices; 
                                              // generic identifier for consolidation devices

        private static int ConsolidationCount = 0;  // Used to create unique DeviceIdentifiers for each Consolidation Device
      
        public DeviceType DeviceType { get { return DeviceParams.DeviceType; } }
        public int? DeviceType_Id { get; private set; }

        public int DatabaseInterval { get; protected set; }

        public bool ResetFirstFullDay { get; set; }
        public DateTime? NextFileDate { get; set; }

        public DateTime? LastRecordTime { get; protected set; }
        public DateTime LastRunTime { get; set; }

        protected bool EmitEvents;

        public DeviceDataRecorders.DeviceParamsBase DeviceParams;  

        public List<DeviceLink> TargetDevices;

        private List<FeaturePeriods> FeaturePeriodsList;

        public List<EnergyEventStatus> EventStatusList;

        public bool Enabled { get; private set; }

        public int HierarchyDepth { get; protected set; }

        public virtual void NotifyPortReset() { }

        public abstract int GetMappableFieldIndex(FeatureType featureType, string name, out FieldType fieldType);

        public DeviceBase(DeviceControl.DeviceManagerBase deviceManager, DeviceManagerDeviceSettings deviceSettings)
        {
            PeriodType = PeriodType.Day; // default
            PeriodOffset = TimeSpan.FromTicks(0); // default

            HierarchyDepth = 0; // Default is correct for a real device

            DeviceParams = new DeviceDataRecorders.DeviceParamsBase();
            DeviceType_Id = null;
            DeviceId = null;

            NextFileDate = null;
            ResetFirstFullDay = false;

            LastRecordTime = null;
            LastRunTime = DateTime.MinValue;

            EmitEvents = GlobalSettings.ApplicationSettings.EmitEvents;
            TargetDevices = new List<DeviceLink>();
            FeaturePeriodsList = new List<FeaturePeriods>();

            DeviceManager = deviceManager;
            DeviceManagerDeviceSettings = deviceSettings;
            DeviceSettings = deviceSettings.DeviceSettings;
            Enabled = deviceSettings.Enabled;

            BuildFeatureEvents();
        }

        public void NotifyUpdatedPeriod(DateTime readingEnd, DateTime readingStart, int depth = 0)
        {
            if (depth > 50)
                throw new Exception("NotifiyUpdatedPeriod - Recursion depth over 50");
            DateTime firstPeriod = DeviceDetailPeriodBase.GetPeriodStart(PeriodType, PeriodOffset, readingStart, false);
            DateTime lastPeriod = DeviceDetailPeriodBase.GetPeriodStart(PeriodType, PeriodOffset, readingEnd);
            foreach (Device.DeviceLink link in TargetDevices)
            {
                for (DateTime period = firstPeriod; period <= lastPeriod; period = DeviceDetailPeriodBase.CalcRelativeDateTime(PeriodType, period, 1))
                    if (!link.UpdatedPeriods.Contains(period))
                    {
                        bool haveMutex = false;
                        try
                        {
                            link.ToDevice.ConsolidationLinksMutex.WaitOne();
                            haveMutex = true;
                            link.UpdatedPeriods.Add(period);
                        }
                        finally
                        {
                            if (haveMutex)
                                link.ToDevice.ConsolidationLinksMutex.ReleaseMutex();
                        }
                    }
                link.ToDevice.NotifyUpdatedPeriod(readingEnd, readingStart, depth + 1);
            }
        }

        private void BuildFeatureEvents()
        {
            EventStatusList = new List<EnergyEventStatus>();
            foreach (FeatureSettings fs in DeviceSettings.FeatureList)
            {
                EnergyEventStatus status = new EnergyEventStatus(this, fs.FeatureType, fs.FeatureId, DeviceManagerDeviceSettings.QueryIntervalInt);
                EventStatusList.Add(status);
            }
        }

        protected void SetAllFeatureEventStatus(DateTime readingTime, TimeSpan readingDuration)
        {
            bool found = false;
            foreach (EnergyEventStatus status in EventStatusList)
                found |= SetOneFeatureEventStatus(status, readingTime, readingDuration);
            if (found)
                DeviceManager.ManagerManager.ScanForEvents();
        }

        protected virtual bool SetOneFeatureEventStatus(EnergyEventStatus status, DateTime readingTime, TimeSpan readingDuration)
        {
            LogMessage("Device.SetOneFeatureEventStatus - Device implementation called - Missing an override", LogEntryType.ErrorMessage);
            throw new NotImplementedException();
        }

        public EnergyEventStatus FindFeatureEventStatus(FeatureType featureType, uint featureId)
        {
            foreach (EnergyEventStatus status in EventStatusList)
                if (featureType == status.FeatureType && featureId == status.FeatureId)
                    return status;
            LogMessage("FindFeatureEventStatus - Cannot locate Feature: " + featureType + " - Id: " + featureId, LogEntryType.ErrorMessage);
            return null;
        }

        public static int IntervalCompare(int intervalSeconds, DateTime time1, DateTime time2)
        {
            if (time1.Date < time2.Date)
                return -1;

            if (time1.Date > time2.Date)
                return 1;

            TimeSpan diff = time1 - time2;
            int interval1 = (int)(time1.TimeOfDay.TotalSeconds / intervalSeconds);
            int interval2 = (int)(time2.TimeOfDay.TotalSeconds / intervalSeconds);

            if (interval1 < interval2)
                return -1;
            if (interval1 > interval2)
                return 1;

            return 0;
        }

        private List<ConsolidateDeviceSettings> GetUniqueConsolidationList()
        {
            List<ConsolidateDeviceSettings> theList = new List<ConsolidateDeviceSettings>();
            foreach (ConsolidateDeviceSettings cds in DeviceManagerDeviceSettings.ConsolidateToDevicesCollection)
                if (cds.ConsolidateToDevice != null)
                {                   
                    if (!cds.ConsolidateFromFeatureType.HasValue || !cds.ConsolidateFromFeatureId.HasValue)
                        LogMessage("BindConsolidations - Missing Feature info: " + cds.ConsolidateFromDevice.Name, LogEntryType.ErrorMessage);
                    else
                    {
                        bool found = false;
                        foreach (ConsolidateDeviceSettings lcds in theList)
                        {                            
                            if (lcds.ConsolidateToDevice == cds.ConsolidateToDevice
                                && lcds.ConsolidateToFeature == cds.ConsolidateToFeature
                                && lcds.ConsolidateToFeatureId == cds.ConsolidateToFeatureId
                                && lcds.ConsolidateFromFeature == cds.ConsolidateFromFeature
                                && lcds.ConsolidateFromFeatureId == cds.ConsolidateFromFeatureId)
                            {
                                // only one of these allowed
                                if (cds.Operation == ConsolidateDeviceSettings.OperationType.Custom)
                                    cds.Operation = lcds.Operation;  // if Custom and another is found for same features use alternate
                                found = true;
                                break;
                            }                                
                        }
                        if (!found)
                            theList.Add(cds);
                    }
                }
            return theList;
        }

        private void AddFieldMapEntry(List<FieldMapEntry> fieldMap, ConsolidateDeviceSettings cds, ConsolidationDevice toDevice)
        {
            if (cds.ConsolidateToField == null || cds.ConsolidateFromField == null)
            {
                GlobalSettings.LogMessage("AddFieldMapEntry", "Field is null - From: " +
                    cds.ConsolidateFromField == null ? "null" : cds.ConsolidateFromField.Name +
                    " - To: " + cds.ConsolidateToField == null ? "null" : cds.ConsolidateToField.Name, LogEntryType.ErrorMessage);
                return; // bypass if both fields not defined
            }

            FieldType toFieldType;
            int toField = toDevice.GetMappableFieldIndex(cds.ConsolidateToFeature.FeatureType, cds.ConsolidateToField.Name, out toFieldType);
            FieldType fromFieldType;
            int fromField = GetMappableFieldIndex(cds.ConsolidateFromFeature.FeatureType, cds.ConsolidateFromField.Name, out fromFieldType);

            if (toField < 0 || fromField < 0) // bypass if either field not found
            {
                GlobalSettings.LogMessage("AddFieldMapEntry", "Field is null - From: " + cds.ConsolidateFromField.Name + " - Index: " + fromField +
                    " - To: " + cds.ConsolidateToField.Name + " - Index: " + toField, LogEntryType.ErrorMessage);
                return;
            }

            FieldMapEntry entry = new FieldMapEntry(toField, fromField, cds.FieldOperation.Value, fromFieldType);
            fieldMap.Add(entry);
        }

        private List<FieldMapEntry> AnalyseConsolidation(ConsolidateDeviceSettings lcds, ConsolidationDevice toDevice)
        {
            List<FieldMapEntry> fieldMap = new List<FieldMapEntry>();

            foreach (ConsolidateDeviceSettings cds in DeviceManagerDeviceSettings.ConsolidateToDevicesCollection)
            {
                if (lcds.ConsolidateToDevice == cds.ConsolidateToDevice
                && lcds.ConsolidateToFeature == cds.ConsolidateToFeature
                && lcds.ConsolidateToFeatureId == cds.ConsolidateToFeatureId
                && lcds.ConsolidateFromFeature == cds.ConsolidateFromFeature
                && lcds.ConsolidateFromFeatureId == cds.ConsolidateFromFeatureId
                && cds.FieldOperation.HasValue)
                    AddFieldMapEntry(fieldMap, cds, toDevice);
            }

            return fieldMap;
        }

        public void UnbindConsolidations()
        {
            while (TargetDevices.Count > 0)
            {
                TargetDevices[0].ToDevice.RemoveSourceLink(TargetDevices[0]);
            }
        }

        public void BindConsolidations(DeviceControl.IDeviceManagerManager mm)
        {
            List<ConsolidateDeviceSettings> theList = GetUniqueConsolidationList();
            foreach (ConsolidateDeviceSettings lcds in theList)
            {
                ConsolidationDevice d = (ConsolidationDevice)mm.FindDeviceFromSettings(lcds.ConsolidateToDevice);
                if (d == null)
                {
                    LogMessage("BindConsolidations - Cannot find device: " + lcds.ConsolidateToDevice.Name, LogEntryType.ErrorMessage);
                    continue;
                }

                List<FieldMapEntry> fieldMap = AnalyseConsolidation(lcds, d);
              
                DeviceLink link = new DeviceLink(this, lcds.ConsolidateFromFeatureType.Value, lcds.ConsolidateFromFeatureId.Value,
                        d, lcds.ConsolidateToFeatureType, lcds.ConsolidateToFeatureId, lcds.Operation, 
                        FindFeatureEventStatus(lcds.ConsolidateFromFeatureType.Value, lcds.ConsolidateFromFeatureId.Value), fieldMap);

                d.AddSourceDevice(link);                    
            }
        }

        public abstract void SplitReadingSub(ReadingBase oldReading, DateTime splitTime, ReadingBase newReading1, ReadingBase newReading2);
        
        public struct FeaturePeriods
        {
            public FeatureType Type;
            public uint Id;
            public DeviceDetailPeriodsBase Periods;
        }

        public virtual DeviceDetailPeriodBase FindOrCreateFeaturePeriod(FeatureType featureType, uint featureId, DateTime periodStart)
        {
            DeviceDetailPeriodsBase periodsBase = FindOrCreateFeaturePeriods(featureType, featureId);
            return periodsBase.FindOrCreate(periodStart);
        }

        public DeviceDetailPeriodsBase FindOrCreateFeaturePeriods(FeatureType featureType, uint featureId)
        {            
            foreach (FeaturePeriods fd in FeaturePeriodsList)
                if (fd.Type == featureType && fd.Id == featureId)
                    return fd.Periods;
            FeaturePeriods fdNew;
            fdNew.Type = featureType;
            fdNew.Id = featureId;
            FeatureSettings fs = DeviceSettings.GetFeatureSettings(featureType, featureId);
            fdNew.Periods = CreateNewPeriods(fs);
            FeaturePeriodsList.Add(fdNew);
            return fdNew.Periods;
        }

        protected DeviceDetailPeriodsBase CreateNewPeriods(FeatureSettings featureSettings)
        {
            if (featureSettings.MeasureType == MeasureType.Energy)
                if (DeviceType == PVSettings.DeviceType.Consolidation)
                    return new DeviceDetailPeriods_EnergyConsolidation(this, featureSettings);
                else
                    return new DeviceDetailPeriods_EnergyMeter(this, featureSettings);
            else if (DeviceType == PVSettings.DeviceType.Consolidation)
                return new DeviceDetailPeriods_QuantityConsolidation(this, featureSettings);
            else
                return new DeviceDetailPeriods_Quantity(this, featureSettings);
        }

        protected void LogMessage(String message, LogEntryType logEntryType)
        {
            DeviceManager.LogMessage(DeviceManagerDeviceSettings.Name, message, logEntryType);
        }

        public bool FindInTargetList(ConsolidationDevice device)
        {
            foreach (DeviceLink dev in TargetDevices)
                if (dev.ToDevice == device)
                    return true;
                else if (dev.ToDevice.FindInTargetList(device))
                    return true;
            return false;
        }

        public void AddTargetDevice(DeviceLink deviceLink)
        {
            TargetDevices.Add(deviceLink);
        }

        public void RemoveTargetDevice(DeviceLink deviceLink)
        {
            TargetDevices.Remove(deviceLink);
        }

        private int? ReadDeviceTypeId(GenConnection con)
        {
            string sql =
                "select Id " +
                "from devicetype " +
                "where Manufacturer = @Manufacturer " +
                "and Model = @Model ";

            try
            {                
                GenCommand cmd = new GenCommand(sql, con);

                cmd.AddParameterWithValue("@Manufacturer", Manufacturer);
                cmd.AddParameterWithValue("@Model", Model);

                GenDataReader dr = (GenDataReader)cmd.ExecuteReader();

                if (dr.Read())
                {
                    int id = dr.GetInt32(0);
                    dr.Close();
                    dr.Dispose();
                    return id;
                }

                dr.Close();
                dr.Dispose();

                return null;
            }
            catch (Exception e)
            {
                DeviceManager.LogMessage("ReadDeviceId", "Exception: " + e.Message, LogEntryType.ErrorMessage);
            }
            return null;
        }

        private int InsertDeviceTypeId(GenConnection con)
        {
            string sql =
                "insert into devicetype (Manufacturer, Model, DeviceType) " +
                "values (@Manufacturer, @Model, @DeviceType) ";
            
            GenCommand cmd = new GenCommand(sql, con);

            cmd.AddParameterWithValue("@Manufacturer", Manufacturer);
            cmd.AddParameterWithValue("@Model", Model);
            cmd.AddParameterWithValue("@DeviceType", DeviceParams.DeviceType.ToString());

            cmd.ExecuteNonQuery();

            int? id = ReadDeviceTypeId(con);

            if (id.HasValue)
                return id.Value;
            else
                throw new Exception("InsertDeviceTypeId cannot find created Id");           
        }

        private int InsertDeviceId(GenConnection con)
        {
            string sql =
                "insert into device (SerialNumber, DeviceType_Id) " +
                "values (@SerialNumber, @DeviceType_Id) ";
            
            GenCommand cmd = new GenCommand(sql, con);

            cmd.AddParameterWithValue("@SerialNumber", SerialNo);
            cmd.AddParameterWithValue("@DeviceType_Id", DeviceType_Id.Value);

            cmd.ExecuteNonQuery();

            int? id = ReadDeviceId(con);

            if (id.HasValue)
                return id.Value;
            else
                throw new Exception("InsertDeviceId cannot find created Id");            
        }

        private int? ReadDeviceId(GenConnection con)
        {
            if (SerialNo == "")
                return null;

            string sql =
                "select Id " +
                "from device " +
                "where SerialNumber = @SerialNumber " +
                "and DeviceType_Id = @DeviceType_Id ";

            try
            {
                if (!DeviceType_Id.HasValue)
                {
                    DeviceType_Id = ReadDeviceTypeId(con);
                    if (!DeviceType_Id.HasValue)
                        DeviceType_Id = InsertDeviceTypeId(con);
                }

                GenCommand cmd = new GenCommand(sql, con);

                cmd.AddParameterWithValue("@SerialNumber", SerialNo);
                cmd.AddParameterWithValue("@DeviceType_Id", DeviceType_Id);

                GenDataReader dr = (GenDataReader)cmd.ExecuteReader();

                if (dr.Read())
                {
                    int id = dr.GetInt32(0);
                    dr.Close();
                    dr.Dispose();
                    return id;
                }

                dr.Close();
                dr.Dispose();

                return null;
            }
            catch (Exception e)
            {
                DeviceManager.LogMessage("ReadDeviceId", "Exception: " + e.Message, LogEntryType.ErrorMessage);
            }
            
            return null;
        }

        private void CheckDeviceSettings()
        {
            if (Manufacturer == DeviceManagerDeviceSettings.Manufacturer
            && Model == DeviceManagerDeviceSettings.Model
            && SerialNo == DeviceManagerDeviceSettings.SerialNo)
                return;

            DeviceManagerDeviceSettings.Manufacturer = Manufacturer;
            DeviceManagerDeviceSettings.Model = Model;
            DeviceManagerDeviceSettings.SerialNo = SerialNo;

            // must save settings to make device visible in configuration and available for consolidatioon
            GlobalSettings.ApplicationSettings.SaveSettings();
        }

        public int GetDeviceId(GenConnection con, bool autoCreate = true)
        {
            bool localCon = false;
            try
            {
                if (DeviceId.HasValue)
                    return DeviceId.Value;

                if (DeviceSettings.DeviceType == PVSettings.DeviceType.Consolidation)
                {
                    // force obvious identity
                    Manufacturer = "PVBC";
                    Model = "Consolidation";
                    if (SerialNo.Trim() == "")
                    {
                        SerialNo = "Consolidation_" + ConsolidationCount++;
                        DeviceManagerDeviceSettings.SerialNo = SerialNo;
                        // must save settings to make consolidation device serialno visible in configuration
                        GlobalSettings.ApplicationSettings.SaveSettings();
                    }
                    else
                        ConsolidationCount++;
                }

                if (con == null)
                {                    
                    con = GlobalSettings.TheDB.NewConnection();
                    localCon = true;
                    GlobalSettings.SystemServices.GetDatabaseMutex();
                }

                int? id = ReadDeviceId(con);
                if (!id.HasValue)
                    if (autoCreate && SerialNo != "")
                        id = InsertDeviceId(con);
                    else
                        throw new DeviceNotAvailableException("GetDeviceId - DeviceId not available for device: " + DeviceManagerDeviceSettings.Name + " - SerialNo: " + DeviceManagerDeviceSettings.SerialNo);

                DeviceId = id;

                CheckDeviceSettings();

                return id.Value;
            }
            catch (DeviceNotAvailableException e)
            {
                DeviceManager.LogMessage("GetDeviceId", "Exception: " + e.Message, LogEntryType.Trace);
                throw e;
            }
            catch (Exception e)
            {
                DeviceManager.LogMessage("GetDeviceId", "Exception: " + e.Message, LogEntryType.ErrorMessage);
                throw e;
            }
            finally
            {
                if (localCon && con != null)
                {
                    GlobalSettings.SystemServices.ReleaseDatabaseMutex();
                    con.Close();
                    con.Dispose();                    
                }
            }
        }

        public uint? ReadDeviceFeature(GenConnection con, FeatureSettings feature,
            ref String featureType, ref bool? isConsumption, ref bool? isAC, ref bool? isThreePhase,
            ref int? stringNumber, ref int? phaseNumber)
        {
            const string sqlRead =
                "select Id, MeasureType, IsConsumption, IsAC, IsThreePhase, StringNumber, PhaseNumber " +
                "from devicefeature " +
                "where Device_Id = @Device_Id " +
                "and FeatureType = @FeatureType " +
                "and FeatureId = @FeatureId ";

            try
            {
                GenCommand cmd = new GenCommand(sqlRead, con);

                cmd.AddParameterWithValue("@Device_Id", DeviceId.Value);
                cmd.AddParameterWithValue("@FeatureType", (int)feature.FeatureType);
                cmd.AddParameterWithValue("@FeatureId", (int)feature.FeatureId);

                GenDataReader dr = (GenDataReader)cmd.ExecuteReader();

                if (dr.Read())
                {
                    //DeviceDetailPeriodsBase periods = FindOrCreateFeaturePeriods(feature.FeatureType, feature.FeatureId);
                    uint deviceFeatureId = (uint)dr.GetInt32(0);
 
                    featureType = dr.GetString(1);
                    isConsumption = dr.GetBoolFromChar(2);
                    isAC = dr.GetBoolFromChar(3);
                    isThreePhase = dr.GetBoolFromChar(4);
                    stringNumber = dr.GetNullableInt32(5);
                    phaseNumber = dr.GetNullableInt32(6);

                    dr.Close();
                    dr.Dispose();
                    return deviceFeatureId;
                }
                else
                {
                    dr.Close();
                    dr.Dispose();
                    return null;
                }
            }
            catch (Exception e)
            {
                DeviceManager.LogMessage("ReadDeviceFeature", "Exception: " + e.Message, LogEntryType.ErrorMessage);
                throw e;
            }
        }

        private int PreviousDatabaseInterval = -1;
        private int CurrentDatabaseInterval = -1;
        private DateTime PreviousDay = DateTime.MinValue;
        private DateTime CurrentDay = DateTime.MinValue;

        public bool IsNewdatabaseInterval(DateTime checkTime)
        {
            int thisInterval = DeviceDataRecorders.PeriodBase.GetIntervalNo(PeriodType, PeriodOffset, checkTime, DatabaseInterval);
            if (CurrentDatabaseInterval == -1)
            {
                CurrentDatabaseInterval = thisInterval;
                CurrentDay = PeriodBase.GetPeriodStart(PeriodType, PeriodOffset, checkTime, true);
            }
            if (CurrentDay != checkTime.Date || thisInterval != CurrentDatabaseInterval)
            {
                PreviousDay = CurrentDay;
                CurrentDay = PeriodBase.GetPeriodStart(PeriodType, PeriodOffset, checkTime, true);
                PreviousDatabaseInterval = CurrentDatabaseInterval;
                CurrentDatabaseInterval = thisInterval;
                return true;
            }
            return false;            
        }

        protected DateTime? PreviousDatabaseIntervalEnd
        {
            get
            {
                if (PreviousDatabaseInterval == -1)
                    return null;
                
                return PreviousDay + TimeSpan.FromSeconds((PreviousDatabaseInterval + 1) * DatabaseInterval);
            }
        }

        public static void BuildOutputReadyFeatureList(List<OutputReadyNotification> featureList, FeatureType featureType, uint featureId, DateTime readingEnd)
        {
            OutputReadyNotification notify;
            notify.FeatureType = featureType;
            notify.FeatureId = featureId;
            notify.ReadingEnd = readingEnd;
            featureList.Add(notify);
        }

        protected void UpdateConsolidations(List<Device.OutputReadyNotification> notificationList)
        {
            // update each consolidation feature to indicate what is ready
            foreach(Device.OutputReadyNotification notify in notificationList)
                for(int i = 0; i < TargetDevices.Count; i++)
                {
                    DeviceLink link = TargetDevices[i];
                    if (notify.FeatureType == link.FromFeatureType && notify.FeatureId == link.FromFeatureId && link.LastReadyTime < notify.ReadingEnd)
                        link.LastReadyTime = notify.ReadingEnd;
                }

            // do this separately as there may be multiple notifications relating to any one device 
            // and all DevLink entries must be ready before notify
            //
            // notify each affected consolidation device once only
            List<ConsolidationDevice> notified = new List<ConsolidationDevice>();
            for (int i = 0; i < TargetDevices.Count; i++)
            {
                ConsolidationDevice d = TargetDevices[i].ToDevice;
                if (!notified.Contains(d))
                {
                    d.NotifyConsolidation();
                    notified.Add(d);
                }
            }
        }
    }

    public abstract class PhysicalDevice : DeviceBase 
    {
        public int DeviceInterval { get; private set; }
        public TimeSpan QueryInterval { get; private set; }

        public override DateTime NextRunTime { get { return LastRunTime + QueryInterval; } }
        public DateTime? FirstFullDay { get { return DeviceSettings != null ? DeviceManagerDeviceSettings.FirstFullDay : null; } }

        public UInt64 Address { get; protected set; }

        public bool FaultDetected { get; protected set; }

        protected DeviceAlgorithm DeviceAlgorithm;

        public PhysicalDevice(DeviceControl.DeviceManagerBase deviceManager, DeviceManagerDeviceSettings deviceSettings, 
            string manufacturer = "", string model = "", DeviceAlgorithm deviceAlgorithm = null)
                : base(deviceManager, deviceSettings)
        {
            DeviceAlgorithm = deviceAlgorithm == null ? deviceManager.ManagerAlgorithm : deviceAlgorithm;
            Manufacturer = manufacturer == "" ? deviceSettings.Manufacturer : manufacturer;
            Model = model == "" ? deviceSettings.Model : model;
            InitialiseSub(deviceSettings);    
        }

        public PhysicalDevice(DeviceControl.DeviceManagerBase deviceManager, DeviceManagerDeviceSettings deviceSettings, DeviceAlgorithm deviceAlgorithm)
            : base(deviceManager, deviceSettings)
        {
            DeviceAlgorithm = deviceAlgorithm == null ? deviceManager.ManagerAlgorithm : deviceAlgorithm;
            Manufacturer = deviceSettings.Manufacturer;
            Model = deviceSettings.Model;
            InitialiseSub(deviceSettings);
        }

        private void InitialiseSub(DeviceManagerDeviceSettings deviceSettings)
        {
            SerialNo = deviceSettings.SerialNo;
            Address = deviceSettings.Address;

            DatabaseInterval = deviceSettings.DBIntervalInt;
            DeviceInterval = deviceSettings.QueryIntervalInt;
            QueryInterval = TimeSpan.FromSeconds(deviceSettings.QueryIntervalInt);

            FaultDetected = false;

            QueryInterval = TimeSpan.FromSeconds(DeviceManagerDeviceSettings.QueryIntervalInt);
            DatabaseInterval = DeviceManagerDeviceSettings.DBIntervalInt;
            Address = DeviceManagerDeviceSettings.Address;
        }

        public void ClearAttributes()
        {
            if (DeviceAlgorithm != null)
                DeviceAlgorithm.ClearAttributes();
        }

        public virtual bool DoExtractReadings(ref bool resetRequired)
        {
            throw new NotImplementedException("PhysicalDevice.DoExtractReadings requires an override");
        }
    }

}
