﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PVSettings;
using MackayFisher.Utilities;
using PVBCInterfaces;
using DeviceDataRecorders;
using Algorithms;

namespace Device
{
    public class InverterDevice_Generic : PhysicalDevice
    {
        private InverterAlgorithm InverterAlgorithm { get { return (InverterAlgorithm)DeviceAlgorithm; } }

        private bool HasStartOfDayEnergyDefect;
        
        private bool UseEnergyTotal;                // Use EnergyTotal unless EnergyToday is available
        private bool UseEnergyTotalSet;

        private EstimateEnergy EstEnergy1;           // sum of energy deltas based upon spot power readings and checked with actuals
        private EstimateEnergy EstEnergy2;
        private EstimateEnergy EstEnergy3;

        private bool RequireDeviceInitialise;

        public override int GetMappableFieldIndex(FeatureType featureType, string name, out Device.FieldType fieldType)
        {
            return EnergyReading.GetMappableFieldIndex(name, out fieldType);
        }
        
        public InverterDevice_Generic(DeviceControl.DeviceManagerBase deviceManager, DeviceManagerDeviceSettings deviceSettings)
            : base(deviceManager, deviceSettings, new InverterAlgorithm(deviceSettings.DeviceSettings, deviceManager.Protocol, deviceManager.ErrorLogger))
        {
            DeviceParams.DeviceType = deviceSettings.DeviceType;
            DeviceParams.QueryInterval = deviceSettings.QueryIntervalInt;
            DeviceParams.RecordingInterval = deviceSettings.DBIntervalInt;

            HasStartOfDayEnergyDefect = DeviceSettings.HasStartOfDayEnergyDefect;
            ResetStartOfDay();

            ResetDevice();
        }

        public override void NotifyPortReset()
        {
            RequireDeviceInitialise = true;
        }

        private DateTime ResetStartOfDayDay = DateTime.Today;
        public void ResetStartOfDay()
        {
            UseEnergyTotal = true;
            UseEnergyTotalSet = false;
            ResetStartOfDayDay = DateTime.Today;
        }

        public override DateTime NextRunTime
        {
            get
            {
                DateTime now = DateTime.Now;
                DateTime nextTime = LastRunTime + QueryInterval;
                if (nextTime < now)
                    nextTime = now;
                TimeSpan? inverterStart = GlobalSettings.ApplicationSettings.InverterStartTime;
                if (inverterStart.HasValue && inverterStart.Value > nextTime.TimeOfDay)
                {
                    nextTime = DateTime.Today + inverterStart.Value;
                    return nextTime;
                }

                TimeSpan? inverterStop = GlobalSettings.ApplicationSettings.InverterStopTime;
                if (inverterStop.HasValue && inverterStop.Value < nextTime.TimeOfDay)
                    return DateTime.Today + inverterStart.Value + TimeSpan.FromDays(1.0);

                return nextTime;
            }
        }

        private void ResetDevice()
        {
            DeviceId = null;
            RequireDeviceInitialise = true;

            Address = DeviceManagerDeviceSettings.Address;
            Manufacturer = DeviceManagerDeviceSettings.Manufacturer;
            Model = DeviceManagerDeviceSettings.Model;
            SerialNo = DeviceManagerDeviceSettings.SerialNo;

            InverterAlgorithm.SetModel(Model);
            InverterAlgorithm.SetManufacturer(Manufacturer);
            InverterAlgorithm.SetSerialNo(SerialNo);
            InverterAlgorithm.SetAddress(Address);

            ClearAttributes();
        }

        private EnergyReading SetupNewReading(FeatureType featureType, uint featureId, DateTime curTime, DeviceDetailPeriods_EnergyMeter days)
        {            
            EnergyReading reading = new EnergyReading();

            if (LastRecordTime.HasValue)
                reading.Initialise(days, curTime, LastRecordTime.Value, false);
            else
                reading.Initialise(days, curTime, TimeSpan.FromSeconds(DeviceInterval), false);

            return reading;
        }

        private void ExtractFeatureReading(FeatureType featureType, uint featureId, DateTime curTime, bool newInterval)
        {
            DeviceDetailPeriods_EnergyMeter days = (DeviceDetailPeriods_EnergyMeter)FindOrCreateFeaturePeriods(featureType, featureId);
            EnergyReading reading = null;                    
            
            if (featureType == FeatureType.YieldAC || featureType == FeatureType.EnergyAC || featureType == FeatureType.ConsumptionAC)
            {
                if (featureId == 0)
                {
                    reading = SetupNewReading(featureType, featureId, curTime, days);
                    reading.Temperature = (float?)InverterAlgorithm.Temperature;

                    if (InverterAlgorithm.IsThreePhase)
                    {
                        reading.Mode = (Int16?)InverterAlgorithm.Status_1;
                        reading.ErrorCode = (uint?)InverterAlgorithm.ErrorCode1;

                        if (InverterAlgorithm.ErrorCodeHigh1.HasValue)
                            if (reading.ErrorCode.HasValue)
                                reading.ErrorCode += (UInt32)(InverterAlgorithm.ErrorCodeHigh1.Value * 65536);
                            else
                                reading.ErrorCode = (UInt32)(InverterAlgorithm.ErrorCodeHigh1.Value * 65536);

                        reading.EnergyToday = (double?)InverterAlgorithm.EnergyTodayAC1;
                        reading.EnergyTotal = (double?)InverterAlgorithm.EnergyTotalAC1;
                        if (reading.EnergyTotal.HasValue && InverterAlgorithm.EnergyTotalACHigh1.HasValue)
                            reading.EnergyTotal += (Double)(InverterAlgorithm.EnergyTotalACHigh1.Value * 65536);

                        reading.EnergyDelta = EstEnergy1.LastEnergyDelta_Power;
                        if (EstEnergy1.LastEnergyDeltaAdjust_Power != 0.0)
                            reading.HistEnergyDelta = EstEnergy1.LastEnergyDeltaAdjust_Power;

                        reading.Power = (Int32?)InverterAlgorithm.PowerAC_Phase1;
                    }
                    else
                    {
                        reading.Mode = (Int16?)InverterAlgorithm.Status;
                        reading.ErrorCode = (uint?)InverterAlgorithm.ErrorCode;

                        if (InverterAlgorithm.ErrorCodeHigh.HasValue)
                            if (reading.ErrorCode.HasValue)
                                reading.ErrorCode += (UInt32)(InverterAlgorithm.ErrorCodeHigh.Value * 65536);
                            else
                                reading.ErrorCode = (UInt32)(InverterAlgorithm.ErrorCodeHigh.Value * 65536);

                        reading.EnergyToday = (double?)InverterAlgorithm.EnergyTodayAC;
                        reading.EnergyTotal = (double?)InverterAlgorithm.EnergyTotalAC;
                        if (reading.EnergyTotal.HasValue && InverterAlgorithm.EnergyTotalACHigh.HasValue)
                            reading.EnergyTotal += (Double)(InverterAlgorithm.EnergyTotalACHigh.Value * 65536);

                        Double phaseFactor = InverterAlgorithm.PowerAC_Total == 0 ? 0 : (Double)InverterAlgorithm.PowerAC_Phase1 / (Double)InverterAlgorithm.PowerAC_Total;
                        reading.EnergyDelta = EstEnergy1.LastEnergyDelta_Power * phaseFactor;
                        if (EstEnergy1.LastEnergyDeltaAdjust_Power != 0.0)
                            reading.HistEnergyDelta = EstEnergy1.LastEnergyDeltaAdjust_Power * phaseFactor;

                        reading.Power = (Int32?)InverterAlgorithm.PowerAC_Phase1;
                    }
                   
                    reading.Volts = (float?)InverterAlgorithm.VoltsAC1;
                    reading.Amps = (float?)InverterAlgorithm.CurrentAC1;
                    reading.Frequency = (float?)InverterAlgorithm.Frequency;                   

                    if (GlobalSettings.SystemServices.LogTrace)
                        LogMessage("ExtractFeatureReading - FeatureType: " + featureType + " - FeatureId: " + featureId + " - ReadingEnd: " + reading.ReadingEnd
                            + " - EnergyToday: " + reading.EnergyToday
                            + " - EnergyTotal: " + reading.EnergyTotal
                            + " - CalculatedEnergy: " + EstEnergy1.EstimateEnergySumOfDeltas
                            + " - PowerInternal: " + reading.PowerInternal
                            + " - Mode: " + reading.Mode
                            + " - FreqAC: " + reading.Frequency
                            + " - Volts: " + reading.Volts
                            + " - Current: " + reading.Amps
                            + " - Temperature: " + reading.Temperature
                            , LogEntryType.Trace);
                }
                else
                {
                    if (featureId == 1)
                    {
                        if (!InverterAlgorithm.IsThreePhase 
                        && (!InverterAlgorithm.PowerAC2.HasValue || InverterAlgorithm.PowerAC2 == 0)
                        && (!InverterAlgorithm.VoltsAC2.HasValue || InverterAlgorithm.VoltsAC2 == 0)
                        && (!InverterAlgorithm.CurrentAC2.HasValue || InverterAlgorithm.CurrentAC2 == 0))
                            return;     // do not record optional empty record

                        reading = SetupNewReading(featureType, featureId, curTime, days);

                        if (InverterAlgorithm.IsThreePhase)
                        {
                            reading.Mode = (Int16?)InverterAlgorithm.Status_2;
                            reading.ErrorCode = (uint?)InverterAlgorithm.ErrorCode2;

                            if (InverterAlgorithm.ErrorCodeHigh2.HasValue)
                                if (reading.ErrorCode.HasValue)
                                    reading.ErrorCode += (UInt32)(InverterAlgorithm.ErrorCodeHigh2.Value * 65536);
                                else
                                    reading.ErrorCode = (UInt32)(InverterAlgorithm.ErrorCodeHigh2.Value * 65536);

                            reading.EnergyToday = (double?)InverterAlgorithm.EnergyTodayAC2;
                            reading.EnergyTotal = (double?)InverterAlgorithm.EnergyTotalAC2;
                            if (reading.EnergyTotal.HasValue && InverterAlgorithm.EnergyTotalACHigh2.HasValue)
                                reading.EnergyTotal += (Double)(InverterAlgorithm.EnergyTotalACHigh2.Value * 65536);

                            reading.EnergyDelta = EstEnergy2.LastEnergyDelta_Power;
                            if (EstEnergy2.LastEnergyDeltaAdjust_Power != 0.0)
                                reading.HistEnergyDelta = EstEnergy2.LastEnergyDeltaAdjust_Power;
                        }
                        else
                        {
                            Double phaseFactor = InverterAlgorithm.PowerAC_Total == 0 ? 0 : (Double)InverterAlgorithm.PowerAC_Phase2 / (Double)InverterAlgorithm.PowerAC_Total;
                            reading.EnergyDelta = EstEnergy2.LastEnergyDelta_Power * phaseFactor;
                            if (EstEnergy1.LastEnergyDeltaAdjust_Power != 0.0)
                                reading.HistEnergyDelta = EstEnergy1.LastEnergyDeltaAdjust_Power * phaseFactor;
                        }

                        reading.Power = (Int32?)InverterAlgorithm.PowerAC_Phase2;                        
                        reading.Volts = (float?)InverterAlgorithm.VoltsAC2;
                        reading.Amps = (float?)InverterAlgorithm.CurrentAC2;
                    }
                    else if (featureId == 2)
                    {
                        if (!InverterAlgorithm.IsThreePhase
                        && (!InverterAlgorithm.PowerAC3.HasValue || InverterAlgorithm.PowerAC3 == 0)
                        && (!InverterAlgorithm.VoltsAC3.HasValue || InverterAlgorithm.VoltsAC3 == 0)
                        && (!InverterAlgorithm.CurrentAC3.HasValue || InverterAlgorithm.CurrentAC3 == 0))
                            return;     // do not record optional empty record

                        reading = SetupNewReading(featureType, featureId, curTime, days);

                        if (InverterAlgorithm.IsThreePhase)
                        {
                            reading.Mode = (Int16?)InverterAlgorithm.Status_3;
                            reading.ErrorCode = (uint?)InverterAlgorithm.ErrorCode3;

                            if (InverterAlgorithm.ErrorCodeHigh3.HasValue)
                                if (reading.ErrorCode.HasValue)
                                    reading.ErrorCode += (UInt32)(InverterAlgorithm.ErrorCodeHigh3.Value * 65536);
                                else
                                    reading.ErrorCode = (UInt32)(InverterAlgorithm.ErrorCodeHigh3.Value * 65536);

                            reading.EnergyToday = (double?)InverterAlgorithm.EnergyTodayAC3;
                            reading.EnergyTotal = (double?)InverterAlgorithm.EnergyTotalAC3;
                            if (reading.EnergyTotal.HasValue && InverterAlgorithm.EnergyTotalACHigh3.HasValue)
                                reading.EnergyTotal += (Double)(InverterAlgorithm.EnergyTotalACHigh3.Value * 65536);

                            reading.EnergyDelta = EstEnergy3.LastEnergyDelta_Power;
                            if (EstEnergy3.LastEnergyDeltaAdjust_Power != 0.0)
                                reading.HistEnergyDelta = EstEnergy2.LastEnergyDeltaAdjust_Power;
                        }
                        else
                        {
                            Double phaseFactor = InverterAlgorithm.PowerAC_Total == 0 ? 0 : (Double)InverterAlgorithm.PowerAC_Phase3 / (Double)InverterAlgorithm.PowerAC_Total;
                            reading.EnergyDelta = EstEnergy1.LastEnergyDelta_Power * phaseFactor;
                            if (EstEnergy1.LastEnergyDeltaAdjust_Power != 0.0)
                                reading.HistEnergyDelta = EstEnergy1.LastEnergyDeltaAdjust_Power * phaseFactor;
                        }

                        reading.Power = (Int32?)InverterAlgorithm.PowerAC_Phase3;        
                        reading.Volts = (float?)InverterAlgorithm.VoltsAC3;
                        reading.Amps = (float?)InverterAlgorithm.CurrentAC3;
                    }
                    else
                    {
                        LogMessage("ExtractFeatureReading - FeatureType: " + featureType + " - FeatureId: " + featureId + " - Feature not supported", LogEntryType.Information);
                        return;
                    }
                    if (GlobalSettings.SystemServices.LogTrace)
                        LogMessage("ExtractFeatureReading - FeatureType: " + featureType + " - FeatureId: " + featureId + " - ReadingEnd: " + reading.ReadingEnd
                            + " - EnergyToday: " + reading.EnergyToday
                            + " - EnergyTotal: " + reading.EnergyTotal
                            + " - CalculatedEnergy: " + EstEnergy1.EstimateEnergySumOfDeltas
                            + " - PowerInternal: " + reading.PowerInternal
                            + " - Mode: " + reading.Mode
                            + " - FreqAC: " + reading.Frequency
                            + " - Volts: " + reading.Volts
                            + " - Current: " + reading.Amps
                            + " - Temperature: " + reading.Temperature
                            , LogEntryType.Trace);
                }
            }
            else if (featureType == FeatureType.YieldDC)
            {
                if (featureId == 0)
                {
                    if ((!InverterAlgorithm.VoltsPV1.HasValue || InverterAlgorithm.VoltsPV1 == 0)
                    && (!InverterAlgorithm.CurrentPV1.HasValue || InverterAlgorithm.CurrentPV1 == 0)
                    && (!InverterAlgorithm.PowerPV1.HasValue || InverterAlgorithm.PowerPV1 == 0)
                    && (!InverterAlgorithm.TemperatureDC.HasValue || InverterAlgorithm.TemperatureDC == 0))
                        return;     // do not record optional empty record
                    reading = SetupNewReading(featureType, featureId, curTime, days);
                    reading.Power = (int?)InverterAlgorithm.PowerPV1;
                    reading.Volts = (float?)InverterAlgorithm.VoltsPV1;
                    reading.Amps = (float?)InverterAlgorithm.CurrentPV1;
                    reading.Temperature = (float?)InverterAlgorithm.TemperatureDC;
                }
                else if (featureId == 1)
                {
                    if ((!InverterAlgorithm.VoltsPV2.HasValue || InverterAlgorithm.VoltsPV2 == 0)
                    && (!InverterAlgorithm.CurrentPV2.HasValue || InverterAlgorithm.CurrentPV2 == 0)
                    && (!InverterAlgorithm.PowerPV2.HasValue || InverterAlgorithm.PowerPV2 == 0))
                        return;     // do not record optional empty record
                    reading = SetupNewReading(featureType, featureId, curTime, days);
                    reading.Power = (int?)InverterAlgorithm.PowerPV2;
                    reading.Volts = (float?)InverterAlgorithm.VoltsPV2;
                    reading.Amps = (float?)InverterAlgorithm.CurrentPV2;
                }
                else if (featureId == 2)
                {
                    if ((!InverterAlgorithm.VoltsPV3.HasValue || InverterAlgorithm.VoltsPV3 == 0)
                    && (!InverterAlgorithm.CurrentPV3.HasValue || InverterAlgorithm.CurrentPV3 == 0)
                    && (!InverterAlgorithm.PowerPV3.HasValue || InverterAlgorithm.PowerPV3 == 0))
                        return;     // do not record optional empty record
                    reading = SetupNewReading(featureType, featureId, curTime, days);
                    reading.Power = (int?)InverterAlgorithm.PowerPV3;
                    reading.Volts = (float?)InverterAlgorithm.VoltsPV3;
                    reading.Amps = (float?)InverterAlgorithm.CurrentPV3;
                }
                else 
                {
                    LogMessage("ExtractFeatureReading - FeatureType: " + featureType + " - FeatureId: " + featureId + " - Feature not supported", LogEntryType.Information);
                    return;
                }
                if (GlobalSettings.SystemServices.LogTrace)
                    LogMessage("ExtractFeatureReading - FeatureType: " + featureType + " - FeatureId: " + featureId + " - ReadingEnd: " + reading.ReadingEnd
                        + " - Mode: " + reading.Mode
                        + " - Volts: " + reading.Volts
                        + " - Current: " + reading.Amps
                        + " - Temperature: " + reading.Temperature
                        , LogEntryType.Trace);
            }
                                           
            days.AddRawReading(reading);

            if (newInterval)
                days.UpdateDatabase(null, reading.ReadingEnd, false);
        }

        private int failureCount = 0;
        public override bool DoExtractReadings(ref bool resetDevice)
        {
            if (FaultDetected)
                return false;

            bool res = false;
            bool alarmFound = false;
            bool errorFound = false;
            String stage = "Identity";
            try
            {
                ClearAttributes();

                if (!DeviceId.HasValue || RequireDeviceInitialise)
                {
                    res = InverterAlgorithm.ExtractIdentity();
                    if (res)
                        failureCount = 0;
                    else
                    {
                        if (++failureCount > 3)
                            resetDevice = true;
                        return false;
                    }
                    resetDevice = false;

                    RequireDeviceInitialise = false;
                    Manufacturer = InverterAlgorithm.Manufacturer.Trim();
                    if (Manufacturer == "")
                        Manufacturer = DeviceManagerDeviceSettings.Manufacturer;
                    if (Manufacturer == "")
                        Manufacturer = "Unknown";
                    Model = InverterAlgorithm.Model.Trim();
                    if (Model == "")
                        Model = DeviceManagerDeviceSettings.Model;
                    if (Model == "")
                        Model = "Unknown";
                    SerialNo = InverterAlgorithm.SerialNo.Trim();
                    if (SerialNo == "")
                        SerialNo = DeviceManagerDeviceSettings.SerialNo;
                    if (SerialNo == "")
                    {
                        LogMessage("DoExtractReadings - Identity - Manufacturer: " + Manufacturer
                            + " - Model: " + Model
                            + " : No Serial Number - Cannot record", LogEntryType.ErrorMessage);
                        return false;
                    }

                    if (GlobalSettings.SystemServices.LogTrace)
                        LogMessage("DoExtractReadings - Identity - Manufacturer: " + Manufacturer
                            + " - Model: " + Model
                            + " - SerialNo: " + SerialNo
                            + " - DeviceId: " + DeviceId
                            + " - Energy Margin: " + InverterAlgorithm.EnergyMargin, LogEntryType.Trace);
                }

                stage = "Reading";

                GenericConnector.DBDateTimeGeneric readingEnd = new GenericConnector.DBDateTimeGeneric();
                readingEnd.Value = DateTime.Now; // reproduce reading date precision adjustment
                DateTime curTime = readingEnd.Value;

                bool newInterval = IsNewdatabaseInterval(curTime);

                res = InverterAlgorithm.ExtractReading(newInterval, ref alarmFound, ref errorFound);
                if (res)
                    failureCount = 0;
                else
                {
                    if (++failureCount > 3)
                        resetDevice = true;
                    return false;
                }
                resetDevice = false;

                TimeSpan duration;
                try
                {
                    if (InverterAlgorithm.EnergyTodayAC.HasValue || InverterAlgorithm.EnergyTotalAC.HasValue)
                        if (!UseEnergyTotalSet)
                        {
                            UseEnergyTotal = !InverterAlgorithm.EnergyTodayAC.HasValue; // once set for a device it must not change
                            UseEnergyTotalSet = true; 
                        }
                        else if (UseEnergyTotal == InverterAlgorithm.EnergyTodayAC.HasValue)
                        {
                            LogMessage("DoExtractReadings - UseEnergyTotal has flipped - was " + UseEnergyTotal, LogEntryType.ErrorMessage);
                            return false;
                        }

                    if (EstEnergy1 == null)
                    {
                        EstEnergy1 = new Device.EstimateEnergyStartOfDayDefect(this, FeatureType.YieldAC, 0, UseEnergyTotal);
                        if (InverterAlgorithm.IsThreePhase)
                        {
                            EstEnergy2 = new Device.EstimateEnergyStartOfDayDefect(this, FeatureType.YieldAC, 1, UseEnergyTotal);
                            EstEnergy3 = new Device.EstimateEnergyStartOfDayDefect(this, FeatureType.YieldAC, 2, UseEnergyTotal);
                        }
                    }

                    if (InverterAlgorithm.IsThreePhase)
                    {
                        duration = EstEnergy1.EstimateFromPower((double)InverterAlgorithm.PowerAC_Phase1, curTime,
                            (double?)(UseEnergyTotal ? InverterAlgorithm.EnergyTotalAC1 : InverterAlgorithm.EnergyTodayAC1),
                            InverterAlgorithm.EnergyTodayEnergyMargin, InverterAlgorithm.EnergyTotalEnergyMargin);

                        EstEnergy2.EstimateFromPower((double)InverterAlgorithm.PowerAC_Phase2, curTime,
                            (double?)(UseEnergyTotal ? InverterAlgorithm.EnergyTotalAC2 : InverterAlgorithm.EnergyTodayAC2),
                            InverterAlgorithm.EnergyTodayEnergyMargin, InverterAlgorithm.EnergyTotalEnergyMargin);

                        EstEnergy3.EstimateFromPower((double)InverterAlgorithm.PowerAC_Phase3, curTime,
                            (double?)(UseEnergyTotal ? InverterAlgorithm.EnergyTotalAC3 : InverterAlgorithm.EnergyTodayAC3),
                            InverterAlgorithm.EnergyTodayEnergyMargin, InverterAlgorithm.EnergyTotalEnergyMargin);
                    }
                    else
                        duration = EstEnergy1.EstimateFromPower((double)InverterAlgorithm.PowerAC_Total, curTime,
                            (double?)(UseEnergyTotal ? InverterAlgorithm.EnergyTotalAC : InverterAlgorithm.EnergyTodayAC),
                            InverterAlgorithm.EnergyTodayEnergyMargin, InverterAlgorithm.EnergyTotalEnergyMargin);
                }
                catch (Exception e)
                {
                    LogMessage("DoExtractReadings - Error calculating EstimateEnergy - probably no PowerAC retrieved - Exception: " + e.Message, LogEntryType.ErrorMessage);
                    return false;
                }

                List<OutputReadyNotification> notificationList = new List<OutputReadyNotification>();                 
                foreach (FeatureSettings fs in DeviceSettings.FeatureList)
                    if (DeviceManagerDeviceSettings.RecordFeature(fs.FeatureType, fs.FeatureId))
                    {
                        ExtractFeatureReading(fs.FeatureType, fs.FeatureId, curTime, newInterval);
                        if (newInterval)
                            BuildOutputReadyFeatureList(notificationList, fs.FeatureType, fs.FeatureId, curTime);
                    }

                if (newInterval)
                    UpdateConsolidations(notificationList);

                LastRecordTime = curTime;
                
                // Note we do first estimate of the day using prev day EstimateEnergy as first reading is usually split across the day boundary
                if (ResetStartOfDayDay != DateTime.Today)
                    ResetStartOfDay();

                if (EmitEvents)
                {
                    stage = "events";
                    SetAllFeatureEventStatus(curTime, duration);
                }

                if (InverterAlgorithm.ErrorCode.HasValue && InverterAlgorithm.ErrorCode != 0 && InverterAlgorithm.HaveErrorRegisters)
                    DeviceManager.ErrorLogger.LogError(InverterAlgorithm.Address.ToString(), curTime, InverterAlgorithm.ErrorCode.ToString(), 2, InverterAlgorithm.ErrorRegisters);

                stage = "errors";
                if (alarmFound)
                    InverterAlgorithm.LogAlarm("Reading", curTime, DeviceManager);
                if (errorFound)
                    InverterAlgorithm.LogError("Reading", curTime, DeviceManager);

            }
            catch (Exception e)
            {
                LogMessage("DoExtractReadings - Stage: " + stage + " - Exception: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }
            return res;
        }

        protected override bool SetOneFeatureEventStatus(EnergyEventStatus status, DateTime readingTime, TimeSpan readingDuration)
        {
            if (status.FeatureType == FeatureType.YieldAC)
            {
                decimal curPower = (status.FeatureId == 0) ? InverterAlgorithm.PowerAC_Phase1 : (status.FeatureId == 1) ? InverterAlgorithm.PowerAC_Phase2 : InverterAlgorithm.PowerAC_Phase3;
                
                status.SetEventReading(readingTime, 0.0, (int)curPower, (int)readingDuration.TotalSeconds, true);
                return true;
            }
            else if (status.FeatureType == FeatureType.YieldDC)
            {
                if (status.FeatureId == 0)
                {
                    decimal? power = null;
                    if (InverterAlgorithm.PowerPV1.HasValue)
                        power = InverterAlgorithm.PowerPV1.Value;
                    else if (InverterAlgorithm.VoltsPV1.HasValue && InverterAlgorithm.CurrentPV1.HasValue)
                        power = InverterAlgorithm.VoltsPV1.Value * InverterAlgorithm.CurrentPV1.Value;
                    if (power.HasValue)
                    {
                        status.SetEventReading(readingTime, 0.0, (int)power.Value, (int)readingDuration.TotalSeconds, true);
                        return true;
                    }
                }
                else if (status.FeatureId == 1)
                {
                    decimal? power = null;
                    if (InverterAlgorithm.PowerPV2.HasValue)
                        power = InverterAlgorithm.PowerPV2.Value;
                    else if (InverterAlgorithm.VoltsPV2.HasValue && InverterAlgorithm.CurrentPV2.HasValue)
                        power = InverterAlgorithm.VoltsPV2.Value * InverterAlgorithm.CurrentPV2.Value;
                    if (power.HasValue)
                    {
                        status.SetEventReading(readingTime, 0.0, (int)power.Value, (int)readingDuration.TotalSeconds, true);
                        return true;
                    }
                }
                else if (status.FeatureId == 2)
                {
                    decimal? power = null;
                    if (InverterAlgorithm.PowerPV3.HasValue)
                        power = InverterAlgorithm.PowerPV3.Value;
                    else if (InverterAlgorithm.VoltsPV3.HasValue && InverterAlgorithm.CurrentPV3.HasValue)
                        power = InverterAlgorithm.VoltsPV3.Value * InverterAlgorithm.CurrentPV3.Value;
                    if (power.HasValue)
                    {
                        status.SetEventReading(readingTime, 0.0, (int)power.Value, (int)readingDuration.TotalSeconds, true);
                        return true;
                    }
                }
            }
            return false;
        }

        public override void SplitReadingSub(ReadingBase oldReading, DateTime splitTime, ReadingBase newReading1, ReadingBase newReading2)
        {
            if (((EnergyReading)newReading1).EnergyToday.HasValue && ((EnergyReading)newReading2).EnergyDelta >= InverterAlgorithm.EnergyTodayEnergyMargin)
                ((EnergyReading)newReading1).EnergyToday -= ((EnergyReading)newReading2).EnergyDelta;
            if (((EnergyReading)newReading1).EnergyTotal.HasValue && ((EnergyReading)newReading2).EnergyDelta >= InverterAlgorithm.EnergyTotalEnergyMargin)
                ((EnergyReading)newReading1).EnergyTotal -= ((EnergyReading)newReading2).EnergyDelta;
        }
    }
}

