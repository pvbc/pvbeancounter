﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PVSettings;
using MackayFisher.Utilities;
using PVBCInterfaces;
using GenericConnector;
using DeviceDataRecorders;

namespace Device
{
    public struct FieldMapEntry
    {
        public int ToFieldIndex;
        public int FromFieldIndex;
        public ConsolidateDeviceSettings.OperationType Operation;
        public FieldType FieldType;

        public FieldMapEntry(int toFieldIndex, int fromFieldIndex, ConsolidateDeviceSettings.OperationType operation, FieldType fieldType)
        {
            ToFieldIndex = toFieldIndex;
            FromFieldIndex = fromFieldIndex;
            Operation = operation;
            FieldType = fieldType;
        }
    }

    public class DeviceLink
    {
        public DeviceBase FromDevice;
        public FeatureType FromFeatureType;
        public uint FromFeatureId;
        public ConsolidationDevice ToDevice;
        public FeatureType ToFeatureType;
        public uint ToFeatureId;
        public ConsolidateDeviceSettings.OperationType Operation;
        public EnergyEventStatus FromEventStatus;
        public EnergyEventStatus ToEventStatus = null;

        public List<FieldMapEntry> FieldMap;

        #region Used for Event Rollup

        #endregion

        #region Used for Consolidation Rollup
        public List<DateTime> UpdatedPeriods;
        public DateTime LastReadyTime = DateTime.MinValue;
        public int PrevIntervalNo;
        #endregion

        public DeviceLink(DeviceBase fromDevice, FeatureType fromFeatureType, uint fromFeatureId, 
            ConsolidationDevice toDevice, FeatureType toFeatureType, uint toFeatureId,
            ConsolidateDeviceSettings.OperationType operation, EnergyEventStatus fromEventStatus, List<FieldMapEntry> fieldMap)
        {
            FromDevice = fromDevice;
            FromFeatureType = fromFeatureType;
            FromFeatureId = fromFeatureId;
            ToDevice = toDevice;
            ToFeatureType = toFeatureType;
            ToFeatureId = toFeatureId;
            Operation = operation;
            FromEventStatus = fromEventStatus;
            FieldMap = fieldMap;
            UpdatedPeriods = new List<DateTime>();
            PrevIntervalNo = -1;
        }
    }

    public abstract class ConsolidationDevice : DeviceBase
    {
        public List<DeviceLink> SourceDevices;

        public System.Threading.Mutex ConsolidationLinksMutex;

        public int PrevOldestIntervalNo { get; protected set; }
        public int PrevNewestIntervalNo { get; protected set; }

        public ConsolidationDevice(DeviceControl.DeviceManagerBase deviceManager, DeviceManagerDeviceSettings settings, PeriodType periodType = PeriodType.Day)
            : base(deviceManager, settings)
        {
            Manufacturer = settings.Manufacturer;
            Model = settings.Model;
            SerialNo = settings.SerialNo;
            SourceDevices = new List<DeviceLink>();
            PeriodType = periodType;
            ConsolidationLinksMutex = new System.Threading.Mutex();
            PrevOldestIntervalNo = -1;
            PrevNewestIntervalNo = -1;
        }

        public List<DateTime> GetModifiedDays(FeatureType featureType, uint featureId)
        {
            bool haveMutex = false;
            List<DateTime> list = new List<DateTime>();
            try
            {
                ConsolidationLinksMutex.WaitOne();
                haveMutex = true;
                
                foreach (DeviceLink link in SourceDevices)
                    if (link.ToFeatureType == featureType && link.ToFeatureId == featureId)
                        foreach(DateTime day in link.UpdatedPeriods)
                            list.Add(day);
            }
            finally
            {
                if (haveMutex)
                    ConsolidationLinksMutex.ReleaseMutex();
            }
            return list;
        }

        public override DateTime NextRunTime
        {
            get { throw new NotSupportedException(); }
        }

        public void RemoveSourceLink(DeviceLink deviceLink)
        {
            SourceDevices.Remove(deviceLink);
            deviceLink.FromDevice.RemoveTargetDevice(deviceLink);
        }

        public void AddSourceDevice(DeviceLink deviceLink)
        {
            // ensure it references this object
            deviceLink.ToDevice = this;
            if (deviceLink.FromDevice.GetType().IsSubclassOf(typeof(ConsolidationDevice)) && FindInTargetList((ConsolidationDevice)deviceLink.FromDevice))
            {
                GlobalSettings.LogMessage("Consolidation.AddSourceDevice", " Recursion detected - Device: " +
                    deviceLink.FromDevice.SerialNo + " - Already in target colsolidation hierarchy on " + 
                    this.SerialNo + " - ignored", LogEntryType.ErrorMessage);
                return;
            }
            deviceLink.ToEventStatus = FindFeatureEventStatus(deviceLink.ToFeatureType, deviceLink.ToFeatureId);
            SourceDevices.Add(deviceLink);
            deviceLink.FromDevice.AddTargetDevice(deviceLink);

            DeviceDetailPeriodsBase periods = FindOrCreateFeaturePeriods(deviceLink.ToFeatureType, deviceLink.ToFeatureId);
            foreach (FieldMapEntry e in deviceLink.FieldMap)
                periods.MappableFieldsPopulated[e.ToFieldIndex].IsMappingTarget = true;
        }

        private int GetIntervalNo(int interval, DateTime targetTime, DateTime targetDay)
        {
            if (targetDay.Date != targetTime.Date)
                return -1;

            return (int)Math.Truncate(targetTime.TimeOfDay.TotalSeconds / interval);
        }

        public void NotifyConsolidation()
        {
            int interval = DeviceManager.ManagerManager.GetOutputManagerInterval(DeviceManagerDeviceSettings.PVOutputSystem);

            // Called by subordinate device when that device has a complete set of readings

            List<OutputReadyNotification> notifyList = new List<OutputReadyNotification>(); ;
            
            DateTime lastTime = LastRecordTime.HasValue ? LastRecordTime.Value : DateTime.MinValue;
            DateTime newTime = lastTime;
            // scan source links - are all source features updated
            // find lowest new date in the features - this is the new LastRecordTime if all are updated

            int oldestIntervalNo = -2;
            int newestIntervalNo = -2;
            bool isFirst = true;

            foreach (DeviceLink sLink in SourceDevices)
            {
                int prevIntervalNo = sLink.PrevIntervalNo;
                int linkIntervalNo = GetIntervalNo(interval, sLink.LastReadyTime, DateTime.Today);
                sLink.PrevIntervalNo = linkIntervalNo;

                if (isFirst)
                {
                    oldestIntervalNo = linkIntervalNo;
                    newestIntervalNo = linkIntervalNo;
                    isFirst = false;
                }
                else if (linkIntervalNo < oldestIntervalNo)
                    oldestIntervalNo = linkIntervalNo;
                else if (linkIntervalNo > newestIntervalNo)
                    newestIntervalNo = linkIntervalNo;

                if (sLink.LastReadyTime > lastTime)
                    if (newTime == lastTime)
                        newTime = sLink.LastReadyTime;
                    else if (sLink.LastReadyTime < newTime)
                        newTime = sLink.LastReadyTime;
            }

            // end of day reset
            if (LastRecordTime.HasValue && newTime.Date != LastRecordTime.Value.Date)
            {
                PrevOldestIntervalNo = -1;
                PrevNewestIntervalNo = -1;
            }

            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("NotifyConsolidation - oldest: " + oldestIntervalNo + " - Prev oldest: " + PrevOldestIntervalNo +
                    " - newest: " + newestIntervalNo + " - Prev newest: " + PrevNewestIntervalNo +
                    " - LastRecordTime: " + LastRecordTime, LogEntryType.Trace);

            if (oldestIntervalNo > PrevOldestIntervalNo || newestIntervalNo > PrevNewestIntervalNo && (newestIntervalNo - oldestIntervalNo) > 1)
            {
                PrevNewestIntervalNo = newestIntervalNo;
                PrevOldestIntervalNo = oldestIntervalNo;
                LastRecordTime = newTime;               

                if (DeviceManagerDeviceSettings.ConsolidationType == ConsolidationType.PVOutput && DeviceManagerDeviceSettings.PVOutputSystem != "")
                {
                    LogMessage("NotifyConsolidation - Notifying PVOutput OutputManager", LogEntryType.Trace);
                    DeviceManager.ManagerManager.SetOutputReady(DeviceManagerDeviceSettings.PVOutputSystem);
                }
                // push update notifications up the consolidation hierarchy
                foreach (DeviceLink sLink in SourceDevices)
                    BuildOutputReadyFeatureList(notifyList, sLink.ToFeatureType, sLink.ToFeatureId, newTime);
                UpdateConsolidations(notifyList);
            }
        }

        public int SetHierarchyDepth(int initial = 0)
        {
            if (initial > 10)
            {
                LogMessage("SetHierarchyDepth - Too many levels - some levels ignored", LogEntryType.Information);
                return initial;
            }

            int val = initial;

            if (SourceDevices.Count > 0)
            {
                val += 1;
                foreach (DeviceLink l in SourceDevices) // if it has SourceDevices it is a consolidation
                    if (l.FromDevice.DeviceType == PVSettings.DeviceType.Consolidation)
                    {
                        int i = ((ConsolidationDevice)l.FromDevice).SetHierarchyDepth(val);
                        if (i > val) val = i;
                    }
            }

            // only set depth at top level
            // lower consolidation levels are set separately via a call where that dev is at the top level
            if (initial == 0)
                HierarchyDepth = val;

            return val;
        }
    }

    public class EnergyConsolidationParams : EnergyParams
    {        
        public override int QueryInterval { get{ return RecordingInterval; } }
        
        public EnergyConsolidationParams(int recordingInterval)
        {
            DeviceType = PVSettings.DeviceType.Consolidation;
            RecordingInterval = recordingInterval;
            EnforceRecordingInterval = true; // Consolidations are always aligned to formal intervals
        }
    }

    public class EnergyConsolidationDevice : ConsolidationDevice
    {
        public EnergyConsolidationDevice(DeviceControl.DeviceManagerBase deviceManager, DeviceManagerDeviceSettings settings, PeriodType periodType = PeriodType.Day)
            : base(deviceManager, settings, periodType)
        {
            int interval;
            if (settings.ConsolidationType == ConsolidationType.PVOutput)
            {
                PvOutputSiteSettings pvo = GlobalSettings.ApplicationSettings.FindPVOutputBySystemId(settings.PVOutputSystem);
                if (pvo == null)
                    throw new Exception("EnergyConsolidationDevice - Cannot find PVOutput system - " + settings.PVOutputSystem);
                interval = pvo.DataIntervalSeconds;
            }
            else
                interval = settings.DBIntervalInt;

            DeviceParams = new EnergyConsolidationParams(interval);
        }

        public override int GetMappableFieldIndex(FeatureType featureType, string name, out Device.FieldType fieldType)
        {
            return EnergyReadingConsolidation.GetMappableFieldIndex(name, out fieldType);
        }

        public override void SplitReadingSub(ReadingBase oldReading, DateTime splitTime, ReadingBase newReading1, ReadingBase newReading2)
        {
            newReading1.IsConsolidationReading = true;
            newReading2.IsConsolidationReading = true;
            if (((EnergyReading)newReading1).EnergyToday.HasValue)
                ((EnergyReading)newReading1).EnergyToday -= ((EnergyReading)newReading2).EnergyDelta;
            if (((EnergyReading)newReading1).EnergyTotal.HasValue)
                ((EnergyReading)newReading1).EnergyTotal -= ((EnergyReading)newReading2).EnergyDelta;
        }
    }

}
