﻿/*
* Copyright (c) 2013 Dennis Mackay-Fisher
*
* This file is part of PVService
* 
* PVService is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PVService is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PVBCInterfaces;
using PVSettings;
using Algorithms;
using DeviceDataRecorders;
using MackayFisher.Utilities;

namespace Device
{
    public class SMA_WebBox_Record
    {
        public DateTime TimeStampe;
        public int Seconds;

        public int? MinPowerAC;
        public int? MaxPowerAC;
        public int? PowerAC;

        public int? MinPowerAC_A;
        public int? MaxPowerAC_A;
        public int? MinPowerAC_B;
        public int? MaxPowerAC_B;
        public int? MinPowerAC_C;
        public int? MaxPowerAC_C;
        public Double? EnergyKwh;
        public Double EnergyDelta;
        public int? PowerAC_A;
        public int? PowerAC_B;
        public int? PowerAC_C;

        public Double? Frequency;
        public Double? VoltsAC_A;
        public Double? CurrentAC_A;
        public Double? VoltsAC_B;
        public Double? CurrentAC_B;
        public Double? VoltsAC_C;
        public Double? CurrentAC_C;

        public Double? VoltsDC1;
        public Double? CurrentDC1;

        public Double? VoltsDC2;
        public Double? CurrentDC2;
        public int? PowerDC1;
        public int? PowerDC2;

        public int? Mode;
        public int? Error;

        public SMA_WebBox_Record(DateTime time, int seconds)
        {
            TimeStampe = time;
            Seconds = seconds;
            EnergyKwh = null;
            MaxPowerAC = null;
            MinPowerAC = null;
            MaxPowerAC_A = null;
            MinPowerAC_A = null;
            MaxPowerAC_B = null;
            MinPowerAC_B = null;
            MaxPowerAC_C = null;
            MinPowerAC_C = null;
            PowerAC = null;
            PowerAC_A = null;
            PowerAC_B = null;
            PowerAC_C = null;
            Frequency = null;
            VoltsAC_A = null;
            CurrentAC_A = null;
            VoltsAC_B = null;
            CurrentAC_B = null;
            VoltsAC_C = null;
            CurrentAC_C = null;
            VoltsDC1 = null;
            CurrentDC1 = null;
            VoltsDC2 = null;
            CurrentDC2 = null;
            PowerDC1 = null;
            PowerDC2 = null;
            EnergyDelta = 0.0;
            Mode = null;
            Error = null;
        }

        public double PowerAC_Total
        {
            get
            {
                if (PowerAC.HasValue)
                    return PowerAC.Value;
                return PowerAC_PhaseA + PowerAC_PhaseB + PowerAC_PhaseC;
            }
        }

        public double PowerAC_PhaseA
        {
            get
            {
                if (PowerAC_A.HasValue)
                    return PowerAC_A.Value;
                if (PowerAC.HasValue)
                    return PowerAC.Value - (PowerAC_PhaseB + PowerAC_PhaseC);
                double? val = VoltsAC_A * CurrentAC_A;
                return val.HasValue ? val.Value : 0.0; 
            }
        }

        public double PowerAC_PhaseB
        {
            get
            {
                if (PowerAC_B.HasValue)
                    return PowerAC_B.Value;
                double? val = VoltsAC_B * CurrentAC_B;
                return val.HasValue ? val.Value : 0.0; 
            }
        }

        public double PowerAC_PhaseC
        {
            get
            {
                if (PowerAC_C.HasValue)
                    return PowerAC_C.Value;
                double? val = VoltsAC_C * CurrentAC_C;
                return val.HasValue ? val.Value : 0.0; 
            }
        }

        public double? PowerAC_PhaseA_Nullable
        {
            get
            {
                if (PowerAC_A.HasValue)
                    return PowerAC_A;
                if (PowerAC.HasValue)
                    return PowerAC.Value - (PowerAC_PhaseB + PowerAC_PhaseC);
                return VoltsAC_A * CurrentAC_A;
            }
        }

        public double? PowerAC_PhaseB_Nullable
        {
            get
            {
                if (PowerAC_B.HasValue)
                    return PowerAC_B;
                return VoltsAC_B * CurrentAC_B;
            }
        }

        public double? PowerAC_PhaseC_Nullable
        {
            get
            {
                if (PowerAC_C.HasValue)
                    return PowerAC_C;
                return VoltsAC_C * CurrentAC_C;
            }
        }

        public static int Compare(SMA_WebBox_Record x, SMA_WebBox_Record y)
        {
            if (x.TimeStampe > y.TimeStampe)
                return 1;
            else if (x.TimeStampe < y.TimeStampe)
                return -1;
            return 0; // equal
        }
    }

    public class SMA_WebBox_Device : MeterDevice<SMA_WebBox_Record, SMA_WebBox_Record>
    {
        private FeatureSettings Feature_YieldAC;

        public override int GetMappableFieldIndex(FeatureType featureType, string name, out Device.FieldType fieldType)
        {
            return EnergyReading.GetMappableFieldIndex(name, out fieldType);
        }

        public SMA_WebBox_Device(DeviceControl.DeviceManager_SMA_WebBox deviceManager, DeviceManagerDeviceSettings deviceSettings, string model, string serialNo)
            : base(deviceManager, deviceSettings, "SMA", model, serialNo)
        {
            DeviceParams = new EnergyParams();
            DeviceParams.DeviceType = PVSettings.DeviceType.EnergyMeter;
            DeviceParams.QueryInterval = deviceSettings.QueryIntervalInt;
            DeviceParams.RecordingInterval = deviceSettings.DBIntervalInt;

            DeviceParams.CalibrationFactor = deviceSettings.CalibrationFactor;
            Feature_YieldAC = DeviceSettings.GetFeatureSettings(FeatureType.YieldAC, 0);
        }

        public DeviceDataRecorders.DeviceDetailPeriods_EnergyMeter Days
        {
            get
            {
                return (DeviceDataRecorders.DeviceDetailPeriods_EnergyMeter)FindOrCreateFeaturePeriods(Feature_YieldAC.FeatureType, Feature_YieldAC.FeatureId);
            }
        }

        private double CalcPhaseEnergy(SMA_WebBox_Record liveReading, uint featureId)
        {
            double totalPower = liveReading.PowerAC_Total;

            if (totalPower == 0.0)
                return 0.0;

            if (featureId == 0)
                return Math.Round(liveReading.EnergyDelta * liveReading.PowerAC_PhaseA / totalPower, 3);
            else if (featureId == 1)
                return Math.Round(liveReading.EnergyDelta * liveReading.PowerAC_PhaseB / totalPower, 3);
            else
                return Math.Round(liveReading.EnergyDelta * liveReading.PowerAC_PhaseC / totalPower, 3);
        }

        private void RecordOneFeature(SMA_WebBox_Record liveReading, FeatureType featureType, uint featureId, List<OutputReadyNotification> notificationList, bool isLive)
        {
            DeviceDetailPeriods_EnergyMeter days = (DeviceDetailPeriods_EnergyMeter)FindOrCreateFeaturePeriods(featureType, featureId);
            EnergyReading reading = new EnergyReading();

            reading.Initialise(days, liveReading.TimeStampe, TimeSpan.FromSeconds(liveReading.Seconds), false);
            LastRecordTime = liveReading.TimeStampe;

            if (featureType == FeatureType.YieldDC)
                if (featureId == 0)
                {
                    reading.Power = liveReading.PowerDC1;
                    reading.Volts = (float?)liveReading.VoltsDC1;
                    reading.Amps = (float?)liveReading.CurrentDC1;
                }
                else
                {
                    reading.Power = liveReading.PowerDC2;
                    reading.Volts = (float?)liveReading.VoltsDC2;
                    reading.Amps = (float?)liveReading.CurrentDC2;
                }
            else if (featureType == FeatureType.YieldAC)
                if (featureId == 0)
                {
                    reading.EnergyTotal = liveReading.EnergyKwh;
                    reading.EnergyDelta = CalcPhaseEnergy(liveReading, featureId);
                    reading.Volts = (float?)liveReading.VoltsAC_A;
                    reading.Amps = (float?)liveReading.CurrentAC_A;
                    reading.Power = (int?)liveReading.PowerAC_PhaseA_Nullable;
                    reading.MinPower = liveReading.MinPowerAC_A;
                    reading.MaxPower = liveReading.MaxPowerAC_A;
                    reading.Frequency = (float?)liveReading.Frequency;
                    reading.Mode = liveReading.Mode;
                    reading.ErrorCode = liveReading.Error;
                }
                else if (featureId == 1)
                {
                    reading.EnergyDelta = CalcPhaseEnergy(liveReading, featureId);
                    reading.Volts = (float?)liveReading.VoltsAC_B;
                    reading.Amps = (float?)liveReading.CurrentAC_B;
                    reading.Power = (int?)liveReading.PowerAC_PhaseB_Nullable;
                    reading.MinPower = liveReading.MinPowerAC_B;
                    reading.MaxPower = liveReading.MaxPowerAC_B;
                }
                else if (featureId == 2)
                {
                    reading.EnergyDelta = CalcPhaseEnergy(liveReading, featureId);
                    reading.Volts = (float?)liveReading.VoltsAC_C;
                    reading.Amps = (float?)liveReading.CurrentAC_C;
                    reading.Power = (int?)liveReading.PowerAC_PhaseC_Nullable;
                    reading.MinPower = liveReading.MinPowerAC_C;
                    reading.MaxPower = liveReading.MaxPowerAC_C;
                }

            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("RecordOneFeature - Time: " + liveReading.TimeStampe + " - FeatureType: " + featureType + " - Id: " + featureId
                    + " - Volts: " + reading.Volts + " - Current: " + reading.Amps, LogEntryType.Trace);

            if (reading.EnergyDelta != 0.0 
            || reading.Power.HasValue && reading.Power > 0
            || featureType == FeatureType.YieldDC && (reading.Volts > 0.0 || reading.Amps > 0.0 ))
            {
                days.AddRawReading(reading, true);
                BuildOutputReadyFeatureList(notificationList, featureType, featureId, reading.ReadingEnd);
                days.UpdateDatabase(null, reading.ReadingEnd, false);
            }
        }

        private bool ProcessOneReading(SMA_WebBox_Record liveReading, bool isLive)
        {
            if (FaultDetected)
                return false;

            bool res = false;

            String stage = "Identity";
            try
            {
                stage = "Reading";

                TimeSpan duration;
                Double estEnergy;
                
                try
                {
                    duration = TimeSpan.FromSeconds(liveReading.Seconds);
                    //estEnergy = (((double)liveReading.Watts) * duration.TotalHours) / 1000.0; // watts to KWH
                    estEnergy = liveReading.EnergyKwh.HasValue ? liveReading.EnergyKwh.Value : 0.0;
                }
                catch (Exception e)
                {
                    LogMessage("ProcessOneReading - Error calculating EstimateEnergy - Exception: " + e.Message, LogEntryType.ErrorMessage);
                    return false;
                }

                List<OutputReadyNotification> notificationList = new List<OutputReadyNotification>();
                
                RecordOneFeature(liveReading, FeatureType.YieldAC, 0, notificationList, isLive);

                if (DeviceManagerDeviceSettings.RecordFeature(FeatureType.YieldAC, 1))
                    RecordOneFeature(liveReading, FeatureType.YieldAC, 1, notificationList, isLive);

                if (DeviceManagerDeviceSettings.RecordFeature(FeatureType.YieldAC, 2))
                    RecordOneFeature(liveReading, FeatureType.YieldAC, 2, notificationList, isLive);

                if (DeviceManagerDeviceSettings.RecordFeature(FeatureType.YieldDC, 0))
                    RecordOneFeature(liveReading, FeatureType.YieldDC, 0, notificationList, isLive);
                    
                if (DeviceManagerDeviceSettings.RecordFeature(FeatureType.YieldDC, 1))
                    RecordOneFeature(liveReading, FeatureType.YieldDC, 1, notificationList, isLive);

                if (isLive)
                {
                    UpdateConsolidations(notificationList);
                    if (EmitEvents)
                    {
                        // make liveReading visible to callback - SetOneFeatureEventStatus
                        LiveReading = liveReading;
                        SetAllFeatureEventStatus(liveReading.TimeStampe, duration);
                        LiveReading = null;
                    }
                }                
            }
            catch (Exception e)
            {
                LogMessage("ProcessOneReading - Stage: " + stage + " - Exception: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }

            return res;
        }

        // LiveReading populated by ProcessOneReading for callback below
        private SMA_WebBox_Record LiveReading = null;
        protected override bool SetOneFeatureEventStatus(EnergyEventStatus status, DateTime readingTime, TimeSpan readingDuration)
        {
            int? power = null;
            if (status.FeatureType == FeatureType.YieldAC)
            {
                if (status.FeatureId == 0)     
                    power = (int?)LiveReading.PowerAC_PhaseA_Nullable;
                else if (status.FeatureId == 1)
                    power = (int?)LiveReading.PowerAC_PhaseB_Nullable;
                else if (status.FeatureId == 2)
                    power = (int?)LiveReading.PowerAC_PhaseC_Nullable;               
            }
            else if (status.FeatureType == FeatureType.YieldDC)
            {
                if (status.FeatureId == 0)
                    power = (int?)(LiveReading.VoltsDC1 * LiveReading.CurrentDC1);
                else if (status.FeatureId == 1)
                    power = (int?)(LiveReading.VoltsDC2 * LiveReading.CurrentDC2);           
            }

            if (power.HasValue)
            {
                status.SetEventReading(readingTime, 0.0, power.Value, (int)readingDuration.TotalSeconds, true);
                return true;
            }
            return false;
        }

        public override bool ProcessOneLiveReading(SMA_WebBox_Record liveReading)
        {
            // used for the latest reading
            return ProcessOneReading(liveReading, true);
        }

        public override bool ProcessOneHistoryReading(SMA_WebBox_Record histReading)
        {
            // used for readings older than the latest
            return ProcessOneReading(histReading, false);
        }

        public override void SplitReadingSub(ReadingBase oldReading, DateTime splitTime, ReadingBase newReading1, ReadingBase newReading2)
        {
            if (((EnergyReading)newReading1).EnergyToday.HasValue)
                ((EnergyReading)newReading1).EnergyToday -= ((EnergyReading)newReading2).EnergyDelta;
            if (((EnergyReading)newReading1).EnergyTotal.HasValue)
                ((EnergyReading)newReading1).EnergyTotal -= ((EnergyReading)newReading2).EnergyDelta;
        }
    }
}
