﻿/*
* Copyright (c) 2013 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PVSettings;
using MackayFisher.Utilities;
using PVBCInterfaces;
using DeviceDataRecorders;
using Algorithms;

namespace Device
{
    public class MeterDevice_Generic : PhysicalDevice
    {
        public MeterListenerAlgorithm Algorithm { get { return (MeterListenerAlgorithm)DeviceAlgorithm; } }

        private EstimateEnergy NetExportEstEnergy;      // sum of energy deltas based upon spot power readings and checked with actuals
        private EstimateEnergy ExportEstEnergy;
        private EstimateEnergy ImportEstEnergy;

        public override int GetMappableFieldIndex(FeatureType featureType, string name, out Device.FieldType fieldType)
        {
            if (featureType == FeatureType.GasConsumed)
                return QuantityReading.GetMappableFieldIndex(name, out fieldType);
            else
                return EnergyReading.GetMappableFieldIndex(name, out fieldType);
        }

        private struct ElectricityReadings
        {
            public DateTime? Time;
            public decimal ImportLow;
            public decimal ImportHigh;
            public decimal ImportPower;

            public decimal ExportLow;
            public decimal ExportHigh;
            public decimal ExportPower;
        }

        private struct GasReadings
        {
            public DateTime? Time;

            public decimal? GasTotal;
        }

        private ElectricityReadings PrevElectricityReadings;
        private GasReadings PrevGasReadings;

        private bool IsGasMeter = false;
        private bool IsElectricityMeter = false;

        public MeterDevice_Generic(DeviceControl.DeviceManagerBase deviceManager, DeviceManagerDeviceSettings deviceSettings, Protocol deviceProtocol)
            : base(deviceManager, deviceSettings, new MeterListenerAlgorithm(deviceSettings.DeviceSettings, deviceProtocol, deviceManager.ErrorLogger))
        {
            DeviceParams.DeviceType = deviceSettings.DeviceType;
            DeviceParams.QueryInterval = deviceSettings.QueryIntervalInt;
            DeviceParams.RecordingInterval = deviceSettings.DBIntervalInt;

            PrevElectricityReadings.Time = null;
            PrevElectricityReadings.ImportLow = 0;
            PrevElectricityReadings.ImportHigh = 0;
            PrevElectricityReadings.ImportPower = 0;
            PrevElectricityReadings.ExportLow = 0;
            PrevElectricityReadings.ExportHigh = 0;
            PrevElectricityReadings.ExportPower = 0;

            PrevGasReadings.Time = null;
            PrevGasReadings.GasTotal = null;

            NetExportEstEnergy = null;
            ExportEstEnergy = null;
            ImportEstEnergy = null;

            foreach (FeatureSettings fs in DeviceSettings.FeatureList)
                if (DeviceManagerDeviceSettings.RecordFeature(fs.FeatureType, fs.FeatureId))
                {
                    if (fs.FeatureType == FeatureType.GasConsumed)
                        IsGasMeter = true;
                    else
                    {
                        IsElectricityMeter = true;
                        if (fs.FeatureType == FeatureType.NetExportAC && fs.FeatureId == 0)
                            NetExportEstEnergy = new Device.EstimateEnergyGeneric(this, FeatureType.NetExportAC, 0);
                        if (fs.FeatureType == FeatureType.ExportAC && fs.FeatureId == 0)
                            ExportEstEnergy = new Device.EstimateEnergyGeneric(this, FeatureType.ExportAC, 0);
                        if (fs.FeatureType == FeatureType.ImportAC && fs.FeatureId == 0)
                            ImportEstEnergy = new Device.EstimateEnergyGeneric(this, FeatureType.ImportAC, 0);
                    }
                }

            if (IsElectricityMeter == IsGasMeter)
                throw new Exception("Cannot determine meter type - Gas or Electricity");

            ResetDevice();
        }

        private void ResetDevice()
        {
            DeviceId = null;

            Address = DeviceManagerDeviceSettings.Address;

            Manufacturer = "";
            Model = "";
            SerialNo = "";

            Algorithm.SetModel(Model);
            Algorithm.SetManufacturer(Manufacturer);
            Algorithm.SetSerialNo(SerialNo);
            Algorithm.SetAddress(Address);

            ClearAttributes();
        }

        public override void SplitReadingSub(ReadingBase oldReading, DateTime splitTime, ReadingBase newReading1, ReadingBase newReading2)
        {
            double margin;
            if (oldReading.FeatureType == FeatureType.GasConsumed)
            {
                margin = Algorithm.GasMargin;
                if (Math.Abs(((QuantityReading)newReading2).QuantityDelta) >= margin)
                {
                    if (((QuantityReading)newReading1).QuantityToday.HasValue)
                        ((QuantityReading)newReading1).QuantityToday -= ((QuantityReading)newReading2).QuantityDelta;
                    if (((QuantityReading)newReading1).QuantityTotal.HasValue)
                        ((QuantityReading)newReading1).QuantityTotal -= ((QuantityReading)newReading2).QuantityDelta;
                }
            }
            else
            {
                if (oldReading.FeatureType == FeatureType.NetExportAC)
                    margin = Algorithm.EnergyMargin;
                else if (oldReading.FeatureType == FeatureType.ExportAC)
                    margin = Algorithm.ExportEnergyMargin;
                else if (oldReading.FeatureType == FeatureType.ImportAC)
                    margin = Algorithm.ImportEnergyMargin;
                else
                    margin = Algorithm.EnergyMargin;

                if (Math.Abs(((EnergyReading)newReading2).EnergyDelta) >= margin)
                {
                    if (((EnergyReading)newReading1).EnergyToday.HasValue)
                        ((EnergyReading)newReading1).EnergyToday -= ((EnergyReading)newReading2).EnergyDelta;
                    if (((EnergyReading)newReading1).EnergyTotal.HasValue)
                        ((EnergyReading)newReading1).EnergyTotal -= ((EnergyReading)newReading2).EnergyDelta;
                }
            }
        }

        private EnergyReading SetupNewReading(FeatureType featureType, uint featureId, DateTime curTime, DeviceDetailPeriods_EnergyMeter days)
        {
            EnergyReading reading = new EnergyReading();

            if (LastRecordTime.HasValue)
                reading.Initialise(days, curTime, LastRecordTime.Value, false);
            else
                reading.Initialise(days, curTime, TimeSpan.FromSeconds(DeviceInterval), false);

            return reading;
        }

        private QuantityReading SetupNewReading(FeatureType featureType, uint featureId, DateTime curTime, DeviceDetailPeriods_Quantity days)
        {
            QuantityReading reading = new QuantityReading();

            if (LastRecordTime.HasValue)
                reading.Initialise(days, curTime, LastRecordTime.Value, false);
            else
                reading.Initialise(days, curTime, TimeSpan.FromSeconds(DeviceInterval), false);

            return reading;
        }

        private DateTime RecordElectricityFeatureReading(FeatureType featureType, uint featureId, ElectricityReadings curReadings, ElectricityReadings curDelta, bool newInterval)
        {
            DeviceDetailPeriodsBase days = FindOrCreateFeaturePeriods(featureType, featureId);
            ReadingBase readingBase = null;

            if (featureType == FeatureType.NetExportAC && featureId == 0)
            {
                EnergyReading reading = SetupNewReading(featureType, featureId, curReadings.Time.Value, (DeviceDetailPeriods_EnergyMeter)days);
                reading.EnergyTotal = (Double)(curReadings.ExportHigh + curReadings.ExportLow - (curReadings.ImportHigh + curReadings.ImportLow));
                reading.EnergyDelta = NetExportEstEnergy.LastEnergyDelta_Power;
                if (NetExportEstEnergy.LastEnergyDeltaAdjust_Power != 0.0)
                    reading.HistEnergyDelta = NetExportEstEnergy.LastEnergyDeltaAdjust_Power;
                reading.Power = (int)(curReadings.ExportPower - curReadings.ImportPower);
                readingBase = reading;
            }
            else if (featureType == FeatureType.ExportAC)
            {
                EnergyReading reading = null;
                if (featureId == 0)
                {
                    reading = SetupNewReading(featureType, featureId, curReadings.Time.Value, (DeviceDetailPeriods_EnergyMeter)days);
                    reading.EnergyTotal = (Double)(curReadings.ExportLow + curReadings.ExportHigh);
                    reading.EnergyDelta = ExportEstEnergy.LastEnergyDelta_Power;
                    if (ExportEstEnergy.LastEnergyDeltaAdjust_Power != 0.0)
                        reading.HistEnergyDelta = ExportEstEnergy.LastEnergyDeltaAdjust_Power;
                    reading.Power = (int)curReadings.ExportPower;
                }
                else if (featureId == 1)
                {
                    reading = SetupNewReading(featureType, featureId, curReadings.Time.Value, (DeviceDetailPeriods_EnergyMeter)days);
                    reading.EnergyTotal = (Double)(curReadings.ExportHigh);
                    reading.EnergyDelta = (Double)(curDelta.ExportHigh);
                    reading.Power = null;
                }
                else if (featureId == 2)
                {
                    reading = SetupNewReading(featureType, featureId, curReadings.Time.Value, (DeviceDetailPeriods_EnergyMeter)days);
                    reading.EnergyTotal = (Double)(curReadings.ExportLow);
                    reading.EnergyDelta = (Double)(curDelta.ExportLow);
                    reading.Power = null;
                }
                readingBase = reading;
            }
            else if (featureType == FeatureType.ImportAC)
            {
                EnergyReading reading = null;
                if (featureId == 0)
                {
                    reading = SetupNewReading(featureType, featureId, curReadings.Time.Value, (DeviceDetailPeriods_EnergyMeter)days);
                    reading.EnergyTotal = (Double)(curReadings.ImportLow + curReadings.ImportHigh);
                    reading.EnergyDelta = ImportEstEnergy.LastEnergyDelta_Power;
                    if (ImportEstEnergy.LastEnergyDeltaAdjust_Power != 0.0)
                        reading.HistEnergyDelta = ImportEstEnergy.LastEnergyDeltaAdjust_Power;
                    reading.Power = (int)curReadings.ImportPower;
                }
                else if (featureId == 1)
                {
                    reading = SetupNewReading(featureType, featureId, curReadings.Time.Value, (DeviceDetailPeriods_EnergyMeter)days);
                    reading.EnergyTotal = (Double)(curReadings.ImportHigh);
                    reading.EnergyDelta = (Double)(curDelta.ImportHigh);
                    reading.Power = null;
                }
                else if (featureId == 2)
                {
                    reading = SetupNewReading(featureType, featureId, curReadings.Time.Value, (DeviceDetailPeriods_EnergyMeter)days);
                    reading.EnergyTotal = (Double)(curReadings.ImportLow);
                    reading.EnergyDelta = (Double)(curDelta.ImportLow);
                    reading.Power = null;
                }
                readingBase = reading;
            }
            else
            {
                LogMessage("RecordElectricityFeatureReading - FeatureType: " + featureType + " - FeatureId: " + featureId + " - Feature not supported", LogEntryType.ErrorMessage);
                throw new NotSupportedException("RecordElectricityFeatureReading - FeatureType: " + featureType + " - FeatureId: " + featureId + " - Feature not supported");
            }

            if (GlobalSettings.SystemServices.LogTrace)
                readingBase.LogRecordDetail(readingBase, "RecordElectricityFeatureReading", "Add reading", "");

            days.AddRawReading(readingBase);

            if (newInterval)
                days.UpdateDatabase(null, readingBase.ReadingEnd, false);
                
            return readingBase.ReadingEnd;
        }

        private DateTime RecordGasFeatureReading(FeatureType featureType, uint featureId, GasReadings curReadings, GasReadings curDelta, bool newInterval)
        {
            DeviceDetailPeriodsBase days = FindOrCreateFeaturePeriods(featureType, featureId);
            ReadingBase readingBase = null;

            if (featureType == FeatureType.GasConsumed)
            {
                if (curReadings.GasTotal.HasValue)
                {
                    QuantityReading reading = SetupNewReading(featureType, featureId, curReadings.Time.Value, (DeviceDetailPeriods_Quantity)days);
                    reading.QuantityTotal = (double?)curReadings.GasTotal;
                    reading.QuantityDelta = (double)(curDelta.GasTotal.HasValue ? curDelta.GasTotal.Value : 0);
                    readingBase = reading;
                }
                else
                    return DateTime.MinValue;
            }
            else
            {
                LogMessage("RecordGasFeatureReading - FeatureType: " + featureType + " - FeatureId: " + featureId + " - Feature not supported", LogEntryType.ErrorMessage);
                throw new NotSupportedException("RecordGasFeatureReading - FeatureType: " + featureType + " - FeatureId: " + featureId + " - Feature not supported");
            }

            if (GlobalSettings.SystemServices.LogTrace)
                readingBase.LogRecordDetail(readingBase, "RecordGasFeatureReading", "Add reading", "");

            days.AddRawReading(readingBase);

            if (newInterval)
                days.UpdateDatabase(null, readingBase.ReadingEnd, false);

            return readingBase.ReadingEnd;
        }

        private bool EstablishSerialNo()
        {
            if (Algorithm.Manufacturer == null)
                Manufacturer = "";
            else
                Manufacturer = Algorithm.Manufacturer.Trim();
            if (Manufacturer == "")
                Manufacturer = DeviceManagerDeviceSettings.Manufacturer;
            if (Manufacturer == "")
                Manufacturer = "Unknown";

            if (Algorithm.Model == null)
                Model = "";
            else
                Model = Algorithm.Model.Trim();
            if (Model == "")
                Model = DeviceManagerDeviceSettings.Model;
            if (Model == "")
                Model = "Unknown";

            if (Algorithm.SerialNo == null)
                SerialNo = "";
            else
            {
                byte[] bytes = SystemServices.HexToBytes(Algorithm.SerialNo.Trim());
                SerialNo = SystemServices.BytesToString(ref bytes, bytes.Length);
            }

            if (SerialNo == "")
                SerialNo = DeviceManagerDeviceSettings.SerialNo;
            if (SerialNo == "")
            {
                LogMessage("EstablishSerialNo - Identity - Manufacturer: " + Manufacturer
                    + " - Model: " + Model
                    + " : No Serial Number - Cannot record", LogEntryType.ErrorMessage);
                return false;
            }

            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("EstablishSerialNo - Identity - Manufacturer: " + Manufacturer
                    + " - Model: " + Model
                    + " - SerialNo: " + SerialNo
                    + " - DeviceId: " + DeviceId
                    + " - Export Energy Margin: " + Algorithm.ExportEnergyMargin
                    + " - Import Energy Margin: " + Algorithm.ImportEnergyMargin, LogEntryType.Trace);

            return true;
        }

        private int identityFailureCount = 0;
        private bool DoExtractElectricityReadings(ref bool resetRequired)
        {
            if (FaultDetected)
                return false;

            bool res = false;
            bool alarmFound = false;
            bool errorFound = false;
            String stage = "Identity";
            try
            {
                ClearAttributes();

                if (!DeviceId.HasValue)
                {
                    res = Algorithm.ExtractIdentity();
                    if (!res)
                    {
                        if (++identityFailureCount > 3)
                        {
                            identityFailureCount = 0;
                            resetRequired = true;
                        }
                        return false;
                    }
                    resetRequired = false;

                    if (!EstablishSerialNo())
                        return false;
                }

                stage = "Reading";

                GenericConnector.DBDateTimeGeneric readingEnd = new GenericConnector.DBDateTimeGeneric();
                readingEnd.Value = DateTime.Now; // reproduce reading date precision adjustment
                DateTime curTime = readingEnd.Value;

                bool newInterval = IsNewdatabaseInterval(curTime);

                bool dbWrite = (LastRecordTime == null
                    || DeviceBase.IntervalCompare(DatabaseInterval, LastRecordTime.Value, curTime) != 0);
                res = Algorithm.ExtractReading(newInterval, ref alarmFound, ref errorFound);
                if (!res)
                    return false;

                TimeSpan duration = TimeSpan.FromSeconds(6.0);
                //if (dbWrite)
                {
                    try
                    {
                        ElectricityReadings CurReadings;

                        CurReadings.ImportLow = Algorithm.ImportLowRate.HasValue ? Algorithm.ImportLowRate.Value : 0;
                        CurReadings.ImportHigh = Algorithm.ImportHighRate.HasValue ? Algorithm.ImportHighRate.Value : 0;
                        CurReadings.ImportPower = Algorithm.ImportPower.HasValue ? Algorithm.ImportPower.Value * 1000 : 0;
                        CurReadings.ExportLow = Algorithm.ExportLowRate.HasValue ? Algorithm.ExportLowRate.Value : 0;
                        CurReadings.ExportHigh = Algorithm.ExportHighRate.HasValue ? Algorithm.ExportHighRate.Value : 0;
                        CurReadings.ExportPower = Algorithm.ExportPower.HasValue ? Algorithm.ExportPower.Value * 1000 : 0;
                        CurReadings.Time = curTime;

                        try
                        {
                            if (ImportEstEnergy != null)
                                duration = ImportEstEnergy.EstimateFromPower((double)CurReadings.ImportPower, curTime,
                                    (double?)(CurReadings.ImportLow + CurReadings.ImportHigh), Algorithm.ImportEnergyMargin, Algorithm.ImportEnergyMargin);
                            if (ExportEstEnergy != null)
                                duration = ExportEstEnergy.EstimateFromPower((double)CurReadings.ExportPower, curTime,
                                    (double?)(CurReadings.ExportLow + CurReadings.ExportHigh), Algorithm.ExportEnergyMargin, Algorithm.ExportEnergyMargin);
                            if (NetExportEstEnergy != null)
                                duration = NetExportEstEnergy.EstimateFromPower((double)(CurReadings.ExportPower - CurReadings.ImportPower), curTime,
                                (double?)((CurReadings.ExportLow + CurReadings.ExportHigh) - (CurReadings.ImportLow + CurReadings.ImportHigh)), Algorithm.ExportEnergyMargin, Algorithm.ExportEnergyMargin);
                        }
                        catch (Exception e)
                        {
                            LogMessage("DoExtractElectricityReadings - Error calculating EstimateEnergy - probably no PowerAC retrieved - Exception: " + e.Message, LogEntryType.ErrorMessage);
                            return false;
                        }

                        if (PrevElectricityReadings.Time.HasValue)
                        {
                            ElectricityReadings CurDelta;
                            CurDelta.Time = curTime;
                            CurDelta.ImportLow = CurReadings.ImportLow - PrevElectricityReadings.ImportLow;
                            CurDelta.ImportHigh = CurReadings.ImportHigh - PrevElectricityReadings.ImportHigh;
                            CurDelta.ExportLow = CurReadings.ExportLow - PrevElectricityReadings.ExportLow;
                            CurDelta.ExportHigh = CurReadings.ExportHigh - PrevElectricityReadings.ExportHigh;
                            CurDelta.ImportPower = CurReadings.ImportPower;
                            CurDelta.ExportPower = CurReadings.ExportPower;

                            List<OutputReadyNotification> notificationList = new List<OutputReadyNotification>();
                            foreach (FeatureSettings fs in DeviceSettings.FeatureList)
                                if (DeviceManagerDeviceSettings.RecordFeature(fs.FeatureType, fs.FeatureId))
                                {
                                    DateTime thisReadingEnd = RecordElectricityFeatureReading(fs.FeatureType, fs.FeatureId, CurReadings, CurDelta, newInterval);
                                    if (newInterval && thisReadingEnd != DateTime.MinValue)
                                        BuildOutputReadyFeatureList(notificationList, fs.FeatureType, fs.FeatureId, thisReadingEnd);
                                }

                            if (newInterval)                                
                                UpdateConsolidations(notificationList);

                            LastRecordTime = curTime;
                        }

                        PrevElectricityReadings = CurReadings;
                    }
                    catch (Exception e)
                    {
                        LogMessage("DoExtractElectricityReadings - Error calculating EstimateEnergy - probably no PowerAC retrieved - Exception: " + e.Message, LogEntryType.ErrorMessage);
                        return false;
                    }
                }

                if (EmitEvents)
                {
                    stage = "events";
                    SetAllFeatureEventStatus(curTime, duration);
                }
            }
            catch (Exception e)
            {
                LogMessage("DoExtractElectricityReadings - Stage: " + stage + " - Exception: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }

            return res;
        }

        private bool DoExtractGasReadings(ref bool resetRequired)
        {
            if (FaultDetected)
                return false;

            bool res = false;
            bool alarmFound = false;
            bool errorFound = false;
            String stage = "Identity";
            try
            {
                ClearAttributes();
                stage = "Reading";

                GenericConnector.DBDateTimeGeneric readingEnd = new GenericConnector.DBDateTimeGeneric();
                readingEnd.Value = DateTime.Now; // reproduce reading date precision adjustment
                DateTime curTime = readingEnd.Value;

                bool newInterval = IsNewdatabaseInterval(curTime);

                bool dbWrite = (LastRecordTime == null
                    || DeviceBase.IntervalCompare(DatabaseInterval, LastRecordTime.Value, curTime) != 0);
                res = Algorithm.ExtractReading(newInterval, ref alarmFound, ref errorFound);
                if (!res)
                {
                    if (++identityFailureCount > 3)
                    {
                        identityFailureCount = 0;
                        resetRequired = true;
                    }
                    return false;
                }
                resetRequired = false;

                if (!DeviceId.HasValue)
                    if (!EstablishSerialNo())
                        return false;

                TimeSpan duration = TimeSpan.FromSeconds(6.0);
                //if (dbWrite)
                {
                    try
                    {
                        GasReadings CurReadings;

                        CurReadings.GasTotal = Algorithm.GasTotal;
                        CurReadings.Time = curTime;

                        if (PrevGasReadings.Time.HasValue)
                        {
                            GasReadings CurDelta;
                            CurDelta.Time = curTime;
                            CurDelta.GasTotal = CurReadings.GasTotal - PrevGasReadings.GasTotal;

                            List<OutputReadyNotification> notificationList = new List<OutputReadyNotification>();
                            foreach (FeatureSettings fs in DeviceSettings.FeatureList)
                                if (DeviceManagerDeviceSettings.RecordFeature(fs.FeatureType, fs.FeatureId))
                                {
                                    DateTime thisReadingEnd = RecordGasFeatureReading(fs.FeatureType, fs.FeatureId, CurReadings, CurDelta, newInterval);
                                    if (newInterval && thisReadingEnd != DateTime.MinValue)
                                        BuildOutputReadyFeatureList(notificationList, fs.FeatureType, fs.FeatureId, thisReadingEnd);
                                }

                            if (newInterval)
                                UpdateConsolidations(notificationList);

                            LastRecordTime = curTime;
                        }

                        PrevGasReadings = CurReadings;
                    }
                    catch (Exception e)
                    {
                        LogMessage("DoExtractGasReadings - Error recording reading - Exception: " + e.Message, LogEntryType.ErrorMessage);
                        return false;
                    }
                }
            }
            catch (Exception e)
            {
                LogMessage("DoExtractGasReadings - Stage: " + stage + " - Exception: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }

            return res;
        }

        public override bool DoExtractReadings(ref bool resetRequired)
        {
            if (IsElectricityMeter)
                return DoExtractElectricityReadings(ref resetRequired);
            else
                return DoExtractGasReadings(ref resetRequired);
        }

        protected override bool SetOneFeatureEventStatus(EnergyEventStatus status, DateTime readingTime, TimeSpan readingDuration)
        {
            int curPower = 0;
            if (status.FeatureType == FeatureType.NetExportAC)
            {
                if (status.FeatureId == 0)
                    curPower = (int)(PrevElectricityReadings.ExportPower - PrevElectricityReadings.ImportPower);
                else
                    return false;
            }
            else if (status.FeatureType == FeatureType.ExportAC)
            {
                if (status.FeatureId == 0)
                    curPower = (int)PrevElectricityReadings.ExportPower;
                else if (status.FeatureId == 1)
                    curPower = (int)(Algorithm.ExportHighRate.HasValue ? Algorithm.ExportHighRate.Value : 0);
                else if (status.FeatureId == 2)
                    curPower = (int)(Algorithm.ExportLowRate.HasValue ? Algorithm.ExportLowRate.Value : 0);
                else
                    return false;
            }
            else if (status.FeatureType == FeatureType.ImportAC)
            {
                if (status.FeatureId == 0)
                    curPower = (int)PrevElectricityReadings.ImportPower;
                else if (status.FeatureId == 1)
                    curPower = (int)(Algorithm.ImportHighRate.HasValue ? Algorithm.ImportHighRate.Value : 0);
                else if (status.FeatureId == 2)
                    curPower = (int)(Algorithm.ImportLowRate.HasValue ? Algorithm.ImportLowRate.Value : 0);
                else
                    return false;
            }
            else
                return false;
            status.SetEventReading(readingTime, 0.0, curPower, (int)readingDuration.TotalSeconds, false);
            return true;
        }
    }
}
