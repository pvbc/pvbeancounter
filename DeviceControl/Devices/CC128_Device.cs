﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PVBCInterfaces;
using PVSettings;
using Algorithms;
using DeviceDataRecorders;
using MackayFisher.Utilities;

namespace Device
{
    public struct CC128_LiveRecord
    {
        public DateTime MeterTime;
        public DateTime TimeStampe;
        public int WattsPhase0;
        public int WattsPhase1;
        public int WattsPhase2;
        public double Temperature;
    }

    public struct CC128_HistoryRecord
    {
        public uint Sensor;
        public DateTime Time;
        public Double Energy;           // original energy reading, or value from fill gaps correction
        public Double? Calculated;      // energy value above adjusted by calibration and / or history pro-rata adjustments
        public int Duration;
        public float? Temperature;
        public int Count;
        public int? MinPower;
        public int? MaxPower;
        public bool? InRange;
    }

    public class CC128EnergyParams : EnergyParams
    {        
        public CC128EnergyParams()
            : base()
        {
            DeviceType = PVSettings.DeviceType.EnergyMeter;    
        }
    }

    public class CC128_Device : MeterDevice<CC128_LiveRecord, CC128_HistoryRecord>
    {
        private bool IsNewInterval = false;
        private DateTime? _PreviousDatabaseIntervalEnd = null;
        private bool IsThreePhase;

        public override int GetMappableFieldIndex(FeatureType featureType, string name, out Device.FieldType fieldType)
        {
            return EnergyReading.GetMappableFieldIndex(name, out fieldType);
        }

        public CC128_Device(DeviceControl.DeviceManager_CC128 deviceManager, DeviceManagerDeviceSettings deviceSettings)
            : base(deviceManager, deviceSettings, "CurrentCost", "CC128", "")
        {
            DeviceParams = new CC128EnergyParams();                    
            DeviceParams.QueryInterval = deviceSettings.QueryIntervalInt;
            DeviceParams.RecordingInterval = deviceSettings.DBIntervalInt;

            DeviceParams.CalibrationFactor = deviceSettings.CalibrationFactor;
            IsThreePhase = DeviceSettings.Name == "CC128_3Phase";
        }

        public override bool ProcessOneLiveReading(CC128_LiveRecord liveReading)      
        {
            if (FaultDetected)
                return false;

            //if (!DeviceManagerDeviceSettings.RecordFeature(Feature_EnergyAC.FeatureType, (uint)liveReading.Feature))
            //    return true;

            bool res = false;

            DeviceDetailPeriods_EnergyMeter days = null;

            String stage = "Identity";
            try
            {
                stage = "Reading";

                DateTime curTime = DateTime.Now;

                IsNewInterval = IsNewdatabaseInterval(liveReading.TimeStampe);
                _PreviousDatabaseIntervalEnd = PreviousDatabaseIntervalEnd;

                int[] power = new int[3];
                power[0] = liveReading.WattsPhase0 >= DeviceManagerDeviceSettings.ZeroThreshold ? liveReading.WattsPhase0 : 0;
                power[1] = liveReading.WattsPhase1 >= DeviceManagerDeviceSettings.ZeroThreshold ? liveReading.WattsPhase1 : 0;
                power[2] = liveReading.WattsPhase2 >= DeviceManagerDeviceSettings.ZeroThreshold ? liveReading.WattsPhase2 : 0;

                foreach (DeviceFeatureSettings dfs in DeviceManagerDeviceSettings.DeviceFeatures.Collection)
                {
                    if (!dfs.FeatureType.HasValue || !dfs.FeatureId.HasValue)
                        continue;

                    TimeSpan duration;
                    int thisPower = 0;
                    if (IsThreePhase)
                        thisPower = power[dfs.FeatureId.Value];
                    else if (dfs.FeatureId.Value == 0)
                        thisPower = power[0] + power[1] + power[2];
                    else if (dfs.FeatureId.Value < 4)
                        thisPower = power[dfs.FeatureId.Value - 1];
                    else
                        throw new Exception("CC128 - Invalid Feature Id: " + dfs.FeatureId.Value);

                    Double estEnergy;
                    try
                    {
                        duration = EstimateEnergy((double)thisPower, curTime, LastRecordTime, 6.0F, out estEnergy);
                    }
                    catch (Exception e)
                    {
                        LogMessage("ProcessOneLiveReading - Error calculating EstimateEnergy - probably no PowerAC retrieved - Exception: " + e.Message, LogEntryType.ErrorMessage);
                        return false;
                    }

                    if (dfs.SaveFeature)
                    {
                        days = (DeviceDetailPeriods_EnergyMeter)FindOrCreateFeaturePeriods(dfs.FeatureType.Value, dfs.FeatureId.Value);

                        EnergyReading reading = new EnergyReading();

                        if (LastRecordTime.HasValue)
                            reading.Initialise(days, liveReading.TimeStampe, LastRecordTime.Value, false);
                        else
                            reading.Initialise(days, liveReading.TimeStampe, TimeSpan.FromSeconds(DeviceInterval), false);

                        reading.EnergyCalibrationFactor = DeviceManagerDeviceSettings.CalibrationFactor;
                        reading.Power = thisPower;
                        reading.Temperature = (float)liveReading.Temperature;
                        reading.EnergyDelta = estEnergy;

                        if (GlobalSettings.SystemServices.LogTrace)
                            LogMessage("ProcessOneLiveReading - Reading - Time: " + liveReading.TimeStampe + " - Duration: " + (int)reading.Duration.TotalSeconds
                                + " - FeatureId: " + reading.FeatureId
                                + " - EnergyToday: " + reading.EnergyToday
                                + " - EnergyTotal: " + reading.EnergyTotal
                                + " - EstEnergy: " + estEnergy
                                + " - Power: " + reading.Power
                                + " - Mode: " + reading.Mode
                                + " - FreqAC: " + reading.Frequency
                                + " - Volts: " + reading.Volts
                                + " - Current: " + reading.Amps
                                + " - Temperature: " + reading.Temperature
                                , LogEntryType.Trace);

                        stage = "record";

                        days.AddRawReading(reading);

                        if (IsNewInterval)
                        {
                            days.UpdateDatabase(null, reading.ReadingEnd, false);
                            stage = "consolidate";
                            List<OutputReadyNotification> notificationList = new List<OutputReadyNotification>();
                            BuildOutputReadyFeatureList(notificationList, dfs.FeatureType.Value, dfs.FeatureId.Value, reading.ReadingEnd);
                            UpdateConsolidations(notificationList);
                        }
                    }

                    if (EmitEvents)
                    {
                        stage = "energy";
                        EnergyEventStatus status = FindFeatureEventStatus(dfs.FeatureType.Value, dfs.FeatureId.Value);
                        status.SetEventReading(curTime, 0.0, thisPower, (int)duration.TotalSeconds, false);
                        DeviceManager.ManagerManager.ScanForEvents();
                    }
                }
                LastRecordTime = liveReading.TimeStampe;

                stage = "errors";
            }
            catch (Exception e)
            {
                LogMessage("ProcessOneLiveReading - Stage: " + stage + " - Time: " + liveReading.TimeStampe + " - Exception: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }

            return res;
        }

        // curTime is supplied if duration is to be calculated on the fly (live readings)
        // curTime is null if duration is from a history record - standardDuration contains the correct duration
        private TimeSpan EstimateEnergy(Double powerWatts, DateTime curTime, DateTime? prevEstTime, float standardDuration, out Double estEnergy)
        {
            TimeSpan duration;
            if (prevEstTime.HasValue)
                duration = (curTime - prevEstTime.Value);
            else
                duration = TimeSpan.FromSeconds(standardDuration);

            estEnergy = (powerWatts * duration.TotalHours) / 1000.0; // watts to KWH

            //if (GlobalSettings.SystemServices.LogTrace)
            //    GlobalSettings.SystemServices.LogMessage("EstimateEnergy", "Time: " + curTime + " - Power: " + powerWatts +
            //        " - Duration: " + duration.TotalSeconds + " - Energy: " + EstEnergy, LogEntryType.Trace);

            return duration;
        }

        public override bool ProcessOneHistoryReading(CC128_HistoryRecord histReading)
        {
            String stage = "Initial";
            try
            {
                if (!DeviceSettings.UseHistory)
                    return true;

                DeviceDetailPeriods_EnergyMeter days = (DeviceDetailPeriods_EnergyMeter)FindOrCreateFeaturePeriods(FeatureType.EnergyAC, 0);
                DeviceDetailPeriod_EnergyMeter day;
                // detect end of day history entry - applies to previous day
                EnergyReading hist;
                if (histReading.Time.TimeOfDay.TotalHours == 1.0) // 1:00am reading (duration 2 hours) crosses a period boundary
                {
                    stage = "Cross Period Boundary";
                    if (GlobalSettings.SystemServices.LogTrace)
                        DeviceManager.LogMessage("ProcessOneHistoryReading", stage + " - Time: " + histReading.Time
                            + " - Duration: " + histReading.Duration + " - Energy: " + histReading.Energy, LogEntryType.Trace);
                    //histReading.Time = histReading.Time.Date; // end of previous day - midnight
                    histReading.Duration = histReading.Duration / 2; // halve duration and energy readings
                    histReading.Energy = histReading.Energy / 2.0;
                    if (histReading.Calculated.HasValue)
                        histReading.Calculated = histReading.Calculated.Value / 2.0;

                    stage = "Previous Day";
                    if (GlobalSettings.SystemServices.LogTrace)
                        DeviceManager.LogMessage("ProcessOneHistoryReading", stage + " - Time: " + histReading.Time.Date
                            + " - Duration: " + histReading.Duration + " - Energy: " + histReading.Energy, LogEntryType.Trace);
                    
                    day = days.FindOrCreate(histReading.Time.Date.AddDays(-1.0));  // midnight reading applies to previous day
                    hist = new EnergyReading(days, histReading.Time.Date, TimeSpan.FromSeconds(histReading.Duration));
                    hist.EnergyDelta = histReading.Energy;
                    hist.Temperature = histReading.Temperature;
                    day.AdjustFromHistory(hist);
                    stage = "Start Day";
                }
                else
                    stage = "Normal";

                if (GlobalSettings.SystemServices.LogTrace)
                    DeviceManager.LogMessage("ProcessOneHistoryReading", stage + " - Time: " + histReading.Time
                        + " - Duration: " + histReading.Duration + " - Energy: " + histReading.Energy, LogEntryType.Trace);
                
                day = days.FindOrCreate(histReading.Time.Date); // get correct day
                hist = new EnergyReading(days, histReading.Time, TimeSpan.FromSeconds(histReading.Duration));
                hist.EnergyDelta = histReading.Energy;
                hist.Temperature = histReading.Temperature;
                day.AdjustFromHistory(hist);

                // if a device stops sending live readings for an extended period it is possible that the history adjustment can 
                // be created that is more recent than LastRecordTime. This must be adjusted to avoid a record collision if / when the device 
                // sends more readings
                if (LastRecordTime < histReading.Time)
                    LastRecordTime = histReading.Time;

                return true;
            }
            catch (Exception e)
            {
                LogMessage("ProcessOneHistoryReading - Stage: " + stage + " - Exception: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }            
        }

        public override void SplitReadingSub(ReadingBase oldReading, DateTime splitTime, ReadingBase newReading1, ReadingBase newReading2)
        {
        }
    }
}