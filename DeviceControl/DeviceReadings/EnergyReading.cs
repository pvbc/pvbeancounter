﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GenericConnector;
using MackayFisher.Utilities;
using PVSettings;
using PVBCInterfaces;
using GenThreadManagement;

namespace DeviceDataRecorders
{
    public class DeviceParamsBase 
    {
        public PVSettings.DeviceType DeviceType { get; set; }
        public virtual int QueryInterval { get; set; }
        public int RecordingInterval { get; set; }
        public bool EnforceRecordingInterval { get; set; }

        public float CalibrationFactor { get; set; }
        
        public DeviceParamsBase()
        {
            CalibrationFactor = 1.0F;
            DeviceType = PVSettings.DeviceType.Unknown;
            QueryInterval = 6;
            RecordingInterval = 60;
            EnforceRecordingInterval = true;
        }
    }

    public class EnergyParams : DeviceParamsBase
    {       
        public EnergyParams()
        {
        }
    }

    public abstract class EnergyReadingBase : ReadingBaseTyped<EnergyReadingBase>, IComparable<EnergyReadingBase> 
    {
        public static int EnergyPrecision = 5;

        public EnergyReadingBase(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, TimeSpan duration)
            : base()
        {
            Initialise(deviceDetailPeriods, readingEnd, duration, false);
        }

        public EnergyReadingBase(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, TimeSpan duration, bool readFromDb)
            : base()
        {
            Initialise(deviceDetailPeriods, readingEnd, duration, readFromDb);
        }

        public EnergyReadingBase() // Must call Initialise after this
            : base()
        {
        }

        public void Initialise(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, TimeSpan duration, bool readFromDb)
        {
            InitialiseBase(deviceDetailPeriods, readingEnd, duration, readFromDb);
            UseInternalCalibration = false;
            EnergyCalibrationFactor = DeviceParams.CalibrationFactor;
        }

        public void Initialise(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, DateTime readingStart, bool readFromDb)
        {
            InitialiseBase(deviceDetailPeriods, readingEnd, readingStart, readFromDb);
            UseInternalCalibration = false;
            EnergyCalibrationFactor = DeviceParams.CalibrationFactor;
        }

        private float EnergyCalibrationFactorInternal = 1.0F;
        public float EnergyCalibrationFactor
        {
            get { return EnergyCalibrationFactorInternal; }
            set
            {
                EnergyCalibrationFactorInternal = value;
                if (value != 1.0F)
                {
                    UseInternalCalibration = true;
                    Calibrate();
                }
                else
                {
                    UseInternalCalibration = false;
                    CalibrationDelta = null;
                }

            }
        }

        public bool UseInternalCalibration { get; private set; }

        protected override void DurationChanged()
        {
            base.DurationChanged();
        }

        // Comparer allows readings to be sorted
        public static int Compare(EnergyReadingBase x, EnergyReadingBase y)
        {
            if (x == null)
                if (y == null)
                    return 0; // both null
                else
                    return -1; // x is null y is not null
            else if (y == null)
                return 1;  // y is null x is not null

            if (x.ReadingEndInternal.Value > y.ReadingEndInternal.Value)
                return 1;
            else if (x.ReadingEndInternal.Value < y.ReadingEndInternal.Value)
                return -1;

            return 0; // equal
        }

        public Int32 CompareTo(EnergyReadingBase other)
        {
            if (ReadingEnd < other.ReadingEnd)
                return -1;
            else if (ReadingEnd > other.ReadingEnd)
                return 1;
            else return 0;
        }

        protected override bool IsSameReading(EnergyReadingBase other)
        {
            return (this == other);
        }

        protected override bool IsSameReadingValues(EnergyReadingBase other)
        {
            if (ReadingEndInternal.Value != other.ReadingEndInternal.Value
            || DurationInternal != other.DurationInternal
            || FeatureType != other.FeatureType
            || FeatureId != other.FeatureId)
                return false;

            if (VoltsInternal != other.VoltsInternal
            || AmpsInternal != other.AmpsInternal
            || FrequencyInternal != other.FrequencyInternal
            || _PowerInternal != other._PowerInternal
            || EnergyDeltaInternal != other.EnergyDeltaInternal
            || CalibrationDeltaInternal != other.CalibrationDeltaInternal
            || HistEnergyDeltaInternal != other.HistEnergyDeltaInternal
            || TemperatureInternal != other.TemperatureInternal
            || EnergyTodayInternal != other.EnergyTodayInternal
            || EnergyTotalInternal != other.EnergyTotalInternal)
                return false;

            return true;
        }

        public override Double MajorReadingValue { get { return EnergyDelta; } }
        public override Double? MajorHistoryAdjustValue { get { return HistEnergyDelta; } }

        public Double TotalReadingDelta 
        { 
            get 
            {
                return EnergyDelta +
                    (CalibrationDelta.HasValue ? CalibrationDelta.Value : 0.0) +
                    (HistEnergyDelta.HasValue ? HistEnergyDelta.Value : 0.0); 
            } 
        }

        public Double CalibrateableReadingDelta
        {
            get
            {
                if (HistEnergyDelta.HasValue)
                    return EnergyDelta + HistEnergyDelta.Value;
                else
                    return EnergyDelta;
            }
        }

        private float? VoltsInternal = null;
        public float? Volts
        {
            get
            {
                return VoltsInternal;
            }

            set
            {
                VoltsInternal = value;
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        private float? AmpsInternal = null;
        public float? Amps
        {
            get
            {
                return AmpsInternal;
            }

            set
            {
                AmpsInternal = value;
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        private float? FrequencyInternal = null;
        public float? Frequency
        {
            get
            {
                return FrequencyInternal;
            }

            set
            {
                FrequencyInternal = value;
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        private int? _PowerInternal = null;
        public int? Power 
        {
            get
            {
                if (_PowerInternal.HasValue)
                    return _PowerInternal;
                else if (!EnergyDeltaInternal.HasValue && !HistEnergyDeltaInternal.HasValue && !CalibrationDelta.HasValue
                    && VoltsInternal.HasValue && AmpsInternal.HasValue)
                    return (int)(VoltsInternal.Value * AmpsInternal.Value);
                else
                    return AveragePower;
            }

            set
            {
                _PowerInternal = value;
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        public int? PowerInternal { get { return _PowerInternal; } }
        
        public int AveragePower 
        {
            get
            {                
                return (int)((1000.0 * TotalReadingDelta) / Duration.TotalHours);
            }
        }
        
        private Double? EnergyDeltaInternal = null;

        public Double? EnergyDeltaNullable
        {
            set
            {
                EnergyDeltaInternal = value;
            }
        }

        public Double EnergyDelta
        {
            get
            {
                return EnergyDeltaInternal.HasValue ? EnergyDeltaInternal.Value : 0.0; ;                
            }
            set
            {                
                EnergyDeltaInternal = Math.Round(value, EnergyPrecision);       
                                
                if (!AttributeRestoreMode)
                {
                    if (UseInternalCalibration)
                        Calibrate();
                    UpdatePending = true;
                }
            }
        }

        public void ResetEnergyDelta()
        {
            EnergyDeltaInternal = null;
            if (!AttributeRestoreMode)
            {
                if (UseInternalCalibration)
                    CalibrationDelta = null;
                UpdatePending = true;
            }
        }

        private Double? CalibrationDeltaInternal = null;
        public Double? CalibrationDelta
        {
            get
            {
                return CalibrationDeltaInternal;
            }
            set
            {
                bool change = false;
                if (value.HasValue && CalibrationDeltaInternal.HasValue)
                {
                    if (CalibrationDeltaInternal.Value != Math.Round(value.Value, EnergyPrecision + 3))
                        change = true;
                }
                else
                    change = value.HasValue != CalibrationDeltaInternal.HasValue;

                if (change)
                {
                    CalibrationDeltaInternal = (value.HasValue ? (Double?)Math.Round(value.Value, EnergyPrecision + 3) : null);
                    if (!AttributeRestoreMode) UpdatePending = true;
                }
            }
        }

        private void Calibrate()
        {
            if (!UseInternalCalibration)
                return;

            Double? newCalc = CalibrateableReadingDelta;
            if (newCalc.HasValue)
                newCalc = newCalc.Value * EnergyCalibrationFactorInternal - newCalc.Value;
            else
                newCalc = null;

            if (newCalc != CalibrationDeltaInternal)
            {
                CalibrationDelta = newCalc;
            }
        }

        private bool _IsHistoryReading = false;
        public override bool IsHistoryReading() { return _IsHistoryReading; }

        private Double? HistEnergyDeltaInternal = null;
        public Double? HistEnergyDelta
        {
            get
            {
                return HistEnergyDeltaInternal;
            }
            set
            {
                HistEnergyDeltaInternal = (value.HasValue ? (Double?)Math.Round(value.Value, EnergyPrecision) : null);
                if (value.HasValue) _IsHistoryReading = true;
                if (!AttributeRestoreMode)
                {
                    if (UseInternalCalibration)
                        Calibrate();
                    UpdatePending = true;
                }
            }
        }

        private Double? EnergyTotalInternal = null;
        public Double? EnergyTotal
        {
            get
            {
                return EnergyTotalInternal;
            }
            set
            {
                EnergyTotalInternal = (value.HasValue ? (Double?)Math.Round(value.Value, EnergyPrecision) : null);
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        private Double? EnergyTodayInternal = null;
        public Double? EnergyToday
        {
            get
            {
                return EnergyTodayInternal;
            }
            set
            {
                EnergyTodayInternal = (value.HasValue ? (Double?)Math.Round(value.Value, EnergyPrecision) : null);
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        private Int32? ModeInternal = null;
        public Int32? Mode
        {
            get
            {
                return ModeInternal;
            }
            set
            {
                ModeInternal = value;
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        private Int64? ErrorCodeInternal = null;
        public Int64? ErrorCode
        {
            get
            {
                return ErrorCodeInternal;
            }
            set
            {
                ErrorCodeInternal = value;
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        private int? MinPowerInternal = null;
        public virtual int? MinPower
        {
            get
            {
                return MinPowerInternal;
            }

            set
            {
                MinPowerInternal = value;
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        private int? MaxPowerInternal = null;
        public virtual int? MaxPower
        {
            get
            {
                return MaxPowerInternal;
            }

            set
            {
                MaxPowerInternal = value;
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        private float? TemperatureInternal = null;
        public virtual float? Temperature
        {
            get
            {
                return TemperatureInternal;
            }

            set
            {
                TemperatureInternal = (value.HasValue ? (float?)Math.Round(value.Value, EnergyPrecision) : null);
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        public override void ClearHistory()
        {
            if (HistEnergyDeltaInternal.HasValue)
            {
                HistEnergyDeltaInternal = 0.0;
                UpdatePending = true;
            }
        }

        protected virtual void CloneFields(DateTime outputTime, TimeSpan duration, EnergyReadingBase newRec)
        {
            Double factor = (Double)duration.TotalSeconds / DurationInternal.TotalSeconds;
            
            newRec.EnergyTotalInternal = EnergyTotalInternal;
            newRec.EnergyTodayInternal = EnergyTodayInternal;
            if (EnergyDeltaInternal.HasValue)
                newRec.EnergyDeltaInternal = EnergyDeltaInternal * factor;
            else
                newRec.EnergyDeltaInternal = null;
            if (CalibrationDeltaInternal.HasValue)
                newRec.CalibrationDeltaInternal = CalibrationDeltaInternal * factor;
            if (HistEnergyDeltaInternal.HasValue)
                newRec.HistEnergyDeltaInternal = HistEnergyDeltaInternal * factor;
            newRec._PowerInternal = _PowerInternal;
            newRec.ModeInternal = ModeInternal;
            newRec.ErrorCodeInternal = ErrorCodeInternal;
            newRec.VoltsInternal = VoltsInternal;
            newRec.AmpsInternal = AmpsInternal;
            newRec.FrequencyInternal = FrequencyInternal;
            newRec.TemperatureInternal = TemperatureInternal;
            newRec.MinPowerInternal = MinPowerInternal;
            newRec.MaxPowerInternal = MaxPowerInternal;

            // if time is not changed, database presence has not changed
            if (outputTime == ReadingEnd)
                newRec.InDatabase = InDatabase;
            else
                newRec.InDatabase = false;

            newRec.UpdatePending = true;
            
            newRec.SetRestoreComplete();
            Calibrate();
        }

        private void AccumulateMergeCommon(EnergyReadingBase reading, double operationFactor)
        {
            if (reading.EnergyDeltaInternal.HasValue)
                if (EnergyDeltaInternal.HasValue)
                    EnergyDeltaInternal += reading.EnergyDeltaInternal.Value * operationFactor;
                else
                    EnergyDeltaInternal = reading.EnergyDeltaInternal.Value * operationFactor;

            if (reading.CalibrationDeltaInternal.HasValue)
                if (CalibrationDeltaInternal.HasValue)
                    CalibrationDeltaInternal += reading.CalibrationDeltaInternal.Value * operationFactor;
                else
                    CalibrationDeltaInternal = reading.CalibrationDeltaInternal.Value * operationFactor;

            if (reading.HistEnergyDelta.HasValue)
                if (HistEnergyDelta.HasValue)
                    HistEnergyDelta += reading.HistEnergyDelta.Value * operationFactor;
                else
                    HistEnergyDelta = reading.HistEnergyDelta.Value * operationFactor;

            if (GlobalSettings.SystemServices.LogTrace)
                LogRecordDetail(this, "AccumulateMergeCommon", "Result", "");
        }

        public override void MergeReading(ReadingBase readingGeneric, MergeMode mergeMode)
        {
            string stage = "initial";
            try
            {
                EnergyReadingBase reading = (EnergyReadingBase)readingGeneric;

                if (GlobalSettings.SystemServices.LogTrace)
                    LogRecordDetail(reading, "MergeReading", mergeMode.ToString(), 
                        "Merging - To FeatureType: " + FeatureType
                        + " - FeatureId: " + FeatureId
                        + " - From FeatureType: " + reading.FeatureType
                        + " - FeatureId: " + reading.FeatureId);

                if (mergeMode == MergeMode.MergeWithDuration)
                    Duration += reading.DurationInternal;

                stage = "before power";

                if (reading._PowerInternal.HasValue)
                    _PowerInternal = reading._PowerInternal.Value;

                if (reading.MinPower.HasValue)
                {
                    if (MinPower.HasValue)
                        MinPower = Math.Min(MinPower.Value, reading.MinPower.Value);
                    else
                        MinPower = reading.MinPower;
                }
                else if (reading.Power.HasValue)
                    if (MinPower.HasValue)
                        MinPower = Math.Min(MinPower.Value, reading.Power.Value);
                    else
                        MinPower = reading.Power;

                if (reading.MaxPower.HasValue)
                {
                    if (MaxPower.HasValue)
                        MaxPower = Math.Max(MaxPower.Value, reading.MaxPower.Value);
                    else
                        MaxPower = reading.MaxPower;
                }
                else if (reading.Power.HasValue)
                    if (MaxPower.HasValue)
                        MaxPower = Math.Max(MaxPower.Value, reading.Power.Value);
                    else
                        MaxPower = reading.Power;

                if (reading.EnergyToday.HasValue)
                    EnergyToday = reading.EnergyToday.Value;
                else
                    EnergyToday = null;

                if (reading.EnergyTotal.HasValue)
                    EnergyTotal = reading.EnergyTotal.Value;
                else
                    EnergyTotal = null;

                if (reading.Volts.HasValue)
                    Volts = reading.Volts.Value;

                if (reading.ErrorCode.HasValue)
                    ErrorCode = reading.ErrorCode.Value;

                if (reading.Mode.HasValue)
                    Mode = reading.Mode.Value;

                if (reading.Frequency.HasValue)
                    Frequency = reading.Frequency;

                if (reading.Amps.HasValue)
                    Amps = reading.Amps.Value;

                if (reading.Temperature.HasValue)
                    Temperature = reading.Temperature.Value;

                stage = "after power";

                AccumulateMergeCommon(reading, 1.0);
            }
            catch (Exception e)
            {
                GlobalSettings.LogMessage("MergeReading", "Stage: " + stage + " - Exception: " + e.Message, LogEntryType.Information);
                throw e;
            }
        }

        public override void AccumulateReading(ReadingBase readingGeneric, ConsolidationMode consolidationMode, Device.DeviceLink deviceLink)
        {
            double operationFactor;
            bool fieldsOnly = deviceLink.Operation == ConsolidateDeviceSettings.OperationType.Custom 
                || !(readingGeneric is EnergyReadingBase);

            if (deviceLink.Operation == ConsolidateDeviceSettings.OperationType.Subtract)
                operationFactor = -1.0;
            else
                operationFactor = 1.0;
            
            string stage = "initial";
            try
            {
                if (GlobalSettings.SystemServices.LogTrace)
                {
                    LogRecordDetail(this, "AccumulateReading", "InitialTarget", "");
                    readingGeneric.LogRecordDetail(readingGeneric, "AccumulateReading",
                        "AccumulationMode: " + consolidationMode.ToString() + " - Operation: " + deviceLink.Operation.ToString(), "");
                }

                if (!fieldsOnly)
                {
                    EnergyReadingBase reading = (EnergyReadingBase)readingGeneric;

                    stage = "before power";

                    // ConsolidateLast is used for the last reading added to a consolidation interval
                    // Other readings added mid but not end interval just use Consolidate
                    if (!fieldsOnly && consolidationMode == ConsolidationMode.ConsolidateLast)
                    {
                        if (reading.Power.HasValue)
                        {
                            if (_PowerInternal.HasValue)
                                _PowerInternal += (int)(reading.Power.Value * operationFactor);
                            else
                                _PowerInternal = (int)(reading.Power.Value * operationFactor);
                        }

                        if (reading.MinPower.HasValue)
                        {
                            if (MinPower.HasValue)
                                MinPower += (int)(reading.MinPower.Value * operationFactor);
                            else
                                MinPower = (int)(reading.MinPower.Value * operationFactor);
                        }
                        else if (reading.Power.HasValue)
                            if (MinPower.HasValue)
                                MinPower += (int)(reading.Power.Value * operationFactor);
                            else
                                MinPower = (int)(reading.Power.Value * operationFactor);

                        if (reading.MaxPower.HasValue)
                        {
                            if (MaxPower.HasValue)
                                MaxPower = (int)(reading.MaxPower.Value * operationFactor);
                            else
                                MaxPower = (int)(reading.MaxPower.Value * operationFactor);
                        }
                        else if (reading.Power.HasValue)
                            if (MaxPower.HasValue)
                                MaxPower = (int)(reading.Power.Value * operationFactor);
                            else
                                MaxPower = (int)(reading.Power.Value * operationFactor);

                        if (reading.EnergyToday.HasValue)
                            if (EnergyToday.HasValue)
                                EnergyToday += (reading.EnergyToday.Value * operationFactor);
                            else
                                EnergyToday = (reading.EnergyToday.Value * operationFactor);

                        if (reading.EnergyTotal.HasValue)
                            if (EnergyTotal.HasValue)
                                EnergyTotal += (reading.EnergyTotal.Value * operationFactor);
                            else
                                EnergyTotal = (reading.EnergyTotal.Value * operationFactor);
                    }

                    stage = "after power";
                    AccumulateMergeCommon(reading, operationFactor);
                }

                ConsolidateFieldMap(readingGeneric, deviceLink.FieldMap, consolidationMode);
            }
            catch (Exception e)
            {
                GlobalSettings.LogMessage("AccumulateReading", "Stage: " + stage + " - Exception: " + e.Message, LogEntryType.Information);
                throw e;
            }
        }

        public override void LogRecordDetail(ReadingBase readingGeneric, String function, String action, String detail)
        {
            if (!GlobalSettings.SystemServices.LogTrace)
                return;

            EnergyReadingBase reading = (EnergyReadingBase)readingGeneric;
            GlobalSettings.LogMessage(function, action
                + " - DeviceId: " + DeviceDetailPeriods.Device.DeviceId
                + " - Name: " + DeviceDetailPeriods.Device.DeviceManagerDeviceSettings.Name
                + " - FeatureType: " + reading.FeatureType
                + " - FeatureId: " + reading.FeatureId
                + " - ReadingEnd: " + reading.ReadingEnd
                + " - OutputTime: " + reading.ReadingEnd.TimeOfDay.TotalSeconds
                + " - Duration: " + reading.Duration
                + " - EnergyToday: " + reading.EnergyToday
                + " - EnergyTotal: " + reading.EnergyTotal
                + " - EnergyDelta: " + reading.EnergyDelta
                + " - HistEnergyDelta: " + reading.HistEnergyDelta
                + " - CalibrationDelta: " + reading.CalibrationDelta
                + " - TotalReadingDelta: " + reading.TotalReadingDelta
                + " - PowerInternal: " + reading.PowerInternal
                + " - AveragePower: " + reading.AveragePower
                + " - Power: " + reading.Power
                + " - FreqAC: " + reading.Frequency
                + " - Volts: " + reading.Volts
                + " - Current: " + reading.Amps
                + " - Temperature: " + reading.Temperature
                + " - Mode: " + reading.Mode
                + " - ErrorCode: " + reading.ErrorCode
                + (detail == "" ? "" : " - " + detail)
                , LogEntryType.Trace);
        }

        public override int Compare(EnergyReadingBase other, int? precision = null)
        {
            int thisPrecision = (precision.HasValue ? precision.Value : EnergyPrecision) - 2;
            double otherEnergy = Math.Round(other.CalibrateableReadingDelta, thisPrecision);
            double thisEnergy = Math.Round(CalibrateableReadingDelta, thisPrecision);

            if (otherEnergy == thisEnergy)
                return 0;
            if (otherEnergy < thisEnergy)
                return -1;
            else
                return 1;
        }


        #region HistoryCalcs

        public override void HistoryAdjust_Average(EnergyReadingBase actualTotal, EnergyReadingBase histRecord)
        {
            Double histEnergy = histRecord.EnergyDelta;
            double histSeconds = histRecord.Duration.TotalSeconds;
            Double actualEnergy = actualTotal.EnergyDelta;
            double actualSeconds = actualTotal.GetModeratedSeconds(3);

            double secondsDiff = Math.Round(histSeconds - actualSeconds, 1);
            if (secondsDiff < 0.0)
                throw new Exception("EnergyReading.HistoryAdjust_Average - histSeconds < actualSeconds - outputTime: " + ReadingEnd +
                    " - histSeconds: " + histSeconds + " - actualSeconds: " + actualSeconds);

            if (secondsDiff == 0.0)
                return;

            Double adjust = ((histEnergy - actualEnergy) * Duration.TotalSeconds) / secondsDiff;

            if (GlobalSettings.SystemServices.LogTrace)
                GlobalSettings.LogMessage("EnergyReading.HistoryAdjust_Average", "Hist ReadingEnd: " + histRecord.ReadingEnd + " Feature: " + histRecord.FeatureId + "hist.Duration: " + histSeconds + " - actual.Duration: " + actualSeconds
                    + " - histEnergy: " + histEnergy + " - actualEnergy: " + actualEnergy + " - this.Duration: " + Duration.TotalSeconds
                    + " - adjust: " + adjust, LogEntryType.Trace);

            if (Math.Round(adjust, EnergyPrecision - 2) != 0.0) // damp out adjustment oscilliations
                if (HistEnergyDelta.HasValue)
                    HistEnergyDelta += adjust;
                else
                    HistEnergyDelta = adjust;
        }

        public override void HistoryAdjust_Prorata(EnergyReadingBase actualTotal, EnergyReadingBase histRecord)
        {
            double thisReadingDelta = CalibrateableReadingDelta;
            if (thisReadingDelta <= 0.0)
                return;

            Double actualEnergy = actualTotal.CalibrateableReadingDelta;
            if (actualEnergy <= 0.0)
                return;

            Double histEnergy = histRecord.EnergyDelta;
            double histSeconds = histRecord.Duration.TotalSeconds;
            double scaleFactor = histEnergy / actualEnergy;
            double actualSeconds = actualTotal.GetModeratedSeconds(3);

            if (histSeconds < actualSeconds)
                throw new Exception("EnergyReading.HistoryAdjust_Prorata - histSeconds < acualTotal.Seconds - outputTime: " + ReadingEnd + " - FeatureId: " + FeatureId +
                    " - histSeconds: " + histSeconds + " - actualTotal.Seconds: " + actualSeconds);

            Double adjust = (thisReadingDelta * scaleFactor) - thisReadingDelta;

            if (GlobalSettings.SystemServices.LogTrace)
                GlobalSettings.LogMessage("EnergyReading.HistoryAdjust_Prorata", "outputTime: " + ReadingEnd 
                    + " - FeatureId: " + FeatureId + " - actualEnergy: " + actualEnergy
                    + " - histEnergy: " + histEnergy + " - thisEnergyDelta: " + thisReadingDelta
                    + " - adjust: " + adjust + " - scaleFactor: " + scaleFactor, LogEntryType.Trace);

            if (Math.Round(adjust, EnergyPrecision - 2) != 0.0) // damp out adjustment oscilliations
                if (HistEnergyDelta.HasValue)
                    HistEnergyDelta += adjust;
                else
                    HistEnergyDelta = adjust;
        }

        #endregion HistoryCalcs

        #region Persistance

        protected override String InsertDeviceReading
        {
            get
            {
                return
                    "INSERT INTO devicereading_energy " +
                        "( ReadingEnd, DeviceFeature_Id, ReadingStart, EnergyTotal, EnergyToday, EnergyDelta, " +
                        "CalcEnergyDelta, HistEnergyDelta, Mode, ErrorCode, Power, Volts, " +
                        "Amps, Frequency, Temperature, " +
                        "MinPower," +
                        "MaxPower) " +
                    "VALUES " +
                        "(@ReadingEnd, @DeviceFeature_Id, @ReadingStart, @EnergyTotal, @EnergyToday, @EnergyDelta, " +
                        "@CalcEnergyDelta, @HistEnergyDelta, @Mode, @ErrorCode, @Power, @Volts, " +
                        "@Amps, @Frequency, @Temperature, " +
                        "@MinPower, " +
                        "@MaxPower) ";
            }
        }

        protected override String UpdateDeviceReading
        {
            get
            {
                return
                    "UPDATE devicereading_energy set " +
                        "ReadingStart = @ReadingStart, " +
                        "EnergyTotal = @EnergyTotal, " +
                        "EnergyToday = @EnergyToday, " +
                        "EnergyDelta = @EnergyDelta, " +
                        "CalcEnergyDelta = @CalcEnergyDelta, " +
                        "HistEnergyDelta = @HistEnergyDelta, " +
                        "Mode = @Mode, " +
                        "ErrorCode = @ErrorCode, " +
                        "Power = @Power, " +
                        "Volts = @Volts, " +
                        "Amps = @Amps, " +
                        "Frequency = @Frequency, " +
                        "Temperature = @Temperature, " +
                        "MinPower = @MinPower, " +
                        "MaxPower = @MaxPower " +
                    "WHERE " +
                        "ReadingEnd = @ReadingEnd " +
                        "AND DeviceFeature_Id = @DeviceFeature_Id ";
            }
        }

        protected override String DeleteDeviceReading
        {
            get
            {
                return "DELETE from devicereading_energy " +
                        "WHERE " +
                            "ReadingEnd = @ReadingEnd " +
                            "AND DeviceFeature_Id = @DeviceFeature_Id ";
            }
        }

        protected override void SetParameters(GenCommand cmd, int deviceId)
        {
            string stage = "Device_Id";
            try
            {
                SetParametersId(cmd, (Int32)DeviceDetailPeriods.DeviceFeatureId);
                
                stage = "ReadingStart";
                cmd.AddParameterWithValue("@ReadingStart", ReadingStartInternal.Value);
                
                stage = "EnergyTotal";
                cmd.AddRoundedParameterWithValue("@EnergyTotal", EnergyTotalInternal, EnergyPrecision);
                stage = "EnergyToday";
                cmd.AddRoundedParameterWithValue("@EnergyToday", EnergyTodayInternal, EnergyPrecision);
                stage = "EnergyDelta";
                cmd.AddRoundedParameterWithValue("@EnergyDelta", EnergyDeltaInternal, EnergyPrecision);
                stage = "CalcEnergyDelta";
                // use rounding - 6 dp more than adequate - SQLite stores all values as text, too many digits wastes DB space
                cmd.AddRoundedParameterWithValue("@CalcEnergyDelta", CalibrationDeltaInternal, EnergyPrecision);
                stage = "HistEnergyDelta";
                cmd.AddRoundedParameterWithValue("@HistEnergyDelta", HistEnergyDeltaInternal, EnergyPrecision);
                stage = "Mode";
                cmd.AddParameterWithValue("@Mode", ModeInternal);

                stage = "ErrorCode";
                if (ErrorCodeInternal.HasValue)
                    cmd.AddParameterWithValue("@ErrorCode", (long)ErrorCodeInternal.Value);
                else
                    cmd.AddParameterWithValue("@ErrorCode", null);
                
                stage = "Power";
                cmd.AddParameterWithValue("@Power", _PowerInternal);
                
                stage = "Volts";
                cmd.AddRoundedParameterWithValue("@Volts", VoltsInternal, 2);
                
                stage = "Amps";
                cmd.AddRoundedParameterWithValue("@Amps", AmpsInternal, 2);
                
                stage = "Frequency";
                cmd.AddRoundedParameterWithValue("@Frequency", FrequencyInternal, 1);
                stage = "Temp";
                cmd.AddRoundedParameterWithValue("@Temperature", TemperatureInternal, 2);

                stage = "MinPower";
                cmd.AddParameterWithValue("@MinPower", MinPowerInternal);
                
                stage = "MaxPower";
                cmd.AddParameterWithValue("@MaxPower", MaxPowerInternal);
            }
            catch (Exception e)
            {
                throw new Exception("SetParameters - Stage: " + stage + " - Exception: " + e.Message);
            }
        }

        #endregion Persistance
    }

    public class EnergyReading : EnergyReadingBase
    {
        public enum MappableFields
        {
            Temperature = 0,
            Voltage,
            Current,
            Frequency,
            Power,
            Mode,
            ErrorCode,
            enumFieldCount
        }

        private static MappableFieldDescriptor[] MappableFieldsPopulatedInternal = null;
        public static MappableFieldDescriptor[] MappableFieldsPopulated
        {
            get
            {
                if (MappableFieldsPopulatedInternal == null)
                {
                    MappableFieldsPopulatedInternal = new MappableFieldDescriptor[(int)MappableFields.enumFieldCount];
                    for (int i = 0; i < (int)MappableFields.enumFieldCount; i++)
                        MappableFieldsPopulatedInternal[i] = new MappableFieldDescriptor(((MappableFields)i).ToString(), i, true);
                }

                return (MappableFieldDescriptor[])MappableFieldsPopulatedInternal.Clone();
            }
        }

        protected override void InitialiseGettersSetters()
        {
            MappableFieldGetters = new GetFieldDelegate[(int)MappableFields.enumFieldCount];
            MappableFieldGetters[(int)MappableFields.Temperature] = () => Temperature;
            MappableFieldGetters[(int)MappableFields.Voltage] = () => Volts;
            MappableFieldGetters[(int)MappableFields.Current] = () => Amps;
            MappableFieldGetters[(int)MappableFields.Frequency] = () => Frequency;
            MappableFieldGetters[(int)MappableFields.Power] = () => Power;
            MappableFieldGetters[(int)MappableFields.Mode] = () => Mode;
            MappableFieldGetters[(int)MappableFields.ErrorCode] = () => ErrorCode;

            MappableFieldSetters = new SetFieldDelegate[(int)MappableFields.enumFieldCount];
            MappableFieldSetters[(int)MappableFields.Temperature] = (v) => { Temperature = v; };
            MappableFieldSetters[(int)MappableFields.Voltage] = (v) => { Volts = v; };
            MappableFieldSetters[(int)MappableFields.Current] = (v) => { Amps = v; };
            MappableFieldSetters[(int)MappableFields.Frequency] = (v) => { Frequency = v; };
            MappableFieldSetters[(int)MappableFields.Power] = (v) => { Power = (int?)v; };
            MappableFieldSetters[(int)MappableFields.Mode] = (v) => { Mode = (int?)v; };
            MappableFieldSetters[(int)MappableFields.ErrorCode] = (v) => { ErrorCode = (int?)v; };
        }

        public override int MappableFieldIndex(string name, out Device.FieldType fieldType)
        {
            return GetMappableFieldIndex(name, out fieldType);
        }

        public static int GetMappableFieldIndex(string name, out Device.FieldType fieldType)
        {
            for (int i = 0; i <= (int)MappableFields.enumFieldCount; i++ )
                if (((MappableFields)i).ToString() == name)
                {
                    if ((name == "Voltage")
                    || (name == "Power")
                    || (name == "Current"))
                        fieldType = Device.FieldType.AccumulateLast;
                    else
                        fieldType = Device.FieldType.Replace;
                    return i;
                }
            fieldType = Device.FieldType.Replace;
            return -1;
        }

        public EnergyReading(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, TimeSpan duration)
            : base(deviceDetailPeriods, readingEnd, duration)
        {
        }

        public EnergyReading(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, TimeSpan duration, bool readFromDb)
            : base(deviceDetailPeriods, readingEnd, duration, readFromDb)
        {
        }

        public EnergyReading() // Must call Initialise after this
            : base()
        {
        }

        public override EnergyReadingBase Clone(DateTime outputTime, TimeSpan duration)
        {
            EnergyReading newRec = new EnergyReading(DeviceDetailPeriods, outputTime, duration, true);
            CloneFields(outputTime, duration, newRec);

            return newRec;
        }
    }

    public class EnergyReadingConsolidation : EnergyReadingBase
    {
        public enum MappableFields
        {
            Temperature = 0,
            Power,
            Voltage,
            Current,
            Frequency,
            VoltagePVO,
            TemperaturePVO,
            ExtendedData1,
            ExtendedData2,
            ExtendedData3,
            ExtendedData4,
            ExtendedData5,
            ExtendedData6,
            enumFieldCount
        }

        private static MappableFieldDescriptor[] MappableFieldsPopulatedInternal = null;
        public static MappableFieldDescriptor[] MappableFieldsPopulated
        {
            get
            {
                if (MappableFieldsPopulatedInternal == null)
                {
                    MappableFieldsPopulatedInternal = new MappableFieldDescriptor[(int)MappableFields.enumFieldCount];
                    for (int i = 0; i < (int)MappableFields.enumFieldCount; i++)
                        MappableFieldsPopulatedInternal[i] = new MappableFieldDescriptor(((MappableFields)i).ToString(), i, (i < (int)MappableFields.VoltagePVO));
                }

                return (MappableFieldDescriptor[])MappableFieldsPopulatedInternal.Clone();
            }
        }

        protected override void InitialiseGettersSetters()
        {
            MappableFieldGetters = new GetFieldDelegate[(int)MappableFields.enumFieldCount];
            MappableFieldGetters[(int)MappableFields.Temperature] = () => Temperature;
            MappableFieldGetters[(int)MappableFields.Power] = () => Power;
            MappableFieldGetters[(int)MappableFields.Voltage] = () => Volts;
            MappableFieldGetters[(int)MappableFields.Current] = () => Amps;
            MappableFieldGetters[(int)MappableFields.Frequency] = () => Frequency;
            MappableFieldGetters[(int)MappableFields.VoltagePVO] = () => VoltagePVO;
            MappableFieldGetters[(int)MappableFields.TemperaturePVO] = () => TemperaturePVO;
            MappableFieldGetters[(int)MappableFields.ExtendedData1] = () => ExtendedData1;
            MappableFieldGetters[(int)MappableFields.ExtendedData2] = () => ExtendedData2;
            MappableFieldGetters[(int)MappableFields.ExtendedData3] = () => ExtendedData3;
            MappableFieldGetters[(int)MappableFields.ExtendedData4] = () => ExtendedData4;
            MappableFieldGetters[(int)MappableFields.ExtendedData5] = () => ExtendedData5;
            MappableFieldGetters[(int)MappableFields.ExtendedData6] = () => ExtendedData6;

            MappableFieldSetters = new SetFieldDelegate[(int)MappableFields.enumFieldCount];
            MappableFieldSetters[(int)MappableFields.Temperature] = (v) => { Temperature = v; };
            MappableFieldSetters[(int)MappableFields.Power] = (v) => { Power = v.HasValue ? (int)v.Value : (int?)null; };
            MappableFieldSetters[(int)MappableFields.Voltage] = (v) => { Volts = v; };
            MappableFieldSetters[(int)MappableFields.Current] = (v) => { Amps = v; };
            MappableFieldSetters[(int)MappableFields.Frequency] = (v) => { Frequency = v; };
            MappableFieldSetters[(int)MappableFields.TemperaturePVO] = (v) => { TemperaturePVO = v; };
            MappableFieldSetters[(int)MappableFields.VoltagePVO] = (v) => { VoltagePVO = v; };
            MappableFieldSetters[(int)MappableFields.ExtendedData1] = (v) => { ExtendedData1 = v; };
            MappableFieldSetters[(int)MappableFields.ExtendedData2] = (v) => { ExtendedData2 = v; };
            MappableFieldSetters[(int)MappableFields.ExtendedData3] = (v) => { ExtendedData3 = v; };
            MappableFieldSetters[(int)MappableFields.ExtendedData4] = (v) => { ExtendedData4 = v; };
            MappableFieldSetters[(int)MappableFields.ExtendedData5] = (v) => { ExtendedData5 = v; };
            MappableFieldSetters[(int)MappableFields.ExtendedData6] = (v) => { ExtendedData6 = v; };
        }

        public EnergyReadingConsolidation(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, TimeSpan duration)
            : base(deviceDetailPeriods, readingEnd, duration)
        {
            
        }

        public EnergyReadingConsolidation(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, TimeSpan duration, bool readFromDb)
            : base(deviceDetailPeriods, readingEnd, duration, readFromDb)
        {
        }

        public EnergyReadingConsolidation() // Must call Initialise after this
            : base()
        {
        }

        public override int MappableFieldIndex(string name, out Device.FieldType fieldType)
        {
            return GetMappableFieldIndex(name, out fieldType);
        }

        public static int GetMappableFieldIndex(string name, out Device.FieldType fieldType)
        {
            for (int i = 0; i <= (int)MappableFields.enumFieldCount; i++)
                if (((MappableFields)i).ToString() == name)
                {
                    if ((name == "Voltage")
                    || (name == "VolytagePVO")
                    || (name == "Power")
                    || (name == "Current"))
                        fieldType = Device.FieldType.AccumulateLast;
                    else
                        fieldType = Device.FieldType.Replace;
                    return i;
                }
            fieldType = Device.FieldType.Replace;
            return -1;
        }

        public override void AccumulateReading(ReadingBase readingGeneric, ConsolidationMode accumulationMode, Device.DeviceLink deviceLink)
        {
            base.AccumulateReading(readingGeneric, accumulationMode, deviceLink);
        }

        public override EnergyReadingBase Clone(DateTime outputTime, TimeSpan duration)
        {
            EnergyReadingConsolidation newRec = new EnergyReadingConsolidation(DeviceDetailPeriods, outputTime, duration, true);
            CloneFields(outputTime, duration, newRec);

            newRec.TemperaturePVO = TemperaturePVO;
            newRec.VoltagePVO = VoltagePVO;
            newRec.ExtendedData1 = ExtendedData1;
            newRec.ExtendedData2 = ExtendedData2;
            newRec.ExtendedData3 = ExtendedData3;
            newRec.ExtendedData4 = ExtendedData4;
            newRec.ExtendedData5 = ExtendedData5;
            newRec.ExtendedData6 = ExtendedData6;

            return newRec;
        }

        private float? TemperaturePVOInternal = null;
        public float? TemperaturePVO
        {
            get
            {
                return TemperaturePVOInternal;
            }

            set
            {
                TemperaturePVOInternal = value;
            }
        }

        private float? VoltagePVOInternal = null;
        public float? VoltagePVO
        {
            get
            {
                return VoltagePVOInternal;
            }

            set
            {
                VoltagePVOInternal = value;
            }
        }

        private float? ExtendedData1Internal = null;
        public float? ExtendedData1
        {
            get
            {
                return ExtendedData1Internal;
            }

            set
            {
                ExtendedData1Internal = value;
            }
        }

        private float? ExtendedData2Internal = null;
        public float? ExtendedData2
        {
            get
            {
                return ExtendedData2Internal;
            }

            set
            {
                ExtendedData2Internal = value;
            }
        }

        private float? ExtendedData3Internal = null;
        public float? ExtendedData3
        {
            get
            {
                return ExtendedData3Internal;
            }

            set
            {
                ExtendedData3Internal = value;
            }
        }

        private float? ExtendedData4Internal = null;
        public float? ExtendedData4
        {
            get
            {
                return ExtendedData4Internal;
            }

            set
            {
                ExtendedData4Internal = value;
            }
        }

        private float? ExtendedData5Internal = null;
        public float? ExtendedData5
        {
            get
            {
                return ExtendedData5Internal;
            }

            set
            {
                ExtendedData5Internal = value;
            }
        }

        private float? ExtendedData6Internal = null;
        public float? ExtendedData6
        {
            get
            {
                return ExtendedData6Internal;
            }

            set
            {
                ExtendedData6Internal = value;
            }
        }
    }
}

