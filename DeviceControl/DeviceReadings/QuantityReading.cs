﻿/*
* Copyright (c) 2013 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GenericConnector;
using MackayFisher.Utilities;
using PVSettings;
using PVBCInterfaces;
using GenThreadManagement;

namespace DeviceDataRecorders
{
    public abstract class QuantityReadingBase : ReadingBaseTyped<QuantityReadingBase>, IComparable<QuantityReadingBase>
    {
        public enum MappableFields
        {
            QuantityTotal,
            QuantityToday,
            QuantityDelta,
            Rate,
            Mode,
            ErrorCode,
            enumFieldCount
        }

        private static MappableFieldDescriptor[] MappableFieldsPopulatedInternal = null;
        public static MappableFieldDescriptor[] MappableFieldsPopulated
        {
            get
            {
                if (MappableFieldsPopulatedInternal == null)
                {
                    MappableFieldsPopulatedInternal = new MappableFieldDescriptor[(int)MappableFields.enumFieldCount];
                    for (int i = 0; i < (int)MappableFields.enumFieldCount; i++)
                        MappableFieldsPopulatedInternal[i] = new MappableFieldDescriptor(((MappableFields)i).ToString(), i, true);
                }

                return (MappableFieldDescriptor[]) MappableFieldsPopulatedInternal.Clone();
            }
        }

        protected override void InitialiseGettersSetters()
        {
            MappableFieldGetters = new GetFieldDelegate[(int)MappableFields.enumFieldCount];
            MappableFieldGetters[(int)MappableFields.QuantityTotal] = () => (float?)QuantityTotal;
            MappableFieldGetters[(int)MappableFields.QuantityToday] = () => (float?)QuantityTodayCalculated;
            MappableFieldGetters[(int)MappableFields.QuantityDelta] = () => (float?)QuantityDelta;
            MappableFieldGetters[(int)MappableFields.Rate] = () => (float?)Rate;
            MappableFieldGetters[(int)MappableFields.Mode] = () => Mode;
            MappableFieldGetters[(int)MappableFields.ErrorCode] = () => ErrorCode;

            MappableFieldSetters = new SetFieldDelegate[(int)MappableFields.enumFieldCount];
            MappableFieldSetters[(int)MappableFields.QuantityTotal] = (v) => { throw new Exception("Setting QuantityTotal not supported"); };
            MappableFieldSetters[(int)MappableFields.QuantityToday] = (v) => { throw new Exception("Setting QuantityToday not supported"); };
            MappableFieldSetters[(int)MappableFields.QuantityDelta] = (v) => { throw new Exception("Setting QuantityDelta not supported"); };
            MappableFieldSetters[(int)MappableFields.Rate] = (v) => { throw new Exception("Setting Rate not supported"); };
            MappableFieldSetters[(int)MappableFields.Mode] = (v) => { throw new Exception("Setting Mode not supported"); };
            MappableFieldSetters[(int)MappableFields.ErrorCode] = (v) => { throw new Exception("Setting ErrorCode not supported"); };
        }

        public override int MappableFieldIndex(string name, out Device.FieldType fieldType)
        {
            return GetMappableFieldIndex(name, out fieldType);
        }

        public static int GetMappableFieldIndex(string name, out Device.FieldType fieldType)
        {
            for (int i = 0; i <= (int)MappableFields.enumFieldCount; i++)
                if (((MappableFields)i).ToString() == name)
                {
                    if (name == "Rate")
                        fieldType = Device.FieldType.AccumulateLast;
                    else
                        fieldType = Device.FieldType.Replace;
                    return i;
                }
            fieldType = Device.FieldType.Replace;
            return -1;
        }

        public static int QuantityPrecision = 5;

        public QuantityReadingBase(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, TimeSpan duration)
            : base()
        {
            Initialise(deviceDetailPeriods, readingEnd, duration, false);
        }

        public QuantityReadingBase(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, TimeSpan duration, bool readFromDb)
            : base()
        {
            Initialise(deviceDetailPeriods, readingEnd, duration, readFromDb);
        }

        public QuantityReadingBase() // Must call Initialise after this
            : base()
        {
        }

        public void Initialise(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, TimeSpan duration, bool readFromDb)
        {
            InitialiseBase(deviceDetailPeriods, readingEnd, duration, readFromDb);
        }

        public void Initialise(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, DateTime readingStart, bool readFromDb)
        {
            InitialiseBase(deviceDetailPeriods, readingEnd, readingStart, readFromDb);
        }

        protected virtual void CloneFields(DateTime outputTime, TimeSpan duration, QuantityReadingBase newRec)
        {
            Double factor = (Double)duration.TotalSeconds / DurationInternal.TotalSeconds;

            newRec.QuantityTotalInternal = QuantityTotalInternal;
            newRec.QuantityTodayInternal = QuantityTodayInternal;
            if (QuantityDeltaInternal.HasValue)
                newRec.QuantityDeltaInternal = Math.Round(QuantityDeltaInternal.Value * factor, QuantityPrecision);
            else
                newRec.QuantityDeltaInternal = null;

            newRec.RateInternal = RateInternal;
            newRec.ModeInternal = ModeInternal;
            newRec.ErrorCodeInternal = ErrorCodeInternal;

            // if time is not changed, database presence has not changed
            if (outputTime == ReadingEnd)
                newRec.InDatabase = InDatabase;
            else
                newRec.InDatabase = false;

            newRec.UpdatePending = true;

            newRec.SetRestoreComplete();
        }

        private void AccumulateMergeCommon(QuantityReading reading, double operationFactor)
        {
            if (reading.QuantityDeltaInternal.HasValue)
                if (QuantityDeltaInternal.HasValue)
                    QuantityDeltaInternal += reading.QuantityDeltaInternal.Value * operationFactor;
                else
                    QuantityDeltaInternal = reading.QuantityDeltaInternal.Value * operationFactor;

            if (GlobalSettings.SystemServices.LogTrace)
                GlobalSettings.LogMessage("AccumumateMergeCommon", "Result"
                    + " - ReadingEnd: " + ReadingEnd
                    + " - Duration: " + Duration
                    + " - QuantityToday: " + QuantityToday
                    + " - QuantityTotal: " + QuantityTotal
                    + " - QuantityDelta: " + QuantityDelta                    
                    + " - Mode: " + Mode
                    , LogEntryType.Trace);
        }

        public override void MergeReading(ReadingBase readingGeneric, MergeMode mergeMode)
        {
            string stage = "initial";
            try
            {
                QuantityReading reading = (QuantityReading)readingGeneric;

                if (GlobalSettings.SystemServices.LogTrace)
                    GlobalSettings.LogMessage("MergeReading", "Merging - To FeatureType: " + FeatureType
                        + " - FeatureId: " + FeatureId
                        + " - From FeatureType: " + reading.FeatureType
                        + " - FeatureId: " + reading.FeatureId
                        + " - ReadingEnd: " + reading.ReadingEnd
                        + " - Duration: " + reading.Duration
                        + " - QuantityToday: " + reading.QuantityToday
                        + " - QuantityTotal: " + reading.QuantityTotal
                        + " - QuantityDelta: " + reading.QuantityDelta
                        + " - Mode: " + reading.Mode
                        + " - MergeMode: " + mergeMode.ToString()
                        , LogEntryType.Trace);

                if (mergeMode == MergeMode.MergeWithDuration)
                    Duration += reading.DurationInternal;

                stage = "before instant";

                if (reading.RateInternal.HasValue)
                    RateInternal = reading.RateInternal.Value;

                if (reading.QuantityToday.HasValue)
                    QuantityToday = reading.QuantityToday.Value;
                else
                    QuantityToday = null;

                if (reading.QuantityTotal.HasValue)
                    QuantityTotal = reading.QuantityTotal.Value;
                else
                    QuantityTotal = null;

                if (reading.ErrorCode.HasValue)
                    ErrorCode = reading.ErrorCode.Value;

                if (reading.Mode.HasValue)
                    Mode = reading.Mode.Value;

                stage = "after instant";

                AccumulateMergeCommon(reading, 1.0);
            }
            catch (Exception e)
            {
                GlobalSettings.LogMessage("MergeReading", "Stage: " + stage + " - Exception: " + e.Message, LogEntryType.Information);
                throw e;
            }
        }

        public override void AccumulateReading(ReadingBase readingGeneric, ConsolidationMode consolidationMode, Device.DeviceLink deviceLink)
        {
            double operationFactor;
            bool fieldsOnly = deviceLink.Operation == ConsolidateDeviceSettings.OperationType.Custom
               || !(readingGeneric is QuantityReadingBase);

            if (deviceLink.Operation == ConsolidateDeviceSettings.OperationType.Subtract)
                operationFactor = -1.0;
            else
                operationFactor = 1.0;

            string stage = "initial";
            try
            {
                if (GlobalSettings.SystemServices.LogTrace)
                    readingGeneric.LogRecordDetail(readingGeneric, "AccumulateReading", 
                        "AccumulationMode: " + consolidationMode.ToString() + " - Operation: " + deviceLink.Operation.ToString(), "");

                if (!fieldsOnly)
                {
                    QuantityReading reading = (QuantityReading)readingGeneric;

                    stage = "before instant";

                    // ConsolidateLast is used for the last reading added to a consolidation interval
                    // Other readings added mid but not end interval just use Consolidate
                    if (!fieldsOnly && consolidationMode == ConsolidationMode.ConsolidateLast)
                    {
                        if (reading.RateInternal.HasValue)
                            if (RateInternal.HasValue)
                                RateInternal += (reading.RateInternal.Value * operationFactor);
                            else
                                RateInternal = (reading.RateInternal.Value * operationFactor);

                        if (reading.QuantityToday.HasValue)
                            if (QuantityToday.HasValue)
                                QuantityToday += (reading.QuantityToday.Value * operationFactor);
                            else
                                QuantityToday = (reading.QuantityToday.Value * operationFactor);

                        if (reading.QuantityTotal.HasValue)
                            if (QuantityTotal.HasValue)
                                QuantityTotal += (reading.QuantityTotal.Value * operationFactor);
                            else
                                QuantityTotal = (reading.QuantityTotal.Value * operationFactor);

                        if (reading.ErrorCode.HasValue)
                            ErrorCode = reading.ErrorCode.Value;

                        if (reading.Mode.HasValue)
                            Mode = reading.Mode.Value;

                        stage = "after instant";
                    }

                    stage = "after power";
                    AccumulateMergeCommon(reading, operationFactor);
                }

                ConsolidateFieldMap(readingGeneric, deviceLink.FieldMap, consolidationMode);
            }
            catch (Exception e)
            {
                GlobalSettings.LogMessage("AccumulateReading", "Stage: " + stage + " - Exception: " + e.Message, LogEntryType.Information);
                throw e;
            }
        }

        public override void LogRecordDetail(ReadingBase readingGeneric, String function, String action, String detail)
        {      
            QuantityReading reading = (QuantityReading)readingGeneric;
            GlobalSettings.LogMessage(function, action
                + " - DeviceId: " + DeviceDetailPeriods.Device.DeviceId
                + " - Name: " + DeviceDetailPeriods.Device.DeviceManagerDeviceSettings.Name
                + " - FeatureType: " + reading.FeatureType
                + " - FeatureId: " + reading.FeatureId
                + " - ReadingEnd: " + reading.ReadingEnd
                + " - Duration: " + reading.Duration
                + " - QuantityToday: " + reading.QuantityToday
                + " - QuantityTotal: " + reading.QuantityTotal
                + " - QuantityDelta: " + reading.QuantityDelta
                + " - Mode: " + reading.Mode
                + " - ErrorCode: " + reading.ErrorCode
                + (detail == "" ? "" : " - " + detail)
                , LogEntryType.Trace);
        }

        public void ResetQuantityDelta()
        {
            QuantityDeltaInternal = null;
            if (!AttributeRestoreMode)
                UpdatePending = true;
        }

        private Double? QuantityTotalInternal = null;
        public Double? QuantityTotal
        {
            get
            {
                return QuantityTotalInternal;
            }
            set
            {
                QuantityTotalInternal = (value.HasValue ? (Double?)Math.Round(value.Value, QuantityPrecision) : null);
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        private Double? QuantityTodayInternal = null;
        public Double? QuantityToday
        {
            get
            {
                return QuantityTodayInternal;
            }
            set
            {
                QuantityTodayInternal = (value.HasValue ? (Double?)Math.Round(value.Value, QuantityPrecision) : null);
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        private Double? QuantityTodayCalculatedInternal = null;
        public Double? QuantityTodayCalculated
        {
            get
            {
                if (QuantityTodayInternal.HasValue)
                    return QuantityTodayInternal;
                if (QuantityTodayCalculatedInternal.HasValue)
                    return QuantityTodayCalculatedInternal;
                else
                {
                    // only calculate this value once - it should not change
                    if (PeriodBase != null)
                    {
                        QuantityTodayCalculatedInternal = PeriodBase.GetMappableFieldTotal((int)MappableFields.QuantityDelta, ReadingEnd);
                        if (!QuantityTodayCalculatedInternal.HasValue)
                            QuantityTodayCalculatedInternal = 0.0;
                    }
                    return QuantityTodayCalculatedInternal;
                }
            }
        }

        public Double? QuantityDeltaNullable
        {
            get
            {
                return QuantityDeltaInternal;
            }
            set
            {
                QuantityDeltaInternal = value;
            }
        }

        private Double? QuantityDeltaInternal = null;
        public Double QuantityDelta
        {
            get
            {
                return QuantityDeltaInternal.HasValue ? QuantityDeltaInternal.Value : 0.0; ;
            }
            set
            {
                QuantityDeltaInternal = Math.Round(value, QuantityPrecision);
                if (!AttributeRestoreMode)
                {
                    UpdatePending = true;
                }
            }
        }

        // Rate is quantity units per hour
        private Double? RateInternal = null;
        public Double? Rate
        {
            get
            {
                if (RateInternal.HasValue)
                    return RateInternal;
                else
                    return QuantityDelta * 60.0 / Duration.TotalMinutes;
            }
            set
            {
                RateInternal = value;
                if (!AttributeRestoreMode)
                {
                    UpdatePending = true;
                }
            }
        }

        private Int32? ModeInternal = null;
        public Int32? Mode
        {
            get
            {
                return ModeInternal;
            }
            set
            {
                ModeInternal = value;
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        private Int64? ErrorCodeInternal = null;
        public Int64? ErrorCode
        {
            get
            {
                return ErrorCodeInternal;
            }
            set
            {
                ErrorCodeInternal = value;
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        // Comparer allows readings to be sorted
        public static int Compare(QuantityReadingBase x, QuantityReadingBase y)
        {
            if (x == null)
                if (y == null)
                    return 0; // both null
                else
                    return -1; // x is null y is not null
            else if (y == null)
                return 1;  // y is null x is not null

            if (x.ReadingEndInternal.Value > y.ReadingEndInternal.Value)
                return 1;
            else if (x.ReadingEndInternal.Value < y.ReadingEndInternal.Value)
                return -1;

            return 0; // equal
        }

        public Int32 CompareTo(QuantityReadingBase other)
        {
            if (ReadingEnd < other.ReadingEnd)
                return -1;
            else if (ReadingEnd > other.ReadingEnd)
                return 1;
            else return 0;
        }

        public override int Compare(QuantityReadingBase other, int? precision = null)
        {
            int thisPrecision = (precision.HasValue ? precision.Value : QuantityPrecision) - 2;
            double otherEnergy = Math.Round(other.QuantityDelta, thisPrecision);
            double thisEnergy = Math.Round(QuantityDelta, thisPrecision);

            if (otherEnergy == thisEnergy)
                return 0;
            if (otherEnergy < thisEnergy)
                return -1;
            else
                return 1;
        }

        protected override bool IsSameReading(QuantityReadingBase other)
        {
            return (this == other);
        }

        protected override bool IsSameReadingValues(QuantityReadingBase other)
        {
            if (ReadingEndInternal.Value != other.ReadingEndInternal.Value
            || DurationInternal != other.DurationInternal
            || FeatureType != other.FeatureType
            || FeatureId != other.FeatureId)
                return false;

            if (QuantityDeltaInternal != other.QuantityDeltaInternal
            || QuantityTodayInternal != other.QuantityTodayInternal
            || QuantityTotalInternal != other.QuantityTotalInternal)
                return false;

            return true;
        }

        public override Double MajorReadingValue { get { return QuantityDelta; } }
        public override Double? MajorHistoryAdjustValue { get { return null; } }

        private bool _IsHistoryReading = false;
        public override bool IsHistoryReading() { return _IsHistoryReading; }

        public override void ClearHistory()
        {
        }

        #region HistoryCalcs

        public override void HistoryAdjust_Average(QuantityReadingBase actualTotal, QuantityReadingBase histRecord)
        {
        }

        public override void HistoryAdjust_Prorata(QuantityReadingBase actualTotal, QuantityReadingBase histRecord)
        {
        }

        #endregion HistoryCalcs

        #region Persistance

        protected override String InsertDeviceReading
        {
            get
            {
                return
                    "INSERT INTO devicereading_quantity " +
                        "( ReadingEnd, DeviceFeature_Id, ReadingStart, QuantityTotal, QuantityToday, QuantityDelta, " +
                        "Mode, ErrorCode, Rate ) " +
                    "VALUES " +
                        "(@ReadingEnd, @DeviceFeature_Id, @ReadingStart, @QuantityTotal, @QuantityToday, @QuantityDelta, " +
                        "@Mode, @ErrorCode, @Rate ) ";
            }
        }

        protected override String UpdateDeviceReading
        {
            get
            {
                return
                    "UPDATE devicereading_quantity set " +
                        "ReadingStart = @ReadingStart, " +
                        "QuantityTotal = @QuantityTotal, " +
                        "QuantityToday = @QuantityToday, " +
                        "QuantityDelta = @QuantityDelta, " +
                        "Mode = @Mode, " +
                        "ErrorCode = @ErrorCode, " +
                        "Rate = @Rate " +
                    "WHERE " +
                        "ReadingEnd = @ReadingEnd " +
                        "AND DeviceFeature_Id = @DeviceFeature_Id ";
            }
        }

        protected override String DeleteDeviceReading
        {
            get
            {
                return  "DELETE from devicereading_quantity " +
                        "WHERE " +
                            "ReadingEnd = @ReadingEnd " +
                        "AND DeviceFeature_Id = @DeviceFeature_Id ";
            }
        }

        protected override void SetParameters(GenCommand cmd, int deviceId)
        {
            string stage = "Device_Id";
            try
            {
                SetParametersId(cmd, (Int32)DeviceDetailPeriods.DeviceFeatureId);

                stage = "ReadingStart";
                cmd.AddParameterWithValue("@ReadingStart", ReadingStartInternal.Value);

                stage = "QuantityTotal";
                cmd.AddRoundedParameterWithValue("@QuantityTotal", QuantityTotalInternal, QuantityPrecision);
                stage = "QuantityToday";
                cmd.AddRoundedParameterWithValue("@QuantityToday", QuantityTodayInternal, QuantityPrecision);
                stage = "QuantityDelta";
                cmd.AddRoundedParameterWithValue("@QuantityDelta", QuantityDeltaInternal, QuantityPrecision);
          
                stage = "Mode";
                cmd.AddParameterWithValue("@Mode", ModeInternal);

                stage = "ErrorCode";
                if (ErrorCodeInternal.HasValue)
                    cmd.AddParameterWithValue("@ErrorCode", (long)ErrorCodeInternal.Value);
                else
                    cmd.AddParameterWithValue("@ErrorCode", null);

                stage = "Rate";
                cmd.AddParameterWithValue("@Rate", RateInternal);
            }
            catch (Exception e)
            {
                throw new Exception("SetParameters - Stage: " + stage + " - Exception: " + e.Message);
            }
        }

        #endregion Persistance
    }

    public class QuantityReading : QuantityReadingBase
    {
        public QuantityReading(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, TimeSpan duration)
            : base(deviceDetailPeriods, readingEnd, duration)
        {
        }

        public QuantityReading(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, TimeSpan duration, bool readFromDb)
            : base(deviceDetailPeriods, readingEnd, duration, readFromDb)
        {
        }

        public QuantityReading() // Must call Initialise after this
            : base()
        {
        }

        public override QuantityReadingBase Clone(DateTime outputTime, TimeSpan duration)
        {
            QuantityReading newRec = new QuantityReading(DeviceDetailPeriods, outputTime, duration, true);
            CloneFields(outputTime, duration, newRec);

            return newRec;
        }
    }

    public class QuantityReadingConsolidation : QuantityReadingBase
    {
        public QuantityReadingConsolidation(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, TimeSpan duration)
            : base(deviceDetailPeriods, readingEnd, duration)
        {

        }

        public QuantityReadingConsolidation(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, TimeSpan duration, bool readFromDb)
            : base(deviceDetailPeriods, readingEnd, duration, readFromDb)
        {
        }

        public QuantityReadingConsolidation() // Must call Initialise after this
            : base()
        {
        }

        public override QuantityReadingBase Clone(DateTime outputTime, TimeSpan duration)
        {
            QuantityReadingConsolidation newRec = new QuantityReadingConsolidation(DeviceDetailPeriods, outputTime, duration, true);
            CloneFields(outputTime, duration, newRec);

            return newRec;
        }
    }
}
