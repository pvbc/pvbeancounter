﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

// Was PVReading

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GenericConnector;
using MackayFisher.Utilities;
using PVSettings;
using PVBCInterfaces;

namespace DeviceDataRecorders
{
    public enum MergeMode
    {
        Merge,              // used for operations on individual device readings - merge / interval boundary normalisation
        MergeWithDuration,  // used for operations on individual device readings - merge / interval boundary normalisation also accumulate duration
    }

    public enum ConsolidationMode
    {
        Consolidate,        // used to consolidate readings that ARE NOT last in the target consolidation interval
        ConsolidateLast     // used to consolidate readings that ARE last in the target consolidation interval
    }

    public struct MappableFieldDescriptor
    {
        public String Name { get; private set; }
        public int Index { get; private set; }
        public bool AlwaysAvailable { get; private set; }
        public bool IsMappingTarget { get; set; }
        public bool Available { get { return AlwaysAvailable || IsMappingTarget; } }

        public MappableFieldDescriptor(String name, int index, bool alwaysAvailable) : this()
        {
            Name = name;
            Index = index;
            AlwaysAvailable = alwaysAvailable;
            IsMappingTarget = false;
        }
    }

    public abstract class ReadingBase
    {
        public delegate void SetFieldDelegate(float? value);
        public delegate float? GetFieldDelegate();

        public GetFieldDelegate[] MappableFieldGetters;
        public SetFieldDelegate[] MappableFieldSetters;

        protected abstract void InitialiseGettersSetters();

        public abstract int MappableFieldIndex(string name, out Device.FieldType fieldType);

        private bool InDatabaseInternal = false;
        public bool InDatabase // true when this value already exists in DB
        {
            get { return InDatabaseInternal; }
            set { InDatabaseInternal = value; }
        }

        // AddReading sets this true for all readings that match an existing reading or are new readings
        // Used to allow auto removal of history readings that no longer exist in the external source e.g. SMA Cunny Explorer data
        public bool? AddReadingMatch = null; 

        private bool IsConsolidationReadingInternal = false;

        public bool IsConsolidationReading
        {
            get
            {
                return IsConsolidationReadingInternal;
            }
            set
            {
                IsConsolidationReadingInternal = value;
                if (value)  // consolidation readings are not stored in the database
                    UpdatePendingInternal = false;
            }
        }

        private bool UpdatePendingInternal = false;

        public bool UpdatePending   // true when database writes are outstanding
        {
            get { return UpdatePendingInternal; }
            protected set
            {
                if (IsConsolidationReading)  // consolidation readings are not stored in the database
                    UpdatePendingInternal = false;
                else
                {
                    UpdatePendingInternal = value;
                    if (value)
                    {
                        if (PeriodBase != null)
                            PeriodBase.PeriodIsDirty();
                        DeviceDetailPeriods.Device.NotifyUpdatedPeriod(ReadingEnd, ReadingStart);
                    }
                }
            }
        }
        public bool AttributeRestoreMode { get; protected set; }    // true when loading values from persistent store

        protected DeviceDataRecorders.DeviceParamsBase DeviceParams;

        protected DeviceDetailPeriodBase PeriodBase { get; private set; }

        public DeviceDetailPeriodsBase DeviceDetailPeriods { get; private set; }

        public ReadingBase() // must call InitialiseBase after this
        {
            DeviceParams = null;
            DeviceDetailPeriods = null;
            PeriodBase = null;
            InitialiseGettersSetters();
        }

        public void RegisterReadingPeriod(DeviceDetailPeriodBase period)
        {
            PeriodBase = period;
        }

        public float? GetMappableFieldValue(int field) { return MappableFieldGetters[field](); }

        public void SetMappableFieldValue(int field, float? value) { MappableFieldSetters[field](value); }

        protected void InitialiseBase(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, TimeSpan duration, bool readFromDb)
        {
            DeviceDetailPeriods = deviceDetailPeriods;
            AttributeRestoreMode = readFromDb;
            ReadingEndInternal.Value = readingEnd;
            Duration = duration;
            InDatabase = readFromDb;
            UpdatePending = !readFromDb;
            DeviceParams = DeviceDetailPeriods.Device.DeviceParams;
        }

        protected void InitialiseBase(DeviceDetailPeriodsBase deviceDetailPeriods, DateTime readingEnd, DateTime readingStart, bool readFromDb)
        {
            DeviceDetailPeriods = deviceDetailPeriods;
            AttributeRestoreMode = readFromDb;
            ReadingEndInternal.Value = readingEnd;
            ReadingStartInternal.Value = readingStart;
            DurationInternal = ReadingEndInternal.Value - ReadingStartInternal.Value;
            InDatabase = readFromDb;
            UpdatePending = !readFromDb;
            DeviceParams = DeviceDetailPeriods.Device.DeviceParams;
        }
        
        protected DBDateTimeGeneric ReadingStartInternal;
        public virtual DateTime ReadingStart
        {
            get
            {
                return ReadingStartInternal.Value;
            }
            set
            {
                if (value > ReadingEndInternal.Value)
                    throw new Exception("ReadingBase.ReadingStart - Attempt to set start after end");
                ReadingStartInternal.Value = value;
                DurationInternal = ReadingEndInternal.Value - value;
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        public FeatureType FeatureType { get { return DeviceDetailPeriods.FeatureType; } }
        public uint FeatureId { get { return DeviceDetailPeriods.FeatureId; } }
        
        protected DBDateTimeGeneric ReadingEndInternal;
        public virtual DateTime ReadingEnd
        {
            get
            {
                return ReadingEndInternal.Value;
            }
        }

        protected TimeSpan DurationInternal = TimeSpan.Zero;
        public virtual TimeSpan Duration
        {
            get
            {
                return DurationInternal;
            }
            set
            {
                DurationInternal = value;
                DurationChanged();
                if (!AttributeRestoreMode) UpdatePending = true;
            }
        }

        public virtual double GetModeratedSeconds(int precision)
        {
            return Math.Round(DurationInternal.TotalSeconds, precision);
        }

        protected virtual void DurationChanged()
        {
            ReadingStartInternal.Value = ReadingEndInternal.Value - DurationInternal;
            DurationInternal = ReadingEndInternal.Value - ReadingStartInternal.Value; // consistency after possible date precision adjustments - DBDateTimeGeneric
        }

        public virtual void SetRestoreComplete()
        {
            AttributeRestoreMode = false;
        }

        public abstract void ClearHistory();

        public abstract void LogRecordDetail(ReadingBase readingGeneric, String function, String action, String detail);
        protected abstract String DeleteDeviceReading { get; }
        protected abstract String InsertDeviceReading { get; }
        protected abstract String UpdateDeviceReading { get; }
        protected abstract void SetParameters(GenCommand cmd, int deviceId);

        public abstract bool IsSameReadingValuesGeneric(ReadingBase other);
        public abstract bool IsHistoryReading();
        public abstract ReadingBase CloneGeneric(DateTime outputTime, TimeSpan duration);
        public abstract void AccumulateReading(ReadingBase reading, ConsolidationMode consolidationMode, Device.DeviceLink deviceLink );
        public abstract void MergeReading(ReadingBase reading, MergeMode consolidationMode);

        public abstract Double MajorReadingValue { get; }
        public abstract Double? MajorHistoryAdjustValue { get; }

        protected void SetParametersId(GenCommand cmd, Int32 deviceFeature_Id)
        {
            string stage = "Id";
            try
            {
                Int32 tempId = deviceFeature_Id;
                DateTime dateTime = ReadingEndInternal.Value;
                cmd.AddParameterWithValue("@DeviceFeature_Id", tempId);
                cmd.AddParameterWithValue("@ReadingEnd", dateTime);
            }
            catch (Exception e)
            {
                throw new Exception("SetParametersId - Stage: " + stage + " - Exception: " + e.Message);
            }
        }

        public bool DeleteReading(GenConnection existingCon, int deviceId)
        {
            if (!InDatabase)
                return false;

            string stage = "Init";
            GenConnection con = null;
            bool haveMutex = false;
            bool externalCon = (existingCon != null);

            try
            {
                GlobalSettings.SystemServices.GetDatabaseMutex();
                haveMutex = true;
                if (externalCon)
                    con = existingCon;
                else
                    con = GlobalSettings.TheDB.NewConnection();

                GenCommand cmd = new GenCommand(DeleteDeviceReading, con);

                SetParametersId(cmd, (int)DeviceDetailPeriods.DeviceFeatureId);

                stage = "Execute";
                cmd.ExecuteNonQuery();

                if (GlobalSettings.SystemServices.LogTrace)
                    LogRecordDetail(this, "DeleteReading", "Delete", "");
                    
                UpdatePending = false;
                InDatabase = false;
                return true;
            }
            catch (Exception e)
            {
                GlobalSettings.LogMessage("ReadingBase.DeleteReading", "Stage: " + stage + " - Exception: " + e.Message, LogEntryType.Information);
            }
            finally
            {
                if (existingCon == null && con != null)
                {
                    con.Close();
                    con.Dispose();
                }
                if (haveMutex)
                    GlobalSettings.SystemServices.ReleaseDatabaseMutex();
            }
            return false;
        }

        public bool PersistReading(GenConnection existingCon, int deviceId)
        {
            //GlobalSettings.LogMessage("ReadingBase", "PersistReading - Starting", LogEntryType.Trace);
            string stage = "Init";
            GenConnection con = null;
            bool haveMutex = false;
            bool externalCon = (existingCon != null);
            bool useInsert = !InDatabase;
            //if (forceAlternate)
            //    useInsert = !useInsert;

            try
            {
                //GlobalSettings.LogMessage("ReadingBase", "PersistReading - Before Connection", LogEntryType.Trace);
                GlobalSettings.SystemServices.GetDatabaseMutex();
                haveMutex = true;
                if (externalCon)
                    con = existingCon;
                else
                    con = GlobalSettings.TheDB.NewConnection();

                //GlobalSettings.LogMessage("ReadingBase", "PersistReading - Before Command", LogEntryType.Trace);
                GenCommand cmd;

                if (useInsert)
                    cmd = new GenCommand(InsertDeviceReading, con);
                else
                    cmd = new GenCommand(UpdateDeviceReading, con);

                //GlobalSettings.LogMessage("ReadingBase", "PersistReading - Before SetParameters", LogEntryType.Trace);
                SetParameters(cmd, deviceId);

                stage = "Execute";
                //GlobalSettings.LogMessage("ReadingBase", "PersistReading - Before Execute", LogEntryType.Trace);
                cmd.ExecuteNonQuery();

                if (GlobalSettings.SystemServices.LogTrace)
                    LogRecordDetail(this, "PersistReading", (useInsert ? "Insert" : "Update"), "");
                    
                UpdatePending = false;
                InDatabase = true;
                return true;
            }
            catch (Exception e)
            {
                GlobalSettings.LogMessage("ReadingBase.PersistReading - ", (useInsert ? "Insert" : "Update") + " - Stage: " 
                    + stage + " - Exception: " + e.Message, LogEntryType.Trace);
            }
            finally
            {
                if (existingCon == null && con != null)
                {
                    con.Close();
                    con.Dispose();
                }
                if (haveMutex)
                    GlobalSettings.SystemServices.ReleaseDatabaseMutex();
            }
            return false;
        }
    }

    public abstract class ReadingBaseTyped<TDeviceReading> : ReadingBase 
        where TDeviceReading : ReadingBase
    {
        public ReadingBaseTyped()
            : base()
        {
        }      
        
        public abstract void HistoryAdjust_Average(TDeviceReading actualTotal, TDeviceReading histRecord);
        public abstract void HistoryAdjust_Prorata(TDeviceReading actualTotal, TDeviceReading histRecord);

        protected abstract bool IsSameReading(TDeviceReading other);
        public override bool IsSameReadingValuesGeneric(ReadingBase other)
        {
            return IsSameReadingValues((TDeviceReading)other);
        }
        protected abstract bool IsSameReadingValues(TDeviceReading other);

        public override ReadingBase CloneGeneric(DateTime outputTime, TimeSpan duration)
        {
            return Clone(outputTime, duration);
        }
        public abstract TDeviceReading Clone(DateTime outputTime, TimeSpan duration);
        public abstract int Compare(TDeviceReading other, int? precision = null);

        protected void ConsolidateFieldMap(ReadingBase reading, List<Device.FieldMapEntry> fieldMap, ConsolidationMode consolidationMode)
        {
            foreach (Device.FieldMapEntry fme in fieldMap)
            {
                float? value = reading.GetMappableFieldValue(fme.FromFieldIndex);
                if (fme.Operation == ConsolidateDeviceSettings.OperationType.Replace || (fme.FieldType == Device.FieldType.Replace && fme.Operation == ConsolidateDeviceSettings.OperationType.Add))
                {
                    if (value.HasValue)
                        SetMappableFieldValue(fme.ToFieldIndex, value);
                }
                else if (fme.Operation == ConsolidateDeviceSettings.OperationType.Negate || (fme.FieldType == Device.FieldType.Replace && fme.Operation == ConsolidateDeviceSettings.OperationType.Subtract))
                {
                    if (value.HasValue)
                    {
                        value = -value.Value;
                        SetMappableFieldValue(fme.ToFieldIndex, value);
                    }
                }
                else if (value.HasValue && (consolidationMode == ConsolidationMode.ConsolidateLast || fme.FieldType == Device.FieldType.AlwaysAccumulate))
                {
                    float? toValue = GetMappableFieldValue(fme.ToFieldIndex);
                    if (toValue.HasValue)
                    {
                        if (fme.Operation == ConsolidateDeviceSettings.OperationType.Subtract)
                            value = toValue.Value - value.Value;
                        else
                            value = toValue.Value + value.Value;
                    }
                    else if (fme.Operation == ConsolidateDeviceSettings.OperationType.Subtract)
                        value = -value.Value;
                    SetMappableFieldValue(fme.ToFieldIndex, value);
                }
            }
        }

    }
}
