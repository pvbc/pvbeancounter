﻿/*
* Copyright (c) 2011 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using PVSettings;
using MackayFisher.Utilities;
using PVBCInterfaces;

namespace DeviceControl
{
    public class EnergyEvents : IEvents
    {
        public enum PendingType
        {
            Status = 0,
            Energy = 1,
            EventType = 2
        }

        public struct EventPending
        {
            public PendingType PendingType;
            public String EmitEventType;
            public String Name;
            public DateTime EventTime;
            public Double EnergyToday;
            public int CurrentPower;
            public String StatusText;
            public String StatusType;
        }

        private ApplicationSettings ApplicationSettings;
        private SystemServices SystemServices;

        public ManualResetEvent PVEventReadyEvent { get; private set; }
        
        private List<EventPending> PendingEvents;

        private Mutex NodeUpdateMutex;
        private Mutex PendingListMutex;

        DateTime LastEmitErrorReported;

        IDeviceManagerManager ManagerManager;

        public int MaxConsolidationDepth { get; set; }

        public EnergyEvents(ApplicationSettings settings, IDeviceManagerManager managerManager)
        {
            ApplicationSettings = settings;
            ManagerManager = managerManager;
            SystemServices = GlobalSettings.SystemServices;
            PVEventReadyEvent = new ManualResetEvent(false);
            
            NodeUpdateMutex = new Mutex();
            PendingListMutex = new Mutex();
            
            PendingEvents = new List<EventPending>();
            LastEmitErrorReported = DateTime.MinValue;
            MaxConsolidationDepth = 0;
        }

        private void EmitDeviceEvents(Device.EnergyEventStatus status)
        {
            bool havePendingMutex = false;
            int lastPower = status.LastPowerEmitted;
            Double lastEnergy = status.LastEnergyEmitted;

            Double energyToday;
            int currentPower;

            status.GetCurrentReading(status.LastEventTime, out energyToday, out currentPower);
            
            foreach (Device.DeviceEventConfig e in status.EmitEvents)
            {
                if (SystemServices.LogEvent)
                    SystemServices.LogMessage("ScanForEvents", " - Name: " + e.EventName +
                        " - Type: " + e.EventType, LogEntryType.Event);

                EventPending pend;
                pend.PendingType = PendingType.Energy;
                pend.Name = e.EventName;
                pend.EmitEventType = e.EventType.ToString();
                pend.EventTime = status.LastEventTime;
                pend.EnergyToday = energyToday;
                pend.CurrentPower = currentPower;
                pend.StatusText = "";
                pend.StatusType = "";
                // Queue pending events so that they are sent outside the Mutex lock
                // allow more events to be recorded without extended thread block
                try
                {
                    havePendingMutex = PendingListMutex.WaitOne();
                    PendingEvents.Add(pend);
                }
                finally
                {
                    if (havePendingMutex)
                    {
                        PendingListMutex.ReleaseMutex();
                        havePendingMutex = false;
                    }
                }
                SystemServices.LogMessage("ScanForEvents", "Event queued", LogEntryType.Event);
            }           
        }

        private void FindDeviceEvents(Device.DeviceBase dev)
        {
            // mark next hierarchy level as having an event ready
            foreach (Device.DeviceLink link in dev.TargetDevices)
            {
                if (link.FromEventStatus.LastEventTime > link.FromEventStatus.LastEmitTime)
                {
                    if (link.ToEventStatus.LastEventTime < link.FromEventStatus.LastEventTime)
                        link.ToEventStatus.LastEventTime = link.FromEventStatus.LastEventTime;
                }
            }

            // for consolidation devices - pull up latest values from the source devices
            if (dev.DeviceType == DeviceType.Consolidation)
                foreach (Device.DeviceLink link in ((Device.ConsolidationDevice)dev).SourceDevices)
                {
                    if (link.ToEventStatus.LastEventTime > link.ToEventStatus.LastEmitTime)
                    {
                        if (link.Operation == ConsolidateDeviceSettings.OperationType.Add)
                        {
                            link.ToEventStatus.EnergyTotal += link.FromEventStatus.EnergyTotal;
                            link.ToEventStatus.EventPower += link.FromEventStatus.EventPower;
                        }
                        else if (link.Operation == ConsolidateDeviceSettings.OperationType.Subtract)
                        {
                            link.ToEventStatus.EnergyTotal -= link.FromEventStatus.EnergyTotal;
                            link.ToEventStatus.EventPower -= link.FromEventStatus.EventPower;
                        }
                        else if (link.Operation == ConsolidateDeviceSettings.OperationType.Replace)
                        {
                            link.ToEventStatus.EnergyTotal = link.FromEventStatus.EnergyTotal;
                            link.ToEventStatus.EventPower = link.FromEventStatus.EventPower;
                        }
                    }
                }

            foreach (Device.EnergyEventStatus status in dev.EventStatusList)
            {
                if (status.LastEventTime > status.LastEmitTime)
                {
                    if (status.EmitEvents.Count > 0)
                        EmitDeviceEvents(status);   // data to emit is available here  
                    status.LastEmitTime = status.LastEventTime;
                }
            }         
        }

        // place new event times on impacted consolidation devices
        private void InitialiseHierarchy(Device.DeviceLink link, DateTime newEventTime)
        {
            link.ToEventStatus.EnergyTotal = 0.0;
            link.ToEventStatus.EventPower = 0;

            if (link.ToEventStatus.LastEventTime < newEventTime)
                link.ToEventStatus.LastEventTime = newEventTime; // copy in new event time from source device
            foreach (Device.DeviceLink l in link.ToDevice.TargetDevices)
                if (l.FromEventStatus == link.ToEventStatus)  // push event time change up the hiererchy
                    InitialiseHierarchy(l, newEventTime);
        }

        public void ScanForEvents()
        {
            bool haveMutex = false;  
    
            try
            {
                haveMutex = NodeUpdateMutex.WaitOne();

                // detect changes in source devices and mark impacted consolidations with the updated event time from their sources
                // this marks all relevant devices as requiring event emit
                foreach (DeviceManagerBase mgr in ManagerManager.RunningDeviceManagers)
                {
                    foreach (Device.DeviceBase dev in mgr.GenericDeviceList)
                        if (dev.HierarchyDepth == 0)  // select bottom level / real devices
                        {
                            foreach (Device.DeviceLink link in dev.TargetDevices)
                                if (link.FromEventStatus.LastEmitTime < link.FromEventStatus.LastEventTime)  // event is ready 
                                    InitialiseHierarchy(link, link.FromEventStatus.LastEventTime);
                        }
                }
                // calculate hierarchy from bottom up - start with real devices
                // start at level 0 - this will process all real devices / devices without consolidations
                // level 1 - all consolidations with 1 level of hierarchy
                // level 2 .....
                for (int level = 0; level <= MaxConsolidationDepth; level++)
                {  
                    if (level == 0)
                        foreach (DeviceManagerBase mgr in ManagerManager.RunningDeviceManagers)
                        {
                            foreach (Device.DeviceBase dev in mgr.GenericDeviceList)
                                if (dev.HierarchyDepth == level)
                                    FindDeviceEvents(dev);
                        } 
                    else  // Consolidation Device Managers are not in the running list - as they do not run in a dedicated thread
                        foreach (DeviceManagerBase mgr in ManagerManager.ConsolidationDeviceManagers)
                        {
                            foreach (Device.DeviceBase dev in mgr.GenericDeviceList)
                                if (dev.HierarchyDepth == level)
                                    FindDeviceEvents(dev);
                        } 
                }
            }
            catch (Exception e)
            {
                SystemServices.LogMessage("ScanForEvents", "Exception: " + e.Message, LogEntryType.ErrorMessage);
            }
            finally
            {
                if (haveMutex)
                    NodeUpdateMutex.ReleaseMutex();
            }

            EmitPendingEvents();
        }

        public void NewStatusEvent(String statusType, String statusText)
        {
            bool havePendingMutex = false;
            try
            {
                havePendingMutex = PendingListMutex.WaitOne();

                EventPending pend;
                pend.PendingType = PendingType.Status;
                pend.Name = "";
                pend.EmitEventType = "";
                pend.EventTime = DateTime.Now;
                pend.CurrentPower = 0;
                pend.EnergyToday = 0.0;
                pend.StatusType = statusType;
                pend.StatusText = statusText;
                PendingEvents.Add(pend);
                SystemServices.LogMessage("ScanForEvents", "Status Event queued - text: " + statusText, LogEntryType.Event);
            }
            catch (Exception e)
            {
                SystemServices.LogMessage("SendStatusEvent", "Exception: " + e.Message, LogEntryType.ErrorMessage);
            }
            finally
            {
                if (havePendingMutex)
                    PendingListMutex.ReleaseMutex();
            }

            EmitPendingEvents();
        }

        private void EmitPendingEvents()
        {
            bool havePendingMutex = false;
            EnergyEventsProxy proxy = null;            
            try
            {
                proxy = new EnergyEventsProxy();
                EventPending node;
                while(PendingEvents.Count > 0)
                {
                    // lock out access briefly so that event add to list proceeds unhindered
                    havePendingMutex = PendingListMutex.WaitOne();
                    node = PendingEvents[0];
                    PendingEvents.RemoveAt(0);
                    if (havePendingMutex)
                    {
                        PendingListMutex.ReleaseMutex();
                        havePendingMutex = false;
                    }

                    EnergyEventsEventId id;
                    id.Name = node.Name;
                    

                    if (node.PendingType == PendingType.Energy)
                    {
                        if (node.EmitEventType == "Yield")
                            proxy.OnYieldEvent(id, node.EventTime, node.EnergyToday, node.CurrentPower);
                        else if (node.EmitEventType == "Consumption")
                            proxy.OnConsumptionEvent(id, node.EventTime, node.EnergyToday, node.CurrentPower);
                        else
                            proxy.OnEnergyEvent(id, node.EventTime, node.EnergyToday, node.CurrentPower);

                        if (SystemServices.LogEvent)
                            SystemServices.LogMessage("EmitPendingEvents", "Type: " + node.EmitEventType + " - Name: " + node.Name + " - Time: " + node.EventTime +
                                " - Power: " + node.CurrentPower + " - Energy: " + node.EnergyToday, LogEntryType.Event);
                    }
                    else if (node.PendingType == PendingType.Status)
                    {
                        proxy.OnStatusChangeEvent(node.StatusType, node.EventTime, node.StatusText);
                        if (SystemServices.LogEvent)
                            SystemServices.LogMessage("EmitPendingEvents", "StatusType: " + node.StatusType + " - Time: " + node.EventTime +
                                " - StatusText: " + node.StatusText, LogEntryType.Event);
                    }
                }
            }
            catch (Exception e)
            {
                // Limit logging to one per minute
                if (LastEmitErrorReported < DateTime.Now.AddMinutes(-1.0))
                {
                    SystemServices.LogMessage("EmitPendingEvents", "Exception: " + e.Message, LogEntryType.ErrorMessage);
                    LastEmitErrorReported = DateTime.Now;
                }
                PendingEvents.Clear();
            }

            try
            {
                if (havePendingMutex)
                    PendingListMutex.ReleaseMutex();

                if (proxy != null)
                    proxy.Close();
            }
            catch (Exception e)
            {
                SystemServices.LogMessage("EmitPendingEvents", "Closing proxy - Exception: " + e.Message, LogEntryType.ErrorMessage);
            }
        }

        private void EmitManagerEventTypes(DeviceManagerBase mgr, EnergyEventsEventInfo[] eventTypes, ref int index)
        {
            foreach (Device.DeviceBase dev in mgr.GenericDeviceList)
                foreach (Device.EnergyEventStatus status in dev.EventStatusList)
                    foreach (Device.DeviceEventConfig eEvent in status.EmitEvents)
                    {
                        eventTypes[index].FeatureType = eEvent.FeatureType.ToString();
                        eventTypes[index].Id.Name = eEvent.EventName;
                        eventTypes[index].EventType = eEvent.EventType.ToString();
                        //eventTypes[index].Description = "";
                        eventTypes[index].FeedInYield = eEvent.UseForFeedIn && eEvent.EventType == EventType.Yield;
                        eventTypes[index].FeedInConsumption = eEvent.UseForFeedIn && eEvent.EventType == EventType.Consumption;
                        index++;
                    }
        }

        public void EmitEventTypes(bool updatedEvents)
        {
            EnergyEventsProxy proxy = null;
            EnergyEventsEventInfo[] eventTypes;

            try
            {
                proxy = new EnergyEventsProxy();
                int count = 0;
                foreach (DeviceManagerBase mgr in ManagerManager.RunningDeviceManagers)                
                    foreach (Device.DeviceBase dev in mgr.GenericDeviceList)                    
                        foreach (Device.EnergyEventStatus status in dev.EventStatusList)                        
                            count += status.EmitEvents.Count;
                foreach (DeviceManagerBase mgr in ManagerManager.ConsolidationDeviceManagers)
                    foreach (Device.DeviceBase dev in mgr.GenericDeviceList)
                        foreach (Device.EnergyEventStatus status in dev.EventStatusList)
                            count += status.EmitEvents.Count;
                            
                eventTypes = new EnergyEventsEventInfo[count];
                if (count > 0)
                {
                    count = 0;
                    foreach (DeviceManagerBase mgr in ManagerManager.RunningDeviceManagers)
                        EmitManagerEventTypes(mgr, eventTypes, ref count);
                    foreach (DeviceManagerBase mgr in ManagerManager.ConsolidationDeviceManagers)
                        EmitManagerEventTypes(mgr, eventTypes, ref count);

                    proxy.AvailableEventList(updatedEvents, eventTypes);
                }
            }
            catch (Exception e)
            {
                SystemServices.LogMessage("EmitEventTypes", "Exception: " + e.Message, LogEntryType.ErrorMessage);
            }

            try
            {
                if (proxy != null)
                    proxy.Close();
            }
            catch (Exception e)
            {
                SystemServices.LogMessage("EmitEventTypes", "Closing proxy - Exception: " + e.Message, LogEntryType.ErrorMessage);
            }
        }

    }
}
