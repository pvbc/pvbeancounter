﻿/*
* Copyright (c) 2013 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PVBCInterfaces;
using PVSettings;

namespace Device
{
    public struct DeviceEventConfig
    {
        public FeatureType FeatureType;
        public String EventName;
        public EventType EventType;
        public bool UseForFeedIn;

        public DeviceEventConfig(DeviceEventSettings settings)
        {
            FeatureType = settings.EventFeatureType.Value;
            EventName = settings.EventName;
            EventType = settings.EventType.Value;
            UseForFeedIn = settings.UseForFeedIn;
        }
    }

    public abstract class FeatureEventStatus
    {
        protected DeviceBase Device;
        public FeatureType FeatureType;
        public uint FeatureId;

        public int Frequency;
        // List of events emitted based on changes in this Feature
        public List<DeviceEventConfig> EmitEvents;

        public DateTime LastEventTime = DateTime.MinValue;
        public DateTime LastEmitTime = DateTime.MinValue;

        public FeatureEventStatus(DeviceBase device, FeatureType featureType, uint featureId, int frequency)
        {
            Device = device;
            FeatureType = featureType;
            FeatureId = featureId;
            Frequency = frequency;

            EmitEvents = new List<DeviceEventConfig>();
            foreach(DeviceEventSettings es in device.DeviceManagerDeviceSettings.DeviceEvents.Collection)
                if (es.EventFeatureType == FeatureType && es.EventFeatureId == FeatureId)
                    EmitEvents.Add(new DeviceEventConfig(es));
        }
    }

    public class EnergyEventStatus : FeatureEventStatus
    {
        public Double EnergyTotal;
        public int EventPower;
        public int LastPowerEmitted;
        public Double LastEnergyEmitted;

        public EnergyEventStatus(DeviceBase device, FeatureType featureType, uint featureId, int frequency)
            : base(device, featureType, featureId, frequency)
        {
            EnergyTotal = 0.0;            
            EventPower = 0;
            LastPowerEmitted = 0;
            LastEnergyEmitted = 0.0;        
        }

        private DateTime PowerExpires
        {
            get
            {
                int limit = Frequency * 4 + 10;
                return LastEventTime.AddSeconds(limit);               
            }
        }

        public void SetEventReading( DateTime time, Double energy, int power, float interval, bool energyIsDayTotal)
        {
            //DateTime eventTime = time;
            if (energyIsDayTotal)
                EnergyTotal = energy;
            else
            {
                if (LastEventTime.Date != time.Date)
                    EnergyTotal = 0.0;
                double sinceDayStart = time.TimeOfDay.TotalSeconds;
                if (interval > sinceDayStart && interval > 0)
                    EnergyTotal += energy * sinceDayStart / interval;  // prorata first reading of day
                else
                    EnergyTotal += energy;
            }
            EventPower = power;
            
            LastEventTime = time;
        }

        public void GetCurrentReading(DateTime asAt, out Double energyToday, out int currentPower)
        {
            Double energy = ((asAt.Date == LastEventTime.Date) ? EnergyTotal : 0.0);
            int power = GetNodePower(asAt);

            energyToday = energy;
            currentPower = power;
            LastPowerEmitted = power;
            LastEnergyEmitted = energy;
        }

        private int GetNodePower(DateTime asAt)
        {
            DateTime expires = PowerExpires;
            
            if (expires >= asAt)
                return EventPower;
            else
            {
                if (GlobalSettings.ApplicationSettings.LogTrace)
                    GlobalSettings.LogMessage("EnergyNode.GetNodePower", "Power has expired - Name: " + Device.DeviceManagerDeviceSettings.Name +
                        " - Feature Type: " + FeatureType + " - Id: " + FeatureId +
                        " - Expired: " + expires, LogEntryType.Trace);
                return 0;
            }           
        }
    }
}
