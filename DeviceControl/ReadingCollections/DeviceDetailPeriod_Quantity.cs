﻿/*
* Copyright (c) 2013 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GenericConnector;
using PVBCInterfaces;
using MackayFisher.Utilities;
using PVSettings;

namespace DeviceDataRecorders
{
    public class DeviceDetailPeriod_Quantity : DeviceDetailPeriod_Physical<QuantityReading, QuantityReadingBase>, IComparable<DeviceDetailPeriod_Quantity>
    {
        public DeviceDetailPeriod_Quantity(DeviceDetailPeriodsBase deviceDetailPeriods, PeriodType periodType, DateTime periodStart, FeatureSettings feature)
            : base(deviceDetailPeriods, periodType, periodStart, feature)
        {
        }

        public int CompareTo(DeviceDetailPeriod_Quantity other)
        {
            return base.CompareTo(other);
        }

        private static String SelectDeviceReading_Quantity =
            "select " +
                "ReadingEnd, ReadingStart, QuantityTotal, QuantityToday, QuantityDelta, " +
                "Mode, ErrorCode, Rate " +
            "FROM devicereading_quantity " +
            "WHERE " +
                "ReadingEnd > @PeriodStart " +
                "AND ReadingEnd <= @NextPeriodStart " +
                "AND DeviceFeature_Id = @DeviceFeature_Id ";

        public override void LoadPeriodFromDatabase(GenConnection existingCon = null)
        {
            ClearReadings();

            String stage = "Initial";
            GenConnection con = existingCon;
            try
            {
                if (con == null)
                    con = GlobalSettings.TheDB.NewConnection();

                if (con == null)
                    GlobalSettings.LogMessage("DeviceDetailPeriod_Quantity.LoadPeriodFromDatabase", "*******con is null", LogEntryType.ErrorMessage);

                stage = "Before cmd setup";
                GenCommand cmd;
                cmd = new GenCommand(SelectDeviceReading_Quantity, con);

                if (cmd == null)
                    GlobalSettings.LogMessage("DeviceDetailPeriod_Quantity.LoadPeriodFromDatabase", "*******cmd is null", LogEntryType.ErrorMessage);

                stage = "Before BindSelectIdentity";
                BindSelectIdentity(cmd);

                stage = "Before cmd.ExecuteReader";
                GenDataReader dr = (GenDataReader)cmd.ExecuteReader();

                if (dr == null)
                    GlobalSettings.LogMessage("DeviceDetailPeriod_Quantity.LoadPeriodFromDatabase", "*******dr is null", LogEntryType.ErrorMessage);

                stage = "Before while";
                while (dr.Read())
                {
                    stage = "loop start";

                    DateTime endTime = dr.GetDateTime(0);
                    DateTime startTime = dr.GetDateTime(1);

                    // select included extra readings that probably do not apply
                    // allows for readings to cross period boundaries by up to PeriodOverlapLimit
                    // discard rows with no overlap at all
                    stage = "Before endTime check";
                    if (endTime <= Start)
                        continue;
                    stage = "Before startTime check";
                    if (startTime >= End)
                        continue;

                    stage = "Before new EnergyReading";
                    QuantityReading newRec = new QuantityReading();

                    if (newRec == null)
                        GlobalSettings.LogMessage("DeviceDetailPeriod_Quantity.LoadPeriodFromDatabase", "*******newRec is null", LogEntryType.ErrorMessage);

                    stage = "Before Initialise";
                    newRec.Initialise(DeviceDetailPeriods, endTime, startTime, true);

                    newRec.QuantityTotal = dr.GetNullableDouble(2, QuantityReading.QuantityPrecision);
                    newRec.QuantityToday = dr.GetNullableDouble(3, QuantityReading.QuantityPrecision);
                    newRec.QuantityDeltaNullable = dr.GetNullableDouble(4, QuantityReading.QuantityPrecision);
                    newRec.Mode = dr.GetNullableInt32(5);
                    newRec.ErrorCode = dr.GetNullableInt64(6);
                    newRec.Rate = dr.GetNullableDouble(7, QuantityReading.QuantityPrecision);

                    stage = "Before SetRestoreComplete";
                    newRec.SetRestoreComplete();

                    stage = "Before AddReading";
                    AddReading(newRec, AddReadingType.Database);
                }

                stage = "Before close and dispose";
                dr.Close();
                dr.Dispose();
                dr = null;
            }
            catch (Exception e)
            {
                GlobalSettings.LogMessage("DeviceDetailPeriod_Quantity.LoadPeriodFromDatabase", "Stage: " + stage + " - Exception: " + e.Message, LogEntryType.ErrorMessage);
            }
            finally
            {
                if (existingCon == null && con != null)
                {
                    con.Close();
                    con.Dispose();
                }
            }
        }

        protected override QuantityReading NewReading(DateTime outputTime, TimeSpan duration, QuantityReading pattern = null)
        {
            QuantityReading newReading;

            if (pattern == null)
                newReading = new QuantityReading(DeviceDetailPeriods, outputTime, duration);
            else
            {
                newReading = (QuantityReading)pattern.Clone(outputTime, duration);
                newReading.ResetQuantityDelta();
            }

            return newReading;
        }
    }

    public class DeviceDetailPeriod_QuantityConsolidation : DeviceDetailPeriod_Consolidation<QuantityReadingConsolidation, QuantityReadingBase>, IComparable<DeviceDetailPeriod_QuantityConsolidation>
    {
        public DeviceDetailPeriod_QuantityConsolidation(DeviceDetailPeriodsBase deviceDetailPeriods, PeriodType periodType, DateTime periodStart,
            FeatureSettings feature)
            : base(deviceDetailPeriods, periodType, periodStart, feature)
        {
        }

        public int CompareTo(DeviceDetailPeriod_QuantityConsolidation other)
        {
            return base.CompareTo(other);
        }

        protected override QuantityReadingConsolidation NewReading(DateTime outputTime, TimeSpan duration, QuantityReadingConsolidation pattern = null)
        {
            QuantityReadingConsolidation newEnergyReading;

            if (pattern == null)
            {
                newEnergyReading = new QuantityReadingConsolidation(DeviceDetailPeriods, outputTime, duration);
                newEnergyReading.IsConsolidationReading = true;
            }
            else
            {
                newEnergyReading = (QuantityReadingConsolidation)pattern.Clone(outputTime, duration);
                newEnergyReading.IsConsolidationReading = true;
                newEnergyReading.ResetQuantityDelta();
            }

            return newEnergyReading;
        }

    }

}


