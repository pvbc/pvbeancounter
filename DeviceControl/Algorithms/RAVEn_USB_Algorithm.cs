﻿/*
* Copyright (c) 2013 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Algorithms;
using PVSettings;
using MackayFisher.Utilities;
using PVBCInterfaces;

namespace DeviceControl
{
    public class RAVEn_USB_Algorithm : DeviceAlgorithm
    {
        // Add String Variables below
        public String Message { get; private set; }
        public String DeviceMacId { get; private set; }
        public String MeterMacId { get; private set; }

        public void SetMessage(String value) { Message = value; }
        public void SetDeviceMacId(String value) { DeviceMacId = value; }
        public void SetMeterMacId(String value) { MeterMacId = value; }

        public String GetDeviceMacId() { return DeviceMacId; }
        public String GetMeterMacId() { return MeterMacId; }

        // End String Variables

        // Add Numeric Variables below

        // End Numeric Variables

        // Add Bytes Variables below

        // End Bytes Variables

        public Double ImportEnergyMargin { get; private set; }
        public Double ExportEnergyMargin { get; private set; }
        public Double EnergyMargin { get; private set; }

        public RAVEn_USB_Algorithm(DeviceSettings deviceSettings, Protocol protocol, ErrorLogger errorLogger)
            : base(deviceSettings, protocol, errorLogger)
        {
            ImportEnergyMargin = 0.01;
            ExportEnergyMargin = 0.01;
            EnergyMargin = 0.01;
        }

        protected override void LoadVariables()
        {
            base.LoadVariables();

            VariableEntry var;

            var = new VariableEntry_String("Message", SetMessage);
            VariableEntries.Add(var);
            var = new VariableEntry_String("DeviceMacId", SetDeviceMacId, GetDeviceMacId);
            VariableEntries.Add(var);
            var = new VariableEntry_String("MeterMacId", SetMeterMacId, GetMeterMacId);
            VariableEntries.Add(var);
        }

        public override void ClearAttributes()
        {
            base.ClearAttributes();

            Message = "";
        }

        public override bool ExtractIdentity()
        {
            bool res = base.ExtractIdentity();
            try
            {
                if (GlobalSettings.SystemServices.LogTrace)
                    LogMessage("DoExtractReadings - Identity - Manufacturer: " + Manufacturer
                        + " - Model: " + Model
                        + " - SerialNo: " + SerialNo
                        + " - Export Energy Margin: " + ExportEnergyMargin
                        + " - Import Energy Margin: " + ImportEnergyMargin, LogEntryType.Trace);
            }
            catch (Exception e)
            {
                LogMessage("RAVEn_USB_Algorithm.ExtractIdentity - Exception: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }
            return res;
        }

    }
}
