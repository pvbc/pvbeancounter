﻿/*
* Copyright (c) 2013 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Algorithms;
using PVSettings;
using MackayFisher.Utilities;
using PVBCInterfaces;
using Conversations;

namespace Device
{
    public class CompositeAlgorithm_xml : DeviceAlgorithm
    {
        public DynamicByteVar MessageData { get; private set; }
        // Add String Variables below

        public String Message { get; private set; }
        
        // End String Variables

        // Add Numeric Variables below

        // End Numeric Variables

        // Add Bytes Variables below

        // End Bytes Variables

        public CompositeAlgorithm_xml(AlgorithmParams algorithmParams)
            : base(algorithmParams)
        {
        }

        public CompositeAlgorithm_xml(DeviceSettings deviceSettings, Protocol protocol, ErrorLogger errorLogger)
            : base(deviceSettings, protocol, errorLogger)
        {
        }

        protected override void LoadVariables()
        {
            base.LoadVariables();
            MessageData = (DynamicByteVar)Params.Protocol.GetSessionVariable("Message", null);
        }

        public override void ClearAttributes()
        {
            base.ClearAttributes();

            Message = "";
        }
       
        public override bool ExtractReading(bool dbWrite, ref bool alarmFound, ref bool errorFound)
        {
            bool res = false;
            if (FaultDetected)
                return res;

            String stage = "Reading";
            try
            {
                res = LoadExchangeType("Reading", true, true, true, ref alarmFound, ref errorFound);
                Message = MessageData.ToString();
            }
            catch (Exception e)
            {
                LogMessage("DoExtractReadings - Stage: " + stage + " - Exception: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }

            return res;
        }        
    }

    public class CompositeAlgorithm_EW4009 : DeviceAlgorithm
    {
        public class DeviceReading
        {
            public decimal? Power { get; set; }
            public String Status { get; set; }

            public DeviceReading()
            {
                Power = null;
                Status = " ";
            }

            public void SetPower(decimal value) { Power = value; }
            public void SetStatus(string value) { Status = value; }
        }

        private DeviceReading[] Readings;

        public DeviceReading GetReading(ulong address)
        {
            if (address > 15)
                return null;
            return Readings[address];
        }

        public CompositeAlgorithm_EW4009(AlgorithmParams algorithmParams)
            : base(algorithmParams)
        {
        }

        public CompositeAlgorithm_EW4009(DeviceSettings deviceSettings, Protocol protocol, ErrorLogger errorLogger)
            : base(deviceSettings, protocol, errorLogger)
        {
            Readings = new DeviceReading[16];
            for (int i = 0; i < 16; i++)
            {
                Readings[i] = new DeviceReading();
            }
        }

        protected override void LoadVariables()
        {
            base.LoadVariables();

            Readings = new DeviceReading[16];
            for (int i = 0; i < 16; i++)
            {
                Readings[i] = new DeviceReading();
            }

            VariableEntry var;
            
            for (int i = 0; i < 16; i++)
            {
                int j = i + 1;
                var = new VariableEntry_Numeric("Power" + j.ToString("00"), Readings[i].SetPower);
                VariableEntries.Add(var);
                var = new VariableEntry_String("State" + j.ToString("00"), Readings[i].SetStatus);
                VariableEntries.Add(var);
            }            
        }

        public override void ClearAttributes()
        {
            base.ClearAttributes();

            for (int i = 0; i < 16; i++)
            {
                Readings[i].Power = null;
                Readings[i].Status = " ";
            }
        }
    }
}
