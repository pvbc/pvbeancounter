﻿/*
* Copyright (c) 2013 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Algorithms;
using PVSettings;
using MackayFisher.Utilities;
using PVBCInterfaces;

namespace Device
{
    public class MeterListenerAlgorithm : DeviceAlgorithm
    {
        // Add String Variables below

        // End String Variables

        // Add Numeric Variables below

        public decimal? ImportLowRate { get; private set; }
        public decimal? ImportHighRate { get; private set; }
        public decimal? ExportLowRate { get; private set; }
        public decimal? ExportHighRate { get; private set; }
        public decimal? ImportPower { get; private set; }
        public decimal? ExportPower { get; private set; }
        public decimal? GasTotal { get; private set; }

        public void SetImportLowRate(decimal value) { ImportLowRate = value; }
        public void SetImportHighRate(decimal value) { ImportHighRate = value; }
        public void SetExportLowRate(decimal value) { ExportLowRate = value; }
        public void SetExportHighRate(decimal value) { ExportHighRate = value; }
        public void SetImportPower(decimal value) { ImportPower = value; }
        public void SetExportPower(decimal value) { ExportPower = value; }
        public void SetGasTotal(decimal value) { GasTotal = value; }

        // End Numeric Variables

        // Add Bytes Variables below

        public byte[] MessageContent { get; private set; }
        public void SetMessageContent(byte[] value) { MessageContent = value; }

        // End Bytes Variables

        public Double ImportEnergyMargin { get; private set; }
        public Double ExportEnergyMargin { get; private set; }
        public Double EnergyMargin { get; private set; }
        public Double GasMargin { get; private set; }

        public MeterListenerAlgorithm(DeviceSettings deviceSettings, Protocol protocol, ErrorLogger errorLogger)
            : base(deviceSettings, protocol, errorLogger)
        {
            ImportEnergyMargin = 0.01;
            ExportEnergyMargin = 0.01;
            EnergyMargin = 0.01;
            GasMargin = 0.001;
        }

        protected override void LoadVariables()
        {
            base.LoadVariables();

            VariableEntry var;

            var = new VariableEntry_Numeric("Address", SetAddress, GetAddress);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("ImportLowRate", SetImportLowRate);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("ImportHighRate", SetImportHighRate);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("ExportLowRate", SetExportLowRate);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("ExportHighRate", SetExportHighRate);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("ImportPower", SetImportPower);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("ExportPower", SetExportPower);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("GasTotal", SetGasTotal);
            VariableEntries.Add(var);

            var = new VariableEntry_Bytes("MessageContent", SetMessageContent);
            VariableEntries.Add(var);
        }

        public override void ClearAttributes()
        {
            base.ClearAttributes();

            ImportLowRate = null;
            ImportHighRate = null;
            ExportLowRate = null;
            ExportHighRate = null;
            ImportPower = null;
            ExportPower = null;
            GasTotal = null;

            MessageContent = null;
        }

        private void LoadEnergyMargin()
        {
            // Energy Margin limits the Energy Estimate deviation from inverter value
            // Inverter identity is sometimes used to dynamically alter details such as Scale / ScaleFactor (refer JFY inverters)
            Register r = FindRegister("", "", "ExportLowRate");           
            if (r != null)
            {
                Double margin = (Double)((RegisterNumber)r).Resolution;
                if (margin > ExportEnergyMargin)
                    ExportEnergyMargin = margin;
            }

            r = FindRegister("", "", "ExportHighRate");
            if (r != null)
            {
                Double margin = (Double)((RegisterNumber)r).Resolution;
                if (margin > ExportEnergyMargin)
                    ExportEnergyMargin = margin;
            }

            r = FindRegister("", "", "ImportLowRate");
            if (r != null)
            {
                Double margin = (Double)((RegisterNumber)r).Resolution;
                if (margin > ImportEnergyMargin)
                    ImportEnergyMargin = margin;
            }

            r = FindRegister("", "", "ImportHighRate");
            if (r != null)
            {
                Double margin = (Double)((RegisterNumber)r).Resolution;
                if (margin > ImportEnergyMargin)
                    ImportEnergyMargin = margin;
            }

            if (ExportEnergyMargin > ImportEnergyMargin)
                EnergyMargin = ExportEnergyMargin;
            else
                EnergyMargin = ImportEnergyMargin;
            
        }

        public override bool ExtractIdentity()
        {
            bool res = base.ExtractIdentity();
            try
            {
                LoadEnergyMargin();

                if (GlobalSettings.SystemServices.LogTrace)
                    LogMessage("DoExtractReadings - Identity - Manufacturer: " + Manufacturer
                        + " - Model: " + Model
                        + " - SerialNo: " + SerialNo
                        + " - Export Energy Margin: " + ExportEnergyMargin
                        + " - Import Energy Margin: " + ImportEnergyMargin, LogEntryType.Trace);
            }
            catch (Exception e)
            {
                LogMessage("MeterListenerAlgorithm.ExtractIdentity - Exception: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }
            return res;
        }

    }
}
