﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Algorithms;
using PVSettings;
using MackayFisher.Utilities;
using PVBCInterfaces;

namespace Device
{
    public class InverterAlgorithm : DeviceAlgorithm
    {
        // Add String Variables below

        // End String Variables

        // Add Numeric Variables below

        public decimal? EnergyTotalAC { get; private set; }
        public decimal? EnergyTotalACHigh { get; private set; }
        public decimal? EnergyTotalAC1 { get; private set; }
        public decimal? EnergyTotalACHigh1 { get; private set; }
        public decimal? EnergyTotalAC2 { get; private set; }
        public decimal? EnergyTotalACHigh2 { get; private set; }
        public decimal? EnergyTotalAC3 { get; private set; }
        public decimal? EnergyTotalACHigh3 { get; private set; }

        public decimal? EnergyTodayAC { get; private set; }
        public decimal? EnergyTodayAC1 { get; private set; }
        public decimal? EnergyTodayAC2 { get; private set; }
        public decimal? EnergyTodayAC3 { get; private set; }

        public decimal? PowerAC { get; private set; }
        public decimal? PowerPV { get; private set; }

        public decimal? Status { get; private set; }
        public decimal? Status_1 { get; private set; }
        public decimal? Status_2 { get; private set; }
        public decimal? Status_3 { get; private set; }

        public decimal? VoltsPV1 { get; private set; }
        public decimal? CurrentPV1 { get; private set; }
        public decimal? PowerPV1 { get; private set; }
        public decimal? VoltsPV2 { get; private set; }
        public decimal? CurrentPV2 { get; private set; }
        public decimal? PowerPV2 { get; private set; }
        public decimal? VoltsPV3 { get; private set; }
        public decimal? CurrentPV3 { get; private set; }
        public decimal? PowerPV3 { get; private set; }
        public decimal? Frequency { get; private set; }
        public decimal? VoltsAC1 { get; private set; }
        public decimal? CurrentAC1 { get; private set; }
        public decimal? PowerAC1 { get; private set; }
        public decimal? PowerAC1High { get; private set; }
        public decimal? VoltsAC2 { get; private set; }
        public decimal? CurrentAC2 { get; private set; }
        public decimal? PowerAC2 { get; private set; }
        public decimal? VoltsAC3 { get; private set; }
        public decimal? CurrentAC3 { get; private set; }
        public decimal? PowerAC3 { get; private set; }
        public decimal? TimeTotal { get; private set; }
        public decimal? TimeTotalHigh { get; private set; }
        public decimal? Temperature { get; private set; }
        public decimal? TemperatureDC { get; private set; }

        public decimal? ErrorCode1 { get; private set; }
        public decimal? ErrorCode2 { get; private set; }
        public decimal? ErrorCode3 { get; private set; }

        public decimal? ErrorCodeHigh { get; private set; }
        public decimal? ErrorCodeHigh1 { get; private set; }
        public decimal? ErrorCodeHigh2 { get; private set; }
        public decimal? ErrorCodeHigh3 { get; private set; }

        public decimal? PowerRating { get; private set; }

        public void SetErrorCode1(decimal value) { ErrorCode1 = value; }
        public void SetErrorCode2(decimal value) { ErrorCode2 = value; }
        public void SetErrorCode3(decimal value) { ErrorCode3 = value; }

        public void SetErrorCodeHigh(decimal value) { ErrorCodeHigh = value; }
        public void SetErrorCodeHigh1(decimal value) { ErrorCodeHigh1 = value; }
        public void SetErrorCodeHigh2(decimal value) { ErrorCodeHigh2 = value; }
        public void SetErrorCodeHigh3(decimal value) { ErrorCodeHigh3 = value; }

        public void SetEnergyTotalAC(decimal value) { EnergyTotalAC = value; }
        public void SetEnergyTotalACHigh(decimal value) { EnergyTotalACHigh = value; }
        public void SetEnergyTotalAC1(decimal value) { EnergyTotalAC1 = value; }
        public void SetEnergyTotalACHigh1(decimal value) { EnergyTotalACHigh1 = value; }
        public void SetEnergyTotalAC2(decimal value) { EnergyTotalAC2 = value; }
        public void SetEnergyTotalACHigh2(decimal value) { EnergyTotalACHigh2 = value; }
        public void SetEnergyTotalAC3(decimal value) { EnergyTotalAC3 = value; }
        public void SetEnergyTotalACHigh3(decimal value) { EnergyTotalACHigh3 = value; }

        public void SetEnergyTodayAC(decimal value) { EnergyTodayAC = value; }
        public void SetEnergyTodayAC1(decimal value) { EnergyTodayAC1 = value; }
        public void SetEnergyTodayAC2(decimal value) { EnergyTodayAC2 = value; }
        public void SetEnergyTodayAC3(decimal value) { EnergyTodayAC3 = value; }

        public void SetStatus(decimal value) { Status = value; }
        public void SetStatus1(decimal value) { Status_1 = value; }
        public void SetStatus2(decimal value) { Status_2 = value; }
        public void SetStatus3(decimal value) { Status_3 = value; }

        public void SetPowerAC(decimal value) { PowerAC = value; }
        public void SetPowerPV(decimal value) { PowerPV = value; }
        public void SetVoltsPV1(decimal value) { VoltsPV1 = value; }
        public void SetCurrentPV1(decimal value) { CurrentPV1 = value; }
        public void SetPowerPV1(decimal value) { PowerPV1 = value; }
        public void SetVoltsPV2(decimal value) { VoltsPV2 = value; }
        public void SetCurrentPV2(decimal value) { CurrentPV2 = value; }
        public void SetPowerPV2(decimal value) { PowerPV2 = value; }
        public void SetVoltsPV3(decimal value) { VoltsPV3 = value; }
        public void SetCurrentPV3(decimal value) { CurrentPV3 = value; }
        public void SetPowerPV3(decimal value) { PowerPV3 = value; }
        public void SetFrequency(decimal value) { Frequency = value; }
        public void SetVoltsAC1(decimal value) { VoltsAC1 = value; }
        public void SetCurrentAC1(decimal value) { CurrentAC1 = value; }
        public void SetPowerAC1(decimal value) { PowerAC1 = value; }
        public void SetPowerAC1High(decimal value) { PowerAC1High = value; }
        public void SetVoltsAC2(decimal value) { VoltsAC2 = value; }
        public void SetCurrentAC2(decimal value) { CurrentAC2 = value; }
        public void SetPowerAC2(decimal value) { PowerAC2 = value; }
        public void SetVoltsAC3(decimal value) { VoltsAC3 = value; }
        public void SetCurrentAC3(decimal value) { CurrentAC3 = value; }
        public void SetPowerAC3(decimal value) { PowerAC3 = value; }
        public void SetTimeTotal(decimal value) { TimeTotal = value; }
        public void SetTimeTotalHigh(decimal value) { TimeTotalHigh = value; }
        public void SetTemperature(decimal value) { Temperature = value; }
        public void SetTemperatureDC(decimal value) { TemperatureDC = value; }
        public void SetPowerRating(decimal value) { PowerRating = value; }

        // End Numeric Variables

        // Add Bytes Variables below

        // End Bytes Variables

        public Double EnergyMargin { get; private set; }
        public Double EnergyTotalEnergyMargin { get; private set; }
        public Double EnergyTodayEnergyMargin { get; private set; }

        public InverterAlgorithm(DeviceSettings deviceSettings, Protocol protocol, ErrorLogger errorLogger)
            :base(deviceSettings, protocol, errorLogger)
        {
            EnergyMargin = 0.01;
        }

        protected override void LoadVariables()
        {
            base.LoadVariables();

            VariableEntry var;

            var = new VariableEntry_Numeric("Address", SetAddress, GetAddress);
            VariableEntries.Add(var);

            var = new VariableEntry_Numeric("EnergyTotalAC", SetEnergyTotalAC);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("EnergyTotalAC1", SetEnergyTotalAC1);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("EnergyTotalAC2", SetEnergyTotalAC2);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("EnergyTotalAC3", SetEnergyTotalAC3);
            VariableEntries.Add(var);

            var = new VariableEntry_Numeric("EnergyTotalACHigh", SetEnergyTotalACHigh);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("EnergyTotalACHigh1", SetEnergyTotalACHigh1);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("EnergyTotalACHigh2", SetEnergyTotalACHigh2);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("EnergyTotalACHigh3", SetEnergyTotalACHigh3);
            VariableEntries.Add(var);

            var = new VariableEntry_Numeric("EnergyTodayAC", SetEnergyTodayAC);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("EnergyTodayAC1", SetEnergyTodayAC1);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("EnergyTodayAC2", SetEnergyTodayAC2);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("EnergyTodayAC3", SetEnergyTodayAC3);
            VariableEntries.Add(var);

            var = new VariableEntry_Numeric("Status", SetStatus);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("Status1", SetStatus1);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("Status2", SetStatus2);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("Status3", SetStatus3);
            VariableEntries.Add(var);

            var = new VariableEntry_Numeric("ErrorCode1", SetErrorCode1);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("ErrorCode2", SetErrorCode2);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("ErrorCode3", SetErrorCode3);
            VariableEntries.Add(var);

            var = new VariableEntry_Numeric("ErrorCodeHigh", SetErrorCodeHigh);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("ErrorCodeHigh1", SetErrorCodeHigh1);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("ErrorCodeHigh2", SetErrorCodeHigh2);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("ErrorCodeHigh3", SetErrorCodeHigh3);
            VariableEntries.Add(var);

            var = new VariableEntry_Numeric("PowerAC", SetPowerAC);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("PowerPV", SetPowerPV);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("VoltsPV1", SetVoltsPV1);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("CurrentPV1", SetCurrentPV1);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("PowerPV1", SetPowerPV1);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("VoltsPV2", SetVoltsPV2);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("CurrentPV2", SetCurrentPV2);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("PowerPV2", SetPowerPV2);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("VoltsPV3", SetVoltsPV3);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("CurrentPV3", SetCurrentPV3);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("PowerPV3", SetPowerPV3);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("Frequency", SetFrequency);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("VoltsAC1", SetVoltsAC1);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("CurrentAC1", SetCurrentAC1);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("PowerAC1", SetPowerAC1);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("PowerAC1High", SetPowerAC1High);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("VoltsAC2", SetVoltsAC2);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("CurrentAC2", SetCurrentAC2);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("PowerAC2", SetPowerAC2);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("VoltsAC3", SetVoltsAC3);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("CurrentAC3", SetCurrentAC3);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("PowerAC3", SetPowerAC3);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("TimeTotal", SetTimeTotal);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("TimeTotalHigh", SetTimeTotalHigh);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("Temperature", SetTemperature);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("TemperatureDC", SetTemperatureDC);
            VariableEntries.Add(var);            
            var = new VariableEntry_Numeric("PowerRating", SetPowerRating);
            VariableEntries.Add(var);
        }

        public override void ClearAttributes()
        {
            base.ClearAttributes();

            EnergyTotalAC = null;
            EnergyTotalAC1 = null;
            EnergyTotalAC2 = null;
            EnergyTotalAC3 = null;

            EnergyTotalACHigh = null;
            EnergyTotalACHigh1 = null;
            EnergyTotalACHigh2 = null;
            EnergyTotalACHigh3 = null;

            EnergyTodayAC = null;
            EnergyTodayAC1 = null;
            EnergyTodayAC2 = null;
            EnergyTodayAC3 = null;

            PowerAC = null;
            PowerPV = null;

            ErrorCode1 = null;
            ErrorCode2 = null;
            ErrorCode3 = null;

            ErrorCodeHigh = null;
            ErrorCodeHigh1 = null;
            ErrorCodeHigh2 = null;
            ErrorCodeHigh3 = null;

            Status = null;
            Status_1 = null;
            Status_2 = null;
            Status_3 = null;

            VoltsPV1 = null;
            CurrentPV1 = null;
            PowerPV1 = null;
            VoltsPV2 = null;
            CurrentPV2 = null;
            PowerPV2 = null;
            VoltsPV3 = null;
            CurrentPV3 = null;
            PowerPV3 = null;
            Frequency = null;
            VoltsAC1 = null;
            CurrentAC1 = null;
            PowerAC1 = null;
            PowerAC1High = null;
            VoltsAC2 = null;
            CurrentAC2 = null;
            PowerAC2 = null;
            VoltsAC3 = null;
            CurrentAC3 = null;
            PowerAC3 = null;
            TimeTotal = null;
            TimeTotalHigh = null;
            Temperature = null;
            TemperatureDC = null;            
        }

        public bool IsThreePhase { get; private set; }  // true if phase level EToday or ETotal values are present

        private void AdjustToDevice()
        {
            // Energy Margin limits the Energy Estimate deviation from inverter value
            // Inverter identity is sometimes used to dynamically alter details such as Scale / ScaleFactor (refer JFY inverters)
            Register eToday = FindRegister("", "", "EnergyTodayAC");
            Register eToday1 = FindRegister("", "", "EnergyTodayAC1");
            Register eToday2 = FindRegister("", "", "EnergyTodayAC2");
            Register eToday3 = FindRegister("", "", "EnergyTodayAC3");

            Register eTotal = FindRegister("", "", "EnergyTotalAC");
            Register eTotal1 = FindRegister("", "", "EnergyTotalAC1");
            Register eTotal2 = FindRegister("", "", "EnergyTotalAC2");
            Register eTotal3 = FindRegister("", "", "EnergyTotalAC3");

            IsThreePhase = (eTotal1 != null && eTotal2 != null && eTotal3 != null)
                || (eToday1 != null && eToday2 != null && eToday3 != null);

            if (IsThreePhase)  // assume all phases use same margin
            {
                if (eToday1 != null)
                {
                    EnergyTodayEnergyMargin = (Double)((RegisterNumber)eToday1).Resolution;
                    EnergyMargin = EnergyTodayEnergyMargin;
                }

                if (eTotal1 != null)
                {
                    EnergyTotalEnergyMargin = (Double)((RegisterNumber)eTotal1).Resolution;
                    if (eToday1 == null)
                        EnergyMargin = EnergyTotalEnergyMargin;
                }
            }
            else
            {
                if (eToday != null)
                {
                    EnergyTodayEnergyMargin = (Double)((RegisterNumber)eToday).Resolution;
                    EnergyMargin = EnergyTodayEnergyMargin;
                }
                
                if (eTotal != null)
                {
                    EnergyTotalEnergyMargin = (Double)((RegisterNumber)eTotal).Resolution;
                    if (eToday == null)
                        EnergyMargin = EnergyTotalEnergyMargin;
                }
            }
        }

        public override bool ExtractIdentity()
        {
            bool res = base.ExtractIdentity();
            try
            {
                AdjustToDevice();
                
                if (GlobalSettings.SystemServices.LogTrace)
                    LogMessage("DoExtractReadings - Identity - Manufacturer: " + Manufacturer
                        + " - Model: " + Model
                        + " - SerialNo: " + SerialNo
                        + " - Energy Margin: " + EnergyMargin, LogEntryType.Trace);
            }
            catch (Exception e)
            {
                LogMessage("InverterAlgorithm.ExtractIdentity - Exception: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }
            return res;
        }

        public Decimal PowerAC_Total
        {
            get
            {
                if (PowerAC.HasValue)
                    return PowerAC.Value;
                
                return PowerAC_Phase1 + PowerAC_Phase2 + PowerAC_Phase3;
            }
        }

        public Decimal PowerAC_Phase1
        {
            get
            {
                if (PowerAC1.HasValue)
                    if (PowerAC1High.HasValue)
                            return PowerAC1.Value + PowerAC1High.Value * 65536;
                    else
                        return PowerAC1.Value;
                else if (VoltsAC1.HasValue && CurrentAC1.HasValue)
                    return VoltsAC1.Value * CurrentAC1.Value;
                return 0;
            }
        }

        public Decimal PowerAC_Phase2
        {
            get
            {
                if (PowerAC2.HasValue)
                    return PowerAC2.Value;
                else if (VoltsAC2.HasValue && CurrentAC2.HasValue)
                    return VoltsAC2.Value * CurrentAC2.Value;
                return 0;
            }
        }

        public Decimal PowerAC_Phase3
        {
            get
            {
                if (PowerAC3.HasValue)
                    return PowerAC3.Value;
                else if (VoltsAC3.HasValue && CurrentAC3.HasValue)
                    return VoltsAC3.Value * CurrentAC3.Value;
                if (PowerAC.HasValue) // do not use PowerAC_Total - possible recursion
                    return PowerAC.Value - (PowerAC_Phase1 + PowerAC_Phase2);
                else
                    return 0;
            }
        }
    }
}
