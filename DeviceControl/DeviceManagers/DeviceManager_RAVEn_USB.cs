﻿/*
* Copyright (c) 2013 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GenThreadManagement;
using PVSettings;
using PVBCInterfaces;
using Conversations;
using DeviceStream;
using Device;

namespace DeviceControl
{
    public class DeviceManager_RAVEn_USB : DeviceManagerTyped<MeterDevice_Generic>
    {
        private bool HaveMeterIdentity;

        public DeviceManager_RAVEn_USB(DeviceManagerSettings mmSettings,
            IDeviceManagerManager imm)
            : base(mmSettings, imm)
        {
            HaveMeterIdentity = false;
        }

        protected override void PrepareForDeviceLoad()
        {
            base.PrepareForDeviceLoad();
            ManagerAlgorithm = (RAVEn_USB_Algorithm)GetDeviceAlgorithm(DeviceManagerSettings.ListenerDeviceSettings);
        }

        protected override MeterDevice_Generic NewDevice(DeviceManagerDeviceSettings dmDevice)
        {
            MeterDevice_Generic device = new MeterDevice_Generic(this, dmDevice, ManagerAlgorithm.Protocol);
            return device;
        }

        private bool SetDeviceType(DeviceManagerDeviceSettings ds)
        {
            bool alarmFound = false;
            bool errorFound = false;

            ((RAVEn_USB_Algorithm)ManagerAlgorithm).SetMeterMacId(ds.SerialNo);
            bool res = ManagerAlgorithm.ExtractExchangeType("GetMeterIdentity", false, false, false, ref alarmFound, ref errorFound);
            if (!res)
            {
                LogMessage("DeviceManager_RAVEn_USB - Cannot retrieve meter detail: " + ds.SerialNo, LogEntryType.Trace);
                return false;
            }

            XElement m = XElementFromXmlString(((RAVEn_USB_Algorithm)ManagerAlgorithm).Message, "msg");

            String meterType = m.Element("MeterType").Value;
            String nickName = m.Element("NickName").Value;

            ds.Name = meterType + " / " + nickName;

            return res;
        }

        public bool ExtractMeterIdentity()
        {
            bool res = false;
            bool alarmFound = false;
            bool errorFound = false;
            String stage = "Identity";
            try
            {
                ManagerAlgorithm.ClearAttributes();

                DateTime curTime = DateTime.Now;

                res = ManagerAlgorithm.ExtractIdentity();                                
                if (!res)
                    return false;

                res = ManagerAlgorithm.ExtractExchangeType("GetMeterList", false, false, false, ref alarmFound, ref errorFound);
                if (!res)
                {
                    LogMessage("DeviceManager_RAVEn_USB - Cannot retrieve meter list", LogEntryType.Trace);
                    return false;
                }

                XElement m = XElementFromXmlString(((RAVEn_USB_Algorithm)ManagerAlgorithm).Message, "msg");

                bool settingsChanged = false;
                foreach (XElement e in m.Elements("MeterMacId"))
                {
                    string serialNo = e.Value;
                    bool found = false;
                    foreach (DeviceManagerDeviceSettings ds in DeviceManagerSettings.DeviceList.Collection)
                        if (serialNo == ds.SerialNo)
                        {
                            found = true;
                            break;
                        }
                    if (found)
                        continue;

                    DeviceManagerDeviceSettings d = null;
                    foreach (DeviceManagerDeviceSettings ds in DeviceManagerSettings.DeviceList.Collection)
                        if (ds.SerialNo == "")
                        {
                            d = ds;
                            break;
                        }

                    // new device will be disabled - requires manual settings completion and activation 
                    if (d == null)
                        d = DeviceManagerSettings.AddDevice();  
                    settingsChanged = true;
                    d.SerialNo = serialNo;
                    d.Enabled = false;
                    SetDeviceType(d);
                }

                if (settingsChanged)
                    DeviceManagerSettings.ApplicationSettings.SaveSettings();

                if (ManagerAlgorithm.ErrorCode.HasValue && ManagerAlgorithm.ErrorCode != 0 && ManagerAlgorithm.HaveErrorRegisters)
                    ErrorLogger.LogError(ManagerAlgorithm.Address.ToString(), curTime, ManagerAlgorithm.ErrorCode.ToString(), 2, ManagerAlgorithm.ErrorRegisters);

                stage = "errors";
                if (alarmFound)
                    ManagerAlgorithm.LogAlarm("Reading", curTime, this);
                if (errorFound)
                    ManagerAlgorithm.LogError("Reading", curTime, this);

                HaveMeterIdentity = true;
            }
            catch (Exception e)
            {
                LogMessage("DoExtractReadings - Stage: " + stage + " - Exception: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }

            return res;
        }

        public override bool DoWork()
        {
            try
            {
                if (!HaveMeterIdentity)
                    ExtractMeterIdentity();

                foreach (MeterDevice_Generic device in DeviceList)
                {
                    if (device.Enabled && device.NextRunTime <= NextRunTimeStamp)
                    {
                        if (device.DeviceSettings.DeviceType == DeviceType.Inverter && !InvertersRunning)
                            continue;
                        /*
                        bool res = device.DoExtractReadings();
                        if (res)
                        {
                            device.LastRunTime = NextRunTimeStamp;
                        }
                        else
                            ContinueRunning = false;
                        */
                    }
                }
            }
            catch (System.Threading.ThreadInterruptedException)
            {

            }
            return true;
        }
    }
}
