﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Threading;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Linq;
using System.Text;
using PVSettings;
using MackayFisher.Utilities;
using Conversations;
using DeviceStream;
using DeviceDataRecorders;
using GenThreadManagement;
using PVBCInterfaces;
using Algorithms;
using Device;
using GenericConnector;

namespace DeviceControl
{
    public interface IDeviceManagerManager
    {
        bool LiveLoadForced { get; }
        void ReleaseErrorLoggers();
        bool RunMonitors { get; }
        int GetOutputManagerInterval(String systemId);
        void SetOutputReady(string systemId);
        void StartService(bool fullStartup);
        void StopService();
        IEvents EnergyEvents { get; }
        void ScanForEvents();
        Device.DeviceBase FindDeviceFromSettings(DeviceManagerDeviceSettings deviceSettings);

        List<DeviceControl.DeviceManagerBase> AllRealDeviceManagers { get; }
        List<DeviceControl.DeviceManagerBase> RunningDeviceManagers { get; }
        List<DeviceControl.DeviceManagerBase> ConsolidationDeviceManagers { get; }
    }

    public abstract class DeviceManagerBase : GenThread, IDeviceManager
    {
        public DeviceManagerSettings DeviceManagerSettings;
        public IDeviceManagerManager ManagerManager { get; private set; }

        public ErrorLogger ErrorLogger { get; private set; }

        public Protocol Protocol { get; protected set; }

        public DeviceAlgorithm ManagerAlgorithm { get; protected set; }

        internal String PortName { get; private set; }
        internal DeviceStream.DeviceStream DeviceStream { get; private set; }
        private bool StreamInitialised = false;

        protected string ConfigFileName = null;
        protected DateTime? NextFileDate = null;      // specifies the next DateTime to be used for extract
        protected string OutputDirectory;               // directory where extract files will be written
        protected string ArchiveDirectory;              // directory where extract files will be moved when processed

        // Note call to ThreadName in constructor is safe as it does not depend on execution of the sub-class constructor
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DeviceManagerBase(DeviceManagerSettings mmSettings,
            IDeviceManagerManager imm)
            : base(GlobalSettings.SystemServices, mmSettings)
        {
            DeviceStream = null;
            ManagerAlgorithm = null;

            OutputDirectory = GlobalSettings.ApplicationSettings.DefaultDirectory;
            ArchiveDirectory = "Archive";
            DeviceManagerSettings = mmSettings;
            ManagerManager = imm;
            Protocol = null;
            PortName = mmSettings.PortName;

            String directory = GlobalSettings.ApplicationSettings.InverterLogs;
            if (directory == "" || directory == null)
                directory = System.IO.Path.Combine(GlobalSettings.ApplicationSettings.DefaultDirectory, "ErrorLogs");
            else if (System.IO.Path.IsPathRooted(directory))
                directory = System.IO.Path.Combine(directory);
            else
                directory = System.IO.Path.Combine(GlobalSettings.ApplicationSettings.DefaultDirectory, directory);

            ErrorLogger = new ErrorLogger(GlobalSettings.SystemServices, ThreadName + "_Err", "", directory);

            LogMessage("Loading", LogEntryType.Trace);
        }

        public override String ThreadName 
        { 
            get 
            {
                if (DeviceStream == null)
                    return "DevMgr_" + DeviceManagerSettings.Name;
                else
                    return "DevMgr_" + DeviceManagerSettings.Name + ": " + PortName; 
            } 
        }

        public override TimeSpan Interval { get { return TimeSpan.FromSeconds(6); } }

        public virtual void ResetStartOfDay()
        {
        }

        public static XElement XElementFromXmlString(string xmlString, string wrapNodeName = "")
        {
            String elementStr;
            if (wrapNodeName == "")
                elementStr = xmlString;
            else
                elementStr = "<" + wrapNodeName + ">" + xmlString + "</" + wrapNodeName + ">";

            XmlReader nodeReader = XmlReader.Create(new StringReader(elementStr));
            nodeReader.MoveToContent();

            XElement xRoot = XElement.Load(nodeReader);

            return xRoot;
        }

        public DeviceAlgorithm GetDeviceAlgorithm(DeviceSettings deviceSettings)
        {
            if (deviceSettings.DeviceAlgorithm == "InverterAlgorithm")
                return new InverterAlgorithm(deviceSettings, Protocol, ErrorLogger);
            else if (deviceSettings.DeviceAlgorithm == "MeterListenerAlgorithm")
                return new MeterListenerAlgorithm(deviceSettings, Protocol, ErrorLogger);
            else if (deviceSettings.DeviceAlgorithm == "RAVEn_USB_Algorithm")
                return new RAVEn_USB_Algorithm(deviceSettings, Protocol, ErrorLogger);
            return null;
        }

        public virtual String ManagerTypeName { get { return DeviceManagerSettings.ManagerTypeName; } }

        protected void LogMessage(String message, LogEntryType logEntryType)
        {
            LogMessage(ThreadName, message, logEntryType);
        }

        public void CloseErrorLogger()
        {
            ErrorLogger.Close();
        }

        public override void Initialise()
        {
            base.Initialise();
            if (DeviceManagerSettings.UsesSerialPort)
            {
                CloseStream();
                InitialiseStream();
            }
        }

        public override void Finalise()
        {
            if (DeviceStream != null)
                CloseStream();     
       
            CloseErrorLogger();
            LogMessage("DeviceManagerBase.Finalise - Name = " + DeviceManagerSettings.Name + " - manager stopped", LogEntryType.Trace);

            base.Finalise();
        }

        public bool InvertersRunning
        {
            get
            {
                TimeSpan curTime = DateTime.Now.TimeOfDay;
                if (GlobalSettings.ApplicationSettings.InverterStartTime.HasValue && curTime < GlobalSettings.ApplicationSettings.InverterStartTime.Value)
                    return false;
                if (GlobalSettings.ApplicationSettings.InverterStopTime.HasValue && curTime >= GlobalSettings.ApplicationSettings.InverterStopTime.Value)
                    return false;
                return true;
            }
        }

        public abstract List<DeviceBase> GenericDeviceList { get; }

        #region DevicePort

        public bool InitialiseStream()
        {
            if (!StreamInitialised)
                try
                {
                    if (PortName == null || PortName.Trim() == "")
                    {
                        LogMessage("StartPortReader - Port Name required for serial communication with device", LogEntryType.ErrorMessage);
                        DeviceStream = null;
                        Protocol.SetDeviceStream(null);
                        return false;
                    }
                    SerialStream stream = new SerialStream(DeviceManagerSettings, false);
                    DeviceStream = stream;
                    Protocol.SetDeviceStream(DeviceStream);
                    stream.Open();
                    
                    StreamInitialised = true;
                }
                catch (Exception e)
                {
                    LogMessage("StartPortReader - Port: " + PortName + " - Exception: " + e.Message, LogEntryType.ErrorMessage);
                    DeviceStream = null;
                    Protocol.SetDeviceStream(null);
                    return false;
                }

            return true;
        }

        private void CloseStream()
        {
            if (StreamInitialised)
                try
                {
                    StreamInitialised = false;
                    Protocol.SetDeviceStream(null);                    
                    ((SerialStream)DeviceStream).Close();
                    DeviceStream = null;

                    foreach (DeviceBase dev in this.GenericDeviceList)
                        dev.NotifyPortReset();
                }
                catch (Exception e)
                {
                    LogMessage("StopPortReader - Port: " + PortName + " - Exception: " + e.Message, LogEntryType.ErrorMessage);

                    ((SerialStream)DeviceStream).ResetToClosed();

                    DeviceStream = null;
                }
        }
        #endregion 
    }

    public abstract class DeviceManagerTyped<TDevice> : DeviceManagerBase where TDevice : DeviceBase
    {
        public List<TDevice> DeviceList { get; protected set; }

        public override List<DeviceBase> GenericDeviceList 
        { 
            get 
            {
                List<DeviceBase> list = new List<DeviceBase>();
                foreach (TDevice d in DeviceList)
                    list.Add((DeviceBase)d);
                return list; 
            } 
        }

        // abstract calls are safe as they do not attempt to use the constructed super type methods or properties
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DeviceManagerTyped(DeviceManagerSettings mmSettings,
            IDeviceManagerManager imm)
            : base(mmSettings, imm)
        {
            SetUpProtocol();
            LoadDevices();
        }

        protected abstract TDevice NewDevice(DeviceManagerDeviceSettings dmDevice);

        // Allows devices to bypass other intermediate level overrides (including the one below) of NextRunTime
        public DateTime NextRunTime_Original()
        {
            return base.NextRunTime();
        }

        public override DateTime NextRunTime()
        {
            DateTime nextTime = DateTime.MaxValue;

            foreach (TDevice dev in DeviceList)
                if (dev.Enabled)
                {
                    DateTime time = dev.NextRunTime;
                    if (time < nextTime)
                        nextTime = time;
                }

            return nextTime > DateTime.Now ? nextTime : DateTime.Now;
        }

        private void SetUpProtocol()
        {
            String protocolName = DeviceManagerSettings.DeviceGroup.Protocol;
            if (protocolName.Trim() == "")
            {
                Protocol = null;
                return;
            }

            ProtocolSettings protocolSettings = GlobalSettings.ApplicationSettings.DeviceManagementSettings.GetProtocol(protocolName);

            foreach (DeviceManagerDeviceSettings dmDeviceSettings in DeviceManagerSettings.DeviceList.Collection)
            {
                if (!dmDeviceSettings.Enabled)
                    continue;

                if (protocolName != dmDeviceSettings.DeviceSettings.Protocol)
                    LogMessage("SetUpProtocol - Incompatible device protocol - Expected: " + protocolName + " - Found: " + dmDeviceSettings.DeviceSettings.Protocol, LogEntryType.ErrorMessage);
            }

            Protocol = new Protocol(protocolSettings);
        }

        private void LoadDevices()
        {
            DeviceList = new List<TDevice>();

            PrepareForDeviceLoad();
                       
            foreach (DeviceManagerDeviceSettings dmDeviceSettings in DeviceManagerSettings.DeviceList.Collection)
            {
                TDevice device = NewDevice( dmDeviceSettings);

                DeviceList.Add(device);
            }
        }

        protected virtual void PrepareForDeviceLoad()
        {
        }

        // Following is sage - all concatenated string values are system generated not user input
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Security", "CA2100:Review SQL queries for security vulnerabilities")]
        protected List<DateTime> FindDaysWithValues(DateTime? startDate)
        {
            // cannot detect complete days until device list is populated
            if (DeviceList.Count == 0)
                return new List<DateTime>();

            GenConnection connection = null;
            String cmdStr;
            GenCommand cmd;
            try
            {
                connection = GlobalSettings.TheDB.NewConnection();
                if (GlobalSettings.SystemServices.LogTrace && startDate != null)
                    LogMessage("FindDaysWithValues", "limit day: " + startDate, LogEntryType.Trace);

                // hack for SQLite - I suspect it does a string compare that results in startDate being excluded from the list                 
                // drop back 1 day for SQLite - the possibility of an extra day in this list does not damage the final result                 
                // (in incomplete days that is)                 
                if (connection.DBType == GenDBType.SQLite && startDate != null)
                {
                    startDate -= TimeSpan.FromDays(1);
                    if (GlobalSettings.SystemServices.LogTrace && startDate != null)
                        LogMessage("FindDaysWithValues", "SQLite adjusted limit day: " + startDate, LogEntryType.Trace);
                }

                string serials = "";
                int serialCount = 0;

                foreach (TDevice device in DeviceList)
                {
                    /*
                    if (serials == "")
                        serials = ("'" + device.SerialNo + "'");
                    else
                        serials += (", '" + device.SerialNo + "'");
                    */

                    serialCount++;
                    if (serials == "")
                        serials = "and (d.SerialNumber = '" + "@SerialNo" + serialCount.ToString() + "'";
                    else
                        serials += " or d.SerialNumber = '" + "@SerialNo" + serialCount.ToString() + "'";
                }
                if (serialCount > 0)
                    serials += ") ";
                else
                    return new List<DateTime>();

                // This implementation treats a day as complete if any device under the device manager reports a full day                  
                if (startDate == null)
                    cmdStr =
                        "select distinct oh.OutputDay " +
                        "from devicedayoutput_v oh, device d " +
                        "where oh.Device_Id = d.Id " +
                        serials +
                        "order by oh.OutputDay;";
                else
                    cmdStr =
                        "select distinct oh.OutputDay " +
                        "from devicedayoutput_v oh, device d " +
                        "where oh.OutputDay >= @StartDate " +
                        "and oh.Device_Id = d.Id " +
                        serials +
                        "order by oh.OutputDay;";

                cmd = new GenCommand(cmdStr, connection);
                if (startDate != null)
                    cmd.AddParameterWithValue("@StartDate", startDate);
                serialCount = 0;
                foreach (TDevice device in DeviceList)
                {
                    serialCount++;
                    cmd.AddParameterWithValue("@SerialNo" + serialCount.ToString(), device.SerialNo);
                }
                
                GenDataReader dataReader = (GenDataReader)cmd.ExecuteReader();
                List<DateTime> dateList = new List<DateTime>(7);
                int cnt = 0;
                bool yesterdayFound = false;
                bool todayFound = false;
                DateTime today = DateTime.Today;
                DateTime yesterday = today.AddDays(-1);
                while (dataReader.Read())
                {
                    DateTime day = dataReader.GetDateTime(0);
                    yesterdayFound |= (day == yesterday);
                    todayFound |= (day == today);
                    if (day < yesterday)
                    {
                        if (GlobalSettings.SystemServices.LogDetailTrace)
                            LogMessage("FindDaysWithValues", "day: " + day, LogEntryType.DetailTrace);
                        dateList.Add(dataReader.GetDateTime(0));
                        cnt++;
                    }
                }
                if (todayFound && yesterdayFound)
                    dateList.Add(yesterday);
                dataReader.Close();
                return dateList;
            }
            catch (Exception e)
            {
                throw new Exception("FindDaysWithValues: error executing query: " + e.Message, e);
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
        }

        protected DateTime FindNewStartDate()
        {
            List<DateTime> dateList;

            try
            {
                dateList = FindEmptyDays(true, false);
            }
            catch (Exception e)
            {
                throw new Exception("FindNewStartDate: " + e.Message, e);
            }

            DateTime newStartDate;

            if (dateList.Count > 0)
                newStartDate = dateList[0];
            else
                newStartDate = DateTime.Today.Date;

            return newStartDate;
        }

        protected List<DateTime> FindEmptyDays(bool ignoreReset, bool extractHasRun)
        {
            bool resetFirstFullDay = DeviceManagerSettings.ResetFirstFullDay && !ignoreReset && !extractHasRun;
            DateTime? startDate = NextFileDate;
            if (!startDate.HasValue)
            {
                foreach (DeviceManagerDeviceSettings ds in DeviceManagerSettings.DeviceList.Collection)
                    if (ds.FirstFullDay.HasValue && (startDate == null || ds.FirstFullDay < startDate))
                        startDate = ds.FirstFullDay;
            }
            List<DateTime> completeDays;

            if (!resetFirstFullDay)
                completeDays = FindDaysWithValues(startDate);
            else
                completeDays = new List<DateTime>();

            try
            {
                // ensure we have a usable startDate                 
                if (startDate == null)
                    if (completeDays.Count > 0)
                    {
                        // limit history retrieval to configured device history limit
                        startDate = completeDays[0];
                        if (startDate < DateTime.Today.AddDays(1 - DeviceManagerSettings.MaxHistoryDays))
                            startDate = DateTime.Today.AddDays(1 - DeviceManagerSettings.MaxHistoryDays);
                    }
                    else
                        startDate = DateTime.Today;

                int numDays = (1 + (DateTime.Today - startDate.Value).Days);
                List<DateTime> incompleteDays = new List<DateTime>(numDays);

                for (int i = 0; i < numDays; i++)
                {
                    DateTime day = startDate.Value.AddDays(i);
                    if (!completeDays.Contains(day))
                    {
                        if (GlobalSettings.SystemServices.LogTrace)
                            LogMessage("FindEmptyDays", "day: " + day, LogEntryType.Trace);
                        incompleteDays.Add(day);
                    }
                }
                return incompleteDays;
            }
            catch (Exception e)
            {
                throw new Exception("FindEmptyDays: error : " + e.Message, e);
            }
        }
    }

    public abstract class DeviceManager_ActiveController<TDevice> : DeviceManagerTyped<TDevice> where TDevice : PhysicalDevice
    {
        public DeviceManager_ActiveController(DeviceManagerSettings mmSettings,
            IDeviceManagerManager imm) : base(mmSettings, imm)
        {            
        }

        public override bool DoWork()
        {
            try
            {
                bool resetRequired = false;
                foreach (TDevice device in DeviceList)
                {
                    if (device.Enabled && device.NextRunTime <= NextRunTimeStamp)
                    {
                        if (device.DeviceSettings.DeviceType == DeviceType.Inverter && !InvertersRunning)
                        {
                            device.LastRunTime = NextRunTimeStamp;
                            continue;
                        }

                        bool deviceResetRequired = false;
                        bool res = device.DoExtractReadings(ref deviceResetRequired);
                        if (res)
                            device.LastRunTime = NextRunTimeStamp;
                        resetRequired |= deviceResetRequired;
                    }
                }
                if (resetRequired)
                {
                    ThreadInfo.NoRestartBefore = DateTime.Now.AddMinutes(2.0);
                    ThreadInfo.RegisterError(DateTime.Now, "No initial response from device - resetting");
                }
                return !resetRequired;
            }
            catch (System.Threading.ThreadInterruptedException)
            {
            }
            return true;
        }
    }

    public abstract class DeviceManager_Listener<TDevice, TLiveRec, THistRec> : DeviceManagerTyped<TDevice> where TDevice : PhysicalDevice
    {
        public DeviceManager_Listener(DeviceManagerSettings mmSettings,
            IDeviceManagerManager imm, bool shareDeviceRecords)
            : base(mmSettings, imm)
        {
            InitialiseDeviceInfo(DeviceList.Count, false, shareDeviceRecords);
        }

        protected abstract void ProcessOneLiveRecord(TDevice device, TLiveRec liveRec);
        protected abstract void ProcessOneHistoryRecord(TDevice device, THistRec liveRec);

        public struct DeviceDetail
        {
            public UInt64 Address;
            public bool Enabled;

            public List<TLiveRec> LiveRecords;
            public List<THistRec> HistoryRecords;
        }

        public struct DeviceReadingInfo
        {
            public bool ShareDeviceRecords;
            public Mutex RecordsMutex;
            public ManualResetEvent RecordsAvailEvent;

            public DeviceDetail[] DeviceList;
        }

        protected DeviceReadingInfo ReadingInfo;

        protected virtual void InitialiseDeviceInfo(int numDevices, bool ignoreConfiguredDevices, bool shareDeviceRecords)
        {
            ReadingInfo.ShareDeviceRecords = shareDeviceRecords;
            ReadingInfo.DeviceList = new DeviceDetail[numDevices];

            List<TLiveRec> liveRecords = null;
            List<THistRec> histRecords = null;

            if (shareDeviceRecords)
            {
                liveRecords = new List<TLiveRec>();
                histRecords = new List<THistRec>();
            }

            for (int i = 0; i < numDevices; i++)
            {
                DeviceDetail entry;
                entry.Address = ulong.MaxValue;
                if (shareDeviceRecords)
                    entry.LiveRecords = liveRecords;
                else
                    entry.LiveRecords = new List<TLiveRec>();

                if (shareDeviceRecords)
                    entry.HistoryRecords = histRecords;
                else
                    entry.HistoryRecords = new List<THistRec>();

                // Some device managers create their own list of devices without the need for explicit config (eg SMA Sunny Explorer)
                if (!ignoreConfiguredDevices)
                    entry.Address = DeviceList[i].Address;

                entry.Enabled = DeviceList[i].Enabled;

                ReadingInfo.DeviceList[i] = entry;
            }

            ReadingInfo.RecordsMutex = new Mutex();
            ReadingInfo.RecordsAvailEvent = new ManualResetEvent(false);
        }

        public int? GetDeviceIndex(uint sensor)
        {
            for (int i = 0; i < ReadingInfo.DeviceList.Length; i++)
            {
                if (ReadingInfo.DeviceList[i].Address == sensor)
                    return i;
            }
            return null;
        }

        public override bool DoWork()
        {
            int index;

            DateTime lastZero = DateTime.MinValue;

            int? liveRecordCount = null;
            int? historyRecordCount = null;

            if (ReadingInfo.RecordsAvailEvent.WaitOne(10000))
            {
                ReadingInfo.RecordsAvailEvent.Reset();

                for (index = 0; index < DeviceList.Count; index++)
                {
                    TDevice device = DeviceList[index];
                    if (!liveRecordCount.HasValue || !ReadingInfo.ShareDeviceRecords)
                        liveRecordCount = ReadingInfo.DeviceList[index].LiveRecords.Count;
                    for(int rec = 0; rec < liveRecordCount; rec++)
                    {
                        try
                        {
                            ProcessOneLiveRecord(device, ReadingInfo.DeviceList[index].LiveRecords[rec]);
                        }
                        catch (Exception e)
                        {
                            LogMessage("DoWork.ProcessOneLiveRecord - Exception: " + e.Message, LogEntryType.ErrorMessage);
                            // discard record causing error - attempt to continue
                        }
                    }

                    // Note care required as this list is extended by another thread for some Device Managers
                    if (!ReadingInfo.ShareDeviceRecords)
                    {
                        ReadingInfo.RecordsMutex.WaitOne();
                        try
                        {
                            while (liveRecordCount > 0)
                            {
                                ReadingInfo.DeviceList[index].LiveRecords.RemoveAt(0);
                                liveRecordCount--;
                            }
                        }
                        finally
                        {
                            ReadingInfo.RecordsMutex.ReleaseMutex();
                        }
                    }
                }

                for (index = 0; index < DeviceList.Count; index++)
                {
                    TDevice device = DeviceList[index];
                    if (!historyRecordCount.HasValue || !ReadingInfo.ShareDeviceRecords)
                        historyRecordCount = ReadingInfo.DeviceList[index].HistoryRecords.Count;
                    if (device.DeviceSettings.UseHistory)
                        for (int rec = 0; rec < historyRecordCount; rec++)
                        {
                            try
                            {
                                ProcessOneHistoryRecord(device, ReadingInfo.DeviceList[index].HistoryRecords[rec]);
                            }
                            catch (Exception e)
                            {
                                LogMessage("DoWork.ProcessOneHistoryRecord - Exception: " + e.Message, LogEntryType.ErrorMessage);
                                // discard record causing error - attempt to continue
                            }
                        }

                    // Note care required as this list is extended by another thread for some Device Managers
                    if (!ReadingInfo.ShareDeviceRecords)
                    {
                        ReadingInfo.RecordsMutex.WaitOne();
                        try
                        {
                            while (historyRecordCount > 0)
                            {
                                ReadingInfo.DeviceList[index].HistoryRecords.RemoveAt(0);
                                historyRecordCount--;
                            }
                        }
                        finally
                        {
                            ReadingInfo.RecordsMutex.ReleaseMutex();
                        }
                    }       
                }

                // Note care required as this list is extended by another thread for some Device Managers
                if (ReadingInfo.ShareDeviceRecords && DeviceList.Count > 0)
                {
                    ReadingInfo.RecordsMutex.WaitOne();
                    try
                    {
                        while (liveRecordCount > 0)
                        {
                            ReadingInfo.DeviceList[0].LiveRecords.RemoveAt(0);
                            liveRecordCount--;
                        }
                        while (historyRecordCount > 0)
                        {
                            ReadingInfo.DeviceList[0].LiveRecords.RemoveAt(0);
                            historyRecordCount--;
                        }
                    }
                    finally
                    {
                        ReadingInfo.RecordsMutex.ReleaseMutex();
                    }
                }
            }           
            return true;
        }

        public override void Initialise()
        {
            base.Initialise();
        }

        public override void Finalise()
        {
            LogMessage("DeviceManager_Listener.Finalise - Name = " + DeviceManagerSettings.Name + " - manager stopped", LogEntryType.Trace);
            base.Finalise();
        }
    }

    public struct GenericMeter_LiveRecord
    {
        public DateTime TimeStampe;
        public byte[] Message;
    }

    public abstract class DeviceManager_GenericListener<TDevice> : DeviceManager_Listener<TDevice, GenericMeter_LiveRecord, GenericMeter_LiveRecord> where TDevice : MeterDevice_Generic
    {
        public PeriodType PeriodType { get; protected set; }
        public TimeSpan PeriodOffset { get; protected set; }

        private int PreviousDatabaseInterval = -1;
        private int CurrentDatabaseInterval = -1;
        private DateTime PreviousDay = DateTime.MinValue;
        private DateTime CurrentDay = DateTime.MinValue;

        protected Protocol DeviceProtocol;

        public DeviceManager_GenericListener(DeviceManagerSettings mmSettings,
            IDeviceManagerManager imm)
            : base(mmSettings, imm, true)
        {
            PeriodType = PeriodType.Day; // default
            PeriodOffset = TimeSpan.FromTicks(0); // default
            ManagerAlgorithm = (MeterListenerAlgorithm)GetDeviceAlgorithm(DeviceManagerSettings.ListenerDeviceSettings);
        }

        protected override void PrepareForDeviceLoad()
        {
            DeviceProtocol = new Protocol(Protocol.ProtocolSettings, Protocol.Calculations);
            SerialStream stream = new SerialStream(DeviceManagerSettings, true);
            DeviceProtocol.SetDeviceStream(stream);
        }

        public override DateTime NextRunTime()
        {
            return DateTime.Now;  // it just listens - no need to wait
        }

        public bool IsNewdatabaseInterval(DateTime checkTime)
        {
            int thisInterval = DeviceDataRecorders.PeriodBase.GetIntervalNo(PeriodType, PeriodOffset, checkTime, DeviceManagerSettings.DBIntervalInt);
            if (CurrentDatabaseInterval == -1)
            {
                CurrentDatabaseInterval = thisInterval;
                CurrentDay = PeriodBase.GetPeriodStart(PeriodType, PeriodOffset, checkTime, true);
            }
            if (CurrentDay != checkTime.Date || thisInterval != CurrentDatabaseInterval)
            {
                PreviousDay = CurrentDay;
                CurrentDay = PeriodBase.GetPeriodStart(PeriodType, PeriodOffset, checkTime, true);
                PreviousDatabaseInterval = CurrentDatabaseInterval;
                CurrentDatabaseInterval = thisInterval;
                return true;
            }
            return false;
        }

        public bool DoExtractReadings()
        {
            bool res = false;
            bool alarmFound = false;
            bool errorFound = false;
            String stage = "Identity";
            try
            {
                ManagerAlgorithm.ClearAttributes();               

                stage = "Reading";

                GenericConnector.DBDateTimeGeneric readingEnd = new GenericConnector.DBDateTimeGeneric();
                readingEnd.Value = DateTime.Now; // reproduce reading date precision adjustment
                DateTime curTime = readingEnd.Value;
                
                bool newInterval = IsNewdatabaseInterval(curTime);
                res = ManagerAlgorithm.ExtractReading(newInterval, ref alarmFound, ref errorFound);
                if (!res)
                    return false;

                if (ManagerAlgorithm.ErrorCode.HasValue && ManagerAlgorithm.ErrorCode != 0 && ManagerAlgorithm.HaveErrorRegisters)
                    ErrorLogger.LogError(ManagerAlgorithm.Address.ToString(), curTime, ManagerAlgorithm.ErrorCode.ToString(), 2, ManagerAlgorithm.ErrorRegisters);

                stage = "errors";
                if (alarmFound)
                    ManagerAlgorithm.LogAlarm("Reading", curTime, this);
                if (errorFound)
                    ManagerAlgorithm.LogError("Reading", curTime, this);

            }
            catch (Exception e)
            {
                LogMessage("DoExtractReadings - Stage: " + stage + " - Exception: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }

            return res;
        }

        public override bool DoWork()
        {
            try
            {
                bool res = DoExtractReadings();
                if (!res)
                {
                    ThreadInfo.NoRestartBefore = DateTime.Now.AddMinutes(2.0);
                    ThreadInfo.RegisterError(DateTime.Now, "No initial response from device channel - resetting");
                    return false;
                }

                DeviceProtocol.DeviceStream.SetFixedBuffer(((MeterListenerAlgorithm)ManagerAlgorithm).MessageContent, 0, ((MeterListenerAlgorithm)ManagerAlgorithm).MessageContent.Length);

                bool resetRequired = false;
                foreach (TDevice device in DeviceList)
                {
                    if (device.Enabled && device.NextRunTime <= NextRunTimeStamp)
                    {
                        if (device.DeviceSettings.DeviceType == DeviceType.Inverter && !InvertersRunning)
                            continue;
                        bool deviceResetRequired = false;
                        res = device.DoExtractReadings(ref deviceResetRequired);
                        // reset so next device can scan the same buffer
                        DeviceProtocol.DeviceStream.ResetFixedBuffer();
                        if (res)
                            device.LastRunTime = NextRunTimeStamp;
                        resetRequired |= deviceResetRequired;
                    }
                }
                if (resetRequired)
                {
                    ThreadInfo.NoRestartBefore = DateTime.Now.AddMinutes(2.0);
                    ThreadInfo.RegisterError(DateTime.Now, "No initial response from device - resetting");
                }
                return !resetRequired;
            }
            catch (System.Threading.ThreadInterruptedException)
            {
            }
            return true;
        }

        public override ErrorLimitAction StopOnErrorLimitReached()
        {
            return ErrorLimitAction.Pause;
        }
    }

    public class DeviceManager_MeterListener : DeviceManager_GenericListener<MeterDevice_Generic>
    {
        public DeviceManager_MeterListener(DeviceManagerSettings mmSettings,
            IDeviceManagerManager imm)
            : base(mmSettings, imm)
        {            
        }

        protected override MeterDevice_Generic NewDevice(DeviceManagerDeviceSettings dmDevice)
        {
            if (dmDevice.DeviceType == DeviceType.EnergyMeter || dmDevice.DeviceType == DeviceType.GasMeter)
            {
                MeterDevice_Generic device = new MeterDevice_Generic(this, dmDevice, DeviceProtocol);
                return device;
            }
            else
                throw new NotImplementedException();
        }

        protected override void ProcessOneLiveRecord(MeterDevice_Generic device, GenericMeter_LiveRecord liveRec)
        {
        }

        protected override void ProcessOneHistoryRecord(MeterDevice_Generic device, GenericMeter_LiveRecord liveRec)
        {
            throw new NotImplementedException();
        }
    }

    public class DeviceManager_Inverter : DeviceManager_ActiveController<InverterDevice_Generic>
    {
        public DeviceManager_Inverter(DeviceManagerSettings mmSettings,
            IDeviceManagerManager imm)
            : base(mmSettings, imm)
        {
        }

        protected override InverterDevice_Generic NewDevice(DeviceManagerDeviceSettings dmDevice)
        {
            if (dmDevice.DeviceType == DeviceType.Inverter)
            {
                InverterDevice_Generic device = new InverterDevice_Generic(this, dmDevice);
                return device;
            }
            else
                throw new NotImplementedException();
        }

    }
}
