﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DeviceDataRecorders;
using PVSettings;
using PVBCInterfaces;

namespace Device
{
    public abstract class EstimateEnergy
    {
        protected Double _LastEnergyDelta_Power;
        protected Double _LastEnergyDeltaAdjust_Power;

        protected Double _EstimateEnergySumOfDeltas;
        protected Double _EstimateEnergySumOfAdjustDeltas;

        protected Double _ActualEnergySumOfDeltas;
        protected Double _LastActualEnergy;

        protected DateTime? LastEstTime;

        protected bool UseEnergyTotal = true;
        protected bool HasStartOfDayDefect = false;

        protected bool EnergyDropFound = false;
        protected bool EnergyDropResolved = false;

        public abstract TimeSpan EstimateFromPower(Double powerWatts, DateTime curTime, Double? actualEnergy,
            Double energyTodayPrecision, Double energyTotalPrecision);

        public Double EstimateEnergySumOfDeltas { get { return _EstimateEnergySumOfDeltas; } }
        public Double LastEnergyDelta_Power { get { return _LastEnergyDelta_Power; } }
        public Double LastEnergyDeltaAdjust_Power { get { return _LastEnergyDeltaAdjust_Power; } }

        protected void CheckStartOfDayTotals(DateTime curTime, TimeSpan duration)
        {
            if (curTime.Date != (curTime - duration).Date)
            {
                double fraction = curTime.TimeOfDay.TotalSeconds / duration.TotalSeconds;
                _EstimateEnergySumOfDeltas = _LastEnergyDelta_Power * fraction;
                _EstimateEnergySumOfAdjustDeltas = _LastEnergyDeltaAdjust_Power * fraction;
                _ActualEnergySumOfDeltas = _EstimateEnergySumOfDeltas; // This is the best guess as the actual energy drop may not always occur right on change of date

                // do not adjust _LastEnergyDelta_Power as the RawReading is split between the days when recorded in DeviceDetailPeriods
                // This adjust is just to keep future energy estimates correct

                EnergyDropFound = false;
                EnergyDropResolved = false;
            }
            else if (LastEstTime.HasValue && curTime.Date != LastEstTime.Value.Date)
            {
                _EstimateEnergySumOfDeltas = _LastEnergyDelta_Power;
                _EstimateEnergySumOfAdjustDeltas = _LastEnergyDeltaAdjust_Power;
                _ActualEnergySumOfDeltas = _EstimateEnergySumOfDeltas;
            }
            LastEstTime = curTime;
        }
    }

    public class EstimateEnergyStartOfDayDefect : EstimateEnergy
    {
        private const double minActionableDelta = 0.001;

        DateTime? FirstReadingTime;

        TimeSpan QueryInterval;
        bool FirstActualFound;
        Double EnergyMargin;
        int CrazyDayStartMinutes;

        bool StartupStatusChecked;  // at initial energy calculation - extract startup state from current DeviceDetailPeriod

        Device.DeviceBase _Device;

        FeatureType FeatureType;
        uint FeatureId;      

        public EstimateEnergyStartOfDayDefect(Device.DeviceBase device, FeatureType featureType, uint featureId, bool useEnergyTotal)
        {
            _Device = device;
            EnergyMargin = 0.010;
            CrazyDayStartMinutes = _Device.DeviceSettings.CrazyDayStartMinutes;
            HasStartOfDayDefect = _Device.DeviceSettings.HasStartOfDayEnergyDefect;
            QueryInterval = TimeSpan.FromSeconds(_Device.DeviceManagerDeviceSettings.QueryIntervalInt);
            FeatureType = featureType;
            FeatureId = featureId;
            StartupStatusChecked = false;
            UseEnergyTotal = useEnergyTotal;

            _EstimateEnergySumOfDeltas = 0.0;
            _EstimateEnergySumOfAdjustDeltas = 0.0;
            _ActualEnergySumOfDeltas = 0.0;
            _LastActualEnergy = 0.0;
            _LastEnergyDelta_Power = 0.0;
            _LastEnergyDeltaAdjust_Power = 0.0;

            FirstReadingTime = null;
            LastEstTime = null;

            FirstActualFound = false;
        }

        private DeviceDetailPeriod_EnergyMeter GetDayPeriod(DateTime curTime)
        {
            // remember similar changes required in EstimateFromPower
            // retrieve period collection for the primary feature
            DeviceDetailPeriods_EnergyMeter days =
                (DeviceDetailPeriods_EnergyMeter)_Device.FindOrCreateFeaturePeriods(FeatureType, FeatureId);
            // locate the required day period list
            return days.FindOrCreate(curTime.Date);
        }

        // when the energy drop is found for HasStartOfDayDefect adjust the first reading if required
        private void AdjustStartTimeEnergy(DateTime curTime, double adjustment)
        {
            if (adjustment <= 0.0)
                return;
            DeviceDetailPeriod_EnergyMeter day = GetDayPeriod(curTime);
            List<EnergyReading> readings = day.GetReadings();
            bool deltaUpdated = false;
            
            foreach (EnergyReading reading in readings)
            {                
                reading.EnergyToday -= adjustment;
                if (reading.EnergyToday < 0)
                    reading.EnergyToday = 0;

                // reflect initial EnergyToday value in first delta value
                if (!deltaUpdated)
                {
                    double adjust = (reading.EnergyToday.Value - reading.EnergyDelta);
                    _ActualEnergySumOfDeltas += adjust;
                    _EstimateEnergySumOfDeltas += adjust;
                    reading.EnergyDelta = reading.EnergyToday.Value;
                    deltaUpdated = true;
                }
            }
        }

        // The following reestablishes state after a restart
        private void CheckStartupStatus(DateTime curTime, Double energyTodayPrecision, Double energyTotalPrecision)
        {
            DeviceDetailPeriod_EnergyMeter day = GetDayPeriod(curTime);

            StartupStatusChecked = true; // set this early to avoid recursion from the following

            foreach (EnergyReading reading in day.GetReadings())
            {
                // accumulate the deltas - if these are calculated from power the total is likely to drift away from the "actualEnergy" below
                _EstimateEnergySumOfDeltas += reading.EnergyDelta; 
                if (reading.HistEnergyDelta.HasValue)
                    _EstimateEnergySumOfAdjustDeltas += reading.HistEnergyDelta.Value; // this can contain out of bounds adjustments (from exceeding energy margin)
                LastEstTime = reading.ReadingEnd;
                if (!FirstReadingTime.HasValue)
                    FirstReadingTime = LastEstTime;

                // the current actual reading - this is generally lower resolution than the sum of deltas
                Double? actualEnergy = (UseEnergyTotal ? reading.EnergyTotal : reading.EnergyToday);

                // if there is no actualEnergy we just use the sum of deltas
                if (actualEnergy.HasValue)
                {
                    // if !HasStartOfDayDefect we want the initial energy to jump straight to the full energy value - _LastEnergyDelta_Power will initially be zero
                    // after first reading all can use actual deltas
                    // at first reading delta would add in all of the first energy reading -
                    // This cannot happen if defect is present
                    // This cannot happen if using EnergyTotal
                    bool useActualDelta = FirstActualFound || !(HasStartOfDayDefect || UseEnergyTotal);
                    if (useActualDelta)
                    {
                        if (actualEnergy >= _LastActualEnergy || FeatureType == PVSettings.FeatureType.NetExportAC) // NetExportAC can move up or down
                            _ActualEnergySumOfDeltas += (actualEnergy.Value - _LastActualEnergy);
                        else
                        {
                            EnergyDropFound = true;
                            _ActualEnergySumOfDeltas += reading.EnergyDelta; // last power estimate is best substitute for the missing actual delta
                        }
                    }
                    else
                    {
                        _ActualEnergySumOfDeltas += reading.EnergyDelta; // last power estimate is best substitute for the missing actual delta
                        FirstActualFound = true;
                    }

                    _LastActualEnergy = actualEnergy.Value;

                    if (UseEnergyTotal)
                        EnergyMargin = energyTotalPrecision;
                    else
                        EnergyMargin = energyTodayPrecision;

                    // Ensure energy estimate based on power remains within the margin of the last reported actual energy sum of deltas
                    double totalDeltas = _EstimateEnergySumOfDeltas + _EstimateEnergySumOfAdjustDeltas;
                    double diff = (Math.Abs(totalDeltas - _ActualEnergySumOfDeltas));
                    if (diff > EnergyMargin)
                    {
                        double calcDelta = (diff - EnergyMargin);
                        if (calcDelta > minActionableDelta)
                            if (totalDeltas > _ActualEnergySumOfDeltas)
                            {
                                _EstimateEnergySumOfAdjustDeltas -= calcDelta;
                                if (reading.HistEnergyDelta.HasValue)
                                    reading.HistEnergyDelta -= calcDelta;
                                else
                                    reading.HistEnergyDelta = -calcDelta;
                            }
                            else
                            {
                                _EstimateEnergySumOfAdjustDeltas += calcDelta;
                                if (reading.HistEnergyDelta.HasValue)
                                    reading.HistEnergyDelta += calcDelta;
                                else
                                    reading.HistEnergyDelta = calcDelta;
                            }
                    }
                }
            }
        }

        public override TimeSpan EstimateFromPower(Double powerWatts, DateTime curTime, Double? actualEnergy,
            Double energyTodayPrecision, Double energyTotalPrecision)
        {
            // remember similar changes required in CheckStartupStatus
            if (!StartupStatusChecked)
                CheckStartupStatus(curTime, energyTodayPrecision, energyTotalPrecision);

            TimeSpan duration;

            if (LastEstTime.HasValue)
            {
                duration = (curTime - LastEstTime.Value);
                // prevent large estimate periods caused by PVBC being offline fo a period
                if (duration.Seconds > _Device.DeviceManagerDeviceSettings.DBIntervalInt)
                    duration = TimeSpan.FromSeconds(_Device.DeviceManagerDeviceSettings.DBIntervalInt);
            }
            else
                duration = QueryInterval;
            
            if (!FirstReadingTime.HasValue)
                FirstReadingTime = curTime;

            _LastEnergyDelta_Power = (powerWatts * duration.TotalHours) / 1000.0; // watts to KWH
            _LastEnergyDeltaAdjust_Power = 0.0;
            _EstimateEnergySumOfDeltas += _LastEnergyDelta_Power;

            if (GlobalSettings.SystemServices.LogTrace)
                _Device.DeviceManager.LogMessage("EstimateEnergy", "EstimateFromPower - Time: " + curTime + " - Power: " + powerWatts +
                    " - Duration: " + duration.TotalSeconds + " - Energy Delta: " + _LastEnergyDelta_Power, LogEntryType.Trace);

            if (actualEnergy.HasValue)
            {
                // if !HasStartOfDayDefect we want the initial energy to jump straight to the full energy value - _LastEnergyDelta_Power will initially be zero
                // after first reading all can use actual deltas
                // at first reading delta would add in all of the first energy reading -
                // This cannot happen if defect is present
                // This cannot happen if using EnergyTotal
                bool useActualDelta = FirstActualFound || !(HasStartOfDayDefect || UseEnergyTotal);
                if (useActualDelta)
                {
                    if (actualEnergy >= _LastActualEnergy) 
                        _ActualEnergySumOfDeltas += (actualEnergy.Value - _LastActualEnergy);
                    else 
                    {                        
                        _ActualEnergySumOfDeltas += _LastEnergyDelta_Power; // last power estimate is best substitute for the missing actual delta
                        if (!UseEnergyTotal && !EnergyDropFound)  // only do start adjust for energy drop once per day
                        {
                            // Energy drop should not occur with UseEnergyTotal
                            // Energy drop does not always occur right on start of day as inverter and PC may trigger start of day at different times
                            // but generally before significant generation
                            // HasStartOfDayDefect is the exception with energy drop for start of day occuring after start of significant generation
                            AdjustStartTimeEnergy(curTime, actualEnergy.Value - _ActualEnergySumOfDeltas);
                        }
                        EnergyDropFound = true;
                    }

                    // after CrazyDayStartMinutes assume energy drop has been missed 
                    if (HasStartOfDayDefect && !UseEnergyTotal && !EnergyDropResolved && !EnergyDropFound && (curTime - FirstReadingTime.Value).TotalMinutes > CrazyDayStartMinutes)
                    {
                        // this occurs when inverter monitoring started after the start of day energy drop
                        // this means we can not detect the correct total energy for today
                        // discard all but the recorded delta - this avoids including yesterday total as part of today
                        EnergyDropResolved = true;
                        AdjustStartTimeEnergy(curTime, actualEnergy.Value - _ActualEnergySumOfDeltas);
                    }
                }
                else
                {
                    _ActualEnergySumOfDeltas += _LastEnergyDelta_Power; // last power estimate is best substitute for the missing actual delta
                    FirstActualFound = true;
                }

                _LastActualEnergy = actualEnergy.Value;

                if (UseEnergyTotal)
                    EnergyMargin = energyTotalPrecision;
                else
                    EnergyMargin = energyTodayPrecision;

                // Ensure energy estimate based on power remains within the margin of the last reported actual energy sum of deltas
                double totalDeltas = _EstimateEnergySumOfDeltas + _EstimateEnergySumOfAdjustDeltas;
                double diff = (Math.Abs(totalDeltas - _ActualEnergySumOfDeltas));
                if (diff > EnergyMargin)
                {
                    double calcDelta = (diff - EnergyMargin);
                    if (calcDelta > minActionableDelta)
                        if (totalDeltas > _ActualEnergySumOfDeltas)
                        {
                            _EstimateEnergySumOfAdjustDeltas -= calcDelta;                            
                            _LastEnergyDeltaAdjust_Power = -calcDelta;
                        }
                        else
                        {
                            _EstimateEnergySumOfAdjustDeltas += calcDelta;
                            _LastEnergyDeltaAdjust_Power = calcDelta;
                        }
                }
                CheckStartOfDayTotals(curTime, duration);
            }

            return duration;
        }
    }

    public class EstimateEnergyGeneric : EstimateEnergy
    {
        private const double minActionableDelta = 0.001;

        TimeSpan QueryInterval;
        bool FirstActualFound;

        DateTime? LastEstTimeStartup = null;

        bool StartupStatusChecked;  // at initial energy calculation - extract startup state from current DeviceDetailPeriod

        Device.DeviceBase _Device;

        FeatureType FeatureType;
        uint FeatureId;

        public EstimateEnergyGeneric(Device.DeviceBase device, FeatureType featureType, uint featureId)
        {
            _Device = device;
            QueryInterval = TimeSpan.FromSeconds(_Device.DeviceManagerDeviceSettings.QueryIntervalInt);
            FeatureType = featureType;
            FeatureId = featureId;
            StartupStatusChecked = false;
            UseEnergyTotal = true;

            _EstimateEnergySumOfDeltas = 0.0;
            _EstimateEnergySumOfAdjustDeltas = 0.0;
            _ActualEnergySumOfDeltas = 0.0;
            _LastActualEnergy = 0.0;
            _LastEnergyDelta_Power = 0.0;
            _LastEnergyDeltaAdjust_Power = 0.0;

            LastEstTime = null;
            FirstActualFound = false;
        }

        private DeviceDetailPeriod_EnergyMeter GetDayPeriod(DateTime curTime)
        {
            // remember similar changes required in EstimateFromPower
            // retrieve period collection for the primary feature
            DeviceDetailPeriods_EnergyMeter days =
                (DeviceDetailPeriods_EnergyMeter)_Device.FindOrCreateFeaturePeriods(FeatureType, FeatureId);
            // locate the required day period list
            return days.FindOrCreate(curTime.Date);
        }

        private void AccumulateActual(Double energyDelta, DateTime curTime, Double actualEnergy)
        {
            if (FirstActualFound)
            {               
                _ActualEnergySumOfDeltas += (actualEnergy - _LastActualEnergy);
                if (GlobalSettings.SystemServices.LogTrace)
                    GlobalSettings.LogMessage("EstimateEnergy", "EstimateFromPowerCommon - summation - Sum: "
                        + _ActualEnergySumOfDeltas + " - actual: " + actualEnergy + " - curTime: " + curTime, LogEntryType.Trace);
            }
            else
            {
                _ActualEnergySumOfDeltas += energyDelta; // last power estimate is best substitute for the missing actual delta
                FirstActualFound = true;
                if (GlobalSettings.SystemServices.LogTrace)
                    GlobalSettings.LogMessage("EstimateEnergy", "EstimateFromPowerCommon - first summation - Sum: "
                        + _ActualEnergySumOfDeltas + " - actual: " + actualEnergy + " - curTime: " + curTime, LogEntryType.Trace);
            }
        }

        private void EstimateFromPowerCommon(Double energyDelta, DateTime curTime, Double actualEnergy,
            Double energyTodayPrecision, Double energyTotalPrecision, bool hasEstimateGap, ref Double energyDeltaAdjust)
        {
            AccumulateActual(energyDelta, curTime, actualEnergy);

            Double energyMargin;
            
            energyMargin = energyTotalPrecision;
            
            // Ensure energy estimate based on power remains within the margin of the last reported actual energy sum of deltas
            double totalDeltas = _EstimateEnergySumOfDeltas + _EstimateEnergySumOfAdjustDeltas;
            double diff = (Math.Abs(totalDeltas - _ActualEnergySumOfDeltas));
            if (diff > energyMargin)
            {
                double calcDelta;
                if (hasEstimateGap)
                    calcDelta = diff; // at an offline period point correct back to match sum of actuals - centrepoint of legal range
                else
                    calcDelta = (diff - energyMargin); // at other points just correct back to in-bounds - edge of in-bounds

                if (calcDelta > minActionableDelta)
                {
                    if (totalDeltas > _ActualEnergySumOfDeltas)
                    {
                        _EstimateEnergySumOfAdjustDeltas -= calcDelta;
                        energyDeltaAdjust = -calcDelta;
                    }
                    else
                    {
                        _EstimateEnergySumOfAdjustDeltas += calcDelta;
                        energyDeltaAdjust = calcDelta;
                    }
                }
            }
            if (GlobalSettings.SystemServices.LogTrace)
                GlobalSettings.LogMessage("EstimateEnergy", "EstimateFromPowerCommon - totalDeltas: " + totalDeltas
                    + " - _ActualEnergySumOfDeltas: " + _ActualEnergySumOfDeltas
                    + " - diff: " + diff + " - energyMargin: " + energyMargin + " - hasEstimateGap: " + hasEstimateGap
                    + " - _EstimateEnergySumOfDeltas: " + _EstimateEnergySumOfDeltas + " - _EstimateEnergySumOfAdjustDeltas: " + _EstimateEnergySumOfAdjustDeltas
                    , LogEntryType.Trace);
   
            _LastActualEnergy = actualEnergy;
        }

        // The following reestablishes state after a restart
        private void CheckStartupStatus(DateTime curTime, Double energyTodayPrecision, Double energyTotalPrecision)
        {
            DeviceDetailPeriod_EnergyMeter day = GetDayPeriod(curTime);

            StartupStatusChecked = true; // set this early to avoid recursion from the following

            if (GlobalSettings.SystemServices.LogTrace)
                GlobalSettings.LogMessage("EstimateEnergy", "CheckStartupStatus - starting", LogEntryType.Trace);

            bool firstRec = true;
            foreach (EnergyReading reading in day.GetReadings())
            {
                // accumulate the deltas - if these are calculated from power the total is likely to drift away from the "actualEnergy" below
                _EstimateEnergySumOfDeltas += reading.EnergyDelta;
                if (reading.HistEnergyDelta.HasValue)
                    _EstimateEnergySumOfAdjustDeltas += reading.HistEnergyDelta.Value; // this can contain out of bounds adjustments (from exceeding energy margin)
                
                // the current actual reading - this is generally lower resolution than the sum of deltas
                Double? actualEnergy = reading.EnergyTotal;
               
                if (firstRec)
                {
                    firstRec = false;
                    _LastActualEnergy = actualEnergy.HasValue ? actualEnergy.Value : 0.0;
                }

                AccumulateActual(reading.EnergyDelta, curTime, actualEnergy.HasValue ? actualEnergy.Value : 0.0);

                _LastActualEnergy = actualEnergy.HasValue ? actualEnergy.Value : 0.0;

                LastEstTime = reading.ReadingEnd;
            }
            if (GlobalSettings.SystemServices.LogTrace)
                GlobalSettings.LogMessage("EstimateEnergy", "CheckStartupStatus - complete", LogEntryType.Trace);
        }

        public override TimeSpan EstimateFromPower(Double powerWatts, DateTime curTime, Double? actualEnergy,
            Double energyTodayPrecision, Double energyTotalPrecision)
        {
            TimeSpan duration;

            if (!StartupStatusChecked)
            {
                CheckStartupStatus(curTime, energyTodayPrecision, energyTotalPrecision);
                LastEstTimeStartup = LastEstTime;
                LastEstTime = null;
            }

            if (LastEstTime.HasValue)
                duration = (curTime - LastEstTime.Value);               
            else
                duration = QueryInterval;
            
            bool hasEstimateGap = false;
            if (LastEstTimeStartup.HasValue && !LastEstTime.HasValue)
            {
                TimeSpan gapDuration = curTime - LastEstTimeStartup.Value;
                if (gapDuration <= duration)
                    duration = gapDuration;
                else
                    hasEstimateGap = true; // the estimate was for a period shorter than the record interval due to PVBC being offline
                _LastEnergyDelta_Power = (powerWatts * duration.TotalHours) / 1000.0; // watts to KWH
                duration = gapDuration;
            }
            else
                _LastEnergyDelta_Power = (powerWatts * duration.TotalHours) / 1000.0; // watts to KWH

            _LastEnergyDeltaAdjust_Power = 0.0;
            _EstimateEnergySumOfDeltas += _LastEnergyDelta_Power;

            if (GlobalSettings.SystemServices.LogTrace)
                _Device.DeviceManager.LogMessage("EstimateEnergy", "EstimateFromPower - Time: " + curTime + " - Power: " + powerWatts +
                    " - Duration: " + duration.TotalSeconds + " - Energy Delta: " + _LastEnergyDelta_Power, LogEntryType.Trace);

            if (actualEnergy.HasValue)
                EstimateFromPowerCommon(_LastEnergyDelta_Power, curTime, actualEnergy.Value, 
                    energyTodayPrecision, energyTotalPrecision, hasEstimateGap, ref _LastEnergyDeltaAdjust_Power);           

            CheckStartOfDayTotals(curTime, duration);

            return duration;
        }
    }

}
