﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PVBCInterfaces;
using GenThreadManagement;


namespace Conversations
{
    public class CC_EnviR_Converse : Converse
    {
        public CC_EnviR_Converse( )
            : base( null)
        {
            // don't bother with ConversationLoader - it is a very simple pattern
            // below creates the conversation definition
            DefaultTimeOut = 2;
            Conversation conv = new Conversation(this, "CC_EnviR_Receive");
            Conversations.Add(conv);
            Message message = new Message(conv, MessageType.Find, "");
            Literal literal = new Literal(Element.StringToHex("<msg>", " "), conv);
            message.Elements.Add(literal);
            conv.Messages.Add(message);

            message = new Message(conv, MessageType.ExtractDynamic, "");
            DynamicByteVar variable = new DynamicByteVar("DATA", 10000, conv);
            Variables.Add(variable);
            UseVariable useVar = new UseVariable(conv, "DATA");
            message.Elements.Add(useVar);
            literal = new Literal(Element.StringToHex("</msg>", " "), conv);
            message.Elements.Add(literal);
            conv.Messages.Add(message);
        }
    }
}
