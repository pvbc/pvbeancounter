﻿/*
* Copyright (c) 2010 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Data.Common;
using GenericConnector;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Text;
using PVSettings;
using MackayFisher.Utilities;
using GenThreadManagement;
using DeviceControl;
using OutputManagers;
using PVBCInterfaces;

namespace PVService
{
    public class ManagerManager : IDeviceManagerManager, IOutputManagerManager
    {
        public bool LiveLoadForced { get; private set; }

        public ExecutionManager ExecutionManager;
        public bool RunMonitors { get; private set; }
        public IEvents EnergyEvents { get; private set; }

        public EventManager EventManager { get; private set; }
        public List<DeviceControl.DeviceManagerBase> AllRealDeviceManagers { get; private set; }
        public List<DeviceControl.DeviceManagerBase> RunningDeviceManagers { get; private set; }
        public List<DeviceControl.DeviceManagerBase> ConsolidationDeviceManagers { get; private set; }
        public List<OutputManagers.PVOutputManager> RunningOutputManagers { get; private set; }

        private Object OutputReadyLock;

        public void ScanForEvents()
        {
            EnergyEvents.ScanForEvents();
        }

        public int GetOutputManagerInterval(String systemId)
        {
            foreach (OutputManagers.PVOutputManager omgr in RunningOutputManagers)
                if (omgr.SystemId == systemId) 
                    return omgr.Settings.DataIntervalSeconds;
               
            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("GetOutputManagerInterval - Cannot locate running PVOutput (probably disabled) - SystemId: " + systemId, LogEntryType.Trace);
            return 300;
        }

        public void SetOutputReady(String systemId)
        {
            bool found = false;
            lock (OutputReadyLock)
            {
                foreach(OutputManagers.PVOutputManager omgr in RunningOutputManagers)
                {
                    if (omgr.SystemId == systemId)
                    {
                        found = true;
                        omgr.OutputReadyEvent.Set();
                        break;
                    }                    
                }
            }
            if (!found)
                if (GlobalSettings.SystemServices.LogTrace)
                    LogMessage("SetPVOutputReady - Cannot locate running PVOutput (probably disabled) - SystemId: " + systemId, LogEntryType.Trace);            
        }

        public ManagerManager(ExecutionManager executionStateManager)
        {
            EnergyEvents = new DeviceControl.EnergyEvents(GlobalSettings.ApplicationSettings, this);
            ExecutionManager = executionStateManager;
            RunMonitors = false;

            // SQLite and MS Access requires explicit concurrency control
            // MySQL and SQL Server isolate concurrent queries from intra-command db changes (such as delete from table where...)
            GlobalSettings.SystemServices.UseDatabaseMutex = (GlobalSettings.ApplicationSettings.DatabaseType != "MySql" && GlobalSettings.ApplicationSettings.DatabaseType != "SQL Server");
            LiveLoadForced = false;

            RunningOutputManagers = new List<OutputManagers.PVOutputManager>();
            AllRealDeviceManagers = new List<DeviceControl.DeviceManagerBase>();
            RunningDeviceManagers = new List<DeviceControl.DeviceManagerBase>();
            ConsolidationDeviceManagers = new List<DeviceControl.DeviceManagerBase>();
            EventManager = null;
            OutputReadyLock = new Object();
        }

        private static void LogMessage(String message, LogEntryType logEntryType)
        {
            GlobalSettings.LogMessage("ManagerManager", message, logEntryType);
        }

        public void ReleaseErrorLoggers()
        {
            foreach (DeviceControl.DeviceManagerBase rmm in RunningDeviceManagers)
            {
                rmm.CloseErrorLogger();
            }
        }

        private DeviceManagerBase ConstructDeviceManager(DeviceManagerSettings dmSettings)
        {
            DeviceManagerBase deviceManager;

            if (dmSettings.ManagerType == DeviceManagerType.SMA_SunnyExplorer)
                deviceManager = new DeviceManager_SMA_SunnyExplorer(dmSettings, this);
            else if (dmSettings.ManagerType == DeviceManagerType.CC128)
                deviceManager = new DeviceManager_CC128(dmSettings, this);
            else if (dmSettings.ManagerType == DeviceManagerType.SMA_WebBox)
                deviceManager = new DeviceManager_SMA_WebBox(dmSettings, this);
            else if (dmSettings.ManagerType == DeviceManagerType.Owl_Meter)
                deviceManager = new DeviceManager_Owl(dmSettings, this);
            else if (dmSettings.ManagerType == DeviceManagerType.EW4009)
                deviceManager = new DeviceManager_EW4009(dmSettings, this);
            else if (dmSettings.ManagerType == DeviceManagerType.Consolidation)
                deviceManager = new DeviceManager_EnergyConsolidation(dmSettings, this);
            else if (dmSettings.ManagerType == DeviceManagerType.DutchSmartMeter)
                deviceManager = new DeviceManager_MeterListener(dmSettings, this);
            else if (dmSettings.ManagerType == DeviceManagerType.RAVEn_USB)
                deviceManager = new DeviceManager_RAVEn_USB(dmSettings, this);
            else
                deviceManager = new DeviceManager_Inverter(dmSettings, this);

            return deviceManager;
        }

        private void StartDeviceManager(DeviceManagerBase deviceManager)
        {
            int genThreadId = ExecutionManager.GenThreadManager.AddThread(deviceManager, null, true);
            ExecutionManager.GenThreadManager.StartThread(genThreadId);            
        }

        public Device.DeviceBase FindDeviceFromSettings(DeviceManagerDeviceSettings deviceSettings)
        {
            foreach (DeviceManagerBase dm in RunningDeviceManagers)
                foreach (Device.DeviceBase d in dm.GenericDeviceList)
                    if (d.DeviceManagerDeviceSettings == deviceSettings)
                        return d;
            foreach (DeviceManagerBase dm in ConsolidationDeviceManagers)
                foreach (Device.DeviceBase d in dm.GenericDeviceList)
                    if (d.DeviceManagerDeviceSettings == deviceSettings)
                        return d;
            return null;
        }

        public Device.EnergyConsolidationDevice FindPVOutputConsolidationDevice(String systemId)
        {
            foreach (DeviceControl.DeviceManagerBase cdm in ConsolidationDeviceManagers)
            {
                Device.EnergyConsolidationDevice d = ((DeviceManager_EnergyConsolidation)cdm).GetPVOutputConsolidationDevice( systemId);
                if (d != null)
                    return d;
            }
            return null;
        }

        private void UnbindConsolidations(DeviceManagerBase devMgr)
        {
            foreach (DeviceManagerBase dm in RunningDeviceManagers)
            {
                if (devMgr == dm)
                    foreach (Device.DeviceBase d in dm.GenericDeviceList)
                        d.UnbindConsolidations();
            }
        }

        private bool StopDeviceManager(DeviceManagerBase devMgr)
        {
            devMgr.IsRunning = false;
            devMgr.ExitEvent.Set();
            try
            {
                GenThreadManager.ThisGenThreadManager.ThreadOperationsLock.AcquireWriterLock(20000);
            }
            catch (ApplicationException)
            {
                return false;
            }
            UnbindConsolidations(devMgr);
            AllRealDeviceManagers.Remove(devMgr);
            RunningDeviceManagers.Remove(devMgr);
            GenThreadManager.ThisGenThreadManager.ThreadOperationsLock.ReleaseWriterLock();
            return true;
        }

        public void DoAutoRestart()
        {
            bool discardStateRestartFound = false;
            foreach (GenThreadInfo info in GenThreadManager.ThisGenThreadManager.Threads)
            {
                if (info.AutoRestart && info.OS_Thread != null
                    && info.CurrentErrorCount < GenThreadInfo.ErrorLimit
                    && (info.NoRestartBefore.HasValue ? (info.NoRestartBefore.Value <= DateTime.Now) : true)
                    && (info.OS_Thread != null)
                    && (info.OS_Thread.ThreadState == ThreadState.Stopped || info.OS_Thread.ThreadState == ThreadState.Aborted)
                    && info.GenThread is DeviceManagerBase
                    && ((DeviceManagerBase)info.GenThread).DeviceManagerSettings.DiscardStateAtRestart)
                {
                    DeviceManagerBase dm = (DeviceManagerBase)info.GenThread;
                    LogMessage("ManagerManager.DoAutoRestart - Discard State Restart - " + dm.ThreadName, LogEntryType.Trace);
                    discardStateRestartFound = true;
                    
                    bool haveLock = false;
                    try
                    {
                        GenThreadManager.ThisGenThreadManager.ThreadOperationsLock.AcquireWriterLock(20000);
                        haveLock = true;
                        UnbindConsolidations(dm);
                        AllRealDeviceManagers.Remove(dm);
                        RunningDeviceManagers.Remove(dm);

                        dm = ConstructDeviceManager(((DeviceManagerBase)info.GenThread).DeviceManagerSettings);
                        info.GenThread = dm;
                        dm.ThreadInfo = info;
                        foreach (Device.DeviceBase d in dm.GenericDeviceList)
                            d.BindConsolidations(this);

                        AllRealDeviceManagers.Add(dm);
                        RunningDeviceManagers.Add(dm);
                        info.OS_Thread = null;
                    }
                    catch (Exception e)
                    {
                        LogMessage("ManagerManager.DoAutoRestart - Exception: " + e.Message, LogEntryType.ErrorMessage);
                    }
                    finally
                    {
                        if (haveLock)
                            GenThreadManager.ThisGenThreadManager.ThreadOperationsLock.ReleaseWriterLock();
                    }
                }               
            }
            if (discardStateRestartFound)
                SetConsolidationDepth();

            foreach (GenThreadInfo info in GenThreadManager.ThisGenThreadManager.Threads)
            {
                if (info.AutoRestart 
                    && info.CurrentErrorCount < GenThreadInfo.ErrorLimit
                    && (info.NoRestartBefore.HasValue ? (info.NoRestartBefore.Value <= DateTime.Now) : true))
                {
                    if (info.GenThread is DeviceManagerBase)
                        if (((DeviceManagerBase)info.GenThread).DeviceManagerSettings.DiscardStateAtRestart)
                        {
                            if (info.OS_Thread == null)
                                GenThreadManager.ThisGenThreadManager.RestartThread(info);
                            continue;                            
                        }

                    if (info.OS_Thread == null || info.OS_Thread.ThreadState == ThreadState.Stopped || info.OS_Thread.ThreadState == ThreadState.Aborted)
                        GenThreadManager.ThisGenThreadManager.RestartThread(info);
                }
            }
        }

        private void SetConsolidationDepth()
        {
            foreach (DeviceManagerBase dm in ConsolidationDeviceManagers)
            {
                int maxDepth = 0;
                foreach (Device.DeviceBase d in dm.GenericDeviceList)
                {
                    int depth = ((Device.ConsolidationDevice)d).SetHierarchyDepth();
                    if (depth > maxDepth)
                        maxDepth = depth;
                }
                EnergyEvents.MaxConsolidationDepth = maxDepth;
            }
        }

        private int StartDeviceManagers()
        {
            int cnt = 0;
            try
            {
                LogMessage("Loading Device Managers", LogEntryType.Information);

                foreach (DeviceManagerSettings dmSettings in GlobalSettings.ApplicationSettings.DeviceManagerListCollection)
                {
                    DeviceManagerBase dm = null;
                    if (dmSettings.ManagerType == DeviceManagerType.Consolidation || dmSettings.Enabled)
                        dm = ConstructDeviceManager(dmSettings);

                    if (dm == null)
                        continue;

                    if (dmSettings.ManagerType == DeviceManagerType.Consolidation)
                    {
                        ConsolidationDeviceManagers.Add(dm);
                        continue;
                    }

                    AllRealDeviceManagers.Add(dm);
                    if (!dmSettings.Enabled)
                        continue;

                    RunningDeviceManagers.Add(dm);
                    cnt++;
                }

                foreach (DeviceManagerBase dm in AllRealDeviceManagers)
                {
                    foreach(Device.DeviceBase d in dm.GenericDeviceList)
                        d.BindConsolidations(this);                    
                }
                foreach (DeviceManagerBase dm in ConsolidationDeviceManagers)
                {
                    foreach (Device.DeviceBase d in dm.GenericDeviceList)
                        d.BindConsolidations(this);
                }
                SetConsolidationDepth();
                
                foreach (DeviceManagerBase dm in RunningDeviceManagers)
                    StartDeviceManager(dm);
                if (RunningDeviceManagers.Count > 0)
                    LogMessage("Device Managers Loaded", LogEntryType.StatusChange);
                else
                    LogMessage("No Device Managers Loaded", LogEntryType.Information);
            }
            catch (Exception e)
            {
                LogMessage("Exception starting Device Managers: " + e.Message, LogEntryType.ErrorMessage);
            }
            return cnt;
        }

        public void StartService(bool fullStartup)
        {
            // The following establishes an appropriate DateTime precision for use with the selected database
            // Need to ensure that an inserted value can be retrieved with a select equality operation
            // and the value bound for insertion is the value returned to the binding after a row retrieval

            DBDateTimeGeneric.SetDefaultDBType(GenDatabase.GetDBType(GlobalSettings.ApplicationSettings.DatabaseType));
            GlobalSettings.TheDB = new GenDatabase(GlobalSettings.ApplicationSettings.Host, GlobalSettings.ApplicationSettings.Database,
                GlobalSettings.ApplicationSettings.UserName, GlobalSettings.ApplicationSettings.Password,
                GlobalSettings.ApplicationSettings.DatabaseType, GlobalSettings.ApplicationSettings.ProviderType,
                GlobalSettings.ApplicationSettings.ProviderName, GlobalSettings.ApplicationSettings.OleDbName,
                GlobalSettings.ApplicationSettings.ConnectionString, GlobalSettings.ApplicationSettings.DefaultDirectory, GlobalSettings.SystemServices);

            if (fullStartup)
            {
                LogMessage("StartService: connecting to database: " + GlobalSettings.TheDB.ConnectionString, LogEntryType.Information);
                PVSettings.VersionManager vm = new VersionManager();
                GenConnection con = GlobalSettings.TheDB.NewConnection();
                vm.UpdateVersion(con);
                con.Close();
                con.Dispose();
            }

            RunMonitors = true;

            if (GlobalSettings.ApplicationSettings.EmitEvents)
                StartEventManager();

            StartDeviceManagers();

            if (GlobalSettings.ApplicationSettings.EnablePVOutput)
                StartOutputManagers();
        }
      
        public void StopService()
        {
            try
            {
                RunMonitors = false;
                ExecutionManager.GenThreadManager.StopThreads();                
                RunningOutputManagers.Clear();
                AllRealDeviceManagers.Clear();
                RunningDeviceManagers.Clear();
                ConsolidationDeviceManagers.Clear();
                EventManager = null;
            }
            catch (Exception e)
            {
                LogMessage("StopService: Exception: " + e.Message, LogEntryType.ErrorMessage);
            }
        }

        private bool StartEventManager()
        {
            try
            {
                LogMessage("Loading Event Manager", LogEntryType.Information);
                EventManager = new EventManager(EnergyEvents);

                int emId = ExecutionManager.GenThreadManager.AddThread(EventManager, null, false);
                ExecutionManager.GenThreadManager.StartThread(emId);

                LogMessage("Event Manager Loaded", LogEntryType.StatusChange);

                return true;
            }
            catch (Exception e)
            {
                LogMessage("Starting Event Manager - Exception: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }
        }

        private int StartOutputManagers()
        {
            int count = 0;

            String managerName = "";
            try
            {
                LogMessage("Loading PvOutput Managers", LogEntryType.Information);

                for (int i = 0; i < GlobalSettings.ApplicationSettings.PvOutputSystemList.Collection.Count; i++)
                {
                    PvOutputSiteSettings managerSettings = GlobalSettings.ApplicationSettings.PvOutputSystemList.Collection[i];
                    managerName = managerSettings.Name;

                    if (!managerSettings.Enable)
                        continue;

                    OutputManagers.PVOutputManager pvOutputManager = new OutputManagers.PVOutputManager(this, managerSettings);

                    int PVOutputId = ExecutionManager.GenThreadManager.AddThread(pvOutputManager, null, true);
                    ExecutionManager.GenThreadManager.StartThread(PVOutputId);

                    LogMessage("PVOutput Manager - " + managerName + " - Loaded", LogEntryType.Trace);

                    count++;
                    RunningOutputManagers.Add(pvOutputManager);
                }

                if (count > 0)
                    LogMessage(count + " PVOutput Managers Loaded", LogEntryType.StatusChange);
                else
                    LogMessage("No PVOutput Managers Loaded", LogEntryType.Information);

                return count;
            }
            catch (Exception e)
            {
                LogMessage("Starting PvOutput Managers - Type: " + managerName + " - Exception: " + e.Message, LogEntryType.ErrorMessage);
                throw new PVException(PVExceptionType.UnexpectedError, "StartInverterManagers: " + e.Message, e);
            }
        }
    }
}
