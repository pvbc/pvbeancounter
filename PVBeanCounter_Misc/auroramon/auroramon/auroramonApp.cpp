
/***************************************************************************
 *   Copyright (C) 2012 by Jonathan Duddington                             *
 *   email: jonsd@users.sourceforge.net                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see:                                 *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

#include <wx/fileconf.h>
#include "auroramon.h"


class auroraApp : public wxApp
{
    public:
        virtual bool OnInit();
        int OnExit();
};


IMPLEMENT_APP(auroraApp);

wxString path_home;

wxString serial_port;



wxString data_dir = wxGetCwd();
wxString data_year_dir = data_dir;


void ConfigInit()
{
    int ix;
    wxString string;
    wxFileConfig *pConfig = new wxFileConfig(_T("aurora"));
	wxFileConfig::Set(pConfig);
#ifdef __WXMSW__
	pConfig->Read(_T("/data"), &data_dir, path_home+_T("\\aurora"));
	pConfig->Read(_T("/serial_port"),&serial_port,_T("COM1"));
#else
	pConfig->Read(_T("/data"), &data_dir, path_home+_T("/aurora"));
	pConfig->Read(_T("/serial_port"),&serial_port,_T("/dev/ttyS0"));
#endif
	pConfig->Read(_T("/longitude"), &Longitude, 500);
	pConfig->Read(_T("/latitude"), &Latitude, 45);
	pConfig->Read(_T("/timezone"), &TimeZone2, 99);
	pConfig->Read(_T("/histogram_max"), &histogram_max, 20.0);
    pConfig->Read(_T("/logchange"), &string, wxEmptyString);
    strncpy0(date_logchange, string.mb_str(wxConvLocal), sizeof(date_logchange));

    pConfig->Read(_T("/extra_readings"), &string, _T("3001 2004"));  // default is Grid_V Grid_Hz
    SetExtraReadings(string);

	for(ix=0; ix<N_INV; ix++)
	{
	    pConfig->SetPath(wxString::Format(_T("/inverter%d"), ix));
	    if(ix==0)
            inverter_address[ix] = pConfig->Read(_T("address"), (long)2);
        else
            inverter_address[ix] = pConfig->Read(_T("address"), (long)0);
        pConfig->Read(_T("today"), &string, _T(""));
        strncpy0(today_done[ix], string.mb_str(wxConvLocal), sizeof(today_done[ix]));
        pConfig->Read(_T("previous_total"), &previous_total[ix], 0);
        pConfig->Read(_T("energy"), &energy_string[ix]);
        pConfig->Read(_T("logperiod"), &logging_period_index[ix], 0);
        pConfig->Read(_T("logpath"), &logging_path[ix], wxEmptyString);
	}

    for(ix=0; ix<N_PANEL_GROUPS; ix++)
    {
	    pConfig->SetPath(wxString::Format(_T("/panelgroup%d"), ix));
	    pConfig->Read(_T("tilt"), &panel_groups[ix].tilt, 0);
	    pConfig->Read(_T("facing"), &panel_groups[ix].facing, 0);
    }

}  // end of ConfigInit


void ConfigSave(int control)
{
    int ix;
    double *e;
    wxString string;
    long value;
	wxFileConfig *pConfig = (wxFileConfig *)(wxConfigBase::Get());

    pConfig->Write(_T("/data"), data_dir);
	pConfig->Write(_T("/serial_port"), serial_port);
	pConfig->Write(_T("/longitude"), Longitude);
	pConfig->Write(_T("/latitude"), Latitude);
	pConfig->Write(_T("/timezone"), TimeZone2);
	pConfig->Write(_T("/histogram_max"), histogram_max);
	pConfig->Write(_T("/logchange"), wxString(date_logchange, wxConvLocal));

    string = wxEmptyString;
    for(ix=0; ix<N_EXTRA_READINGS; ix++)
    {
        value = extra_readings[ix].dsp_code;
        if(value > 0)
        {
            if(extra_readings[ix].graph_slot > 0)
                value += 1000;
            if(extra_readings[ix].status_slot > 0)
                value += 2000;
        }
        string += wxString::Format(_T("%d "), value);

    }
    pConfig->Write(_T("/extra_readings"), string);

	for(ix=0; ix<N_INV; ix++)
	{
	    e = energy_total[ix];
	    pConfig->SetPath(wxString::Format(_T("/inverter%d"), ix));
        pConfig->Write(_T("address"),inverter_address[ix]);
        pConfig->Write(_T("today"), wxString(today_done[ix], wxConvLocal));
        pConfig->Write(_T("previous_total"), previous_total[ix]);
        if(e[4] != 0)
        {
            pConfig->Write(_T("energy"), wxString::Format(_T("%f %f %f %f %f %f"),
                e[0],e[1],e[2],e[3],e[4],e[5]));
        }
        pConfig->Write(_T("logperiod"), logging_period_index[ix]);
        pConfig->Write(_T("logpath"), logging_path[ix]);
	}

    for(ix=0; ix<N_PANEL_GROUPS; ix++)
    {
	    pConfig->SetPath(wxString::Format(_T("/panelgroup%d"), ix));
	    pConfig->Write(_T("tilt"), panel_groups[ix].tilt);
	    pConfig->Write(_T("facing"), panel_groups[ix].facing);
    }


	if(control == 0)
		delete wxFileConfig::Set((wxFileConfig *)NULL);
    else
        pConfig->Flush();
}

bool auroraApp::OnInit()
{
#ifdef __WXMSW__
    wxString home_drive;
    wxString home_path;
    wxGetEnv(_T("HOMEPATH"), &home_path);
    wxGetEnv(_T("HOMEDRIVE"), &home_drive);
    path_home = home_drive + home_path;
#else
    wxGetEnv(_T("HOME"), &path_home);
#endif
    ConfigInit();
    Mainframe* frame = new Mainframe(0L, _("Aurora"));
    frame->Show();

    return true;
}

int auroraApp::OnExit()
{
	ConfigSave(0);
	return(0);
}


void strncpy0(char *to, const char* from, int size)
{
    strncpy(to, from, size);
    to[size-1] = 0;
}
