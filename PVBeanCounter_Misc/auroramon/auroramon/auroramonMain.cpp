
/***************************************************************************
 *   Copyright (C) 2012 by Jonathan Duddington                             *
 *   email: jonsd@users.sourceforge.net                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see:                                 *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

#include <wx/aboutdlg.h>
#include <wx/generic/aboutdlgg.h>
#include <wx/filename.h>
#include <wx/sizer.h>
#include <wx/html/htmlwin.h>
#include "auroramon.h"

DECLARE_EVENT_TYPE(wxEVT_MY_EVENT, -1)
DEFINE_EVENT_TYPE(wxEVT_MY_EVENT)
wxCommandEvent my_event(wxEVT_MY_EVENT);

BEGIN_EVENT_TABLE(Mainframe, wxFrame)
    EVT_CLOSE(Mainframe::OnClose)
    EVT_MENU(idMenuQuit, Mainframe::OnQuit)
    EVT_MENU(idMenuAbout, Mainframe::OnAbout)
    EVT_MENU(idDisplayToday, Mainframe::OnDisplay)
    EVT_MENU(idDisplayDate, Mainframe::OnDisplay)
    EVT_MENU(idNextDate, Mainframe::OnDisplay)
    EVT_MENU(idPrevDate, Mainframe::OnDisplay)
    EVT_MENU(idImportStored, Mainframe::OnDisplay)
    EVT_MENU(idDisplayEnergy, Mainframe::OnDisplay)
    EVT_MENU(idMenuSetup, Mainframe::OnCommand)
    EVT_MENU(idMenuLocation, Mainframe::OnCommand)
    EVT_MENU(idMenuEditCharts, Mainframe::OnCommand)
    EVT_MENU(idMenuEditHistogram, Mainframe::OnCommand)
    EVT_MENU(idMenuExtraReadings, Mainframe::OnCommand)
    EVT_MENU(idMenuLogging, Mainframe::OnCommand)
    EVT_MENU(idMenuSunInfo, Mainframe::OnCommand)
    EVT_MENU(idMenuHelp, Mainframe::OnCommand)
    EVT_MENU(idMenuInverter0Info, Mainframe::OnCommand)
    EVT_MENU(idMenuInverter1Info, Mainframe::OnCommand)
    EVT_MENU(idMenuInverterState, Mainframe::OnCommand)
    EVT_MENU(idMenuFullscreen, Mainframe::OnCommand)
//    EVT_END_PROCESS(wxID_ANY, Mainframe::OnDataEvent)
	EVT_KEY_DOWN(Mainframe::OnKey)
    EVT_TIMER(idPulseTimer, Mainframe::OnPulseTimer)
//    EVT_TIMER(idRetryTimer, Mainframe::OnRetryTimer)
    EVT_COMMAND(idInverterData, wxEVT_MY_EVENT, Mainframe::OnInverterEvent)
    EVT_COMMAND(idInverterFail, wxEVT_MY_EVENT, Mainframe::OnInverterEvent)
END_EVENT_TABLE()


BEGIN_EVENT_TABLE(GraphPanel,wxScrolledWindow)
//	EVT_LEFT_DOWN(GraphPanel::OnMouse)
	EVT_KEY_DOWN(GraphPanel::OnKey)
    EVT_COMMAND(idRefreshGraph, wxEVT_MY_EVENT, GraphPanel::OnMyEvent)
END_EVENT_TABLE()



void SendCommand(int inv, int type);
void InitComms();


extern void MonitorOutput(int power);
extern void SunInfo();
extern void InitCharts();
extern void InitCharts2();
extern void InitDates();


GraphPanel *graph_panel;
wxPanel *status_panel;
DlgSetup *dlg_setup;
DlgLocation *dlg_location;
DlgChart *dlg_chart;
DlgHistogram *dlg_histogram;
DlgExtraR *dlg_extrareadings;
DlgLogging *dlg_logging;


Mainframe *mainframe;
int display_today = 0;
wxString graph_date;
wxString graph_date_ymd;
char date_ymd[12] = "";   // this is today, unless the next day has arrived and we haven't yet noticed
char date_logchange[12] = "";

#define N_AVERAGES 7
int current_period[N_INV] = {-1. -1};
int inverter_address[N_INV] = {2, 0};
char today_done[N_INV][10];
double previous_total[N_INV];
int time_offset[N_INV];
int inverter_alive[N_INV];
int inverter_alarm[N_INV];
int inverter_comms_error[N_INV];
int get_extra_dsp[N_INV] = {0, 0};

FILE *f_log[N_INV];
FILE *f_logfull[N_INV];
int logging_period[N_INV] = {0, 0};
int logging_period_index[N_INV] = {0, 0};
int logging_period_tab[6] = {0, 6, 3, 2, 1, -1};
wxString logging_path[N_INV];
int logheaders_changed[N_INV];

int inverter_seconds[N_INV] = {0, 0};
float averages[N_INV][N_AVERAGES];
int average_count[N_INV] = {0, 0};
int previous_minute[N_INV] = {0, 0};
int previous_10sec[N_INV] = {0, 0};
int period_in_minute[N_INV] = {0, 0};
double energy_total[N_INV][6];
wxString energy_string[N_INV];


char current_timestring[N_INV][10];


PowerMeter::PowerMeter(wxWindow *parent)
    : wxFrame(parent, -1, _T("Power Meter"), wxPoint(0, 100), wxSize(330,280+DLG_HX), wxCAPTION | wxSTAY_ON_TOP)
{

}

PowerMeter::~PowerMeter()
{
}

PowerMeter *power_meter;



int GetFileLength(const char *filename)
{//====================================
	struct stat statbuf;

	if(stat(filename,&statbuf) != 0)
		return(0);

	if((statbuf.st_mode & S_IFMT) == S_IFDIR)
		return(-2);  // a directory

	return(statbuf.st_size);
}  // end of GetFileLength



void OpenLogFull(int inv)
{//======================
    wxString fname_log;
    wxString name;

    if(logging_path[inv].Find('/') == wxNOT_FOUND)
    {
        if(logging_path[inv] == wxEmptyString)
            fname_log = data_dir + _T("/") + wxString::Format(_T("log%d_"), inverter_address[inv]);
        else
            fname_log = data_dir + _T("/") + logging_path[inv];
    }
    else
        fname_log = logging_path[inv];

    fname_log += wxString(date_ymd, wxConvLocal);

    f_logfull[inv] = fopen(fname_log.mb_str(wxConvLocal), "a");
}


void LogColumnHeaders(int inverter, int control)
{//=============================================
// control: 1= only if the header has changed
    int ix;
    int n_columns;
    int type;
    char columns[N_EXTRA_READINGS+1];
    static char prev_columns[N_EXTRA_READINGS+1] = {0};

    if(f_log[inverter] == NULL)
        return;

    for(ix=0, n_columns=0; ix<N_EXTRA_READINGS; ix++)
    {
        if(((type = extra_readings[ix].type) > 0) && (extra_readings[ix].graph_slot > 0))
        {
            columns[n_columns++] = type;
        }
    }
    columns[n_columns] = 0;

    if((control == 1) && (strcmp(columns, prev_columns) == 0))
        return;

    strcpy(prev_columns, columns);

    fprintf(f_log[inverter], "%s", "time\tpout\tpw1\tvt1\tpw2\tvt2\ttmpr");

    for(ix=0; ix<n_columns; ix++)
    {
        fprintf(f_log[inverter], "\t%s", extra_reading_types[(int)columns[ix]].mnemonic);
    }
    fputc('\n', f_log[inverter]);
    fflush(f_log[inverter]);
    logheaders_changed[inverter] = 0;
}


void OpenLogFiles(int create, int control)
{//=======================================
// open log file for power and voltage readings
// create = inverter number, or -1 (don't create log files)  -2 program start, also start new com_log
    int inv = 0;
    struct tm *btime;
    time_t current_time;
    char fname[256];
    wxString fname_log;

    time(&current_time);
    btime = localtime(&current_time);

    if(TimeZone2 > 24)
    {
#ifdef __WXMSW__
        Timezone = (double)(timezone) / 3600.0;
#else
        // automatic, get from computer time
        Timezone = (double)(btime->tm_gmtoff) / 3600.0;
#endif
    }
    else
    {
        Timezone = TimeZone2;
    }
    if(Longitude > 360)
    {
        // Longitude is not set, guess from TimeZone
        Longitude = Timezone * 360/24;
    }

    graph_date.Printf(_T("%.4d-%.2d-%.2d"), btime->tm_year+1900, btime->tm_mon+1, btime->tm_mday);
    sprintf(date_ymd, "%.4d%.2d%.2d", btime->tm_year+1900, btime->tm_mon+1, btime->tm_mday);
    graph_date_ymd = wxString(date_ymd, wxConvLocal);

    if(create == -2)
    {
        if(strcmp(date_ymd, date_logchange) != 0)
        {
            // change to a new com_log
            fname_log = data_dir+_T("/com_log.txt");
            if(wxFileExists(fname_log))
            {
                wxRenameFile(fname_log, data_dir+_T("/com_log_yesterday.txt"), true);
            }
            strncpy0(date_logchange, date_ymd, sizeof(date_logchange));
            ConfigSave(1);
        }
    }

    if (!wxDirExists(data_dir))
        wxMkdir(data_dir);
    data_year_dir = data_dir + wxString::Format(_T("/%.4d"), btime->tm_year+1900);

    if (!wxDirExists(data_year_dir))
        wxMkdir(data_year_dir);


    graph_panel->NewDay();
    for(inv=0; inv<N_INV; inv++)
    {
        if(f_log[inv] != NULL)
        {
            fclose(f_log[inv]);
            f_log[inv] = NULL;
        }
        if(f_logfull[inv] != NULL)
        {
            fclose(f_logfull[inv]);
            f_logfull[inv] = NULL;
        }

        if(inverter_address[inv] <= 0)
            continue;

        fname_log = data_year_dir + wxString::Format(_T("/d%d_"), inverter_address[inv]) + wxString(date_ymd, wxConvLocal);

        // load any data from today's log file
        graph_panel->ReadEnergyFile(inv, fname_log, control, NULL);

        strcpy(fname, fname_log.mb_str(wxConvLocal));
        strcat(fname, ".txt");

        if(GetFileLength(fname))
        {
            f_log[inv] = fopen(fname, "a");
            if(create == -2)
                logheaders_changed[inv] = 2;   // program start, write column headers when the first log entry is written
        }
        else
        if(create == inv)
        {
            if((f_log[inv] = fopen(fname, "w")) == NULL)
            {
                wxLogError(_T("Can't write to log file: ") + fname_log);
            }
            else
            {
                fprintf(f_log[inv], "AUR1 %s #%d\n", date_ymd, inverter_address[inv]);
                logheaders_changed[inv] = 2;   // write column headers when the first log entry is written
            }
        }
    }

    CalcSun(graph_date.mb_str(wxConvLocal));   // called after we set the time zone
    graph_panel->ResetGraph();
    display_today = 1;
}  // end of OpenLogFiles



void GotInverterInfo(int inv, wxString cmdoutput)
{//==============================================
    char *data = NULL;
    int length;
    FILE *f;
    wxString dir = data_dir + _T("/system");
    wxString savefile = dir + wxString::Format(_T("/InverterInfo%d"), inverter_address[inv]);
    wxString fname;
    wxString noresponse = wxEmptyString;

    length = GetFileLength(cmdoutput.mb_str(wxConvLocal));
    if(length <= 0)
    {
        noresponse = _T(" (no response)");

        // show previously saved data instead
        length = GetFileLength(savefile.mb_str(wxConvLocal));
        fname = savefile;
    }
    else
    {
        // save a copy in aurora/system
        if (!wxDirExists(dir))
            wxMkdir(dir);
        wxCopyFile(cmdoutput, savefile);
        fname = cmdoutput;
    }

    if(length == 0)
    {
        wxLogError(wxString::Format(_T("No response from inverter #%d"), inverter_address[inv]));
    }
    else
    {
        if((data = (char *)malloc(length + 1)) == NULL)
            return;

        if((f = fopen(fname.mb_str(wxConvLocal), "r")) != NULL)
            length = fread(data, 1, length, f);
        else
            length = 0;

        data[length] = 0;
        wxMessageBox(wxString(data, wxConvLocal), wxString::Format(_T("Inverter address %d") + noresponse, inverter_address[inv]), wxOK, mainframe);
        free(data);
    }
    wxRemove(cmdoutput);
}  // end of GotInverterInfo



void Mainframe::ShowEnergy(int inv)
{//================================
    int ix;
    double *e;
    wxString format;

    e = energy_total[inv];
    if(e[4] == 0)
        return;   // total energy, zero implies that data is not available

    for(ix=0; ix < 6; ix++)
    {
        txt_energy[inv][ix]->Clear();
        if(e[ix] >= 10000)
            format = _T("%6.0f");
        else
        if(e[ix] >= 1000)
            format = _T("%6.1f");
        else
            format = _T("%6.2f");
        txt_energy[inv][ix]->WriteText(wxString::Format(format, e[ix]));
    }
}


int Mainframe::DataResponse(int data_ok, INVERTER_RESPONSE *ir)
{//============================================================
    int ix;
    int hrs, mins;
    int seconds;
    int computer_seconds;
    DATAREC datarec;
    struct tm *btime;
    time_t current_time;
    wxString fname_daily;
    static float tmpr_max = 0;
    int inv = 0;
    int period_type=0;  // 0=other 1= 10sec, 2= 1 minute
    int power_value;
    double *etot;
    char ymd[20];

    time(&current_time);
    btime = localtime(&current_time);
    computer_seconds = btime->tm_hour*3600 + btime->tm_min*60 + btime->tm_sec;
    sprintf(ymd, "%.4d%.2d%.2d", btime->tm_year+1900, btime->tm_mon+1, btime->tm_mday);

    inv = command_inverter;
    etot = energy_total[inv];


    if(data_ok == 1)
    {
        if(ir->flags & IR_HAS_STATE)
        {
            GotInverterStatus(command_inverter);
        }

        if((f_log[command_inverter] == NULL) || (memcmp(ymd, date_ymd, 8) != 0))
        {
            // The first data of a new day.
            // Open a new log file and clear the graphs
            OpenLogFiles(command_inverter, 0);
        }

        seconds = computer_seconds - 1;  // allow for delay in receiving the response from the inverter
        if(ir->flags & IR_HAS_TIME)
        {
            wxString time_string;
            inverter_seconds[inv] = ir->seconds;
            time_offset[inv] = inverter_seconds[inv] - seconds;
            time_string = wxString(&(ir->timedate[9]), wxConvLocal);
//wxLogStatus(time_string + wxString::Format(_T("  inverter time difference = %d "), time_offset[inv]));
        }
        else
        {
            inverter_seconds[inv] = seconds + time_offset[inv];
        }


        if(ir->flags & IR_HAS_ENERGY_TODAY)
        {
            etot[0] = ir->energy[0];
        }
        else
        if(ir->flags & IR_HAS_ENERGY)
        {
            etot[0] = ir->energy[0];
            etot[1] = ir->energy[1];
            etot[2] = ir->energy[2];
            etot[3] = ir->energy[3];
            etot[4] = ir->energy[4];
            etot[5] = ir->energy[5];

            if((strcmp(ymd, today_done[inv]) != 0) && (ir->pw[0] > 0))
            {
                // daily and weekly seem to get reset before monthly and yearly, so
                // wait until the inverter is generating power before recording
                // the start-of-day totals.
                FILE *f_daily;
                double e;
                double yesterday;
                bool exists;
                char yesterday_str[20];

                e = etot[0];  // energy today
                if(previous_total[inv] == 0)
                    yesterday = 0;
                else
                    yesterday = etot[4] - e - previous_total[inv];

                fname_daily = data_year_dir + wxString::Format(_T("/inverter%d_%d_") + data_year_dir.Right(4) + _T(".txt"), inv, inverter_address[inv]);
                exists = wxFileExists(fname_daily);
                if((f_daily = fopen(fname_daily.mb_str(wxConvLocal), "a")) != NULL)
                {
                    if(!exists)
                    {
                        // write column headers at the start of the file
                        fprintf(f_daily,"#end of    today     week     month     year      total     partial\n");
                    }

                    // print the yesterday, week, month, year, total, partial totals for the start of the day
                    strcpy(yesterday_str, YesterdayDate(wxString(ymd, wxConvLocal)).mb_str(wxConvLocal));
                    fprintf(f_daily, "%s %7.3f  %8.3f %8.3f %9.3f %10.3f %9.3f\n", yesterday_str, yesterday,
                            etot[1]-e, etot[2]-e, etot[3]-e, etot[4]-e, etot[5]-e);
                    fclose(f_daily);
                }
                previous_total[inv] = etot[4] - e;
                strcpy(today_done[inv], ymd);
                ConfigSave(1);
            }
        }


        period_type = 0;


        if((seconds / 10) != current_period[inv])
        {
            // this is the first response in this 10 second period
            period_type = 1;   // start of  10 second period
            period_in_minute[inv]++;

            if((seconds / 60) != previous_minute[inv])
            {
                period_type = 2;   // start of a minute period
                period_in_minute[inv] = 0;
                previous_minute[inv] = seconds / 60;

                if((inv==0) || (inverter_alive[0] == 0))
                    ConfigSave(1);    // save energy totals every minute
            }

            // get extra reading values
            for(ix=0; ix<N_EXTRA_READINGS; ix++)
            {
                int dsp_code;
                int j;
                EXTRA_READING_TYPE *ptype;
                unsigned short *p;
                if(((dsp_code = extra_readings[ix].dsp_code) > 0) && (extra_readings[ix].graph_slot > 0))
                {
                    if(dsp_data[inv][dsp_code] == NULL)
                    {
                        dsp_data[inv][dsp_code] = p = (unsigned short *)malloc(sizeof(unsigned short) * N_10SEC);
                        for(j=0; j<N_10SEC; j++)
                            p[j] = 0xffff;
                    }
                    ptype = &extra_reading_types[extra_readings[ix].type];
                    dsp_data[inv][dsp_code][current_period[inv]] = (int)((ir->dsp[dsp_code] + ptype->addition) * ptype->multiply);
                }
            }

            if((current_period[inv] >= 0) && (ir->n_av_pwin > 0))
            {
                // write out the average values for the previous 10 second period
                if(f_log[inv] != NULL)
                {
                    double pw0, pw1, pw2, vt1, vt2;
                    static double tmpr = 0;
                    pw0 = averages[inv][0] / ir->n_av_pw0;
                    pw1 = averages[inv][1] / ir->n_av_pwin;
                    pw2 = averages[inv][3] / ir->n_av_pwin;
                    vt1 = averages[inv][2] / ir->n_av_pwin;
                    vt2 = averages[inv][4] / ir->n_av_pwin;
                    if(ir->n_av_tmpr > 0)
                    {
                        tmpr = averages[inv][5] / ir->n_av_tmpr;
                    }

                    if((pw1 >= 5.0) || (pw2 >= 5.0))
                    {
                        // Only log when input power is more than 5 watts (to avoid logging useless data
                        // Ensure all power and voltage values are not negative

                        if(logheaders_changed[inv] != 0)
                        {
                            LogColumnHeaders(inv, logheaders_changed[inv]);   // column headers for extra readings may have changed
                        }

                        if(pw0 < 0) pw0 = 0;
                        if(vt1 < 0) vt1 = 0;
                        if(vt2 < 0) vt2 = 0;
                        fprintf(f_log[inv], "%s\t%6.1f\t%6.1f\t%6.2f\t%6.1f\t%6.2f\t%5.2f", current_timestring[inv],
                            pw0, pw1, vt1, pw2, vt2, tmpr);

                        // write out any extra readings
                        for(ix=0; ix<N_EXTRA_READINGS; ix++)
                        {
                            if((extra_readings[ix].type > 0) && (extra_readings[ix].graph_slot > 0))
                            {
                                fprintf(f_log[inv], "\t%f", ir->dsp[extra_readings[ix].dsp_code]);
                            }
                        }
                        fputc('\n', f_log[inv]);
                        if(period_in_minute[inv] == 0)
                            fflush(f_log[inv]);
                    }
                }

            }
            current_period[inv] = seconds / 10;
            hrs = seconds/3600;
            mins = (seconds % 3600) /60;
            sprintf(current_timestring[inv], "%.2d:%.2d:%.2d", hrs, mins, seconds % 60);

            averages[inv][0] = 0;
            averages[inv][1] = 0;
            averages[inv][2] = 0;
            averages[inv][3] = 0;
            averages[inv][4] = 0;
            averages[inv][5] = 0;
            averages[inv][6] = 0;
            ir->n_av_pw0 = 0;
            ir->n_av_pwin = 0;
            ir->n_av_tmpr = 0;
        }  // end of new 10 second period


        // add the new data values to the averages for this 10 second period
        averages[inv][0] += ir->pw[0];  // power output reading is always present
        ir->n_av_pw0++;

        if(ir->flags & IR_HAS_POWERIN)
        {
            averages[inv][1] += ir->pw[1];
            averages[inv][2] += ir->vt[1];
            averages[inv][3] += ir->pw[2];
            averages[inv][4] += ir->vt[2];
            averages[inv][6] += ir->vt[0];
            ir->n_av_pwin++;
        }
        if(ir->flags & IR_HAS_TEMPR)
        {
           tmpr_max = ir->tempr[0];
            if(ir->tempr[1] > tmpr_max)
                tmpr_max = ir->tempr[1];
            averages[inv][5] += tmpr_max;
            ir->n_av_tmpr++;
        }

#ifdef deleted
        if(logging_period[inv] != 0)
        {
            // write to an extra log file
            int do_logging = 0;

            if(f_logfull[inv] == NULL)
            {
                OpenLogFull(inv);
            }

            if(logging_period[inv] == -1)
                do_logging = 1;  // log all responses

            if((period_type > 0) && (logging_period[inv] > 0))
            {
                if((logging_period[inv] == 1) || ((period_in_minute[inv] % logging_period[inv]) == 0))
                {
                    do_logging = 1;
                }
            }

            if(do_logging)
            {
                hrs = seconds/3600;
                mins = (seconds % 3600) /60;
                fprintf(f_logfull[inv], "%s-%.2d:%.2d:%.2d %10.5f %9.5f %10.5f %10.5f %9.5f %10.5f %10.5f %9.5f %11.5f %10.5f %10.5f %10.5f %10.5f  OK\n", ymd, hrs, mins, seconds % 60,
                        ir->vt[1], ir->ct[1], ir->pw[1], ir->vt[2], ir->ct[2], ir->pw[2], ir->vt[0], ir->ct[0], ir->pw[0], ir->freq, ir->effic, ir->tempr[0], ir->tempr[1]);
                if(period_type == 2)
                    fflush(f_logfull[inv]);   // flush every minute
            }
        }
#endif

        if((display_today) && (ir->n_av_pwin > 0))
        {
            // update the power graph display
            if(ir->n_av_pw0 > 0)
            {
                datarec.pw0 = (unsigned short)(averages[inv][0] * X_PO / ir->n_av_pw0);
            }

            if(ir->n_av_pwin > 0)
            {
                datarec.pw1 = (unsigned short)(averages[inv][1] * X_PI / ir->n_av_pwin);
                datarec.vt1 = (unsigned short)(averages[inv][2] * X_VI / ir->n_av_pwin);
                datarec.pw2 = (unsigned short)(averages[inv][3] * X_PI / ir->n_av_pwin);
                datarec.vt2 = (unsigned short)(averages[inv][4] * X_VI / ir->n_av_pwin);
            }

            if(ir->n_av_tmpr > 0)
            {
                datarec.tmpr= (short)(averages[inv][5] * 100 / ir->n_av_tmpr);
            }
            else
            {
                datarec.tmpr = (unsigned short)(tmpr_max * 100);
            }
            datarec.pwi = 0;
            graph_panel->AddPoint(inv, current_period[inv], &datarec);
        }

    }


    if((data_ok == 1) || (inverter_alive[inv ^ 1] == 0))
    {
        // don't clear if these will be updated by the other inverter
        txt_powertot->Clear();
    }

    txt_power[inv]->Clear();

    if((ir->flags & IR_HAS_POWERIN) || (data_ok == 0))
    {
        txt_power1[inv]->Clear();
        txt_power2[inv]->Clear();
        txt_volts1[inv]->Clear();
        txt_volts2[inv]->Clear();
        txt_current1[inv]->Clear();
        txt_current2[inv]->Clear();
        txt_efficiency[inv]->Clear();
        txt_temperature[inv]->Clear();
    }
    ShowEnergy(inv);

//txt_powertot->WriteText(_T("12345"));  // TEST
    if(data_ok == 1)
    {

        txt_power[inv]->WriteText(wxString::Format(_T("%4d"), (int)(ir->pw[0] + 0.5)));
        power_total[inv] = ir->pw[0];
        power_value = (int)(power_total[0] + power_total[1] + 0.5);

        SetTitle(wxString::Format(_T("Aurora %2d"), power_value));
        txt_powertot->WriteText(wxString::Format(_T("%4d"), power_value));
        MonitorOutput(power_value);

        if(ir->flags & IR_HAS_POWERIN)
        {
            txt_power1[inv]->WriteText(wxString::Format(_T("%4d"), (int)(ir->pw[1] + 0.5)));
            txt_power2[inv]->WriteText(wxString::Format(_T("%4d"), (int)(ir->pw[2] + 0.5)));
            txt_volts1[inv]->WriteText(wxString::Format(_T("%3d"), (int)(ir->vt[1] + 0.5)));
            txt_volts2[inv]->WriteText(wxString::Format(_T("%3d"), (int)(ir->vt[2] + 0.5)));
            txt_current1[inv]->WriteText(wxString::Format(_T("%.2f"), ir->ct[1]));
            txt_current2[inv]->WriteText(wxString::Format(_T("%.2f"), ir->ct[2]));
            txt_efficiency[inv]->WriteText(wxString::Format(_T("%.1f"), ir->effic));
            txt_temperature[inv]->WriteText(wxString::Format(_T("%.1f"), tmpr_max));
        }

        if(ir->flags & IR_HAS_EXTRA)
        {
            int ix;
            int slot;
            int code;
            wxString format;
            double value;
            EXTRA_READING_TYPE *ex;

            for(ix=0; ix<N_EXTRA_READINGS; ix++)
            {
                if((slot = extra_readings[ix].status_slot) > 0)
                {
                    ex = &extra_reading_types[extra_readings[ix].type];
                    format.Printf(_T("%%.%df"), ex->decimalplaces);
                    code = extra_readings[ix].dsp_code;
                    value = ir->dsp[code] * ex->multiplier;
                    txt_dsp_param[inv][slot-1]->ChangeValue(wxString::Format(format, value));
                }
            }
        }
   }
   else
   {
        inverter_alive[inv] = 0;
        power_total[inv] = 0;
        if(inverter_alive[inv ^ 1] == 0)
            SetTitle(_T("Aurora"));  // neither inverter is alive
   }
    return(period_type);
}  // end of DataResponse





void Mainframe::OnKey(wxKeyEvent &event)
{//=====================================
    if(graph_panel->OnKey2(event) >= 0)
        return;
    event.Skip();
}



void Mainframe::OnDisplay(wxCommandEvent &event)
{//=============================================
    wxString fname;
    wxString dir;
    wxString date2;
    int result;
    int id;
    wxString date;
    long int value;

    switch(id = event.GetId())
    {
    case idDisplayToday:
        OpenLogFiles(-1, 0);
        graph_panel->SetMode(0);
        break;

    case idDisplayDate:
        fname = wxFileSelector(_T("Select date"), data_dir, wxEmptyString, _T(""), _T("d*"));
        if(fname == wxEmptyString)
            return;

        value = 0;
        fname.Right(8).ToLong(&value);
        dir = fname.BeforeLast('/');

        DisplayDateGraphs(fname.Right(8), 0);
        break;

    case idNextDate:
        DisplayDateGraphs(NextDate(graph_date_ymd, 1), 1);
        break;

    case idPrevDate:
        DisplayDateGraphs(PrevDate(graph_date_ymd, 1), 1);
        break;

    case idImportStored:
        fname = wxFileSelector(_T("Select energy file"), data_dir, wxEmptyString, _T(""), _T("q_*"));
        if(fname == wxEmptyString)
            return;

        date = wxGetTextFromUser(_T("Search file for date (yyyy-mm-dd):"), _T("Aurora"), graph_date);
        result = graph_panel->ReadEnergyFile(-1, fname, 0, date.mb_str(wxConvLocal));
        if(result == -2)
        {
            wxLogError(_T("Data for ") + date + _T(" not found in this file"));
        }
        else
        if(result != 0)
        {
            wxLogError(_T("Failed to read data file: ") + fname);
        }
        display_today = 0;
        break;

    case idDisplayEnergy:
        graph_panel->SetMode(1);
        break;
    }
}


const wxString key_info = _T(""
"F2     \tToday's charts.\n"
"F3     \tHistogram of daily energy totals.\n"
"F4     \tInverter status and alarm state.\n"
"F11    \tToggle full-screen display.\n"
"Escape \tEnd full-screen display.\n"
" [     \tShow the charts for the previous day.\n"
" ]     \tShow the charts for the next day.\n"
"\n"
"Page Up\tDisplay the next chart page.\n"
"Page Down\tDisplay the previous chart page.\n"
"Home   \tDisplay the first chart page.\n"
"End    \tDisplay the last chart page.\n"
" <   > \tChange the x-scale.\n"
"Up,  Down\tChange the y-scale (power graphs only).\n"
"Left,  Right\tHorizontal scroll.");


wxHtmlWindow *html_help = NULL;
wxDialog *dlg_help = NULL;

void Mainframe::ShowHelp()
{//=======================
    int width, height;
    wxString fname;

#ifdef __WXMSW__
    fname = _T("help.html");
#else
    fname = data_dir + _T("/system/help.html");
    if(wxFileExists(fname) == false)
    {
        wxMessageBox(key_info, _T("Key commands"), wxOK, this);
        return;
    }
#endif

    mainframe->GetClientSize(&width, &height);
    if(dlg_help == NULL)
    {
        dlg_help = new wxDialog(mainframe, -1, wxString(_T("Aurora Monitor Help")));
        html_help = new wxHtmlWindow(dlg_help);
    }
    dlg_help->SetSize(width-800, 0, 800, height);
    html_help->LoadPage(fname);
    dlg_help->Show(true);
}

void Mainframe::OnCommand(wxCommandEvent &event)
{//=============================================
    switch(event.GetId())
    {
    case idMenuSetup:
        dlg_setup->ShowDlg();
        break;

    case idMenuLocation:
        dlg_location->ShowDlg();
        break;

    case idMenuEditCharts:
        dlg_chart->ShowDlg();
        break;

    case idMenuEditHistogram:
        dlg_histogram->ShowDlg();
        break;

    case idMenuExtraReadings:
        dlg_extrareadings->ShowDlg();
        break;

    case idMenuLogging:
        dlg_logging->ShowDlg();
        break;

    case idMenuInverter0Info:
    case idMenuInverter1Info:
        QueueCommand(event.GetId() - idMenuInverter0Info, cmdInverterInfo);
        break;

    case idMenuSunInfo:
        SunInfo();
        break;

    case idMenuHelp:
        ShowHelp();
        break;

    case idMenuInverterState:
        if(dlg_alarms->IsShown())
            dlg_alarms->Close();
        else
            ShowInverterStatus();
        break;

    case idMenuFullscreen:
        if(mainframe->IsFullScreen())
            mainframe->ShowFullScreen(false);
        else
            mainframe->ShowFullScreen(true);
        Refresh();
        break;
    }
}





Mainframe::Mainframe(wxFrame *frame, const wxString& title)
    : wxFrame(frame, -1, title, wxDefaultPosition, wxSize(1024,768), wxDEFAULT_FRAME_STYLE)
{//========================================================
    int inv;
    double *e;
    char buf[200];

    mainframe = this;
    CreateStatusBar(1);

    // create a menu bar
    wxMenuBar* mbar = new wxMenuBar();
    wxMenu* fileMenu = new wxMenu(_T(""));
    fileMenu->Append(idMenuQuit, _("&Quit\tAlt-F4"), _("Quit the application"));
    mbar->Append(fileMenu, _("&File"));

    wxMenu *displayMenu = new wxMenu(_T(""));
    displayMenu->Append(idDisplayToday, _T("Today\tF2"));
    displayMenu->Append(idDisplayDate, _T("Select day"));
    displayMenu->Append(idNextDate, _T("Next day\tN"));
    displayMenu->Append(idPrevDate, _T("Previous day\tP"));
    displayMenu->AppendSeparator();
//    displayMenu->Append(idImportStored, _T("Import -q energy data"));
//    displayMenu->AppendSeparator();
    displayMenu->Append(idDisplayEnergy, _T("Energy histogram\tF3"));
    displayMenu->AppendSeparator();
    displayMenu->Append(idMenuInverterState, _T("Inverter status\tF4"));
    displayMenu->AppendSeparator();
    displayMenu->Append(idMenuFullscreen, _("Fullscreen\tF11"));
    mbar->Append(displayMenu, _T("&Display"));

    wxMenu *settingsMenu = new wxMenu(_T(""));
    settingsMenu->Append(idMenuSetup, _T("Setup"));
    settingsMenu->Append(idMenuLocation, _T("Location"));
    settingsMenu->Append(idMenuEditCharts, _T("Charts"));
    settingsMenu->Append(idMenuEditHistogram, _T("Histograms"));
    settingsMenu->Append(idMenuExtraReadings, _T("Extra readings"));
//    settingsMenu->Append(idMenuLogging, _T("Logs"));
    mbar->Append(settingsMenu, _T("&Settings"));

    wxMenu* helpMenu = new wxMenu(_T(""));
    helpMenu->Append(idMenuSunInfo, _("Sun information"));
    menu_InverterInfo[0] = new wxMenuItem(helpMenu, idMenuInverter0Info, _("Inverter A information"));
    menu_InverterInfo[1] = new wxMenuItem(helpMenu, idMenuInverter1Info, _("Inverter B information"));
    helpMenu->Append(menu_InverterInfo[0]);
    helpMenu->Append(menu_InverterInfo[1]);
    helpMenu->Append(idMenuHelp, _("Help information\tF1"));
    helpMenu->Append(idMenuAbout, _("&About"), _("Show info about this application"));
    mbar->Append(helpMenu, _("&Help"));

    SetMenuBar(mbar);
    pulsetimer.SetOwner(this->GetEventHandler(), idPulseTimer);

    status_panel = new wxPanel(this, -1, wxDefaultPosition, wxSize(100, 52), wxSUNKEN_BORDER);
    graph_panel = new GraphPanel(this, wxDefaultPosition, wxSize(100,100));
    wxBoxSizer *frame_sizer = new wxBoxSizer(wxVERTICAL);
    frame_sizer->Add(graph_panel, 1, wxEXPAND);
    frame_sizer->Add(status_panel, 0, wxEXPAND);
    SetSizer(frame_sizer);

    MakeStatusPanel();
	Maximize();

    dlg_setup = new DlgSetup(this);
    dlg_location = new DlgLocation(this);
    dlg_chart = new DlgChart(this);
    dlg_histogram = new DlgHistogram(this);
    dlg_extrareadings = new DlgExtraR(this);
    dlg_logging = new DlgLogging(this);
    dlg_alarms = new DlgAlarms(this);
    power_meter = new PowerMeter(this);

    memset(averages, 0, sizeof(averages));
    memset(energy_total, 0, sizeof(energy_total));
    memset(inverter_response, 0, sizeof(inverter_response));
    memset(dsp_data, 0, sizeof(dsp_data));

    for(inv=0; inv<N_INV; inv++)
    {
        f_log[inv] = NULL;
        f_logfull[inv] = NULL;
        time_offset[inv] = 0xffff;
        power_total[inv] = 0;
        inverter_alive[inv] = 0;
        inverter_alarm[inv] = 0;
        logging_period[inv] = logging_period_tab[logging_period_index[inv]];
        logheaders_changed[inv] = 2;

        strncpy0(buf, energy_string[inv].mb_str(wxConvLocal), sizeof(buf));
        e = energy_total[inv];
        sscanf(buf, "%lf %lf %lf %lf %lf %lf", &e[0], &e[1], &e[2], &e[3], &e[4], &e[5]);
        ShowEnergy(inv);
    }

    InitDates();
    InitCharts();
    InitCharts2();
    SetupStatusPanel(1);   // call this after InitCharts()

    OpenLogFiles(-2, 0);  // Read today's data if it exists

    InitComms();

    SendCommand(0,5);  // start the inverter commands

	my_event.SetId(idRefreshGraph);
    wxPostEvent(graph_panel->GetEventHandler(), my_event);
}





Mainframe::~Mainframe()
{//====================
    int ix;
    wxRemove(cmdoutput);
    SerialClose();

    for(ix=0; ix<N_INV; ix++)
    {
        if(f_log[ix] != NULL)
            fclose(f_log[ix]);

        if(f_logfull[ix] != NULL)
            fclose(f_logfull[ix]);
    }
}

void Mainframe::OnClose(wxCloseEvent& WXUNUSED(event))
{
    Destroy();
}

void Mainframe::OnQuit(wxCommandEvent& WXUNUSED(event))
{
    Destroy();
}



void Mainframe::OnAbout(wxCommandEvent& WXUNUSED(event))
{
    wxAboutDialogInfo info;
    info.SetName(_("Aurora Monitor"));
    info.SetVersion(_("1.02"));
    info.SetDescription(_("Receives and displays data from up to 2 Aurora power inverters.\n"));
    info.SetCopyright(_T("(C) 2012 Jonathan Duddington <jonsd@users.sourceforge.net>"));
    info.SetLicence(_T("GNU GENERAL PUBLIC LICENSE Version 3\n\nhttp://www.gnu.org/licenses/gpl.html"));

    wxAboutBox(info);


}
