
/***************************************************************************
 *   Copyright (C) 2012 by Jonathan Duddington                             *
 *   email: jonsd@users.sourceforge.net                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see:                                 *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/


#include <wx/cmndata.h>
#include <wx/colordlg.h>
#include <wx/choice.h>
#include "auroramon.h"




/************************************************************
                Setup Dialog
*************************************************************/

BEGIN_EVENT_TABLE(DlgSetup, wxDialog)
	EVT_BUTTON(dlgOK, DlgSetup::OnButton)
	EVT_BUTTON(dlgCancel, DlgSetup::OnButton)
END_EVENT_TABLE()


DlgSetup::DlgSetup(wxWindow *parent)
    : wxDialog(parent, -1, _T("Setup"), wxDefaultPosition, wxSize(320,280+DLG_HX))
{//=================================
    int y;
    int x;
    x = 96;
    y = 22;
    t_serialport = new wxTextCtrl(this, dlgSerialPort, serial_port, wxPoint(x, y), wxSize(200, 20));
    new wxStaticText(this, -1, _T("Serial port"), wxPoint(8, y+3));
    y = 62;
    t_inverter1 = new wxTextCtrl(this, dlgInverter1, wxString::Format(_T("%d"), inverter_address[0]), wxPoint(x+20,y), wxSize(40, 20));
    t_inverter2 = new wxTextCtrl(this, dlgInverter2, wxString::Format(_T("%d"), inverter_address[1]), wxPoint(x+60,y), wxSize(40, 20));
    new wxStaticText(this, -1, _T("Inverter addresses"), wxPoint(8, y+3));
    y = 96;
    new wxStaticText(this, -1, _T("Data directory"), wxPoint(8, y));
    t_data_dir = new wxTextCtrl(this, dlgDataDir, data_dir, wxPoint(6, y+16), wxSize(306, 20));

    y = 240;
    button_OK = new wxButton(this, dlgOK, _T("OK"), wxPoint(130,y));
    button_OK->SetDefault();
    button_Cancel = new wxButton(this, dlgCancel, _T("Cancel"), wxPoint(220,y));

}

void DlgSetup::ShowDlg()
{//=====================
    t_serialport->ChangeValue(serial_port);

    if(inverter_address[0] > 0)
        t_inverter1->ChangeValue(wxString::Format(_T("%d"), inverter_address[0]));
    else
        t_inverter1->ChangeValue(_T(""));

    if(inverter_address[1] > 0)
        t_inverter2->ChangeValue(wxString::Format(_T("%d"), inverter_address[1]));
    else
        t_inverter2->ChangeValue(_T(""));

    t_data_dir->ChangeValue(data_dir);
    ShowModal();
}


void DlgSetup::OnButton(wxCommandEvent &event)
{//===========================================
    int id;
    long int value;
    wxString dir;
    wxString s;

    id = event.GetId();
    if(id == dlgOK)
    {
        serial_port = t_serialport->GetValue();
        value = 0;
        t_inverter1->GetValue().ToLong(&value);
        inverter_address[0] = value;
        t_inverter2->GetValue().ToLong(&value);
        inverter_address[1] = value;
        data_dir = t_data_dir->GetValue();


        mainframe->SetupStatusPanel(1);
        status_panel->Refresh();

        if(!wxDirExists(data_dir))
        {
            if(wxMessageBox(_T("Create directory '") + data_dir + _T("' ?"), _T("Setup"), wxYES_NO, this) != wxYES)
            {
                return;
            }
            wxMkdir(data_dir);
        }

        dir = data_dir + _T("/system");
        if (!wxDirExists(dir))
            wxMkdir(dir);

//        CalcSun(graph_date.mb_str(wxConvLocal));  // done in OpenLogFiles()
//        graph_panel->Refresh();  // done in OpenLogFiles()
        OpenLogFiles(-1, 0);  // because the inverter address may have changed
        ConfigSave(1);

    }
    Show(false);
}


//=======================================================================================================================


/************************************************************
                Location Dialog
*************************************************************/

BEGIN_EVENT_TABLE(DlgLocation, wxDialog)
	EVT_BUTTON(dlgOK, DlgLocation::OnButton)
	EVT_BUTTON(dlgCancel, DlgLocation::OnButton)
END_EVENT_TABLE()


wxString degrees_to_string(double degrees)
{//=======================================
    int mins;
    int secs;
    int deg;

    deg = (int)degrees;
    if(degrees < 0) degrees = -degrees;
    mins = (int)(degrees*60) % 60;
    secs = (int)(degrees*3600) % 60;
    return(wxString::Format(_T("%2d:%.2d:%.2d"), deg, mins, secs));
}

double string_to_degrees(wxString string)
{//======================================
    int deg = 0;
    int mins = 0;
    float secs = 0;
    double degrees = 0;
    int sign;
    char buf[200];

    strncpy(buf, string.mb_str(wxConvLocal), sizeof(buf));
    buf[sizeof(buf)-1] = 0;

    if(strchr(buf,':') == NULL)
    {
        string.ToDouble(&degrees);
    }
    else
    {
        sscanf(buf, "%d:%d:%f", &deg, &mins, &secs);
        sign = 1;
        if(deg < 0)
        {
            sign = -1;
            deg = -deg;
        }
        degrees = sign * (deg + (double)mins / 60 + secs/3600);
    }
    return(degrees);
}

DlgLocation::DlgLocation(wxWindow *parent)
    : wxDialog(parent, -1, _T("Location"), wxDefaultPosition, wxSize(320,280+DLG_HX))
{//======================================
    int y;
    int x;
    int ix;
    x = 110;
    y = 22;

    new wxStaticText(this, -1, _T("Longitude"), wxPoint(8, y+3));
    new wxStaticText(this, -1, _T("Latitude"), wxPoint(8, y+25));
    new wxStaticText(this, -1, _T("Time Zone (hrs E)"), wxPoint(8, y+47));
    t_longitude = new wxTextCtrl(this, -1, wxEmptyString, wxPoint(x, y), wxSize(90, 20));
    t_latitude = new wxTextCtrl(this, -1, wxEmptyString, wxPoint(x, y+22), wxSize(90, 20));
    t_timezone = new wxTextCtrl(this, -1, wxEmptyString, wxPoint(x, y+44), wxSize(90, 20));
#ifdef __WXMSW__
    t_tz_auto = new wxCheckBox(this, -1, _T("auto"), wxPoint(x+92, y+46));
#else
    t_tz_auto = new wxCheckBox(this, -1, _T("auto"), wxPoint(x+90, y+41));
#endif

    y = 104;
    new wxStaticText(this, -1, _T("Panel Group"), wxPoint(8, y));
    new wxStaticText(this, -1, _T("Tilt"), wxPoint(8, y+20));
    new wxStaticText(this, -1, _T("Facing CW from N"), wxPoint(8, y+40));

    x = 120;
    for(ix=0; ix<N_PANEL_GROUPS; ix++)
    {
        new wxStaticText(this, -1, wxString(wxChar('A' + ix)), wxPoint(x+20, y));
        t_tilt[ix] = new wxTextCtrl(this, -1, wxEmptyString, wxPoint(x, y+18), wxSize(50, 20), wxTE_CENTRE);
        t_facing[ix] = new wxTextCtrl(this, -1, wxEmptyString, wxPoint(x, y+38), wxSize(50, 20), wxTE_CENTRE);
        x += 50;
    }

    y = 240;
    button_OK = new wxButton(this, dlgOK, _T("OK"), wxPoint(130,y));
    button_OK->SetDefault();
    button_Cancel = new wxButton(this, dlgCancel, _T("Cancel"), wxPoint(220,y));

}

void DlgLocation::ShowDlg()
{//=====================
    int ix;

    t_longitude->ChangeValue(degrees_to_string(Longitude));
    t_latitude->ChangeValue(degrees_to_string(Latitude));
    t_timezone->ChangeValue(wxString::Format(_T("%.2f"), Timezone));
    if(TimeZone2 > 24)
        t_tz_auto->SetValue(true);
    else
        t_tz_auto->SetValue(false);

    for(ix=0; ix<N_PANEL_GROUPS; ix++)
    {
        t_tilt[ix]->ChangeValue(wxString::Format(_T("%.1f"), panel_groups[ix].tilt));
        t_facing[ix]->ChangeValue(wxString::Format(_T("%.1f"), panel_groups[ix].facing));
    }
    ShowModal();
}


void DlgLocation::OnButton(wxCommandEvent &event)
{//==============================================
    int id;
    int ix;
    wxString dir;
    wxString s;

    id = event.GetId();
    if(id == dlgOK)
    {
        s = t_longitude->GetValue();
        if(s.Find(':') == wxNOT_FOUND)
        {
            Longitude = 0;
            s.ToDouble(&Longitude);
        }
        else
        {
            Longitude = string_to_degrees(s);
        }

        s = t_latitude->GetValue();
        if(s.Find(':') == wxNOT_FOUND)
        {
            Latitude = 0;
            s.ToDouble(&Latitude);
        }
        else
        {
            Latitude = string_to_degrees(s);
        }

        t_timezone->GetValue().ToDouble(&Timezone);
        if(t_tz_auto->GetValue() == true)
            TimeZone2 = 99;
        else
            TimeZone2 = Timezone;

        for(ix=0; ix<N_PANEL_GROUPS; ix++)
        {
            t_tilt[ix]->GetValue().ToDouble(&panel_groups[ix].tilt);
            t_facing[ix]->GetValue().ToDouble(&panel_groups[ix].facing);
        }
        ConfigSave(1);
        CalcSun(graph_date.mb_str(wxConvLocal));
        graph_panel->Refresh();
    }
    Show(false);
}


//=======================================================================================================================


/************************************************************
                Logging Dialog
*************************************************************/

BEGIN_EVENT_TABLE(DlgLogging, wxDialog)
	EVT_BUTTON(dlgOK, DlgLogging::OnButton)
	EVT_BUTTON(dlgCancel, DlgLogging::OnButton)
END_EVENT_TABLE()



DlgLogging::DlgLogging(wxWindow *parent)
    : wxDialog(parent, -1, _T("Extra log file"), wxDefaultPosition, wxSize(330,280+DLG_HX))
{//=================================
    int y;
    wxString msg = _T(
"This log stores all the data which is received from the\n"
"inverter using the -d command, with the computer time.\n\n"
"This file is additional to the 10 second log which is\n"
"stored in  <aurora directory>/yyyy/d2_yyymmdd\n\n"
"The filename is made by adding the date in the form\n"
"yyyymmdd to the end of the contents of 'Path'.");


    freq_names.Add(_T("No log"));
    freq_names.Add(_T(" 1 minute"));
    freq_names.Add(_T("30 seconds"));
    freq_names.Add(_T("20 seconds"));
    freq_names.Add(_T("10 seconds"));
    freq_names.Add(_T("All responses"));

    y = 12;
    new wxStaticText(this, -1, msg, wxPoint(10, y));

    y = 136;
    t_static[0] = new wxStaticText(this, -1, wxString::Format(_T("Inverter #%d"), inverter_address[0]), wxPoint(10, y));
    t_frequency[0] = new wxChoice(this, -1, wxPoint(79, y-4), wxDefaultSize, freq_names);
    t_static2[0] = new wxStaticText(this, -1, _T("Path"), wxPoint(10, y+24));
    t_path[0] = new wxTextCtrl(this, -1, data_dir, wxPoint(60, y+22), wxSize(260, 20));

    y = 188;
    t_static[1] = new wxStaticText(this, -1, wxString::Format(_T("Inverter #%d"), inverter_address[1]), wxPoint(10, y));
    t_frequency[1] = new wxChoice(this, -1, wxPoint(79, y-4), wxDefaultSize, freq_names);
    t_static2[1] = new wxStaticText(this, -1, _T("Path"), wxPoint(10, y+24));
    t_path[1] = new wxTextCtrl(this, -1, data_dir, wxPoint(60, y+22), wxSize(260, 20));

    y = 240;
    button_OK = new wxButton(this, dlgOK, _T("OK"), wxPoint(134,y));
    button_OK->SetDefault();
    button_Cancel = new wxButton(this, dlgCancel, _T("Cancel"), wxPoint(224,y));

}

void DlgLogging::ShowDlg()
{//=======================
    bool show = true;

     t_frequency[0]->SetSelection(logging_period_index[0]);
     t_frequency[1]->SetSelection(logging_period_index[1]);
     t_path[0]->ChangeValue(logging_path[0]);
     t_path[1]->ChangeValue(logging_path[1]);

     if(inverter_address[1] == 0)
     {
        show = false;
        t_frequency[1]->SetSelection(0);
     }
     t_frequency[1]->Show(show);
     t_path[1]->Show(show);
     t_static[1]->Show(show);
     t_static2[1]->Show(show);
     ShowModal();
}


void DlgLogging::OnButton(wxCommandEvent &event)
{//=============================================
    int id;
    int inv;

    id = event.GetId();
    if(id == dlgOK)
    {
        for(inv=0; inv<2; inv++)
        {
            logging_period_index[inv] = t_frequency[inv]->GetSelection();
            logging_period[inv] = logging_period_tab[logging_period_index[inv]];
            logging_path[inv] = t_path[inv]->GetValue();
        }

        if((inverter_address[1] != 0) && (logging_path[0] != wxEmptyString) && (logging_path[0] == logging_path[1]))
        {
            wxLogError(_T("Path is the same for both inverters"));
            return;
        }
        OpenLogFiles(-1, 0);
        ConfigSave(1);
    }
    Show(false);
}


//=======================================================================================================================


/************************************************************
                Histogram Dialog
*************************************************************/

BEGIN_EVENT_TABLE(DlgHistogram, wxDialog)
	EVT_BUTTON(dlgOK, DlgHistogram::OnButton)
	EVT_BUTTON(dlgCancel, DlgHistogram::OnButton)
END_EVENT_TABLE()


double histogram_max;

DlgHistogram::DlgHistogram(wxWindow *parent)
    : wxDialog(parent, -1, _T("Energy Histogram"), wxDefaultPosition, wxSize(324,200+DLG_HX))
{//=================================
    int y;

    y = 24;
    new wxStaticText(this, -1, _T("Maximum daily energy"), wxPoint(10, y));
    t_dailymax = new wxTextCtrl(this, -1, data_dir, wxPoint(136, y-3), wxSize(46, 20));
    new wxStaticText(this, -1, _T("kW"), wxPoint(184, y));

    y = 160;
    button_OK = new wxButton(this, dlgOK, _T("OK"), wxPoint(134,y));
    button_OK->SetDefault();
    button_Cancel = new wxButton(this, dlgCancel, _T("Cancel"), wxPoint(224,y));

}

void DlgHistogram::ShowDlg()
{//=========================

     t_dailymax->ChangeValue(wxString::Format(_T("%4.1f"), histogram_max));
     ShowModal();
}


void DlgHistogram::OnButton(wxCommandEvent &event)
{//=============================================
    int id;

    id = event.GetId();
    if(id == dlgOK)
    {
        t_dailymax->GetValue().ToDouble(&histogram_max);
        ConfigSave(1);
        graph_panel->Refresh();
    }
    Show(false);
}


