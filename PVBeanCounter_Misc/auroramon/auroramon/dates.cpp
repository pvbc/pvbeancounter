

/***************************************************************************
 *   Copyright (C) 2012 by Jonathan Duddington                             *
 *   email: jonsd@users.sourceforge.net                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see:                                 *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/


#include <wx/filefn.h>
#include <wx/filename.h>
#include "auroramon.h"

#define YEAR_FIRST  2000
#define N_YEARS     100

int year_today;
int yearix_first;
wxArrayString years;

static int collected_dates = 0;
unsigned char *yeartab_flags[N_YEARS];   // bit 0 - we have some power data, bits 4,5 inv0,inv1 energy has an average value
unsigned short *yeartab_energy[N_INV][N_YEARS];

static short monthstart1[13] = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};  // non-leap years
static short monthstart2[13] = {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366};  // leap years

wxFont Font_Year(13, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD);
wxFont Font_Default(10, wxFONTFAMILY_SWISS, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_NORMAL);


void InitDates()
{//=============
    struct tm *btime;
    time_t current_time;

    time(&current_time);
    btime = localtime(&current_time);
    year_today = btime->tm_year+1900;
    yearix_first = year_today;

    collected_dates = 0;
    memset(yeartab_flags, 0, sizeof(yeartab_flags));
    memset(yeartab_energy, 0, sizeof(yeartab_energy));
}


int NYearDays(int year, short **m)
{//===============================
    if((year % 4) == 0)
    {
        if(m != NULL)
            *m = monthstart2;
        return(366);
    }
    if(m != NULL)
        *m = monthstart1;
    return(365);
}

int DayOfYear2(int year, int month, int day)
{//=========================================
    short *monthstart;

    NYearDays(year, &monthstart);

    if((month <= 0) || (month > 12) || (day <= 0) || (day > 31))
        return(-1);

    return(monthstart[month-1] + (day-1));
}


int DayOfYear(const wxString date_str, int *out_yearix)
{//====================================================
    int yearix, doy;
    long int year, month, day;
    wxString year_str, month_str, day_str;

    if(date_str == wxEmptyString)
        return(-1);

    year_str = date_str.Left(4);
    month_str = date_str.Mid(4, 2);
    day_str = date_str.Right(2);

    if(year_str.ToLong(&year) && month_str.ToLong(&month) && day_str.ToLong(&day))
    {
        yearix = year - YEAR_FIRST;
        if((yearix < 0) || (yearix >= N_YEARS) || ((doy = DayOfYear2(year, month, day)) < 0) || (doy > 365))
        {
            return(-1);
        }

        if(out_yearix != NULL)
            *out_yearix = yearix;
        return(doy);
    }
    return(-1);
}

wxString DoyToDate(int doy, int yearix)
{//====================================
    int ix;
    int year;
    static wxString date;

    short *monthstart;

    year = yearix + YEAR_FIRST;
    if((year % 4) == 0)
        monthstart = monthstart2;  //NOTE, next non-leap (year%4) is 2100
    else
        monthstart = monthstart1;

    for(ix=1; ix<13; ix++)
    {
        if(doy < monthstart[ix])
        {
            date.Printf(_T("%.4d%.2d%.2d"), year, ix, doy-monthstart[ix-1]+1);
            return(date);
        }
    }
    return(wxEmptyString);
}


void FindDates()
{//=============
    unsigned int year_count;
    int doy;  // 0 to 364 (or 365 in leap years)
    int yearix;
    long inverter;  // inverter slot, 0 or 1
    long value;
    int n;
    int energy1;
    wxString fname;
    wxString leaf;
    wxString extension;
    wxString s;
    FILE *f;
    int prev_doy = 999;
    int prev_yearix = -1;
    float prev_energy_total=0;
    float energy[6];
    char date_str[20];
    char buf[256];

    for(year_count=0; year_count<years.Count(); year_count++)
    {
        fname = wxFindFirstFile(data_dir+_T("/")+years[year_count]+_T("/d*_2*"), wxFILE);
        while(!fname.empty())
        {
            leaf = wxFileName(fname).GetName();   // without the extension

            doy = DayOfYear(leaf.Right(8), &yearix);

            if(doy >= 0)
            {
                if(yeartab_flags[yearix] == NULL)
                    break;

                yeartab_flags[yearix][doy] = 1;   // bit 0: we have some power data for this day
            }
            fname = wxFindNextFile();
        }

        // file names are of the for:  inverterN_A_yyyy
        // Where N is 0 or 1,  A is the inverter address, yyyy is the year
        fname = wxFindFirstFile(data_dir+_T("/")+years[year_count]+_T("/inverter*_2*"), wxFILE);
        while(!fname.empty())
        {
            leaf = wxFileName(fname).GetName();
            extension = wxFileName(fname).GetExt();

            s = leaf.Right(4);
            value = 0;
            s.ToLong(&value);
            if((value >= 2000) && (value < 3000) && (extension == _T("txt")))
            {
                // filename must end with a year
                s = leaf.Mid(8, 1);
                s.ToLong(&inverter);

                if((f = fopen(fname.mb_str(wxConvLocal), "r")) != NULL)
                {
                    while(fgets(buf, sizeof(buf), f) != NULL)
                    {
                        if(buf[0] == '#') continue;

                        n = sscanf(buf, "%s %f %f %f %f %f %f", date_str, &energy[0], &energy[1], &energy[2], &energy[3], &energy[4], &energy[5]);
                        if(n < 2)
                           continue;

                        doy = DayOfYear(wxString(date_str, wxConvLocal), &yearix);

                        if((doy >= 0) && (yeartab_energy[inverter][yearix] != NULL))
                        {
                            if((n >= 6) && (yearix == prev_yearix) && ((doy - prev_doy) > 1))
                            {
                                // There is a gap in the records of daily energy output.
                                // Fill with the average.
                                // Note: not implemented across different years.
                                int j;
                                int ndays;
                                ndays = doy - prev_doy;
                                energy1 = (int)((energy[4]-prev_energy_total)/ndays * 1000);
                                if(energy1 < 0)
                                    energy1 = 0;
                                for(j=1; j <= ndays; j++)
                                {
                                    yeartab_energy[inverter][yearix][prev_doy+j] = energy1;
                                    yeartab_flags[yearix][prev_doy+j] |= (1 << (inverter+4));   // bits 4,5 this is an average value
                               }
                            }
                            else
                            {
                                energy1 = (int)(energy[0] * 1000);
                                if(energy1 < 0)
                                    energy1 = 0;
                                yeartab_energy[inverter][yearix][doy] = energy1;
                            }
                        }
                        prev_yearix = yearix;
                        prev_doy = doy;
                        prev_energy_total = energy[4];
                    }

                    fclose(f);
                }
            }
            fname = wxFindNextFile();
        }
    }
}


void FindYears()
{//=============
    int yearix;
    long int year;
    wxString dir;
    wxString leaf;

    years.Empty();

    dir = wxFindFirstFile(data_dir+_T("/2*"), wxDIR);
    while(!dir.empty())
    {
        leaf = wxFileName(dir).GetFullName();
        if(leaf.Len() == 4)
        {
            if(leaf.ToLong(&year) == true)
            {
                if((year >= YEAR_FIRST) && (year < (YEAR_FIRST + N_YEARS)))
                {
                    years.Add(leaf);
                    yearix = year - YEAR_FIRST;

                    if(yearix < yearix_first)
                        yearix_first = yearix;

                    yeartab_flags[yearix] = (unsigned char *)realloc(yeartab_flags[yearix], 366 * sizeof(unsigned char));
                    yeartab_energy[0][yearix] = (unsigned short *)realloc(yeartab_energy[0][yearix], 366 * sizeof(unsigned short));
                    yeartab_energy[1][yearix] = (unsigned short *)realloc(yeartab_energy[1][yearix], 366 * sizeof(unsigned short));

                    memset(yeartab_flags[yearix], 0, 366 * sizeof(unsigned char));
                    memset(yeartab_energy[0][yearix], 0, 366 * sizeof(unsigned short));
                    memset(yeartab_energy[1][yearix], 0, 366 * sizeof(unsigned short));
                }
            }
        }
        dir = wxFindNextFile();
    }
    years.Sort();
}



void CollectDates(int force)
{//=========================
    if((collected_dates==1) && (force==0))
        return;  // already done

    FindYears();
    FindDates();
    collected_dates = 0;

}


wxString NextDate(const wxString date_str, int skip)
{//=================================================
    int doy;
    int yearix;

    CollectDates(0);

    if((doy = DayOfYear(date_str, &yearix)) < 0)
        return(wxEmptyString);  // not a valid date_str

    doy++;

    if(skip == 0)
    {
        if(doy >= NYearDays(yearix + YEAR_FIRST, NULL))
        {
            doy = 0;
            yearix++;
            if(yeartab_flags[yearix] == NULL)
                return(wxEmptyString);

        }
        return(DoyToDate(doy, yearix));
    }

    while(yearix <= (year_today - YEAR_FIRST))
    {
        if(yeartab_flags[yearix] != NULL)
        {
            while(doy < 366)
            {
                if(yeartab_flags[yearix][doy] & 1)
                {
                    return(DoyToDate(doy, yearix));
                }
                doy++;
            }
        }
        yearix++;
        doy = 0;
    }
    return(wxEmptyString);
}


wxString YesterdayDate(const wxString date_str)
{//============================================
    int doy;
    int yearix;

    if((doy = DayOfYear(date_str, &yearix)) < 0)
        return(wxEmptyString);  // not a valid date_str

    doy--;
    if(doy < 0)
    {
        yearix--;
        doy = NYearDays(yearix + YEAR_FIRST, NULL) - 1;
    }

    return(DoyToDate(doy, yearix));
}


wxString PrevDate(const wxString date_str, int skip)
{//=================================================
    int doy;
    int yearix;

    CollectDates(0);


    if((doy = DayOfYear(date_str, &yearix)) < 0)
        return(wxEmptyString);  // not a valid date_str

    doy--;

    if(skip == 0)
    {
        if(doy < 0)
        {
            yearix--;
            if(yeartab_flags[yearix] == NULL)
                return(wxEmptyString);

            doy = NYearDays(yearix + YEAR_FIRST, NULL) - 1;
        }

        return(DoyToDate(doy, yearix));
    }

    while(yearix >= 0)
    {
        if(yeartab_flags[yearix] != NULL)
        {
            while(doy >= 0)
            {
                if(yeartab_flags[yearix][doy] & 1)
                {
                    return(DoyToDate(doy, yearix));
                }
                doy--;
            }
        }
        yearix--;
        doy = 365;   // Note, 365 only has data in a leap-year
    }
    return(wxEmptyString);
}



int DateEnergy(const wxString date_str)
{//====================================
    int doy;
    int yearix;
    int energy = 0;

    CollectDates(0);

    doy = DayOfYear(date_str, &yearix);
    if(doy < 0)
        return(0);

    if(yeartab_energy[0][yearix] != NULL)
        energy += yeartab_energy[0][yearix][doy];
    if(yeartab_energy[1][yearix] != NULL)
        energy += yeartab_energy[1][yearix][doy];

    return(energy);
}


int DisplayDateGraphs(const wxString date_str, int control)
{//========================================================
// Display the power data for a specified date
    int doy;
    int yearix;
    int ix = 0;
    int j;
    long int addr;
    wxString dir;
    wxString fname;
    wxString fname2;
    wxString s;
    wxString not_placed[2] = {wxEmptyString, wxEmptyString};
    int done[2] = {0, 0};

    graph_panel->SetMode(0);

    doy = DayOfYear(date_str, &yearix);
    if(doy < 0)
        return(-1);

    graph_date_ymd = date_str;
    graph_date = date_str.Left(4) + _T("-") + date_str.Mid(4,2) + _T("-") + date_str.Right(2);

    if(date_str == wxString(date_ymd, wxConvLocal))
    {
        // This is today's date
        OpenLogFiles(-1, 1);
    }
    else
        display_today = 0;

    graph_panel->NewDay();

    dir = data_dir + _T("/") + date_str.Left(4);

    fname = wxFindFirstFile(dir+_T("/d*_*"), wxFILE);
    while(!fname.empty())
    {
        fname2 = fname;
        if((fname2.Right(4) == _T(".txt")) || (fname2.Right(4) == _T(".dat")))
            fname2 = wxFileName(fname).GetName();

        if(fname2.Right(8) == date_str)
        {
            s = fname2.AfterLast('d');
            s = s.BeforeFirst('_');
            if(s.ToLong(&addr) == false)
                continue;

            // current inverter addresses may differ from earlier dates
            // Fill matching addresses first
            if(addr == inverter_address[0])
            {
                graph_panel->ReadEnergyFile(0, fname, control, NULL);
                done[0] = 1;
            }
            else
            if(addr == inverter_address[1])
            {
                graph_panel->ReadEnergyFile(1, fname, control, NULL);
                done[1] = 1;
            }
            else
            {
                if(ix < 2)
                    not_placed[ix++] = fname;
            }
        }
        fname = wxFindNextFile();
    }

    // Find free slots for any mis-matched inverter addresses
    for(j=0; j<ix; j++)
    {
        if(done[0] == 0)
            graph_panel->ReadEnergyFile(0, not_placed[j], control, NULL);
        else
        if(done[1] == 0)
            graph_panel->ReadEnergyFile(1, not_placed[j], control, NULL);
    }

    CalcSun(graph_date.mb_str(wxConvLocal));
    graph_panel->ResetGraph();

//wxLogStatus(wxString::Format(_T("startix %d"), graph_panel->start_ix));
    return(0);
}  // end of DisplayDateGraphs



//wxColour colour_energy1(0x64c8ff);
//wxColour colour_energy2(0x84c8ff);
wxColour month_colours[12] = {
    wxColour(0xccc97a), wxColour(0xb2e673), wxColour(0x7ee673),
    wxColour(0x73e69c), wxColour(0x6ae6d9), wxColour(0x6593f0),
    wxColour(0x6565f0), wxColour(0xa373e6), wxColour(0x7566cc),
    wxColour(0x5a6bb3), wxColour(0x6382a6), wxColour(0xbfa39b)};

wxColour colour_average(0x404040);
wxString month_names[12] = {_("Jan"),_("Feb"),_("Mar"),_("Apr"),_("May"),_("Jun"),_("Jul"),_("Aug"),_("Sep"),_("Oct"),_("Nov"),_("Dec")};

int energy_n_days = 0;

void GraphPanel::DrawEnergy(wxDC &dc)
{//==================================
    int ix;
    int width, height;
    int yearix;
    int doy;
    int dow;
    int dom=0;
    int n_yeardays;
    int energy;
    int day_ix;
    int initial_year;
    int x, y;
    int month;
    short *monthstart;
    double yscale;
    double xscale;
    int bar_width;
    int y_max;
    int y_grad;
    int y_grad2;
    int max_x;
    int today;
    int total_energy = 0;
    int total_days = 0;
    wxColour colour;
    wxColour colour_week;
    wxColour colour_month;
    struct tm btime;
    int histogram_mode;
    int prev_x = 0;
    int today_doy;
    int today_yearix;

    histogram_mode = histogram_page;

    static int hist_scale[N_HIST_SCALE] = {1, 2, 3, 4, 6, 9, 13, 18, 24, 30, 38, 50};
    xscale = hist_scale[hist_xscale_ix];
    if(histogram_mode == 1)
        xscale /= 2;   // weeks
    if(histogram_mode == 2)
        xscale /= 4;   // months

    bar_width = xscale - 1;
    if(bar_width < 1) bar_width = 1;

    CollectDates(0);
    GetClientSize(&width,&height);
    today_doy = DayOfYear(wxString(date_ymd, wxConvLocal), &today_yearix);

    if(energy_n_days == 0)
    {
        // first time called, examine the data
        day_ix = -1;
        for(yearix = yearix_first; yearix <= (year_today - YEAR_FIRST); yearix++)
        {
            n_yeardays = NYearDays(yearix+YEAR_FIRST, &monthstart);
            for(doy=0; doy < n_yeardays; doy++)
            {
                energy = 0;
                if(yeartab_energy[0][yearix] != NULL)
                    energy += yeartab_energy[0][yearix][doy];
                if(yeartab_energy[1][yearix] != NULL)
                    energy += yeartab_energy[1][yearix][doy];
                if((day_ix < 0) && (energy > 0))
                    day_ix = 0;   // the first day with an energy reading
                else
                if(day_ix >= 0)
                    day_ix++;

                if(energy > 0)
                    energy_n_days = day_ix;
            }
        }
    }

    max_x = energy_n_days * xscale;
    x = width - max_x - 120;
    if(offset_x < x)
    {
        offset_x = x;
    }
    if(offset_x > 40)
        offset_x = 40;


    dc.SetTextForeground(*wxWHITE);
    dc.SetTextBackground(*wxBLACK);

    y_max = (int)(histogram_max * 1000 * range_adjust[hist_range_ix]);
    yscale = (double)(height)/y_max;

    y_grad = 1000;
    y_grad2 = 5000;
    if(y_max > 10000)
    {
        y_grad = 2000;
        y_grad2 = 10000;
    }
    if(y_max > 30000)
    {
        y_grad = 5000;
        y_grad2 = 10000;
    }
    // draw vertical scale
    for(ix=0; ix < y_max; ix+=y_grad)
    {
        y = height - (int)(ix * yscale);
        if((ix % y_grad2) == 0)
            dc.SetPen(colour_grid1);
        else
            dc.SetPen(colour_grid2);
        dc.DrawLine(0, y, width, y);
        dc.DrawText(wxString::Format(_T("%2dkW"), ix / 1000), width-30, y);
    }

    memset(&btime, 0, sizeof(btime));
    btime.tm_year = yearix_first + YEAR_FIRST - 1900;
    btime.tm_mday = 1;
    mktime(&btime);
    dow = btime.tm_wday-1;

    colour_month = colour_week = month_colours[0];
    day_ix = -1;
    initial_year = 1;
    for(yearix = yearix_first; yearix <= (year_today - YEAR_FIRST); yearix++)
    {
        month = 0;

        n_yeardays = NYearDays(yearix+YEAR_FIRST, &monthstart);
        for(doy=0; doy < n_yeardays; doy++)
        {
            dow++;
            if(dow == 7)
                dow = 0;

            energy = 0;
            if(yeartab_energy[0][yearix] != NULL)
                energy += yeartab_energy[0][yearix][doy];
            if(yeartab_energy[1][yearix] != NULL)
                energy += yeartab_energy[1][yearix][doy];
            if((day_ix < 0) && (energy > 0))
                day_ix = 0;   // the first day with an energy reading
            else
            if(day_ix >= 0)
                day_ix++;

            today = 0;
            if((doy==today_doy) && (yearix==today_yearix) && (energy == 0))
            {
                energy = (int)((energy_total[0][0] + energy_total[1][0]) * 1000);
                today = 1;

                if(day_ix < 0)
                    day_ix = 0;
            }

            x = day_ix*xscale + offset_x;

            if(doy >= monthstart[month])
            {
                // start of a month
                dom = 0;
                if(day_ix >= 0)
                {
                    // draw horizontal scale
                    if((month==0) || (initial_year && (month < 10) && (xscale > 2)))
                    {
                        initial_year = 0;
                        dc.SetFont(Font_Year);
                        dc.DrawText(wxString::Format(_T("%4d"), yearix+YEAR_FIRST), x+4, 17);
                        dc.SetPen(colour_grid1);
                        dc.DrawLine(x, 0, x, height);
                    }
                    else
                    if(xscale > 0.49)
                    {
                        dc.SetPen(colour_grid2);
                        dc.DrawLine(x, 0, x, height);
                    }

                    dc.SetFont(Font_Default);
                    if(xscale < 1.0)
                        dc.DrawText(month_names[month][0], x+4, 2);
                    else
                    if(xscale > 0.49)
                        dc.DrawText(month_names[month], x+4, 2);
                }

                colour_month = colour;
                colour = month_colours[month];
                if(Latitude < 0)
                {
                    colour = month_colours[(month + 6) % 12];
                }
                month++;
            }
            dom++;

            dc.SetPen(*wxTRANSPARENT_PEN);

            if(energy > 0)
            {
                if(today == 0)
                {
                    total_energy += energy;
                    total_days++;
                }

                if(histogram_mode == 0)
                {
                    if(yeartab_flags[yearix][doy] & 0x30)
                        dc.SetBrush(colour_average);   // this value is an average
                    else
                        dc.SetBrush(colour);
                    y = (int)(energy * yscale);
                    if(today)
                        dc.SetBrush(*wxWHITE_BRUSH);
                    dc.DrawRectangle(x, height-y, bar_width, y);
                }
            }

            if((histogram_mode == 1) && (dow == 0))
            {
                if(total_days > 0)
                {
                    dc.SetBrush(colour_week);
                    energy = total_energy / total_days;
                    y = (int)(energy * yscale);
                    if(xscale < 1.0)
                        dc.DrawRectangle(prev_x, height-y, x - prev_x, y);
                    else
                        dc.DrawRectangle(prev_x, height-y, x - prev_x - 1, y);
                }
                colour_week = colour;
                prev_x = x;
                total_days = 0;
                total_energy = 0;
            }
            if((histogram_mode == 2) && (dom == 1))
            {
                if(total_days > 0)
                {
                    dc.SetBrush(colour_month);
                    energy = total_energy / total_days;
                    y = (int)(energy * yscale);
                    dc.DrawRectangle(prev_x, height-y, x - prev_x - 1, y);
                }
                prev_x = x;
                total_days = 0;
                total_energy = 0;
            }
        }
    }

#ifdef deleted
    if(histogram_mode == 2)
    {
        if(total_days > 0)
        {
            energy = total_energy / total_days;
            y = (int)(energy * yscale);
            if(xscale < 1.0)
                dc.DrawRectangle(prev_x, height-y, x - prev_x, y);
            else
                dc.DrawRectangle(prev_x, height-y, x - prev_x - 1, y);
        }
    }
#endif

}

