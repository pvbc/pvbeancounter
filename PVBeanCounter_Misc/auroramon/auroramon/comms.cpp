
/***************************************************************************
 *   Copyright (C) 2012 by Jonathan Duddington                             *
 *   email: jonsd@users.sourceforge.net                                    *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, see:                                 *
 *               <http://www.gnu.org/licenses/>.                           *
 ***************************************************************************/

#ifdef __WXMSW__
#include <windows.h>
//#include <wx/msw/registry.h>
#else
#include <termios.h>
#endif

#include <errno.h>
#include <wx/filename.h>
#include <wx/thread.h>
#include "auroramon.h"


#define STATE_OK   0

#define opGetDSP   59
#define opGetCE    78



int big_endian = 0;
int SerialBlocking = 1;             // serial port timeout for read
int max_send_attempts = 3;
#ifdef __WXMSW__
static int max_read_attempts = 6;
#else
static int max_read_attempts = 5;
#endif

static int comms_error = 0;
static int transmission_state_error = 0;
static int serial_port_error = 0;
static int comms_inverter = 0;
#define N_CLR_ATTEMPTS 1000

#define N_SERIALBUF 11
static char SerialBuf[N_SERIALBUF];
static const char *SerialBufSpaces = "          ";   // fill SerialBuf with 10 speaces and /0

wxString cmdoutput;
int command_inverter = 0;
int command_type = 0;
int command_queue = 0;
static int inverter_fails[N_INV] = {0,0};

void SendCommand(int inv, int type);

INVERTER_RESPONSE inverter_response[N_INV];
INVERTER_RESPONSE *ir;




void LogMessage2(wxString string)
{//==============================
    FILE *f;
    char buf[256];
    struct tm *btime;
    time_t current_time;

//return;

    if((inverter_alive[comms_inverter] == 0) && (inverter_fails[comms_inverter] > 3))
        return;

    time(&current_time);
    btime = localtime(&current_time);

    strncpy0(buf, wxString(data_dir+_T("/com_log.txt")).mb_str(wxConvLocal), sizeof(buf));
    if((f = fopen(buf, "a")) != NULL)
    {
        strncpy0(buf, string.mb_str(wxConvLocal), sizeof(buf));
        fprintf(f, "%.2d:%.2d:%.2d  %s\n", btime->tm_hour, btime->tm_min, btime->tm_sec, buf);
        fclose(f);
    }
}


class InverterThread: public wxThread
{//============================
    public:
        InverterThread(void);
        virtual void *Entry();

    private:
        int addr;
        int type;
};

InverterThread::InverterThread(void)
        : wxThread()
{//=================================
    type = 0;
}


InverterThread *inverter_thread;



void QueueCommand(int inv, int type)
{//=================================
    command_queue = (type << 8) + inv;
}




#ifdef __WXMSW__
static HANDLE fd_serial = NULL;

#ifdef deleted
void FindSerialPorts()
{//===================
    int ix;
    size_t nSubKeys;
    wxString strTemp;
    wxRegKey *pRegKey = new wxRegKey("HKEY_LOCAL_MACHINE\\HARDWARE\\DEVICEMAP\\SERIALCOMM");

    if(!pRegKey->Exists())
    {
    }

    //Retrive the number of SubKeys and enumerate them
    pRegKey->GetKeyInfo(&nSubKeys,NULL,NULL,NULL);

    pRegKey->GetFirstKey(strTemp,1);
    for(int ix=0;ix<nSubKeys;ix++)
    {
         wxMessageBox(strTemp,"SubKey Name",0,this);
         pRegKey->GetNextKey(strTemp,1);
    }
}
#endif


void SerialClose()
{//===============
    if(fd_serial != NULL)
        CloseHandle(fd_serial);
    fd_serial = NULL;
}

int SerialOpen()
{//=============
	int err =0;
    char dev_name[30];
    DCB dcbSerialParams = {0};
    COMMTIMEOUTS timeouts = {0};

    strncpy0(dev_name, serial_port.mb_str(wxConvLocal), sizeof(dev_name));

    fd_serial = CreateFile(dev_name, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);

    if(fd_serial == INVALID_HANDLE_VALUE)
    {
        if(GetLastError() == ERROR_FILE_NOT_FOUND)
        {
            // serial port does not exist
			err = -1;
        }
		else
		{
			// some other error
			err = -2;
		}
		return(err);
    }

    dcbSerialParams.DCBlength = sizeof(dcbSerialParams);

    if(!GetCommState(fd_serial, &dcbSerialParams))
    {
        // error getting state
		err = -3;
    }
    else
    {
        dcbSerialParams.BaudRate = CBR_19200;
        dcbSerialParams.ByteSize = 8;
        dcbSerialParams.StopBits = ONESTOPBIT;
        dcbSerialParams.Parity = NOPARITY;

        if(!SetCommState(fd_serial, &dcbSerialParams))
        {
            // error setting serial port state
            err = -4;
        }
        else
        {
            timeouts.ReadIntervalTimeout = 50;
            timeouts.ReadTotalTimeoutConstant = 80;
            timeouts.ReadTotalTimeoutMultiplier = 10;
            timeouts.WriteTotalTimeoutConstant = 50;
            timeouts.WriteTotalTimeoutMultiplier = 10;

            if(!SetCommTimeouts(fd_serial, &timeouts))
            {
                // error in setting timeouts
                err = -5;
            }
        }
    }

    if(err != 0)
    {
        SerialClose();
    }

	return(err);
}


#else  // LINUX

static struct termios old_terminal;    // saved previous serial device configuration
static int fd_serial = -1;

int SerialConfig()
{//===============
    struct termios terminal;    // serial device configuration

    tcgetattr(fd_serial, &old_terminal);     // save previous port settings

    memset(&terminal, 0, sizeof(terminal));
    terminal.c_cflag &= ~PARENB;	    // no parity
    terminal.c_cflag &= ~CSTOPB;		// one stop bit
    terminal.c_cflag &= ~CSIZE;			// character size mask
    terminal.c_cflag &= ~HUPCL;			// no hangup
//    if (bRTSCTS)
//        terminal.c_cflag |= CRTSCTS;             /* enable hardware flow control */
//    else
        terminal.c_cflag &= ~CRTSCTS;             /* disable hardware flow control */
    terminal.c_cflag |= CS8 | CLOCAL | CREAD;	// 8 bit - ignore modem control lines - enable receiver
//    if (bXonXoff)
//        terminal.c_iflag |= (IXON | IXOFF);		/* enable XON/XOFF flow control on output & input */
//    else
        terminal.c_iflag &= ~(IXON | IXOFF);		/* disable XON/XOFF flow control on output & input*/
    terminal.c_iflag |= IGNBRK | IGNPAR;		/* ignore BREAK condition on input & framing errors & parity errors */
    terminal.c_oflag = 0;	    			/* set serial device input mode (non-canonical, no echo,...) */
    terminal.c_oflag &= ~OPOST;			/* disable output processing */
    terminal.c_lflag = 0;
    terminal.c_cc[VTIME]    = SerialBlocking;		/* timeout in 1/10 sec intervals */
    terminal.c_cc[VMIN]     = 0;			/* block until char or timeout */

    if(cfsetospeed(&terminal, B19200) != 0)
    {
        // failed to set output speed
        return(-2);
    }

    if(cfsetispeed(&terminal, B19200) != 0)
    {
        // failed to set input speed
        return(-3);
    }

    if(tcflush(fd_serial, TCIFLUSH) != 0)
    {
    }
    if(tcsetattr(fd_serial, TCSANOW, &terminal) != 0)
    {
        return(-4);
    }

    if(tcflush(fd_serial, TCIOFLUSH) != 0)
    {
        return(-5);
    }

    return(0);
}


int SerialOpen()
{//==============
    int result;
    char dev_name[20];

    strncpy0(dev_name, serial_port.mb_str(wxConvLocal), sizeof(dev_name));

    // open the serial port
    if((fd_serial = open(dev_name, O_RDWR | O_NOCTTY )) < 0)
    {
        // failed to open serial port device
        return(-1);
    }

    if((result = SerialConfig()) < 0)
    {
        close(fd_serial);
        fd_serial = -1;
        return(result);
    }

    return(0);
}


void SerialClose()
{//===============
    if(fd_serial < 0)
        return;

    if(tcsetattr(fd_serial, TCSANOW, &old_terminal) != 0)
    {
        return;
    }
    if(tcflush(fd_serial, TCIOFLUSH) != 0)
    {
    }
    close(fd_serial);
    fd_serial = -1;
}
#endif


/*--------------------------------------------------------------------------
    crc16
                                         16   12   5
    this is the CCITT CRC 16 polynomial X  + X  + X  + 1.
    This is 0x1021 when x is 2, but the way the algorithm works
    we use 0x8408 (the reverse of the bit pattern).  The high
    bit is always assumed to be set, thus we only use 16 bits to
    represent the 17 bit value.
----------------------------------------------------------------------------*/

#define POLY 0x8408   /* 0x1021 bit reversed */

unsigned short crc16(char *data_p, unsigned short length)
{//======================================================
    unsigned char i;
    unsigned int data;
    unsigned int crc = 0xffff;

    if (length == 0)
        return (~crc);
    do
    {
        for (i=0, data=(unsigned int)0xff & *data_p++;
                i < 8;
                i++, data >>= 1)
        {
            if ((crc & 0x0001) ^ (data & 0x0001))
                crc = (crc >> 1) ^ POLY;
            else
                crc >>= 1;
        }
    } while (--length);

    crc = ~crc;
    return (crc);
}


int ReadNextChar(char *out, int timeout)
{//=====================================

#ifdef __WXMSW__
    unsigned long n_read;
    if(!ReadFile(fd_serial, out, 1, &n_read, NULL))
    {
    }
#else
    int n_read;
    errno = 0;
    n_read = read(fd_serial, out, 1);

// we get result=0 if inverter is not alive
    if(errno != 0)
    {
    }
    if(n_read == -1)
    {
        // failed to read from serial device
    }
#endif
    return(n_read);
}


int ReadToBuffer(char *out, int n_chars)
{//=====================================
    int ix;
    char ch;
    int result;
    int retry = 0;

    ix = 0;
    while(ix < n_chars)
    {
        while((result = ReadNextChar(&ch, 0)) == 0)
        {
            retry++;
            if(retry > max_read_attempts)
            {
                return(0);
            }
        }

        if(result == 1)
        {
            out[ix++] = ch;
        }
        else
            return(result);

    }

    if(retry > 1)
    {
LogMessage2(wxString::Format(_T("   Wait %d"), retry));
    }
    return(ix);  // OK
}


int Communicate(int check_state)
{//=============================
    int count = 0;
    unsigned long nchars = 0;
    int result = 0;
    int crc_ok = 0;
    int crcValue;
    char ch;
    int max_attempts;
    int attempts;
    char SerialBufSave[N_SERIALBUF];

//wxStartTimer();

    memcpy(SerialBufSave, SerialBuf, N_SERIALBUF);

    max_attempts = max_send_attempts;
    if(inverter_alive[comms_inverter] == 0)
    {
        // This inverter is not alive.  Don't do repeat attempts, and so don't slow down the polling rate of the other inverter too much.
        max_attempts = 1;

        if((inverter_fails[comms_inverter] > 3) && (inverter_alive[comms_inverter ^ 1] == 0))
        {
            // The other inverter is also not alive. Reduce the polling rate.
            inverter_thread->Sleep(300);
            max_attempts = 2;
        }
    }

    for(attempts = 1; (crc_ok == 0) && (attempts <= max_attempts); attempts++)
    {
        if(attempts > 1)
        {
            SerialClose();
            SerialOpen();
            inverter_comms_error[comms_inverter] = 1;
        }
        memcpy(SerialBuf, SerialBufSave, N_SERIALBUF);

        while((ReadNextChar(&ch, 0) != 0) && (count < N_CLR_ATTEMPTS))
        {
            count++;
        }

        crcValue = crc16(SerialBuf, 8);
        SerialBuf[8] = crcValue & 0xff;
        SerialBuf[9] = (crcValue >> 8) & 0xff;
        SerialBuf[10] = 0;

#ifdef __WXMSW__
        WriteFile(fd_serial, SerialBuf, 10, &nchars, NULL);

#else
        if(tcflush(fd_serial, TCIOFLUSH) != 0)
        {
            result = 1;
        }
        nchars = write(fd_serial, SerialBuf, 10);   // should we repeat until all chars have been sent?
        if(tcdrain(fd_serial) != 0)
        {
            result = 2;
        }
#endif

        if(nchars != 10)
        {
LogMessage2(wxString::Format(_T("%d  Only written %d chars"), attempts, nchars));
            if(nchars <= 0)
                continue;   // we havent sent anything, so no point in looking for a reply
        }

        strcpy(SerialBuf, SerialBufSpaces);

        result = ReadToBuffer(SerialBuf, 8);

        if(result == 8)
        {
            crcValue = crc16(SerialBuf, 6);
            if(((unsigned char)SerialBuf[6] != (crcValue & 0xff)) || ((unsigned char)SerialBuf[7] != ((crcValue >> 8) & 0xff)))
            {
                // crc error
LogMessage2(wxString::Format(_T("%d  CRC error, nchars %d opcode %2d %2d"), attempts, result, SerialBufSave[1], SerialBufSave[2]));
            }
            else
            {
                crc_ok = 1;
            }
        }
        else
        if(result == 0)
        {
if(attempts >= max_attempts)
LogMessage2(wxString::Format(_T("%d  Wait %d timeout\n"), attempts, max_read_attempts+1));
else
LogMessage2(wxString::Format(_T("%d  Wait %d timeout"), attempts, max_read_attempts+1));
        }
        else
        {
            // failed to read from serial device
LogMessage2(wxString::Format(_T("%d  ReadToBuffer %d chars"), attempts, result));
        }
    }


    if(crc_ok == 0)
    {
        if(result > 0)
        {
LogMessage2(_T("CRC error, failed\n"));
        }
        result = -1;
    }
    if((check_state==1) && (SerialBuf[0] != STATE_OK))
    {
        transmission_state_error = SerialBuf[0];
        if(transmission_state_error != 0x20)
        {
            // 0x20 is the space character we put into SerialBuf[0], unchanged
LogMessage2(wxString::Format(_T("Transmission state error %d\n"), transmission_state_error));
        }
        result = -2;
    }
    if(result <= 0)
    {
        comms_error |= 1;
    }
    else
    {
//LogMessage2(wxString::Format(_T("OK   opcode %2d %2d  result %.2x %.2x %.2x %.2x %.2x %.2x"), SerialBufSave[1], SerialBufSave[2],
//                             SerialBuf[0], SerialBuf[1], SerialBuf[2]&0xff, SerialBuf[3]&0xff, SerialBuf[4]&0xff, SerialBuf[5]&0xff));
    }

//LogMessage2(wxString::Format(_T("  time %d"), wxGetElapsedTime()));
    return(result);
}


unsigned long ConvertLong(char *buf)
{//=================================
    unsigned long *p;
    unsigned char buf2[4];

    if(big_endian)
    {
        buf2[0] = buf[0];
        buf2[1] = buf[1];
        buf2[2] = buf[2];
        buf2[3] = buf[3];
    }
    else
    {
        buf2[0] = buf[3];
        buf2[1] = buf[2];
        buf2[2] = buf[1];
        buf2[3] = buf[0];
    }

    p = (unsigned long *)buf2;
    return(*p);
}


float ConvertFloat(char *buf)
{//==========================
    float *p;
    unsigned char buf2[4];

    if(big_endian)
    {
        buf2[0] = buf[0];
        buf2[1] = buf[1];
        buf2[2] = buf[2];
        buf2[3] = buf[3];
    }
    else
    {
        buf2[0] = buf[3];
        buf2[1] = buf[2];
        buf2[2] = buf[1];
        buf2[3] = buf[0];
    }

    p = (float *)buf2;
    return(*p);
}


int GetCEdata(int addr, int param)
{//===============================
    strcpy(SerialBuf, SerialBufSpaces);
    SerialBuf[0] = addr;
    SerialBuf[1] = opGetCE;
    SerialBuf[2] = param;
    SerialBuf[3] = 0;

    if(Communicate(1) <= 0)
    {
        return(-1);  // failed
    }

    return(ConvertLong(&SerialBuf[2]));
}


float GetDSPdata(int addr, int param)
{//==================================
    float value;
    int result;

    strcpy(SerialBuf, SerialBufSpaces);
    SerialBuf[0] = addr;
    SerialBuf[1] = opGetDSP;
    SerialBuf[2] = param;
    SerialBuf[3] = 0;

    if((result = Communicate(1)) <= 0)
    {
        return(-97999);  // big negative number indicates fail
    }

    value = ConvertFloat(&SerialBuf[2]);
    return(value);
}


typedef struct {
   int  value;
   const char *name;
} NAME_TABLE;

// eg. PVI-4.2-OUTD-UK-W
NAME_TABLE model_names[] = {
{'1', "PVI-3.0-OUTD"},
{'2', "PVI-3.3-OUTD"},
{'3', "PVI-3.6-OUTD"},
{'4', "PVI-4.2-OUTD"},
{'5', "PVI-5000-OUTD"},
{'6', "PVI-6000-OUTD"},
{'A', "PVI-CENTRAL-350"},
{'B', "PVI-CENTRAL-350"},
{'C', "PVI-MODULE-50"},
{'D', "PVI-12.5-OUTD"},
{'G', "UNO-2.5-I"},
{'H', "PVI-4.6-I-OUTD"},
{'I', "PVI-3600"},
{'L', "PVI-CENTRAL-350"},
{'M', "PVI-CENTRAL-250"},
{'O', "PVI-3600-OUTD"},
{'P', "3-PHASE-INTERFACE"},
{'T', "PVI-12.5-I-OUTD"},
{'U', "PVI-12.5-I-OUTD"},
{'V', "PVI-12.5-I-OUTD"},
{'X', "PVI-10.0-OUTD"},
{'Y', "PVI-TRIO-30-OUTD"},
{'Z', "PVI-12.5-I-OUTD"},
{'g', "UNO-2.0-I"},
{'h', "PVI-3.8-I"},
{'i', "PVI-2000"},
{'o', "PVI-2000-OUTD"},
{'t', "PVI-10.0-I-OUTD"},
{'u', "PVI-10.0-I-OUTD"},
{'v', "PVI-10.0-I-OUTD"},
{'w', "PVI-10.0-I-OUTD"},
{'y', "PVI-TRIO-20-OUTD"},
{'z', "PVI-10.0-I-OUTD"},
{-1, NULL}
};

NAME_TABLE standard_names[] = {
{'A', "US UL1741"},
{'B', "BE VDE Belgium Model"},
{'C', "CZ Czech Republic"},
{'E', "DE VDE0126"},
{'F', "FR VDE0126"},
{'G', "GR VDE Greece Model"},
{'H', "HU Hungary"},
{'I', "IT ENEL DK 5950"},
{'K', "AU AS 4777"},
{'O', "KR Korea"},
{'P', "PT Portugal"},
{'Q', "CN China"},
{'R', "IE EN50438"},
{'S', "ES DR 1663/2000"},
{'T', "TW Taiwan"},
{'U', "UK G83"},
{'W', "DE BDEW"},

{'a', "US UL1741"},
{'b', "US UL1741"},
{'c', "US UL1741"},
{'e', "   VDE AR-N-4105"},
{'k', "IL Isreal - Derived from AS"},
{'o', "   Corsica"},
{'u', "UK G59"},
{-1, NULL}
};


const char *LookupName(NAME_TABLE *t, int value)
{//=============================================
    while(t->name != NULL)
    {
        if(t->value == value)
            return(t->name);
        t++;
    }
    return(NULL);
}


int GetInverterInfo(int addr, FILE *f_inv)
{//=======================================
// -fgmnpv GetVerFW  GetMfgDate  GetConf  GetSN  GetPN  GetVer
    const char *pc, *pm, *ps, *pt, *pw;
    char firmware[4];
    char serial_number[7];
    char part_number[7];
    char model_name_buf[50];
    char standard_name_buf[50];
    char country_letters[4];
    static const char *unknown = "unknown";
    static char manufacture_week[3] = "  ";
    static char manufacture_year[3] = "  ";

    if(f_inv == NULL)
        return(-1);

    comms_error = 0;
    strcpy(SerialBuf, SerialBufSpaces);
    SerialBuf[0] = addr;
    SerialBuf[1] = 72;  // firmware release
    SerialBuf[2] = 0;
    if(Communicate(1) <= 0) return(-1);
    firmware[0] = SerialBuf[2];
    firmware[1] = SerialBuf[3];
    firmware[2] = SerialBuf[4];
    firmware[3] = SerialBuf[5];

    strcpy(SerialBuf, SerialBufSpaces);
    SerialBuf[0] = addr;
    SerialBuf[1] = 65;  // manufacturing week and year
    SerialBuf[2] = 0;
    if(Communicate(1) <= 0) return(-1);
    manufacture_week[0] = SerialBuf[2];
    manufacture_week[1] = SerialBuf[3];
    manufacture_year[0] = SerialBuf[4];
    manufacture_year[1] = SerialBuf[5];

    strcpy(SerialBuf, SerialBufSpaces);
    SerialBuf[0] = addr;
    SerialBuf[1] = 77;  // system configuration
    SerialBuf[2] = 0;
    if(Communicate(1) <= 0) return(-1);
    switch(SerialBuf[2])
    {
        case 0: pc = "System operating with both strings."; break;
        case 1: pc = "String 1 connected, String 2 disconnected."; break;
        case 2: pc = "String 2 connected, String 1 disconnected."; break;
        default: pc = unknown;
    }

    strcpy(SerialBuf, SerialBufSpaces);
    SerialBuf[0] = addr;
    SerialBuf[1] = 63;  // serial number
    SerialBuf[2] = 0;
    if(Communicate(0) <= 0) return(-1);
    strncpy(serial_number, SerialBuf, 6);
    serial_number[6] = 0;

    strcpy(SerialBuf, SerialBufSpaces);
    SerialBuf[0] = addr;
    SerialBuf[1] = 52;  // part number
    SerialBuf[2] = 0;
    if(Communicate(0) <= 0) return(-1);
    strncpy(part_number, SerialBuf, 6);
    part_number[6] = 0;

    strcpy(SerialBuf, SerialBufSpaces);
    SerialBuf[0] = addr;
    SerialBuf[1] = 58;  // inverter version
    SerialBuf[2] = 0;
    if(Communicate(1) <= 0) return(-1);

    pm = LookupName(model_names, SerialBuf[2] & 0xff);
    if(pm == NULL)
    {
        sprintf(model_name_buf, "Aurora ??? (%.2x)", SerialBuf[2] & 0xff);
        pm = model_name_buf;
    }

    ps = LookupName(standard_names, SerialBuf[3] & 0xff);
    if(ps == NULL)
    {
        sprintf(standard_name_buf, "   Standard ?? (%.2x)", SerialBuf[3] & 0xff);
        ps = standard_name_buf;
    }

    country_letters[0] = 0;
    if(ps[0] > ' ')
    {
        country_letters[0] = '-';
        country_letters[1] = ps[0];
        country_letters[2] = ps[1];
        country_letters[3] = 0;
    }

    switch(SerialBuf[4])
    {
        case 'T': pt = "Transformer"; break;
        case 'N': pt = "Transformerless"; break;
        default:  pt = unknown;
    }
    switch(SerialBuf[5])
    {
        case 'W': pw = "Wind"; break;
        case 'N': pw = "Photovoltaic"; break;
        default:  pw = unknown;
    }

    fprintf(f_inv, "\nPart Number:  %s\n\nSerial Number:  %s\n\n"
            "Firmware:  %c.%c.%c.%c\n\nManufacturing Date:  20%s Week %s\n\n"
            "Inverter Version:  -- %s%s --\n     -- %s, %s version --\n\nReference Standard:  %s\n\n%s\n",
            part_number, serial_number, firmware[0], firmware[1], firmware[2], firmware[3],
            manufacture_year, manufacture_week, pm, country_letters, pt, pw, &ps[3], pc);
    return(0);
}


int GetState(int addr)
{//===================
    strcpy(SerialBuf, SerialBufSpaces);
    SerialBuf[0] = addr;
    SerialBuf[1] = 50;  // state request
    SerialBuf[2] = 0;
    if(Communicate(0) <= 0) return(-1);
    memcpy(ir->state, &SerialBuf[1], 5);
    return(0);
}


#ifdef deleted
int GetTime(int addr)
{//==================
    time_t timeval = 0;
    int gmtoff = 0;
    struct tm tim;

    strcpy(SerialBuf, SerialBufSpaces);
    SerialBuf[0] = addr;
    SerialBuf[1] = 70;
    SerialBuf[2] = 0;

    if(Communicate() <= 0)
        return(0);

    if(SerialBuf[0] != STATE_OK)
        return(-1);

    timeval = time(NULL);

}
#endif



int GetAllDsp(int addr)
{//====================
    int ix;
    int dsp;
    FILE *f;
    const char *name;
    wxString fname = data_dir + _T("/system/read_all_dsp.txt");

    if((f = fopen(fname.mb_str(wxConvLocal),"w")) == NULL)
        return(-1);

    for(dsp=0; dsp<N_DSP; dsp++)
    {
        name = NULL;
        for(ix=1; ix<n_graphtypes; ix++)
        {
            if((dsp > 0) && (graph_types[ix].dsp_code == dsp))
            {
                name = graph_types[ix].name;
                break;
            }
        }

        for(ix=1; (name == NULL) && (extra_reading_types[ix].name != NULL); ix++)
        {
            if(extra_reading_types[ix].dsp_code == dsp)
                name = extra_reading_types[ix].name;
        }
        if(name == NULL)
            name = "";

        fprintf(f, "%2d  value %11f  %s\n", dsp, GetDSPdata(addr, dsp), name);
    }
    fclose(f);
    return(0);
}


int GetPowerIn(int addr)
{//=====================
    float p;
    float pw_in = 0;

    ir->vt[0] = GetDSPdata(addr, 1);
    if(comms_error != 0)
        return(-1);

    ir->vt[1] = GetDSPdata(addr, 23);
    ir->ct[1] = GetDSPdata(addr, 25);
    ir->pw[1] = ir->vt[1]*ir->ct[1];
    if(comms_error != 0)
        return(-1);

    ir->vt[2] = GetDSPdata(addr, 26);
    ir->ct[2] = GetDSPdata(addr, 27);
    ir->pw[2] = ir->vt[2]*ir->ct[2];
    if(comms_error != 0)
        return(-1);

    if((p = ir->vt[1] * ir->ct[1]) > 0)
        pw_in = p;
    if((p = ir->vt[2] * ir->ct[2]) > 0)
        pw_in += p;

    if(pw_in == 0)
        ir->effic = 0;
    else
        ir->effic = (ir->pw[0] * 100)/ pw_in;
    return(0);
}


int GetEnergy(int addr)
{//====================
    ir->energy[0] = (double)GetCEdata(addr, 0) / 1000.0;
    if(comms_error != 0)
        return(-1);

    ir->energy[1] = (double)GetCEdata(addr, 1) / 1000.0;
    if(comms_error != 0)
        return(-1);

    ir->energy[2] = (double)GetCEdata(addr, 3) / 1000.0;
    if(comms_error != 0)
        return(-1);

    ir->energy[3] = (double)GetCEdata(addr, 4) / 1000.0;
    if(comms_error != 0)
        return(-1);

    ir->energy[4] = (double)GetCEdata(addr, 5) / 1000.0;
    if(comms_error != 0)
        return(-1);

    ir->energy[5] = (double)GetCEdata(addr, 6) / 1000.0;
    if(comms_error != 0)
        return(-1);

    return(0);
}


int GetData(int addr, int type)
{//============================
    int ix;
    int dsp_param;

    comms_error = 0;
    ir->flags = 0;

    // always get the power output
    ir->pw[0] = GetDSPdata(addr, 3);
    if(comms_error != 0)
        return(-1);

    switch(type)
    {
    case 0:
        if(GetPowerIn(addr) < 0)
            break;
        ir->tempr[0] = GetDSPdata(addr, 21);  // Booster temperature (usually higher than Inverter temperature)
        GetState(addr);
        ir->flags = IR_HAS_POWERIN | IR_HAS_TEMPR | IR_HAS_STATE;
        break;

    case 1:
        if(GetPowerIn(addr) < 0)
            break;
        ir->tempr[1] = GetDSPdata(addr, 22);
        GetState(addr);
        ir->flags = IR_HAS_POWERIN | IR_HAS_TEMPR | IR_HAS_STATE;
        break;

    case 2:
        if(GetEnergy(addr) < 0)
            break;
        ir->tempr[0] = GetDSPdata(addr, 21);
        GetState(addr);
        ir->flags = IR_HAS_POWERIN | IR_HAS_TEMPR | IR_HAS_STATE;
        break;

    case 3:
        if(GetPowerIn(addr) < 0)
            break;
        ir->tempr[1] = GetDSPdata(addr, 22);
        GetState(addr);
        ir->flags = IR_HAS_POWERIN | IR_HAS_TEMPR | IR_HAS_STATE;
        break;

    case 4:
        if(GetPowerIn(addr) < 0)
            break;
        GetState(addr);
        ir->energy[0] = (double)GetCEdata(addr, 0) / 1000.0;  // today energy
        ir->flags = IR_HAS_POWERIN | IR_HAS_STATE | IR_HAS_ENERGY_TODAY;
        break;

    case 5:
        GetState(addr);
        ir->flags = IR_HAS_STATE;
        break;

    case cmdExtraDsp:  // get extra DSP data (more than 2 parameters)
        for(ix=0; ix < N_EXTRA_READINGS; ix++)
        {
            if((dsp_param = extra_readings[ix].dsp_code) != 0)
            {
                if((extra_readings[ix].status_slot != 0) || (extra_readings[ix].graph_slot != 0))
                    ir->dsp[dsp_param] = GetDSPdata(addr, dsp_param);
            }
        }
        ir->flags = IR_HAS_EXTRA;


        if(n_extra_readings < 7)
        {
            GetState(addr);
            ir->flags |= IR_HAS_STATE;
        }
        if(n_extra_readings < 6)
        {
            ir->tempr[0] = GetDSPdata(addr, 21);
            ir->flags |= IR_HAS_TEMPR;
        }
        if(n_extra_readings < 5)
        {
            ir->tempr[0] = GetDSPdata(addr, 22);
        }
        if(n_extra_readings < 4)
        {
            ir->energy[0] = (double)GetCEdata(addr, 0) / 1000.0;  // today energy
            ir->flags |= IR_HAS_ENERGY_TODAY;
        }
        break;
    }

    if(comms_error != 0)
    {
        ir->flags = 0;
        return(-1);
    }
    return(0);
}




void Mainframe::OnInverterEvent(wxCommandEvent &event)
{//===================================================
    int data_ok;
    int inv_info = -1;
    int period_type = 0;
    static int next_command[N_INV] = {0, 0};

    if(event.GetId() == idInverterData)
    {
        data_ok = 1;
        if(inverter_alive[command_inverter] == 0)
        {
            // start with a request which reads temperature (so we don't start with an unknown temperature value)
            next_command[0] = next_command[1] = 0;
        }
        inverter_alive[command_inverter] = 1;
    }
    else
    {
        data_ok = 0;
        inverter_alive[command_inverter] = 0;
    }

    if(command_type == cmdInverterInfo)
    {
        inv_info = command_inverter;
    }
    else
    {
        period_type = DataResponse(data_ok, &inverter_response[command_inverter]);
        if((n_extra_readings > 0) && (period_type > 0))
        {
            // get the extra dsp readings at thbe start of a 10 second period
            get_extra_dsp[command_inverter] = 1;
        }
    }

    DrawStatusPulse(command_inverter+1);   // set bit 0 or bit 1
    pulsetimer.Start(160, true);

    if(command_queue != 0)
    {
        // a special command has been requested
        SendCommand(command_queue & 0xf, command_queue >> 8);
        command_queue = 0;
    }
    else
    {
        int inv;
        // change to the next inverter
        inv = (command_inverter+1) % 2;
        if(inverter_address[inv] == 0)
            inv = command_inverter;

        if((inverter_alive[inv] == 0) || (inverter_response[inv].state[1] != INVERTER_STATE_RUN))
        {
            SendCommand(inv, 5);  // start with just alarm and power-out
        }
        else
        if(get_extra_dsp[inv] > 0)
        {
            SendCommand(inv, cmdExtraDsp);
            get_extra_dsp[inv] = 0;
        }
        else
        {
            if(next_command[inv] >= 5)
                next_command[inv] = 0;
            SendCommand(inv, next_command[inv]);
            next_command[inv]++;
        }
    }

    if(inv_info >= 0)
    {
        // do this after sending the pulse and the next command, so the modal dialog doesn't pause them
        GotInverterInfo(inv_info, cmdoutput);
    }
}  // end of OnInverterEvent



void SendCommand(int inv, int type)
{//=================================
    wxThreadError error;

    command_type = type;
    command_inverter = inv;
//return;  // TESTING
//wxLogStatus(wxString::Format(_T("SendCommand %d  %d  live=%d"), inv, type, inverter_alive[0]));
    if(serial_port_error != 0)
    {
        if(serial_port_error == -1)
        {
            wxLogStatus(_T("Failed to open serial port: ") + serial_port);
        }
    }

    inverter_thread = new InverterThread();
    error = inverter_thread->Create();
    if(error != wxTHREAD_NO_ERROR)
    {
        wxLogError(_T("Failed to create inverter thread"));
    }
    error = inverter_thread->Run();
    if(error != wxTHREAD_NO_ERROR)
    {
        wxLogError(_T("Failed to run inverter thread"));
    }

}


void *InverterThread::Entry()
{//==========================
    int addr;
    int type;
    int result;
    FILE *f;

    result = SerialOpen();
    serial_port_error = result;

    if(result != 0)
    {
        // pause so that we don't run continuously trying to open the port
        Sleep(1000);   // thread sleep
    }
    else
    {
        comms_inverter = command_inverter;
        addr = inverter_address[comms_inverter];
        type = command_type;

        ir = &inverter_response[comms_inverter];
        ir->flags = 0;
        if(type == cmdInverterInfo)
        {
            // write information to a temporary file
            result = -1;
            f = fopen(cmdoutput.mb_str(wxConvLocal), "w");
            if(f != NULL)
            {
                result = GetInverterInfo(addr, f);
                fclose(f);
            }
        }
        else
        {
            result = GetData(addr, type);
        }
        SerialClose();
    }

    if(result == 0)
    {
        inverter_fails[comms_inverter] = 0;
        my_event.SetId(idInverterData);
    }
    else
    {
        my_event.SetId(idInverterFail);
        inverter_fails[comms_inverter]++;
    }

    wxPostEvent(mainframe->GetEventHandler(), my_event);
    return(NULL);
}

void InitComms()
{//=============

    memset(inverter_response, 0, sizeof(inverter_response));
    inverter_response[0].state[0] = 0xff;
    inverter_response[1].state[0] = 0xff;
    cmdoutput = wxFileName::CreateTempFileName(_T("aurora"));

}
