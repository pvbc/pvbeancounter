// Xantrex PV inverter monitor
//
// version 0.2 - rather arbitrary but it's the second stable version of the code
//
// Copyright (C) 2011 John Lindsay
// http://jslblog.com
// @bigjsl
//
// uses Georg Kaindl's DHCP and DNS code
// 
// This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.
//
// This means you can use it for you own purposes but not sell the end product without a commercial license from me.
//
// If you make some changes to the code you must share them.
//
// If this doesn't suit your needs then go write your own code using the libraries.  It isn't hard.
//
// This code works under Arduino 0018 but not under later versions
// This is because later versions change the Ethernet library in a way that breaks the DHCP code
// At some point, someone will fix these libraries.  That someone may well be YOU!
//
// Go to PVaddStatus and put your API key and SID in where indicated
//
// This needs to be:
// const char APIkey[] = "xxxx";
// const char SID[] = "xxx";
//
// and then added to the sprintf statement in PVaddStatus, but I haven't tested that so it's not in this release
//

#include <Ethernet.h>           // The all important built-in Ethernet library

#include <EthernetDHCP.h>       // This is special and makes life much easier
#include <EthernetDNS.h>        // The DNS library isn't being used yet but I'm loading it because it's BIG
                                // If you want to use the Twitter library you need a special version of this
                                // http://gkaindl.com/software/arduino-ethernet
                                // there is also a Bonjour library there which would be nifty in some future version
                                // but DHCP and DNS use a LOT of memory so there might not be space
                                // there is less than 8K of 32K left as of this version

#include <NTP.h>                // NTP library 
                                // it depends on additional modules in the main Ethernet library for UDP sadly
                                // https://github.com/cynshard/arduino-ntp

#include <Time.h>               // very handy unix style time library that lets us talk minutes() and hours()
                                // http://www.arduino.cc/playground/Code/Time

#include <Wire.h>               // if you use the I2C LCD you need these libraries
                                // Wire is built in

#include <LiquidCrystal_I2C.h>  // The is used to show_status. The code works quite happily without it.
                                // You will need a different library if you use a different LCD
                                // This library is written up here:
                                // http://www.dfrobot.com/wiki/index.php?title=I2C/TWI_LCD1602_Module_(SKU:_DFR0063)
                                // library downloaded from here:
                                // http://www.dfrobot.com/image/data/DFR0063/Arduino_library.zip

// #include <NewSoftSerial.h>   // you're a sad disappointment NewSoftSerial...... you mangle received characters at 9600

byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };  // if you have two units change a byte because every one must be unique
                                                      // this is a traditional geek joke DEADBEEFFEED

const char* ip_to_str(const uint8_t*);  // ooh, declaring a function in advance, how nice

LiquidCrystal_I2C lcd(0x27,16,2);  // set the LCD address to 0x27 for a 16 chars and 2 line display
                                   // this LCD interface uses I2C which uses two I/O lines and supports other devices
                                   // this may be useful later when I/O is running out

enum NTPStatus {NO, YES};          // lame - I should use a boolean but I was LEARNING at the time
NTPStatus NTPSetup = NO;

//byte NTPaddr[4] = { 0, 0, 0, 0 };  // this will be set later using DNS
byte NTPaddr[4] = { 192, 231, 203, 132 };  // ntp.on.net - you might use pool.ntp.org if not a Node customer

NTP ntp(NTPaddr);                 // get lined up to set the clock with NTP

// time_t prevDisplay = 0; // when the digital clock was displayed  - not used at present

// timeZoneOffset needs some smarts - need to find a simple way to set timezone and DST

const  long timeZoneOffset = 37800L; // set this to the offset in seconds to your local time - 34200 non CDST
                                     // need to work out how to shift this back and forth - I know the date....

byte PVserver[] = { 173, 230, 151, 11 }; // PVoutput.org - need to do this with DNS too
                                         // found some nice prototype code in the Twitter library!

Client PVclient(PVserver, 80);    // 

// system wide variables for upload values

unsigned long starting_wh = 0UL;
long exported_wh = 0L;
long exported_peak = 0L;
long imported_wh = 0L;
long imported_peak = 0L;

// scheduler stuff

static unsigned long PV_add_WaitMillis = 0UL;

void setup()
{
  // Initiate a DHCP session. The argument is the MAC (hardware) address that
  // you want your Ethernet shield to use. The second argument enables polling
  // mode
  EthernetDHCP.begin(mac, 1);
  
  lcd.init();                      // initialize the lcd 
  lcd.backlight();
  
  show_status("PV Monitor v0.2","by @bigjsl");
      
  Serial.begin(9600);  // using the normal serial interface which means RS232 must be disabled while programming
  // either disconnect the 5 volt power to the interface or disconnect Rx

}

void loop()
{
  // for DHCP
  static DhcpState prevState = DhcpStateNone;
  static unsigned long prevTime = 0;
  // for DNS
  char hostName[512];
  int length = 0;

  // for MainLoop

  enum States { LoopStateNone, NTP, XANTREX };   // not in use
  static States PrevLoopState = LoopStateNone;   // not in use

  // for temperature
  // for watts
  // for xantrex
  
  // poll() queries the DHCP library for its current state

  DhcpState state = EthernetDHCP.poll();

  if (prevState != state) {
    switch (state) {
      case DhcpStateDiscovering:
        show_status("DHCP Status:","Discovering");
        break;
      case DhcpStateRequesting:
        show_status("DHCP Status:","Requesting lease.");
        break;
      case DhcpStateRenewing:
        show_status("DHCP Status:","Renewing lease.");
        break;
      case DhcpStateLeased: {
        show_status("DHCP Status:","Obtained lease!");

        // Since we're here, it means that we now have a DHCP lease, so we
        // print out some information.
        const byte* MyipAddr = EthernetDHCP.ipAddress();  // Ethernet library makes these type const. Why?!?
        const byte* gatewayAddr = EthernetDHCP.gatewayIpAddress();
        const byte* dnsAddr = EthernetDHCP.dnsIpAddress();

        delay(1000);
        show_status("Gateway IP:",ip_to_str(gatewayAddr));
        delay(1000);
        show_status("DNS IP:",ip_to_str(dnsAddr));
        delay(1000);
        show_status("My IP:", ip_to_str(MyipAddr));
        
        EthernetDNS.setDNSServer(dnsAddr); // from DHCP example

        break;
      }
    }
  } else if (state != DhcpStateLeased && millis() - prevTime > 300) {
     prevTime = millis();
  }

  if ((prevState == state) && (state == DhcpStateLeased)) {

     // DNS lookup for NTP - need to pick a server ANYONE can use
     // if it fails, loop around and try again
     // pool.ntp.org returns a list of servers in (random, maybe) order
     // I fear this may require more work to get right because the DNS library is fairly simple

//     if (Set_DNS("pool.ntp.org",NTPaddr) && (NTPSetup == NO)) {
//     if (Set_DNS("ntp.on.net",NTPaddr) && (NTPSetup == NO)) {
       if (NTPSetup == NO) {
       show_status("Status:","Syncing clock");
       unsigned long time = ntp.get_unix_gmt();
       setSyncProvider(getNtpTime);
       while(timeStatus()== timeNotSet){
         show_status("Status:","Waiting NTP");
       } // wait until the time is set by the sync provider
     NTPSetup = YES;
     show_status("Status:","time set");

     get_xantrex_stats();  // do this now so we don't have to wait during debugging
     int tries = 5;
     while ( !PVaddStatus() && (tries > 0) ) { tries--; }
     // otherwise I have to wait for up to 10 minutes which sux

     }

    // do some measurements
    if( now() % 10 == 5) //update every 10 seconds
    {
      digitalClockDisplay();  
      delay(2000);
      get_xantrex_stats();  // not actually used by this code yet
      show_status("Mains Volts:",get_xantrex("VIN?"));
      delay(2000);
      show_status("PV KWh today:",get_xantrex("KWHTODAY?"));
      delay(2000);
      show_status("PV Wh lifetime:",get_xantrex("WHLIFE?"));
      delay(2000);
      show_status("PV Watts:",get_xantrex("POUT?"));
      delay(2000);
    }
    
    // send the data places

    // after 5am and before 10pm
    if( ( (long)( millis() - PV_add_WaitMillis ) >= 0) && 
        ( (exported_peak > 0L) || (imported_peak > 0L) ) && 
        (minute() % 10 == 0) && (hour() >= 5) && (hour() <= 22))
      {
        // millis is now later than my 'next' time
        //    { do something }
        get_xantrex_stats();
        int tries = 5;
        while ( !PVaddStatus() && (tries > 0) ) { tries--; }
        PV_add_WaitMillis = (long)( millis() + 540000);  // do it again 10 minutes later - schedule for 9
      }
      
    // tweet here!

// Turning off the backlight makes my LCD unreadable :-(
//
//    if( (hour() >= 8) && (hour() <= 18)) { // low-tech but effective - backlight on from 6pm until 8am
//      lcd.noBacklight();
//    } else {
//      lcd.backlight();
//    }

  }  
  prevState = state;
} // end of loop()

void digitalClockDisplay(){
  // digital clock display of the time
  static char buf1[16];
//  static char buf2[16];
//  sprintf(buf1, "%02d:%02d:%02d\0", hour(), minute(), second());
  sprintf(buf1, "%02d:%02d %02d/%02d/%d\0", hour(), minute(), day(), month(), year());
  show_status("PV Monitor",buf1);
}

unsigned long getNtpTime()
{
  return (ntp.get_unix_gmt()+timeZoneOffset);
}

// Just a utility function to nicely format an IP address.
const char* ip_to_str(const uint8_t* ipAddr)
{
  static char buf[16];
  sprintf(buf, "%d.%d.%d.%d\0", ipAddr[0], ipAddr[1], ipAddr[2], ipAddr[3]);
  return buf;
}

// emit status to LCD
void show_status(const char* heading, const char* status)
{
  lcd.clear();
  lcd.print(heading);
  lcd.setCursor(0,1);
  lcd.print(status);
}

// emit status to LCD
void new_show_status(const char* heading, const char* status)
{
  char l1[16] = "               ";
  char l2[16] = "               ";
  for(int i=0;i<16;i++) {   // breaks if heading is short....
    l1[i] = heading[i];
    l2[i] = status[i];
  }
  lcd.clear();
  lcd.print(l1);
  lcd.setCursor(0,1);
  lcd.print(l2);
}

char* get_xantrex(char* Command) {
  byte receive_char = 0;
  static char received[64];
  int count = 0;
  unsigned long maxtime = 0;
  
//  show_status("Polling","Sending");
  
  for(int i=0;i<63;i++) {
    received[i] = ' ';
  }
  
  Serial.flush();
  Serial.println(Command);
  delay(2);
  maxtime = millis() + 1000UL;
  do {
//    show_status("Polling","Reading");
    if (Serial.available()) {
      receive_char = Serial.read();
      received[count++] = char(receive_char);
    }
  } while (receive_char != 13 && millis() < maxtime);

  if (count > 0) {
//    show_status("Polling","Got something");
//    delay(1000);
    if (receive_char == 13) { count--; }
    received[count] = 0;
//    show_status("Rec:",received);
//    delay(1000);
    return received;
  }
  else {
//    show_status("Polling Timeout",Command);
    show_status("Polling Timeout",Command);
    return "0";
  }
}

void get_xantrex_stats(void) {
  char* rec_string;
  unsigned long t_exported_wh = 0L;
  long t_exported_peak = 0L;
  long t_imported_wh = 0L;
  long t_imported_peak = 0L;
  float t_f = 0.0;

  show_status("PV Monitor","Polling inverter");

  rec_string = get_xantrex("POUT?");
  
//  show_status("back from","POUT");
  
  t_f = atof(rec_string)*1.0;
  t_exported_peak = long(t_f);
  
  if (t_exported_peak > exported_peak) {
    exported_peak = t_exported_peak;
  }

  rec_string = get_xantrex("WHLIFE?");

//  show_status("back from","WHLIFE");

  t_exported_wh = strtoul(rec_string,NULL,10);
  
  // just check if we have just booted here then return to processing WHLIFE
  
  if (starting_wh == 0UL) { // we must have just booted!
  
    rec_string = get_xantrex("KWHTODAY?");
    
//  show_status("back from","KHWTODAY");
        
    t_f = atof(rec_string)*1000.0;
    starting_wh = t_exported_wh - (unsigned long)(t_f);
  }
  
  if (t_exported_wh > exported_wh) {
    exported_wh = long(t_exported_wh - starting_wh);  // daily today since last inverter standby
  }

  rec_string = get_xantrex("RELAY?");

//  show_status("back from","RELAY");
  
  if (strcmp(rec_string, "OFF") == 0) {  // it's OFF
    if (hour() < 5) {  // only do this in the morning before sun or after booting
      starting_wh = t_exported_wh;
      exported_peak = 0L;
      exported_wh = 0L;
      show_status("Inverter relay Open","Stats zeroed");
      delay(1000);
    } else {
      show_status("Inverter relay Open","Stats NOT zeroed");
      delay(1000);
    }
  }
}

boolean PVaddStatus(void) {
  char status_url[250];
  char in_buf[17];
  int in_count = 0;
  char last_rec = ' ';
  char rec = ' ';
  boolean start_line = true;

  show_status("PVoutput add","started");

  // set the API key and SID here as per example            

  // sprintf(status_url, "GET /service/r1/addstatus.jsp?key=12345d929a043a91924904732176d2c6fda0c7bb&sid=999&d=%d%02d%02d&t=%02d:%02d&v1=%05ld&v2=%04ld&v3=%06ld&v4=%05ld HTTP/1.0\0", year(), month(), day(), hour(), minute(), exported_wh, exported_peak, imported_wh, imported_peak);

  sprintf(status_url, "GET /service/r1/addstatus.jsp?key=XXX&sid=XXX&d=%d%02d%02d&t=%02d:%02d&v1=%05ld&v2=%04ld&v3=%06ld&v4=%05ld HTTP/1.0\0", year(), month(), day(), hour(), minute(), exported_wh, exported_peak, imported_wh, imported_peak);

  // DNS lookup here in case the IP address has changed  

//  if (Set_DNS("pvoutput.org",PVserver) && PVclient.connect()) {
  if (PVclient.connect()) {
 
    show_status("PVoutput add","connected");
        
    PVclient.println(status_url);  // "GET /search?q=arduino HTTP/1.0"
    PVclient.println();

    // check the response and display it

    unsigned long max_wait = millis() + 3000UL;
    
    show_status("PVoutput add","waiting");
    
    while ((millis() < max_wait) && PVclient.connected() && !PVclient.available()) {  // wait for a response if any
      delay(10);
    }

 //   delay(2000);  // give the site some time to respond
    
    if (PVclient.available()) {
      show_status("PVoutput add","responded");
      
      while (PVclient.available()) {
        for(int i=0;i<16;i++) {
          in_buf[i] = 32; // pad it out with spaces
        }
        for(int i=0;i<16;i++) {
          if (!PVclient.available()) { // we're done here
            break;
          }
          in_buf[i] = PVclient.read();
          if (in_buf[i] == 10) {
            in_buf[i] = 32; // blank out the LF
            if (PVclient.available())  {  // just grab the next character
              in_buf[i] = PVclient.read();
            }
          }
          if (in_buf[i] == 13) {
            in_buf[i] = 32;
            break;
          }
        }
        in_buf[16] = 0;
        show_status("received",in_buf);
        delay(500);
      }   
      exported_peak = 0L; // set back to zero because it's peak per period
      imported_peak = 0L; // set back to zero because it's peak per period
      PVclient.stop();
      return true;
    } else {
      show_status("PVoutput add","no response");
      PVclient.stop();
      return false;
    }
  } else {
    show_status("PVoutput add","connection failed");
    PVclient.stop();
    return false;
  }
}

boolean Set_DNS(char* hostName, byte *ipaddr[4]) {
  
  return true; // not working yet :-(  // need to pull apart twitter library for inspiration
  
  char buff[64];
  
//  char hostName[] = "pvoutput.org";
  
  show_status("DNS resolving",hostName);
  delay(1000);
    
  // Resolve the host name and block until a result has been obtained.
//  DNSError err = EthernetDNS.resolveHostName(hostName, PVserver);
  DNSError err = EthernetDNS.resolveHostName("ntp.on.net", *ipaddr);
  
  // Finally, we have a result. We're just handling the most common errors
  // here (success, timed out, not found) and just print others as an
  // integer. A full listing of possible errors codes is available in
  // EthernetDNS.h
  if (DNSSuccess == err) {
    return true;
  } else if (DNSTimedOut == err) {
    show_status("DNS error","Timed out");
    delay(5000);
    return false;
  } else if (DNSNotFound == err) {
    show_status("DNS error","Does not exist.");
    delay(5000);
    return false;
  } else {
    itoa((int)err,buff,10);
    show_status("DNS error",buff);  // some code to finish nicely - need a buffer
    delay(5000);
    return false;
    //    Serial.print((int)err, DEC);
  }
}
