KLNE-Modbus protocol
A communication parcel example
The MODBUS Protocol RTU mode 9600bps, 8 data bits, 1 stop bit, no parity bit passed.
All communication to the "package", the master station to send the parcel known as the request, called the response from the station to send parcels. Each MODBUS
Parcel by the address domain, function code domain, data domain, verify domain four-part composition.
􀀹 address field length of 6 bytes of BCD code.
􀀹 function code field length of 1 byte to inform what to do from the station should function code used is as follows.
Function code meaning function
03 Read Holding Registers parameter query
04 Reading the input register to read the real-time data
05 clear the total power, total time to write a single coil
16 Write Multiple Registers parameter modification
􀀹 data domain. Length uncertainty, "BIG INDIAN" mode, ie, big endian low byte in the post. Should pay attention to
Is: each read or set in the development Sunteams 1500 5000 inverter product communication software, data frame bytes
Should not exceed 60 bytes, once issued a register number should not be greater than 24.
􀀹 calibration domain. 16 of the CRC, low in front, high in the CRC checksum code attached for reference.
1.1 to read holding registers (function code 03)
Request response
Domain sample (hex) Domain Name Example (Hex)
Mailing Address 123 456 789 012 (BCD code) Mailing Address 123 456 789 012 (BCD code)
Function Code 03 function code 03
Start address high 01-byte count of 02
The starting address of the low 30 high 01 of the register values
High of 00 registers of the register number of the value of the low 03
Register the number of low 01 CRC checksum -
CRC checksum ------
1.2 Reading input registers (function code 04)
Request response
© <Beijing Quinlan New Energy Technology Co., Ltd. R & D Department, 2011 Page 2 of 7
Domain sample (hex) Domain Name Example (Hex)
Mailing Address 123 456 789 012 (BCD code) Mailing Address 123 456 789 012 (BCD code)
Function Code 04 function code 04
Start address high 00-byte count of 02
The starting address of low 70 high input register values ​​02
Enter the register number of high input register values ​​00 low of 03
Enter the register number of the low 01 CRC checksum -
CRC checksum ------
1.3 write a single coil (function code 05)
Request response
Domain sample (hex) Domain Name Example (Hex)
Mailing Address 123 456 789 012 (BCD code) Mailing Address 123 456 789 012 (BCD code)
Function Code 05 function code 05
Output address high 01 output address high of 01
Output address low 00 output address low of 00
High output value of FF high output value of FF
Output value output value in the low 00 low 00
CRC checksum - CRC checksum -
1.4 Write Multiple Registers (function code 16)
Request response
Domain sample (hex) Domain Name Example (Hex)
Mailing Address 123 456 789 012 (BCD code) Mailing Address 123 456 789 012 (BCD code)
Function Code 10 Function Code 10
Start address high 01 high 01 of the start address
The starting address of low 30 the starting address of low 30
Register the number of high 00 register the number of high 00
Register 02 registers the number of low number of low 02
The byte count 04 CRC checksum -
Register values ​​high 00 ----
Register value of the low 05 ----
Register values ​​high 00 ----
Register value of the low 73 ----
CRC checksum ------
© <Quinlan, New Energy Technology Co., Ltd. Product Research and Development Department, 2011 Page 3 of 7
1.5 abnormal response
Request response
Domain sample (hex) Domain Name Example (Hex)
Mailing Address 123 456 789 012 (BCD code) Mailing Address 123 456 789 012 (BCD code)
Function Code 06 Function Code 86
Start address high 10 exception code 02
The starting address of low 02 CRC checksum -
Register data 21 ----
Register data 03 ----
CRC checksum ------
Exception code = 2 data address is invalid = 3 data value is invalid, = 1 the function code is invalid.
2 Register Definition
2.1 logic coil register
Register number attribute definition Remarks
00001 RW, clear electrical degrees, the total time 1 -
00 002 RW, start automatically test 1 - Effective
00003 RW, empty
00004 RW, empty
00 005 RW, empty
00006 RW, empty
00 007 RW, empty
00 008 RW, empty
2.2 switch status register
Fault registers:
Register number attribute definition Remarks
10001 RO 2.5V Ref. The Fault Bit0
10002 Damaged of Bit1 of the RO SCI1
10003 the RO Not Consistent. Bit 2
10004 the RO standby
10005 the RO standby
10 006 the RO standby
10 007 the RO standby
© <Beijing Quinlan New Energy Technology Co., Ltd. R & D Department, 2011 Page 4 of 7
10 008 the RO standby
10009 the RO standby
10010 the RO standby
10 011 the RO standby
10012 the RO standby
10013 the RO
Alternate
10 014 the RO standby
10015 RO Auto test failed Bit14
10016 Bit15 the RO Please Initial
10017 RO Relay Damaged Bit16
10018 RO EEPROM Damaged Bit17
10019 RO GFCI Damaged Bit18
10020 RO Sensor Damaged Bit19
10021 RO SCI2 Damaged Bit20
10022 RO Disconnect Grid Bit21
10023 RO Low Isolation Bit22
10024 RO High Ground I Bit23
10025 RO High DC INJ Bit24
10 026 the RO High PV Voltage Bit25
10027 RO High Bus Voltage Bit26
10028 RO Grid V Fault Bit27
10029 RO Grid F Fault Bit28
10030 RO High Temperature Bit29
10031 the RO standby
10032 the RO standby
2.3 Real-time data register
Register number attribute defines the smallest unit of Remarks
39 991 the RO sequence No. 1 high word of the BCD byte
39 992 RO serial number of the middle of words the BCD bytes
39 993 the RO sequence No. 1 low word of the BCD byte
39994 the RO to retain
39995 the RO to retain
39996 the RO to retain
39997 the RO to retain
39998 the RO to retain
© <Quinlan New Energy Technology Co., Ltd. R & D department>, 2011 Page 5 of 7
39999 the RO to retain
40 000 the RO to retain
40 001 RO DC voltage of a 0.1 Unit: V
40 002 RO DC voltage 2 0.1: V
40 003 RO DC current of a 0.1:
40004 RO DC current of 0.1:
40005 RO AC voltage of a 0.1 Unit: V
40006 RO AC voltage 2 0.1: V
40 007 RO AC voltage 3 0.1: V
40 008 RO AC current of a 0.1:
40009 RO AC current 0.1: A
40.01 thousand RO AC current 0.1: A
40 011 RO AC frequency of 0.01 units: Hz
40012 RO AC power (high) 0.1 unit: W
40013 the RO AC power (low)
40 014 the RO that day - 0.1 units: kWh
40015 RO total electrical degrees (high word) 0.1: kWh
40016 RO total electrical degrees (low word)
40 017 the RO day running time 1 unit: min
40018 the RO the day run time (low)
40 019 total running time of the RO (high word) units: h
40 020 the RO total running time (low word)
40021 the RO temperature of 0.1 units: ° C
40 022 the RO current zero drift 11:
40023 the RO current zero drift 21:
40024 the RO current zero drift 31:
40025 RO DCI1 1 (signed) unit: mA
40026 RO DCI2 1 (signed) unit: mA
40027 RO DCI3 1 (signed) unit: mA
40028 RO logical coil state a specific meaning to refer to the 2.1 logic coil state
40029 RO switch status 11 specific meaning of the reference to the status of the 2.2 switch
40.03 thousand RO switch status 21 specific meaning with reference to 2.2 switch status
40031 RO inverter state 1 0 - wait state; 1 - inverter status;
2 - alarm status; 3 - fault condition;
4 - Download the state;
© <Quinlan, New Energy Technology Co., Ltd. Product Research and Development Department, 2011 Page 6 of 7
Attachment: CRC checksum code
/ * CRC value of the high byte table * /
const unsigned char auchCRCHi [] = {
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40, 0x00, 0xC1, 0x81, 0x40,
0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0, 0x80, 0x41, 0x00, 0xC1,
0x81, 0x40, 0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41,
0x00, 0xC1, 0x81, 0x40, 0x01, 0xC0, 0x80, 0x41, 0x01, 0xC0,
0x80, 0x41, 0x00, 0xC1, 0x81, 0x40
;}
/ * Low byte of the CRC value table * /
const unsigned char auchCRCLo [] = {
0x00, 0xC0, 0xC1, 0x01, 0xC3, 0x03, 0x02, 0xC2, 0xC6 and, 0x06,
0x07, 0xC7, 0x05, 0xC5, 0xC4, 0x04, 0xCC, 0x0C, 0x0D, 0xCD
0x0F, 0xCF, 0xCE, 0x0E, 0x0A, 0xCA, 0xCB, a mix of 0x0B, 0xC9, 0x09,
0x08, 0xC8, 0xD8, 0x18, 0x19, 0xD9, 0x1B, 0xDB, 0xDA, 0x1A,
0x1E, 0xDE, 0xDF, 0x1F, 0xDD, 0x1D, 0x1C, 0xDC, 0x14, 0xD4,
0xD5, 0x15, 0xD7, 0x17, 0x16, 0xD6, 0xD2, 0x12, 0x13, 0xD3,
0x11, 0xD1, 0xD0, 0x10, 0xF0, 0x30, 0x31, 0xF1, 0x33, 0xF3,
0xF2, 0x32, 0x36, 0xF6, 0xF7, 0x37, 0xF5, 0x35, 0x34, 0xF4,
0x3C, 0xFC, a 0xFD, 0x3D, 0xFF, 0x3F, 0x3E, 0xFE, 0xFA, 0x3A
0x3B, 0xFB, 0x39, 0xF9, 0xF8, 0x38, 0x28, 0xE8, 0xE9, 0x29,
0xEB, 0x2B, 0x2A, 0xEA 0xEE, 0x2E, 0x2F, 0xEF, 0x2D, ​​0xED
0xEC, 0x2C, 0xE4, 0x24, 0x25, 0xE5, 0x27, 0xE7, 0xE6, 0x26,
0x22, 0xE2, 0xE3, 0x23, 0xE1, 0x21, 0x20, value of 0xE0, 0xA0, 0x60,
© <Quinlan, New Energy Technology Co., Ltd. Product Research and Development Department, 2011 Page 7 of 7
0x61, 0xA1, 0x63, 0xA3, 0xA2, 0x62, 0x66, 0xA6, 0xA7, 0x67,
0xA5, 0x65, 0x64, 0xA4, 0x6C, 0xAC, 0xAD, 0x6D, 0xAF, 0x6F,
0x6E, 0xAE, 0xAA, 0x6A, 0x6B, 0xAB, 0x69, 0xA9, 0xA8, 0x68,
0x78, 0xB8, 0xB9, 0x79, 0xBB, 0x7B, 0x7A, 0xBA, 0xBE, 0x7E,
0x7F, 0xBF, 0x7D, 0xBD, 0xBC, 0x7C, 0xB4, 0x74, 0x75, 0xB5,
0x77, 0xB7, 0xB6, 0x76, 0x72, 0xB2, 0xB3, 0x73, 0xB1, 0x71,
0x70, 0xB0, 0x50, 0x90, 0x91, 0x51, 0x93, 0x53, 0x52, 0x92,
0x96, 0x56, 0x57, 0x97, 0x55, 0x95, 0x94, 0x54, 0x9C, 0x5C,
0x5D, 0x9D, 0x5F, 0x9F, 0x9E, 0x5E, 0x5A, 0x9A, 0x9B, 0x5B,
0x99, 0x59 An, 0x58, 0x98, 0x88, 0x48, 0x49, 0x89, 0x4B, 0x8B,
0x8A, 0x4A, 0x4E, 0x8E, 0x8F, 0x4F, 0x8D, 0x4D, 0x4C, 0x8C,
0x44, 0x84, 0x85, 0x45, 0x87, 0x47, 0x46, 0x86, 0x82, 0x42,
0x43, 0x83, 0x41, 0x81, 0x80, 0x40
;}
unsigned int crc16 (unsigned char * puchMsg, int usDataLen)
{
unsigned char uchCRCHi = 0xFF; / * high CRC byte initialization * /
unsigned char uchCRCLo = 0xFF; / * low CRC byte initialization * /
unsigned int uIndex; / * CRC loop index * /
while (usDataLen -) / * transmit message buffer * /
{
uIndex = uchCRCHi ^ * puchMsg + +; / * calculate the CRC * /
uchCRCHi = uchCRCLo ^ auchCRCHi [uIndex];
uchCRCLo = auchCRCLo [uIndex];
}
return (((unsigned int) uchCRCLo) << 8 | uchCRCHi); / * put the crc 8 results Hi, Lo, put
8 high * /
}
