﻿/*
* Copyright (c) 2010 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ServiceProcess;
using PVSettings;
using TimeControl;
using PVBCInterfaces;
using MackayFisher.Utilities;

namespace PVBeanCounter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        ManageService ManageService; 
        ApplicationSettings ApplicationSettings;
        SystemServices SystemServices;
        CheckEnvironment CheckEnvironment;
        DeviceDisplay DeviceUpdate;
        OwlDatabaseInfo OwlDatabaseInfo;

        bool saveRequired = false;
        bool checkRequired = false;

        PvOutputSiteSettings PVSettings = null;
        DeviceManagerSettings DeviceManagerSettings = null;
        DeviceManagerDeviceSettings DeviceManagerDeviceSettings = null;

        void SettingsChangedCallback()
        {
            butSavePVSettings.IsEnabled = true;
            comboBoxStandardDB.Text = "Custom";
        }

        void SettingsSavedCallback()
        {
            butSavePVSettings.IsEnabled = false;
            ManageService.ForceRefresh = saveRequired;
            saveRequired = false;
        }

        void SettingsSaved()
        {
            butSavePVSettings.IsEnabled = saveRequired;
        }   

        private void SyncServiceStartup()
        {
            try
            {
                ServiceManager PVBCService;
                ServiceManager PublisherService;
                PVBCService = new ServiceManager("PVService");
                PublisherService = new ServiceManager("PVPublisherService");

                PVBCService.SyncServiceStartup(ApplicationSettings.AutoStartPVBCService, ApplicationSettings.AutoRestartPVBCService);
                PublisherService.SyncServiceStartup(ApplicationSettings.AutoStartPVBCService && ApplicationSettings.EmitEvents, false);
            }
            catch (Exception)
            {
            }
        }

        public MainWindow()
        {
            InitializeComponent();

            // Thread.Sleep(30000);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                ManageService = new ManageService();                
                ApplicationSettings = new PVSettings.ApplicationSettings();
                GlobalSettings.ApplicationSettings = ApplicationSettings;
                SystemServices = new MackayFisher.Utilities.SystemServices("Configuration.log");
                //SystemServices = new SystemServices();
            
                CheckEnvironment = new CheckEnvironment(ApplicationSettings, SystemServices);

                ApplicationSettings.SetSystemServices(SystemServices);

                OwlDatabaseInfo = null;

                gridPVSettings.DataContext = ApplicationSettings;

                comboBoxMMPortName.ItemsSource = SerialPortSettings.SerialPortsList;
                comboBoxMMBaudRate.ItemsSource = SerialPortSettings.BaudRateList;
                comboBoxMMParity.ItemsSource = SerialPortSettings.ParityList;
                comboBoxMMDataBits.ItemsSource = SerialPortSettings.DataBitsList;
                comboBoxMMStopBits.ItemsSource = SerialPortSettings.StopBitsList;
                comboBoxMMHandshake.ItemsSource = SerialPortSettings.HandshakeList;

                DeviceUpdate = new DeviceDisplay(ApplicationSettings, SystemServices);

                dataGridInverterList.ItemsSource = DeviceUpdate.DeviceList;
                //applianceConsumeSiteIdColumn.ItemsSource = ApplicationSettings.PvOutputSiteList;
                //comboBoxMAConsumeSystem.ItemsSource = ApplicationSettings.PvOutputSiteList;

                ManageService.StartMonitorService(ServiceStatusCallback);
                Closing += new System.ComponentModel.CancelEventHandler(OnClosing);

                ApplicationSettings.SetNotifications(SettingsChangedCallback, SettingsSavedCallback);

                buttonDeleteSite.IsEnabled = ApplicationSettings.PvOutputSystemList.Collection.Count > 1;

                SyncServiceStartup();

                LoadModbusSettings();
                saveRequired = ApplicationSettings.InitialSave;
                checkRequired = ApplicationSettings.InitialCheck;
                if (saveRequired || checkRequired)
                    butStartService.IsEnabled = false;
                butSavePVSettings.IsEnabled = saveRequired;

                if (dataGridDeviceList != null && dataGridDeviceList.SelectedItem != null)
                {
                    var selected = dataGridDeviceList.SelectedItem;
                    dataGridDeviceList.SelectedItem = null;
                    dataGridDeviceList.SelectedItem = selected;
                }
                consolidateToDeviceColumn.ItemsSource = ApplicationSettings.AllConsolidationDevicesList;
                consolidateFromDeviceColumn.ItemsSource = ApplicationSettings.AllDevicesList;

                SetDeviceVisibility();
            }
            catch (Exception ex)
            {
                SystemServices.LogMessage("Window_Loaded", "Exception: " + ex.Message, LogEntryType.ErrorMessage);
                System.Windows.MessageBox.Show(ex.Message, "Startup Exception Occurred", MessageBoxButton.OK);
            }
        }

        private void LoadModbusSettings()
        {
            comboBoxDeviceGroup.ItemsSource = ApplicationSettings.DeviceManagementSettings.DeviceGroupList;
            comboBoxMsgInterval.ItemsSource = ApplicationSettings.DeviceManagementSettings.IntervalList;
            comboBoxDBInterval.ItemsSource = ApplicationSettings.DeviceManagementSettings.IntervalList;
            comboBoxDeviceQueryInterval.ItemsSource = ApplicationSettings.DeviceManagementSettings.IntervalList;
            comboBoxDeviceDBInterval.ItemsSource = ApplicationSettings.DeviceManagementSettings.IntervalList;

            buttonDeleteDeviceMgr.IsEnabled = ApplicationSettings.DeviceManagerListCollection.Count > 0;
        }

        private void butSavePVSettings_Click(object sender, RoutedEventArgs e)
        {
            String envLog;

            if (!CheckEnvironment.SetupEnvironment(out envLog))
            {
                SystemServices.LogMessage("SaveSettings", "Fail Environment Check: " + envLog, LogEntryType.ErrorMessage);
                System.Windows.MessageBox.Show(envLog, "Environment Setup Problem", MessageBoxButton.OK);
            }

            try
            {
                ApplicationSettings.InitialSave = false;
                ApplicationSettings.SaveSettings();
                SystemServices.LogMessage("SaveSettings", "Save Location: " + ApplicationSettings.DefaultDirectory, LogEntryType.ErrorMessage);
                
                ManageService.ReloadSettings();
            }
            catch (Exception ex)
            {
                SystemServices.LogMessage("SaveSettings", "Exception: " + ex.Message, LogEntryType.ErrorMessage);
                System.Windows.MessageBox.Show("Exception saving settings: " + ex.Message, "Save Settings Exception", MessageBoxButton.OK);
            }          
        }

        private void butDefaultLogSettings_Click(object sender, RoutedEventArgs e)
        {
            if (!ApplicationSettings.LogInformation)
                ApplicationSettings.LogInformation = true;
            if (ApplicationSettings.LogTrace)
                ApplicationSettings.LogTrace = false;
            if (ApplicationSettings.LogDatabase)
                ApplicationSettings.LogDatabase = false;
            if (ApplicationSettings.LogEvent)
                ApplicationSettings.LogEvent = false;
            if (ApplicationSettings.LogDetailTrace)
                ApplicationSettings.LogDetailTrace = false;
            if (ApplicationSettings.LogMessageContent)
                ApplicationSettings.LogMessageContent = false;
            if (!ApplicationSettings.LogStatus)
                ApplicationSettings.LogStatus = true;
            if (!ApplicationSettings.LogError)
                ApplicationSettings.LogError = true;
            if (ApplicationSettings.EnableThreadTrace)
                ApplicationSettings.EnableThreadTrace = false;

            ApplicationSettings.LogRetainDays = 62;
            ApplicationSettings.NewLogEachDay = true;
            ApplicationSettings.InverterLogs = "";
        }

        private void butReleaseErrorLoggers_Click(object sender, RoutedEventArgs e)
        {
            ManageService.ReleaseErrorLoggers();
        }

        private void butCheckEnv_Click(object sender, RoutedEventArgs e)
        {
            String envLog;
            bool res = CheckEnvironment.SetupEnvironment(out envLog);
            if (res)
            {
                SystemServices.LogMessage("CheckEnvironment", envLog, LogEntryType.ErrorMessage);
                System.Windows.MessageBox.Show(envLog, "Environment Configured", MessageBoxButton.OK);
                ManageService.ForceRefresh = checkRequired;
                checkRequired = false;
                ApplicationSettings.InitialCheck = false;
            }
            else
            {
                SystemServices.LogMessage("CheckEnvironment", envLog, LogEntryType.ErrorMessage);
                System.Windows.MessageBox.Show(envLog, "Environment Problem Detected", MessageBoxButton.OK);
            }
        }

        private void butStartService_Click(object sender, RoutedEventArgs e)
        {
            butStopService.IsEnabled = false;
            butStartService.IsEnabled = false;
            ManageService.StartService(ApplicationSettings.EmitEvents);
        }

        private void butStopService_Click(object sender, RoutedEventArgs e)
        {
            butStopService.IsEnabled = false;
            butStartService.IsEnabled = false;
            ManageService.StopService(ApplicationSettings.EmitEvents);
        }

        public void OnClosing(Object sender, System.ComponentModel.CancelEventArgs e)
        {
            ManageService.StopMonitorService();
        }

        internal void ServiceStatusCallback(ServiceControllerStatus status)
        {

            if (status == ServiceControllerStatus.Running)
            {
                butStopService.IsEnabled = true;
                butStartService.IsEnabled = false;
                txtServiceStatus.Text = "Running";
            }
            else if (status == ServiceControllerStatus.Stopped)
            {
                butStopService.IsEnabled = false;
                butStartService.IsEnabled = !saveRequired && !checkRequired;
                if (checkRequired)
                    txtServiceStatus.Text = "Stopped - Check Required";
                else
                    txtServiceStatus.Text = "Stopped";
            }
            else
            {
                butStopService.IsEnabled = false;
                butStartService.IsEnabled = false;
                if (status == ServiceControllerStatus.ContinuePending)
                    txtServiceStatus.Text = "Continue Pending";
                else if (status == ServiceControllerStatus.Paused)
                    txtServiceStatus.Text = "Paused";
                else if (status == ServiceControllerStatus.PausePending)
                    txtServiceStatus.Text = "Pause Pending";
                else if (status == ServiceControllerStatus.StartPending)
                    txtServiceStatus.Text = "Start Pending";
                else if (status == ServiceControllerStatus.StopPending)
                    txtServiceStatus.Text = "Stop Pending";
            }
        }

        private void butDefaultDirectory_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog myDialog = new FolderBrowserDialog();
            myDialog.ShowNewFolderButton = true;
            myDialog.SelectedPath = ApplicationSettings.DefaultDirectory;
            String permResult = "";

            if (myDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ApplicationSettings.DefaultDirectory = myDialog.SelectedPath;
            }

            if (permResult != "")
            {
                System.Windows.MessageBox.Show(permResult, "Default Directory - Permissions Problem", MessageBoxButton.OK);
            }
        }

        private void butExecutable_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog myDialog = new OpenFileDialog();
            myDialog.CheckFileExists = true;
            myDialog.Multiselect = false;
            myDialog.FileName = ((DeviceManagerSettings)gridDeviceManagers.DataContext).ExecutablePath;
            myDialog.InitialDirectory = System.IO.Path.GetDirectoryName(myDialog.FileName);
            
            if (myDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ((DeviceManagerSettings)gridDeviceManagers.DataContext).ExecutablePath = myDialog.FileName;
            }
        }

        private void butPushDirectory_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog myDialog = new FolderBrowserDialog();
            myDialog.ShowNewFolderButton = true;
            myDialog.SelectedPath = ((DeviceManagerSettings)gridDeviceManagers.DataContext).WebBoxPushDirectory;
            String permResult = "";

            if (myDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ((DeviceManagerSettings)gridDeviceManagers.DataContext).WebBoxPushDirectory = myDialog.SelectedPath;
            }

            if (permResult != "")
            {
                System.Windows.MessageBox.Show(permResult, "WebBox Push Directory - Permissions Problem", MessageBoxButton.OK);
            }
        }

        private void dataGridPvOutputSiteList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PVSettings = (PvOutputSiteSettings) dataGridPvOutputSiteIds.SelectedItem;

            gridPvOutputSite.DataContext = PVSettings;
            if (PVSettings != null) 
                if (PVSettings.HaveSubscription)
                    SetLiveDaysComboBox(90);
                else
                    SetLiveDaysComboBox(14);
            SetPVOutputWarning();
        }

        private void dataGridDeviceManagers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DeviceManagerSettings = (DeviceManagerSettings)dataGridDeviceManagers.SelectedItem;
            if (DeviceManagerSettings != gridDeviceManagers.DataContext)
                SetDeviceContext(null);
            
            gridDeviceManagers.DataContext = DeviceManagerSettings;
            comboBoxDeviceGroup_SelectionChanged();

            if (e.RemovedItems.Count > 0)
                ((DeviceManagerSettings)(e.RemovedItems[0])).IsSelected = false;
            if (DeviceManagerSettings != null)
            {
                DeviceManagerSettings.IsSelected = true;
                if (DeviceManagerSettings.ManagerType == DeviceManagerType.Consolidation)
                    checkBoxDevMgrEnabled.Visibility = System.Windows.Visibility.Collapsed;
                else
                    checkBoxDevMgrEnabled.Visibility = System.Windows.Visibility.Visible;

                buttonDeleteDevice.IsEnabled = DeviceManagerSettings.DeviceList.Collection.Count > 0 && DeviceManagerSettings.Name != ApplicationSettings.PVOutputConsolidationName;
                buttonDeleteDeviceMgr.IsEnabled = ApplicationSettings.DeviceManagerListCollection.Count > 0 && DeviceManagerSettings.Name != ApplicationSettings.PVOutputConsolidationName;
                textBoxDeviceManagerName.IsEnabled = DeviceManagerSettings.Name != ApplicationSettings.PVOutputConsolidationName;
                buttonAddDevice.IsEnabled = textBoxDeviceManagerName.IsEnabled;
                comboBoxDeviceGroup.IsEnabled = DeviceManagerSettings.DeviceList.Collection.Count == 0;
                if (DeviceManagerSettings.DeviceList.Collection.Count > 0)
                    dataGridDeviceList.SelectedItem = DeviceManagerSettings.DeviceList.Collection[0];
            }
            else
                buttonDeleteDeviceMgr.IsEnabled = false;
        }

        private void buttonAddSite_Click(object sender, RoutedEventArgs e)
        {
            ApplicationSettings.AddPvOutputSite();
            if (ApplicationSettings.PvOutputSystemList.Collection.Count > 0)
                dataGridPvOutputSiteIds.SelectedItem = ApplicationSettings.PvOutputSystemList.Collection[ApplicationSettings.PvOutputSystemList.Collection.Count - 1];
            buttonDeleteSite.IsEnabled = ApplicationSettings.PvOutputSystemList.Collection.Count > 1;
        }

        private void buttonDeleteSite_Click(object sender, RoutedEventArgs e)
        {
            int cur = dataGridPvOutputSiteIds.SelectedIndex;

            if (cur == -1)
                return;

            ApplicationSettings.DeletePvOutputSite((PvOutputSiteSettings)dataGridPvOutputSiteIds.SelectedItem);

            if (ApplicationSettings.PvOutputSystemList.Collection.Count > 0)
                if (cur < ApplicationSettings.PvOutputSystemList.Collection.Count)
                    dataGridPvOutputSiteIds.SelectedItem = ApplicationSettings.PvOutputSystemList.Collection[cur];
                else
                    dataGridPvOutputSiteIds.SelectedItem = ApplicationSettings.PvOutputSystemList.Collection[ApplicationSettings.PvOutputSystemList.Collection.Count - 1];

            buttonDeleteSite.IsEnabled = ApplicationSettings.PvOutputSystemList.Collection.Count > 1;
        }

        private void buttonInverterRefresh_Click(object sender, RoutedEventArgs e)
        {
            DeviceDisplay iu = new DeviceDisplay(ApplicationSettings, SystemServices);
            DeviceUpdate.LoadDeviceList();
        }

        private void butBrowseOwlDb_Click(object sender, RoutedEventArgs e)
        {
            DeviceManagerSettings ommSettings = (DeviceManagerSettings)gridDeviceManagers.DataContext;
            OpenFileDialog myDialog = new OpenFileDialog();
            myDialog.CheckFileExists = true;
            myDialog.Multiselect = false;
            myDialog.FileName = ommSettings.OwlDatabase;
            if (myDialog.FileName != "")
            {
                try
                {
                    myDialog.InitialDirectory = System.IO.Path.GetDirectoryName(myDialog.FileName);
                }
                catch (Exception)
                {
                    myDialog.FileName = "";
                }
            }
            if (myDialog.FileName == "")
            {
                myDialog.InitialDirectory = @"C:\ProgramData\2SE";
                myDialog.FileName = "be.db";
            }

            if (myDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ommSettings.OwlDatabase = myDialog.FileName;
            }
        }

        private void passwordBoxServiceAccount_PasswordChanged(object sender, RoutedEventArgs e)
        {
            ApplicationSettings.ServiceAccountPassword = passwordBoxServiceAccount.Password;
            ApplicationSettings.ServiceAccountName = ApplicationSettings.ServiceAccountName;
            ApplicationSettings.ServiceAccountRequiresPassword = true;
            ApplicationSettings.ServiceDetailsChanged = true;
        }

        private void comboBoxServiceAccount_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (comboBoxServiceAccount.SelectedIndex == -1)
            {
                ApplicationSettings.ServiceAccountRequiresPassword = true;
                passwordBoxServiceAccount.IsEnabled = true;
                checkBoxServiceRequiresPassword.IsEnabled = true;
            }
            else
            {
                if (ApplicationSettings.ServiceAccountRequiresPassword)
                    ApplicationSettings.ServiceAccountRequiresPassword = false;
                    
                passwordBoxServiceAccount.IsEnabled = false;
                checkBoxServiceRequiresPassword.IsEnabled = false;
            }
        }

        private void checkBoxServiceRequiresPassword_Click(object sender, RoutedEventArgs e)
        {
            if (!checkBoxServiceRequiresPassword.IsChecked.Value)
            {
                ApplicationSettings.ServiceAccountPassword = "";
                passwordBoxServiceAccount.Password = "";
            }
        }

        private void textBoxDeviceDB_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            try
            {
                DeviceManagerSettings mmSettings = (DeviceManagerSettings)gridDeviceManagers.DataContext;
                OwlDatabaseInfo = new OwlDatabaseInfo(mmSettings, SystemServices);
                OwlDatabaseInfo.LoadApplianceList();

                if (OwlDatabaseInfo.OwlAppliances.Count < 1)
                {
                    int cnt = 0;
                    foreach (DeviceManagerDeviceSettings appl in mmSettings.DeviceList.Collection)
                    {
                        appl.Address = (ulong)cnt;
                        cnt++;
                    }
                }

                comboBoxOwlApplianceNo.ItemsSource = OwlDatabaseInfo.OwlAppliances;
            }
            catch
            {
            }

        }

        private void butInverterLogs_Click(object sender, RoutedEventArgs e)
        {
            FolderBrowserDialog myDialog = new FolderBrowserDialog();
            myDialog.ShowNewFolderButton = true;
            if (ApplicationSettings.InverterLogs != "")
                myDialog.SelectedPath = ApplicationSettings.InverterLogs;
            else
                myDialog.SelectedPath = ApplicationSettings.DefaultDirectory;
            String permResult = "";

            if (myDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                ApplicationSettings.InverterLogs = myDialog.SelectedPath;
            }

            if (permResult != "")
            {
                System.Windows.MessageBox.Show(permResult, "Inverter Logs - Permissions Problem", MessageBoxButton.OK);
            }

        }

        private void buttonAddModbusMgr_Click(object sender, RoutedEventArgs e)
        {
            dataGridDeviceManagers.SelectedItem = ApplicationSettings.AddDeviceManager();
            buttonDeleteDeviceMgr.IsEnabled = true;
            buttonDeleteDevice.IsEnabled = false;
            buttonAddDevice.IsEnabled = true;
        }

        private void buttonDeleteModbusMgr_Click(object sender, RoutedEventArgs e)
        {
            DeviceManagerSettings mmSettings = (DeviceManagerSettings)dataGridDeviceManagers.SelectedItem;
            ApplicationSettings.DeleteDeviceManager(mmSettings);
            if (ApplicationSettings.DeviceManagerListCollection.Count < 1)
                buttonDeleteDeviceMgr.IsEnabled = false;
            mmSettings = (DeviceManagerSettings)dataGridDeviceManagers.SelectedItem;
            if (mmSettings == null)
            {
                buttonAddDevice.IsEnabled = false;
                buttonDeleteDevice.IsEnabled = false;
            }
            else
            {
                buttonAddDevice.IsEnabled = true;
                buttonDeleteDevice.IsEnabled = mmSettings.DeviceList.Collection.Count > 0;
            }
        }

        private void buttonAddDevice_Click(object sender, RoutedEventArgs e)
        {
            DeviceManagerSettings mmSettings = (DeviceManagerSettings)dataGridDeviceManagers.SelectedItem;
            if (mmSettings == null)
                return;
            mmSettings.AddDevice();

            dataGridDeviceList.SelectedItem = mmSettings.DeviceList.Collection[mmSettings.DeviceList.Collection.Count - 1];
            buttonDeleteDevice.IsEnabled = true;
            comboBoxDeviceGroup.IsEnabled = DeviceManagerSettings.DeviceList.Collection.Count == 0;
        }

        private void buttonDeleteDevice_Click(object sender, RoutedEventArgs e)
        {
            DeviceManagerSettings mmSettings = (DeviceManagerSettings)dataGridDeviceManagers.SelectedItem;
            if (mmSettings == null)
                return;
            mmSettings.DeleteDevice((DeviceManagerDeviceSettings)dataGridDeviceList.SelectedItem);
            buttonDeleteDevice.IsEnabled = mmSettings.DeviceList.Collection.Count > 0;
            comboBoxDeviceGroup.IsEnabled = DeviceManagerSettings.DeviceList.Collection.Count == 0;
        }

        private void SetDeviceContext(DeviceManagerDeviceSettings device)
        {
            gridDevice.DataContext = device;
            gridConsolidations.DataContext = device;
            comboBoxDeviceType.ItemsSource = device == null ? null : device.DeviceManagerSettings.DeviceListItems;
        }

        private void SelectDevice()
        {
            DeviceManagerDeviceSettings dSettings = (DeviceManagerDeviceSettings)dataGridDeviceList.SelectedItem;
            if (dSettings != null)
            {
                gridDevice.Visibility = System.Windows.Visibility.Visible;
                buttonDeleteDevice.IsEnabled = dSettings.DeviceManagerSettings.Name != ApplicationSettings.PVOutputConsolidationName;
                textBoxDeviceName.IsEnabled = dSettings.DeviceManagerSettings.Name != ApplicationSettings.PVOutputConsolidationName;
                textBoxDeviceManagerName.IsEnabled = textBoxDeviceName.IsEnabled;
                comboBoxConsolidationType.IsEnabled = textBoxDeviceName.IsEnabled;
                comboBoxDeviceType.IsEnabled = textBoxDeviceName.IsEnabled;
                comboBoxPVOutputSystem.IsEnabled = textBoxDeviceName.IsEnabled;
                if (!dSettings.DeviceManagerSettings.IsSelected)
                {
                    dataGridDeviceManagers.SelectedItem = dSettings.DeviceManagerSettings;
                    dataGridDeviceList.SelectedItem = dSettings;
                }             
            }
            else
                gridDevice.Visibility = System.Windows.Visibility.Hidden;

            SetDeviceContext(null);
            if (dSettings != null)
            {
                SetDeviceContext(dSettings);
                dSettings.NotifySelectionChange();
            }

            SetDeviceVisibility();
        }


        // below - element is usually a label but can be others such as a StackPanel
        private void SetControlPairVisibility(System.Windows.FrameworkElement element, System.Windows.Controls.Control ctrl, SettingsUsage usage, RowDefinition row = null)
        {
            if (usage == SettingsUsage.Enabled)
            {
                if (element != null)
                    element.Visibility = System.Windows.Visibility.Visible;
                ctrl.Visibility = System.Windows.Visibility.Visible;
                ctrl.IsEnabled = true;
                if (row != null)
                    row.Height = GridLength.Auto;
            }
            else if (usage == SettingsUsage.Disabled)
            {
                if (element != null)
                    element.Visibility = System.Windows.Visibility.Visible;
                ctrl.Visibility = System.Windows.Visibility.Visible;
                ctrl.IsEnabled = false;
                if (row != null)
                    row.Height = GridLength.Auto;
            }
            else if (usage == SettingsUsage.Hidden)
            {
                if (element != null)
                    element.Visibility = System.Windows.Visibility.Hidden;
                ctrl.Visibility = System.Windows.Visibility.Hidden;
                if (row != null)
                    row.Height = new GridLength(0.0);
            }
        }

        private void SetGridRowVisibility(System.Windows.Controls.Grid grid)
        {
            bool[] isRowVisible = new bool[grid.RowDefinitions.Count];
            
            foreach (UIElement uie in grid.Children)
            {
                int row = Grid.GetRow(uie);
                isRowVisible[row] |= (uie.Visibility == System.Windows.Visibility.Visible);                
            }

            for (int i = 0; i < grid.RowDefinitions.Count; i++ )
            {
                if (isRowVisible[i])
                    grid.RowDefinitions[i].Height = GridLength.Auto;
                else
                    grid.RowDefinitions[i].Height = new GridLength(0);
            }
        }

        private void SetDeviceVisibility()
        {
            DeviceManagerDeviceSettings = (DeviceManagerDeviceSettings)dataGridDeviceList.SelectedItem;
            if (DeviceManagerDeviceSettings == null)
            {
                gridDevice.Visibility = System.Windows.Visibility.Hidden;
                return;
            }

            gridDevice.Visibility = System.Windows.Visibility.Visible;

            SetControlPairVisibility(labelDeviceAddress, textBoxDeviceAddress, DeviceManagerDeviceSettings.UseAddress);
            SetControlPairVisibility(labelSerialNo, textBoxSerialNo, DeviceManagerDeviceSettings.UseSerialNo);
            SetControlPairVisibility(labelMake, textBoxMake, DeviceManagerDeviceSettings.UseMake);
            SetControlPairVisibility(labelModel, textBoxModel, DeviceManagerDeviceSettings.UseModel);
            SetControlPairVisibility(labelDeviceQueryInterval, comboBoxDeviceQueryInterval, DeviceManagerDeviceSettings.UseQueryInterval);
            SetControlPairVisibility(labelDBInterval, comboBoxDeviceDBInterval, DeviceManagerDeviceSettings.UseDBInterval);
            SetControlPairVisibility(labelCalibrate, textBoxCalibrate, DeviceManagerDeviceSettings.UseCalibrate);
            SetControlPairVisibility(stackPanelThreshold, labelThreshold, DeviceManagerDeviceSettings.UseThreshold);
            SetControlPairVisibility(null, checkBoxHistoryAdjust, DeviceManagerDeviceSettings.UseHistoryAdjust);
            //SetControlPairVisibility(labelPVOutputSystem, comboBoxPVOutputSystem, DeviceManagerDeviceSettings.UsePVOutputSystem);

            comboBoxOwlApplianceNo.Visibility = (DeviceManagerSettings.ManagerType == DeviceManagerType.Owl_Meter) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Hidden;

            #region ConsolidationType
            if (DeviceManagerDeviceSettings.UseConsolidationType == SettingsUsage.Enabled)
            {
                expanderConsolidatedFrom.Visibility = System.Windows.Visibility.Visible;
                gridConsolidationType.Visibility = System.Windows.Visibility.Visible;
                gridConsolidationType.IsEnabled = true;
                rowPVOutput.Height = GridLength.Auto;
                SetControlPairVisibility(labelPVOutputSystem, comboBoxPVOutputSystem,
                    (DeviceManagerDeviceSettings.ConsolidationType == ConsolidationType.PVOutput) ? SettingsUsage.Disabled : SettingsUsage.Hidden);
            }
            else if (DeviceManagerDeviceSettings.UseConsolidationType == SettingsUsage.Disabled)
            {
                expanderConsolidatedFrom.Visibility = System.Windows.Visibility.Visible;
                gridConsolidationType.Visibility = System.Windows.Visibility.Visible;
                gridConsolidationType.IsEnabled = false;
                rowPVOutput.Height = GridLength.Auto;
                SetControlPairVisibility(labelPVOutputSystem, comboBoxPVOutputSystem, SettingsUsage.Disabled);
            }
            else if (DeviceManagerDeviceSettings.UseConsolidationType == SettingsUsage.Hidden)
            {
                expanderConsolidatedFrom.Visibility = System.Windows.Visibility.Collapsed;
                gridConsolidationType.Visibility = System.Windows.Visibility.Hidden;
                gridConsolidationType.IsEnabled = false;
                rowPVOutput.Height = new GridLength(0.0);
                SetControlPairVisibility(labelPVOutputSystem, comboBoxPVOutputSystem, SettingsUsage.Hidden);
            }
            #endregion

            SetGridRowVisibility(gridDevice);
            SetGridRowVisibility(gridAdvanced);
        }

        private void dataGridDeviceList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectDevice();
        }

        private void dataGridDeviceList_GotFocus(object sender, RoutedEventArgs e)
        {
            SelectDevice();
        }

        private bool IsRefresh = false;
        private void comboBoxDeviceGroup_SelectionChanged()
        {
            try
            {
                if (gridDeviceManagers.DataContext == null)
                {
                    gridDeviceManagers.Visibility = System.Windows.Visibility.Hidden;
                    gridDevice.Visibility = System.Windows.Visibility.Hidden;
                }
                else
                {
                    gridDeviceManagers.Visibility = System.Windows.Visibility.Visible;
                    if (gridDevice.DataContext == null)
                        gridDevice.Visibility = System.Windows.Visibility.Hidden;
                    else
                        gridDevice.Visibility = System.Windows.Visibility.Visible;
                }

                ProtocolSettings p = ApplicationSettings.DeviceManagementSettings.GetProtocol(((DeviceGroup)comboBoxDeviceGroup.SelectedItem).Protocol);
                gridSerialBasic.Visibility = p.UsesSerialPort ? Visibility.Visible : Visibility.Collapsed;
                gridSerialDetail.Visibility = gridSerialBasic.Visibility;
                gridListenerDevice.Visibility = p.Type == ProtocolSettings.ProtocolType.Listener ? Visibility.Visible : Visibility.Hidden;

                comboBoxDBInterval.Visibility = DeviceManagerSettings.UseDBInterval ? Visibility.Visible : Visibility.Hidden;
                labelDBInt.Visibility = comboBoxDBInterval.Visibility;
                comboBoxMsgInterval.Visibility = DeviceManagerSettings.UseQueryInterval ? Visibility.Visible : Visibility.Hidden;
                labelMsgInt.Visibility = comboBoxMsgInterval.Visibility;

                if (gridListenerDevice.Visibility == Visibility.Hidden && comboBoxDBInterval.Visibility == Visibility.Hidden && comboBoxMsgInterval.Visibility == Visibility.Hidden)
                {
                    gridListenerDevice.Visibility = Visibility.Collapsed;
                    comboBoxDBInterval.Visibility = Visibility.Collapsed;
                    comboBoxMsgInterval.Visibility = Visibility.Collapsed;
                    labelDBInt.Visibility = Visibility.Collapsed;
                    labelMsgInt.Visibility = Visibility.Collapsed;
                }

                gridExecutablePath.Visibility = p.Type == ProtocolSettings.ProtocolType.Executable ? Visibility.Visible : Visibility.Collapsed;                               
                
                if (!IsRefresh)
                {
                    IsRefresh = true;
                    gridDeviceManagers.DataContext = DeviceManagerSettings;
                }
                else
                    IsRefresh = false;

                AdjustAfterProtocolChange();   
            }
            catch
            {
            }
        }

        private void comboBoxDeviceGroup_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            comboBoxDeviceGroup_SelectionChanged();
        }

        private void AdjustAfterProtocolChange()
        {
            if (gridDeviceManagers.DataContext != null)
            {
                comboBoxDeviceType.ItemsSource = ((DeviceManagerSettings)gridDeviceManagers.DataContext).DeviceListItems;
                //comboBoxListenerDevice.ItemsSource = ((DeviceManagerSettings)gridDeviceManagers.DataContext).ListenerListItems;

                if (((DeviceManagerSettings)gridDeviceManagers.DataContext).ManagerType == DeviceManagerType.SMA_SunnyExplorer)
                {
                    gridFirstDay.Visibility = System.Windows.Visibility.Visible;
                    gridSunnyExplorerAdvanced.Visibility = Visibility.Visible;
                    labelDeviceDB.Visibility = Visibility.Collapsed;
                    textBoxDeviceDB.Visibility = Visibility.Collapsed;
                    butBrowseOwlDb.Visibility = Visibility.Collapsed;
                    gridFTP.Visibility = System.Windows.Visibility.Collapsed;
                    
                }
                else if (((DeviceManagerSettings)gridDeviceManagers.DataContext).ManagerType == DeviceManagerType.Owl_Meter)
                {
                    gridFirstDay.Visibility = System.Windows.Visibility.Collapsed;
                    gridSunnyExplorerAdvanced.Visibility = Visibility.Collapsed;
                    labelDeviceDB.Visibility = Visibility.Visible;
                    textBoxDeviceDB.Visibility = Visibility.Visible;
                    butBrowseOwlDb.Visibility = Visibility.Visible;
                    OwlDatabaseInfo = new OwlDatabaseInfo(((DeviceManagerSettings)gridDeviceManagers.DataContext), SystemServices);
                    OwlDatabaseInfo.LoadApplianceList();
                    comboBoxOwlApplianceNo.ItemsSource = OwlDatabaseInfo.OwlAppliances;
                    gridFTP.Visibility = System.Windows.Visibility.Collapsed;
                }
                else if (((DeviceManagerSettings)gridDeviceManagers.DataContext).ManagerType == DeviceManagerType.SMA_WebBox)
                {
                    gridFirstDay.Visibility = System.Windows.Visibility.Visible;
                    gridSunnyExplorerAdvanced.Visibility = Visibility.Collapsed;
                    labelDeviceDB.Visibility = Visibility.Collapsed;
                    textBoxDeviceDB.Visibility = Visibility.Collapsed;
                    butBrowseOwlDb.Visibility = Visibility.Collapsed;
                    gridFTP.Visibility = System.Windows.Visibility.Visible;
                    AdjustForPushSetting();
                }
                else
                {
                    gridFirstDay.Visibility = System.Windows.Visibility.Collapsed;
                    gridSunnyExplorerAdvanced.Visibility = Visibility.Collapsed;
                    labelDeviceDB.Visibility = Visibility.Collapsed;
                    textBoxDeviceDB.Visibility = Visibility.Collapsed;
                    butBrowseOwlDb.Visibility = Visibility.Collapsed;
                    gridFTP.Visibility = System.Windows.Visibility.Collapsed;
                }
                gridHistoryHours.Visibility =
                        ((DeviceManagerSettings)gridDeviceManagers.DataContext).ManagerType == DeviceManagerType.CC128 ? Visibility.Visible : Visibility.Collapsed;
                gridHistoryDays.Visibility =
                        ((DeviceManagerSettings)gridDeviceManagers.DataContext).ManagerType == DeviceManagerType.SMA_SunnyExplorer ? Visibility.Visible : Visibility.Collapsed;
            }
        }

        private void comboBoxDeviceGroup_LostFocus(object sender, RoutedEventArgs e)
        {
            AdjustAfterProtocolChange();
        }

        private void expanderDeviceAdvanced_Expanded(object sender, RoutedEventArgs e)
        {
            expanderDeviceMgrAdvanced.IsExpanded = false;
            expanderConsolidatedFrom.IsExpanded = false;
            expanderConsolidatesTo.IsExpanded = false;
            expanderDeviceEvents.IsExpanded = false;
            expanderDeviceFeatures.IsExpanded = false;
        }

        private void expanderDeviceMgrAdvanced_Expanded(object sender, RoutedEventArgs e)
        {
            expanderDeviceAdvanced.IsExpanded = false;
            expanderConsolidatedFrom.IsExpanded = false;
            expanderConsolidatesTo.IsExpanded = false;
            expanderDeviceEvents.IsExpanded = false;
            expanderDeviceFeatures.IsExpanded = false;
        }

        private void buttonAddConsolidateTo_Click(object sender, RoutedEventArgs e)
        {
            DeviceManagerDeviceSettings device = ((DeviceManagerDeviceSettings)gridDevice.DataContext);
            if (device == null)
                return;
            device.AddDeviceConsolidation(ConsolidateDeviceSettings.OperationType.Add);
        }

        private void buttonDeleteConsolidateTo_Click(object sender, RoutedEventArgs e)
        {
            DeviceManagerDeviceSettings device = ((DeviceManagerDeviceSettings)gridDevice.DataContext);
            if (device == null)
                return;
            device.DeleteDeviceConsolidation((ConsolidateDeviceSettings)datGridConsolidateToDevices.SelectedItem);
            buttonDeleteConsolidateTo.IsEnabled &= device.ConsolidateToDevicesCollection.Count > 0;
        }

        private void buttonAddDeviceEvent_Click(object sender, RoutedEventArgs e)
        {
            DeviceManagerDeviceSettings device = ((DeviceManagerDeviceSettings)gridDevice.DataContext);
            if (device == null)
                return;
            device.AddEvent();
            DeviceEventSettings de = (DeviceEventSettings)dataGridDeviceEvents.SelectedItem;
            buttonDeleteDeviceEvent.IsEnabled = device.DeviceEvents.Collection.Count > 0 && de != null;
        }

        private void buttonDeleteDeviceEvent_Click(object sender, RoutedEventArgs e)
        {
            DeviceManagerDeviceSettings device = ((DeviceManagerDeviceSettings)gridDevice.DataContext);
            if (device == null)
                return;
            device.DeleteEvent((DeviceEventSettings)dataGridDeviceEvents.SelectedItem);
            DeviceEventSettings de = (DeviceEventSettings)dataGridDeviceEvents.SelectedItem;
            buttonDeleteDeviceEvent.IsEnabled = device.DeviceEvents.Collection.Count > 0 && de != null;
        }

        private void datGridConsolidateToDevices_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ConsolidateDeviceSettings consol = (ConsolidateDeviceSettings)datGridConsolidateToDevices.SelectedItem;
            buttonDeleteConsolidateTo.IsEnabled = consol != null;
        }

        private void comboBoxDeviceType_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            DeviceManagerDeviceSettings curDev = (DeviceManagerDeviceSettings)dataGridDeviceList.SelectedItem;
            ApplicationSettings.RefreshAllDevices();
            dataGridDeviceList.SelectedItem = curDev;
            SelectDevice();
        }

        private void comboBoxPVOutputSystem_SourceUpdated(object sender, DataTransferEventArgs e)
        {
             
        }

        private void comboBoxConsolidationType_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            SetDeviceVisibility();
        }

        private void expanderConsolidatedFrom_Expanded(object sender, RoutedEventArgs e)
        {
            expanderDeviceMgrAdvanced.IsExpanded = false;
            expanderDeviceAdvanced.IsExpanded = false;
            expanderConsolidatesTo.IsExpanded = false;
            expanderDeviceEvents.IsExpanded = false;
            expanderDeviceFeatures.IsExpanded = false;
        }

        private void expanderConsolidatesTo_Expanded(object sender, RoutedEventArgs e)
        {
            expanderDeviceMgrAdvanced.IsExpanded = false;
            expanderDeviceAdvanced.IsExpanded = false;
            expanderConsolidatedFrom.IsExpanded = false;
            expanderDeviceEvents.IsExpanded = false;
            expanderDeviceFeatures.IsExpanded = false;
        }

        private void expanderDeviceFeatures_Expanded(object sender, RoutedEventArgs e)
        {
            expanderDeviceMgrAdvanced.IsExpanded = false;
            expanderDeviceAdvanced.IsExpanded = false;
            expanderConsolidatedFrom.IsExpanded = false;
            expanderConsolidatesTo.IsExpanded = false;
            expanderDeviceEvents.IsExpanded = false;
        }

        private void expanderDeviceEvents_Expanded(object sender, RoutedEventArgs e)
        {
            expanderDeviceMgrAdvanced.IsExpanded = false;
            expanderDeviceAdvanced.IsExpanded = false;
            expanderConsolidatedFrom.IsExpanded = false;
            expanderConsolidatesTo.IsExpanded = false;
            expanderDeviceFeatures.IsExpanded = false;
            checkBoxAutoEvents_Auto.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void expanderDeviceEvents_Collapsed(object sender, RoutedEventArgs e)
        {
            checkBoxAutoEvents_Auto.Visibility = System.Windows.Visibility.Visible;
        }

        private void dataGridDeviceEvents_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DeviceEventSettings de = (DeviceEventSettings)dataGridDeviceEvents.SelectedItem;
            buttonDeleteDeviceEvent.IsEnabled = de != null && de.Device.ManualEvents;
        }

        private void checkBoxHaveSubscription_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            if (PVSettings.HaveSubscription)
                SetLiveDaysComboBox(90);
            else               
                SetLiveDaysComboBox(14);            
        }

        private void SetLiveDaysComboBox(int days)
        {
            int val = PVSettings.LiveDaysInt;
            PVSettings.LiveDaysChangeIgnore = true;
            comboBoxLiveDays.Items.Clear();
            comboBoxLiveDays.Items.Add("");
            for (int i = 1; i <= days; i++)
            {
                comboBoxLiveDays.Items.Add(i.ToString());
            }
            
            if (val > days)
                PVSettings.LiveDays = days.ToString();
            else
                PVSettings.LiveDays = val.ToString();
            PVSettings.LiveDaysChangeIgnore = false;
        }

        private void AdjustForPushSetting()
        {
            if (((DeviceManagerSettings)gridDeviceManagers.DataContext).WebBoxUsePush)
            {
                gridFtpPush.Visibility = System.Windows.Visibility.Visible;
                gridFtpPull.Visibility = System.Windows.Visibility.Collapsed;
                gridFTPCredentials.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
            {
                gridFtpPush.Visibility = System.Windows.Visibility.Collapsed;
                gridFtpPull.Visibility = System.Windows.Visibility.Visible;
                gridFTPCredentials.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void checkBoxUseFTPPush_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            AdjustForPushSetting();
        }

        private void butSelectedSetupPVOutput_Click(object sender, RoutedEventArgs e)
        {
            SetupPVOutput(((PvOutputSiteSettings)dataGridPvOutputSiteIds.SelectedItem).SystemId, sender, e);
        }

        private void butSetupPVOutput_Click(object sender, RoutedEventArgs e)
        {
            SetupPVOutput("", sender, e);
        }

        private void SetupPVOutput(String systemId, object sender, RoutedEventArgs e)
        {
            checkBoxSetupEnabled.IsChecked = false;
            checkBoxSetupEnabled2.IsChecked = false;
            AutoConfigPVOutput autoPVOutput = new AutoConfigPVOutput();
            String message = autoPVOutput.ConfigurePVOutput(systemId);
            System.Windows.MessageBox.Show(message, "PVOutput Auto Configuration", MessageBoxButton.OK);
        }

        private void SetPVOutputWarning()
        {
            if (PVSettings == null || PVSettings.Enable && PVSettings.UploadEnable)
                labelUploadWarning.Visibility = System.Windows.Visibility.Collapsed;
            else
                labelUploadWarning.Visibility = System.Windows.Visibility.Visible;
        }

        private void checkBoxEnable_Click(object sender, RoutedEventArgs e)
        {
            SetPVOutputWarning();
        }

        private void checkBoxEnableUpload_Click(object sender, RoutedEventArgs e)
        {
            SetPVOutputWarning();
        }

        private void comboBoxDBInterval_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            // WPF defect - when property setter changes the selected value the binding does not reflect the change in the control (TwoWay did not work)
            comboBoxDBInterval.SelectedValue = ((DeviceManagerSettings)gridDeviceManagers.DataContext).DBInterval;

            DeviceManagerDeviceSettings device = ((DeviceManagerDeviceSettings)gridDevice.DataContext);
            if (device == null)
                return;
            device.RefreshAfterDefaultChange();
        }

        private void comboBoxMessageInterval_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            // WPF defect - when property setter changes the selected value the binding does not reflect the change in the control (TwoWay did not work)
            comboBoxMsgInterval.SelectedValue = ((DeviceManagerSettings)gridDeviceManagers.DataContext).MessageInterval;

            DeviceManagerDeviceSettings device = ((DeviceManagerDeviceSettings)gridDevice.DataContext);
            if (device == null)
                return;
            device.RefreshAfterDefaultChange();
        }

        private void comboBoxDeviceQueryInterval_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            comboBoxDeviceQueryInterval.SelectedValue = ((DeviceManagerDeviceSettings)dataGridDeviceList.SelectedItem).QueryInterval;
        }

        private void comboBoxDeviceDBInterval_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            comboBoxDeviceDBInterval.SelectedValue = ((DeviceManagerDeviceSettings)dataGridDeviceList.SelectedItem).DBInterval;
        }

        private void comboBoxLiveDays_SourceUpdated(object sender, DataTransferEventArgs e)
        {           
            comboBoxLiveDays.SelectedValue = ((PvOutputSiteSettings)gridPvOutputSite.DataContext).LiveDays;
        }

        private void comboBoxMMBaudRate_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            // WPF defect - when property setter changes the selected value the binding does not reflect the change in the control (TwoWay did not work)
            comboBoxMMBaudRate.SelectedValue = ((DeviceManagerSettings)gridDeviceManagers.DataContext).BaudRate;
        }

        private void comboBoxMMParity_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            // WPF defect - when property setter changes the selected value the binding does not reflect the change in the control (TwoWay did not work)
            comboBoxMMParity.SelectedValue = ((DeviceManagerSettings)gridDeviceManagers.DataContext).Parity;
        }

        private void comboBoxMMDataBits_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            comboBoxMMDataBits.SelectedValue = ((DeviceManagerSettings)gridDeviceManagers.DataContext).DataBits;
        }

        private void comboBoxMMStopBits_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            comboBoxMMStopBits.SelectedValue = ((DeviceManagerSettings)gridDeviceManagers.DataContext).StopBits;
        }

        private void comboBoxMMHandshake_SourceUpdated(object sender, DataTransferEventArgs e)
        {
            comboBoxMMHandshake.SelectedValue = ((DeviceManagerSettings)gridDeviceManagers.DataContext).Handshake;
        }
    }

    public class NullableValueConverter : IValueConverter
    {
        #region IValueConverter Members

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;            
            if (string.IsNullOrEmpty(value.ToString()))
                return null;
            return value;
        }

        #endregion
    }

}
