﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PVSettings;
using PVBCInterfaces;
using MackayFisher.Utilities;

namespace Algorithms
{
    public abstract class AlgorithmAction
    {
        public String Name { get; protected set; }
        public String Type { get; protected set; }
        protected DeviceAlgorithm Device;
        protected Exchange Exchange;
        protected ActionSettings ActionSettings;
        protected AlgorithmAction ParentAction;

        public bool ContinueOnFailure { get; protected set; }
        public bool ExitOnSuccess { get; protected set; }
        public bool ExitOnFailure { get; protected set; }
        public bool OnDbWriteOnly { get; protected set; }

        protected List<AlgorithmAction> Actions;
        protected List<ActionParameter> Parameters;
        

        public AlgorithmAction(DeviceAlgorithm device, AlgorithmAction algorithmAction, ActionSettings actionSettings)
        {
            Device = device;
            
            ParentAction = algorithmAction;
            ActionSettings = actionSettings;
            Type = actionSettings.Type;
            Name = actionSettings.Name;
            OnDbWriteOnly = actionSettings.OnDbWriteOnly;

            CommonConstruction();
        }

        protected void LogMessage(String message, LogEntryType logEntryType)
        {
            GlobalSettings.LogMessage("AlgorithmAction", message, logEntryType);
        }

        private void CommonConstruction()
        {
            ExitOnSuccess = ActionSettings.ExitOnSuccess;
            ExitOnFailure = ActionSettings.ExitOnFailure;
            if (ExitOnSuccess || ExitOnFailure)
                ContinueOnFailure = true;
            else
                ContinueOnFailure = ActionSettings.ContinueOnFailure;

            Exchange = FindExchange();
            LoadActions();
            LoadParameters();
        }

        private void LoadActions()
        {
            Actions = new List<AlgorithmAction>();
            foreach (ActionSettings actionSettings in ActionSettings.ActionList)
            {
                AlgorithmAction action = null;

                if (actionSettings.Type == "GetExchange")
                    action = new AlgorithmAction_GetExchange(Device, this, actionSettings);
                else if (actionSettings.Type == "SendExchange")
                    action = new AlgorithmAction_SendExchange(Device, this, actionSettings);
                else if (actionSettings.Type == "LogError")
                    action = new AlgorithmAction_LogError(Device, this, actionSettings);
                else if (actionSettings.Type == "RepeatCountTimes")
                    action = new AlgorithmAction_RepeatCountTimes(Device, this, actionSettings);
                else if (actionSettings.Type == "Repeat")
                    action = new AlgorithmAction_Repeat(Device, this, actionSettings);

                Actions.Add(action);
            }
        }

        private void LoadParameters()
        {
            Parameters = new List<ActionParameter>();
            foreach (ParameterSettings parameterSettings in ActionSettings.ParameterList)
            {
                ActionParameter parameter = new ActionParameter(Device, this, parameterSettings);
                Parameters.Add(parameter);

                // do not bind references to special values
                if (!parameter.IsSpecialValue)
                    parameter.Register = Device.FindRegister("", Exchange.Name, parameter.Name);
            }
        }

        internal abstract bool ExecuteInternal(int depth, OptionList options);

        protected bool ExecuteActions(int depth, OptionList options)
        {
            int newDepth = depth + 1;
            foreach (AlgorithmAction action in Actions)
                if (!action.ExecuteInternal(newDepth, options))
                {
                    if (action.ExitOnFailure)
                        return true;
                    if (!action.ContinueOnFailure)
                        return false;
                }
                else if (action.ExitOnSuccess)
                    return true;
            return true;
        }

        public Exchange FindExchange()
        {
            // return local exchange if present
            if (Exchange != null)
                return Exchange;

            string exchangeName = ActionSettings.ExchangeName;
            if (exchangeName != "")
            {
                Exchange = Device.FindExchange(exchangeName);
                if (Exchange == null)
                    throw new Exception("AlgorithmAction - Cannot find target exchange: '" + exchangeName + "'");
            }
            else if (ParentAction != null) // climb the parent tree
                Exchange = ParentAction.FindExchange();
            else
                Exchange = null;

            return Exchange;
        }

        protected void ConfigureParameters(bool isGetExchange = false)
        {
            if (isGetExchange && Exchange != null)
            {
                foreach (Register item in Exchange.ExchangeAllRegisters)
                {
                    item.ClearSetValueDelegate();
                }
            }

            foreach (ActionParameter param in Parameters)
            {
                param.SetParameterValue(isGetExchange);
            }
        }
    }

    public class Algorithm : AlgorithmAction
    {
        public Algorithm(DeviceAlgorithm device, ActionSettings actionSettings)
            : base(device, null, actionSettings)
        {
        }

        public virtual bool Execute(OptionList options)
        {
            return ExecuteInternal(0, options);
        }

        internal override bool ExecuteInternal(int depth, OptionList options)
        {
            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("Algorithm Starting - Depth: " + depth + " - Name: " + Name + " - Type: " + Type, LogEntryType.Trace);
            bool res = ExecuteActions(depth, options);
            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("Algorithm Complete - Depth: " + depth + " - Name: " + Name + " - Result: " + (res ? "true" : "false"), LogEntryType.Trace);
            return res;
        }
    }

    public class AlgorithmAction_SendExchange : AlgorithmAction
    {
        public AlgorithmAction_SendExchange(DeviceAlgorithm device, Algorithm algorithm, ActionSettings actionSettings)
            : base(device, algorithm, actionSettings)
        {
            CommonConstruction();
        }

        public AlgorithmAction_SendExchange(DeviceAlgorithm device, AlgorithmAction action, ActionSettings actionSettings)
            : base(device, action, actionSettings)
        {
            CommonConstruction();
        }

        private void CommonConstruction()
        {
            if (Exchange == null)
                throw new Exception("AlgorithmAction_SendExchange - Cannot resolve target exchange");
        }

        internal override bool ExecuteInternal(int depth, OptionList options)
        {
            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("Action SendExchange - Starting - Depth: " + depth + " - Name: " + Name + " - Type: " + Type, LogEntryType.Trace);
            bool res = ExecuteActions(depth, options);
            if (res)
            {
                ConfigureParameters();
                Exchange.SendExchange(ContinueOnFailure);
            }
            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("Action SendExchange - Complete - Depth: " + depth + " - Name: " + Name + " - Result: " + (res ? "true" : "false"), LogEntryType.Trace);
            return res;
        }
    }

    public class AlgorithmAction_GetExchange : AlgorithmAction
    {
        public AlgorithmAction_GetExchange(DeviceAlgorithm device, Algorithm algorithm, ActionSettings actionSettings)
            : base(device, algorithm, actionSettings)
        {
            CommonConstruction();
        }

        public AlgorithmAction_GetExchange(DeviceAlgorithm device, AlgorithmAction action, ActionSettings actionSettings)
            : base(device, action, actionSettings)
        {
            CommonConstruction();
        }

        private void CommonConstruction()
        {
            if (Exchange == null)
                throw new Exception("AlgorithmAction_GetExchange - Cannot resolve target exchange");
        }

        internal override bool ExecuteInternal(int depth, OptionList options)
        {
            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("Action GetExchange - Starting - Depth: " + depth + " - Name: " + Name + " - Type: " + Type, LogEntryType.Trace);
            ConfigureParameters(true);
            bool res = Exchange.GetExchange(ContinueOnFailure, true, options);
            if (res)
                res = ExecuteActions(depth, options);
            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("Action GetExchange - Complete - Depth: " + depth + " - Name: " + Name + " - Result: " + (res ? "true" : "false"), LogEntryType.Trace);
            return res || Exchange.Optional; // Do nor report failure on optional exchanges
        }
    }

    public class AlgorithmAction_LogError : AlgorithmAction
    {
        public AlgorithmAction_LogError(DeviceAlgorithm device, Algorithm algorithm, ActionSettings actionSettings)
            : base(device, algorithm, actionSettings)
        {
            CommonConstruction();
        }

        public AlgorithmAction_LogError(DeviceAlgorithm device, AlgorithmAction action, ActionSettings actionSettings)
            : base(device, action, actionSettings)
        {
            CommonConstruction();
        }

        private void CommonConstruction()
        {
            if (Exchange == null)
                throw new Exception("AlgorithmAction_LogError - Cannot resolve target exchange");
        }

        internal override bool ExecuteInternal(int depth, OptionList options)
        {
            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("Action LogError - Starting - Depth: " + depth + " - Name: " + Name + " - Type: " + Type, LogEntryType.Trace);
            ConfigureParameters();
            bool res = ExecuteActions(depth, options);

            if (res)
            {
                ErrorLogger logger = Device.Params.ErrorLogger;
                DateTime now = DateTime.Now;
                foreach (Register item in Exchange.ExchangeAllRegisters)
                {
                    if (!item.IsErrorDetail)
                        continue;
                    if (item.GetType() == typeof(RegisterBytes))
                        logger.LogError(((int)Device.Address).ToString(), now, item.Settings.Name, 2, ((RegisterBytes)item).ValueBytes);
                    else if (item.GetType() == typeof(RegisterNumber))
                        logger.LogError(((int)Device.Address).ToString(), now, item.Settings.Name + ":(" + ((RegisterNumber)item).ValueDecimal.ToString() + ")");
                    else if (item.GetType() == typeof(RegisterString))
                        logger.LogError(((int)Device.Address).ToString(), now, item.Settings.Name + ":(" + ((RegisterString)item).ValueString + ")");
                }
            }

            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("Action LogError - Complete - Depth: " + depth + " - Name: " + Name + " - Result: " + (res ? "true" : "false"), LogEntryType.Trace);

            return res;
        }
    }

    public class AlgorithmAction_RepeatCountTimes : AlgorithmAction
    {
        private RegisterNumber CountMapItem;
        private int Count = 0;

        public AlgorithmAction_RepeatCountTimes(DeviceAlgorithm device, Algorithm algorithm, ActionSettings actionSettings)
            : base(device, algorithm, actionSettings)
        {
            CommonConstruction();
        }

        public AlgorithmAction_RepeatCountTimes(DeviceAlgorithm device, AlgorithmAction action, ActionSettings actionSettings)
            : base(device, action, actionSettings)
        {
            CommonConstruction();
        }

        private void CommonConstruction()
        {
            String countName = ActionSettings.Count;
            if (countName == "")
                throw new Exception("AlgorithmAction_RepeatCountTimes - Count not specified");
            Register countMapItem = Device.FindRegister("", "", countName);
            if (countMapItem == null)
                throw new Exception("AlgorithmAction_RepeatCountTimes - Cannot find DeviceMapItem: " + countName);
            if (countMapItem.GetType() != typeof(RegisterNumber))
                throw new Exception("AlgorithmAction_RepeatCountTimes - Count is not numeric: " + countName);
            CountMapItem = (RegisterNumber)countMapItem;
        }

        internal override bool ExecuteInternal(int depth, OptionList options)
        {
            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("Action RepeatCountTimes - Starting - Depth: " + depth + " - Name: " + Name + " - Type: " + Type, LogEntryType.Trace);
            ConfigureParameters();
            Count = (int)CountMapItem.ValueDecimal;
            bool res = true;
            for (int i = 0; i < Count; i++)
                if (!ExecuteActions(depth, options))
                {
                    if (!ContinueOnFailure)
                    {
                        res = false;
                        break;
                    }
                }
            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("Action RepeatCountTimes - Complete - Depth: " + depth + " - Name: " + Name + " - Result: " + (res ? "true" : "false"), LogEntryType.Trace);
            return res;
        }
    }

    public class AlgorithmAction_Repeat : AlgorithmAction
    {
        public AlgorithmAction_Repeat(DeviceAlgorithm device, Algorithm algorithm, ActionSettings actionSettings)
            : base(device, algorithm, actionSettings)
        {
            CommonConstruction();
        }

        public AlgorithmAction_Repeat(DeviceAlgorithm device, AlgorithmAction action, ActionSettings actionSettings)
            : base(device, action, actionSettings)
        {
            CommonConstruction();
        }

        private void CommonConstruction()
        {
            bool found = false;
            foreach (AlgorithmAction action in Actions)
            {
                if (action.ExitOnSuccess || action.ExitOnFailure)
                {
                    found = true;
                    break;
                }
            }

            if (!found)
                throw new Exception("AlgorithmAction_Repeat - Cannot find exit condition");
        }

        internal override bool ExecuteInternal(int depth, OptionList options)
        {
            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("Action Repeat - Starting - Depth: " + depth + " - Name: " + Name + " - Type: " + Type, LogEntryType.Trace);
            ConfigureParameters();

            bool contin = true;
            bool retVal = true;
            int count = 0;
            int newDepth = depth + 1;
            while (contin)
            {
                foreach (AlgorithmAction action in Actions)
                    if (!action.ExecuteInternal(newDepth, options))
                    {
                        if (action.ExitOnFailure)
                        {
                            contin = false;
                            break;
                        }
                        if (!action.ContinueOnFailure)
                        {
                            contin = false;
                            retVal = false;
                            break;
                        }
                    }
                    else if (action.ExitOnSuccess)
                    {
                        contin = false;
                        break;
                    }
                if (contin)
                    count++;
            }

            foreach (ActionParameter param in Parameters)
            {
                if (param.Name == "!RepeatCount" && param.ContentVariable != null)
                {
                    ((VariableEntry_Numeric)param.ContentVariable).ExportValueDelegate(count);
                }
            }

            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("Action RepeatCountTimes - Complete - Depth: " + depth + " - Name: " + Name + " - Result: " + (retVal ? "true" : "false"), LogEntryType.Trace);

            return retVal;
        }
    }
}
