﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using PVSettings;
using MackayFisher.Utilities;
using Conversations;
using PVBCInterfaces;

namespace Algorithms
{
    public struct AlgorithmParams
    {
        public Protocol Protocol;
        public string DeviceName;
        public ObservableCollection<ExchangeSettings> ExchangeList;
        public ObservableCollection<ActionSettings> AlgorithmList;
        public EndianConverter16Bit EndianConverter16Bit;
        public EndianConverter32Bit EndianConverter32Bit;
        public ErrorLogger ErrorLogger;
    }

    public class OptionList
    {
        private List<PVSettings.TranslateOption> Options;

        public OptionList()
        {
            Options = new List<TranslateOption>();
        }

        public void Clear()
        {
            Options.Clear();
        }

        public bool HasOptions { get { return Options.Count > 0; } }

        public void Add(TranslateOptionList optionList)
        {
            foreach (TranslateOption o in optionList.OptionList)
            {
                bool found = false;
                foreach (TranslateOption o2 in Options)
                    if (o == o2)
                    {
                        found = true;
                        return;
                    }
                if (!found)
                    Options.Add(o);
            }
        }

        public bool Execute(DeviceAlgorithm deviceAlgorithm, bool mandatory, bool dbWrite, bool continueOnFailure, ref bool alarmFound, ref bool errorFound)
        {
            bool found = true;
            foreach (TranslateOption o in Options)
            {
                if (o.OptionType == TranslateOptionType.Exchange)
                {
                    found &= deviceAlgorithm.LoadExchangeType(o.Name, mandatory, dbWrite, continueOnFailure, ref alarmFound, ref errorFound); 
                }
                else
                    deviceAlgorithm.ExecuteAlgorithmType(o.Name, mandatory, dbWrite, continueOnFailure, ref alarmFound, ref errorFound);
            }
            return found;
        }
    }

    public abstract class DeviceAlgorithm 
    {
        // Add String Variables below

        public String Model { get; private set; }
        public String Manufacturer { get; private set; }
        public String SerialNo { get; private set; }

        public void SetModel(string value) { Model = value; }
        public void SetManufacturer(string value) { Manufacturer = value; }
        public void SetSerialNo(string value) { SerialNo = value; }

        public String GetSerialNo() { return SerialNo; }

        // End String Variables
        
        public decimal? ErrorCode { get; private set; }
        public void SetErrorCode(decimal value) { ErrorCode = value; }

        // Add Bytes Variables below

        public byte[] AlarmRegisters { get; private set; }
        public byte[] ErrorRegisters { get; private set; }
        public bool HaveErrorRegisters { get; private set; }

        public void SetAlarmRegisters(byte[] value) { AlarmRegisters = value; }
        public void SetErrorRegisters(byte[] value) { ErrorRegisters = value; HaveErrorRegisters = true; }

        // End Bytes Variables

        protected List<VariableEntry> VariableEntries;

        protected List<Exchange> DeviceExchanges;
        protected List<AlgorithmAction> Algorithms;

        public ByteVar InverterAddress { get; set; }
        public ByteVar ModbusCommand { get; set; }
        public ByteVar RegisterCount { get; set; }
        public ByteVar FirstModbusAddress { get; set; }

        public ByteVar DeviceDataSize { get; set; }
        public ByteVar DeviceData { get; set; }
        public ByteVar DeviceDataValueSize { get; set; }
        public ByteVar DeviceDataValue { get; set; }

        public bool FaultDetected { get; protected set; }

        public Protocol Protocol { get; private set; }

        public UInt64 Address { get; set; }

        public void SetAddress(decimal value) { Address = (UInt64)value; }
        public decimal GetAddress() { return (decimal)Address; }

        public AlgorithmParams Params;

        public OptionList Options;

        protected bool UseStaticReset;

        public DeviceAlgorithm(AlgorithmParams algorithmParams)
        {
            Options = new OptionList();
            DoInitialise(algorithmParams);            
        }

        public System.Globalization.NumberFormatInfo DeviceNumberFormat;

        public DeviceAlgorithm(DeviceSettings deviceSettings, Protocol protocol, ErrorLogger errorLogger)
        {
            Options = new OptionList();
            Protocol = protocol;
            AlgorithmParams aParams;
            aParams.EndianConverter16Bit = protocol.EndianConverter16Bit;
            aParams.EndianConverter32Bit = protocol.EndianConverter32Bit;
            aParams.Protocol = Protocol;
            DeviceNumberFormat = deviceSettings.DeviceNumberFormat;
            aParams.AlgorithmList = deviceSettings.AlgorithmList;
            aParams.ExchangeList = deviceSettings.ExchangeList;
            aParams.DeviceName = deviceSettings.Name;
            aParams.ErrorLogger = errorLogger;

            UseStaticReset = deviceSettings.StaticReset;

            DoInitialise(aParams);
        }

        private void DoInitialise(AlgorithmParams algorithmParams)
        {
            FaultDetected = false;
            VariableEntries = new List<VariableEntry>();
            Params = algorithmParams;            

            SetupVariables();

            LoadVariables();
            LoadDevice();
        }

        protected void LogMessage(String message, LogEntryType logEntryType)
        {
            GlobalSettings.LogMessage(Params.DeviceName, message, logEntryType);
        }

        protected void SetupVariables()
        {
            InverterAddress = (ByteVar)Params.Protocol.GetSessionVariable("%Address", null);
            ModbusCommand = (ByteVar)Params.Protocol.GetSessionVariable("%CommandId", null);
            RegisterCount = (ByteVar)Params.Protocol.GetSessionVariable("%Registers", null);
            FirstModbusAddress = (ByteVar)Params.Protocol.GetSessionVariable("%FirstAddress", null);
            DeviceData = (ByteVar)Params.Protocol.GetSessionVariable("%Data", null);
            DeviceDataSize = (ByteVar)Params.Protocol.GetSessionVariable("%DataSize", null);
            DeviceDataValue = (ByteVar)Params.Protocol.GetSessionVariable("%DataValue", null);
            DeviceDataValueSize = (ByteVar)Params.Protocol.GetSessionVariable("%DataValueSize", null);
        }

        private void LoadDevice()
        {
            try
            {
                DeviceExchanges = new List<Exchange>();
                foreach (ExchangeSettings exchangeSettings in Params.ExchangeList)
                {
                    Exchange exchange;
                    if (Params.Protocol.Type == ProtocolSettings.ProtocolType.Modbus)
                        exchange = new Exchange_Modbus(this, exchangeSettings, Params.Protocol);
                    else if (Params.Protocol.Type == ProtocolSettings.ProtocolType.Phoenixtec)
                        exchange = new Exchange_Phoenixtec(this, exchangeSettings, Params.Protocol);
                    else
                        exchange = new Exchange_RequestResponse(this, exchangeSettings, Params.Protocol);
                    DeviceExchanges.Add(exchange);
                }
            }
            catch (Exception e)
            {
                FaultDetected = true;
                LogMessage("Device.LoadDevice - Loading DeviceExchanges - Exception: " + e.Message, LogEntryType.ErrorMessage);
            }

            try
            {
                Algorithms = new List<AlgorithmAction>();
                foreach (ActionSettings settings in Params.AlgorithmList)
                {
                    AlgorithmAction algorithm = new Algorithm(this, settings);
                    Algorithms.Add(algorithm);
                }
            }
            catch (Exception e)
            {
                FaultDetected = true;
                LogMessage("Device.LoadDevice - Loading Algorithms - Exception: " + e.Message, LogEntryType.ErrorMessage);
            }
        }

        protected internal bool LoadExchangeType(String type, bool mandatory, bool dbWrite, bool continueOnFailure, ref bool alarmFound, ref bool errorFound)
        {
            bool found = false;
            errorFound = false;
            alarmFound = false;

            foreach (Exchange exchange in DeviceExchanges)
            {
                if (exchange.Type == type && (dbWrite || !exchange.OnDbWriteOnly))
                {
                    if (UseStaticReset)
                        Protocol.DeviceStream.ResetFixedBuffer();
                    if (!exchange.GetExchange(continueOnFailure, dbWrite, Options))
                        return false;
                    found = true;
                    errorFound |= exchange.ErrorFound;
                    alarmFound |= exchange.AlarmFound;
                }
            }

            return (!mandatory || found);
        }

        protected internal bool ExecuteAlgorithmType(String type, bool mandatory, bool dbWrite, bool continueOnFailure, ref bool alarmFound, ref bool errorFound)
        {
            bool found = false;

            foreach (Algorithm algorithm in Algorithms)
            {
                if (algorithm.Type == type && (dbWrite || !algorithm.OnDbWriteOnly))
                {
                    if (!algorithm.Execute(Options))
                        return false;
                    found = true;
                }
            }

            return (!mandatory || found);
        }

        protected internal Exchange FindExchange(String exchangeName)
        {
            foreach (Exchange exchange in DeviceExchanges)
                if (exchange.Name == exchangeName)
                    return exchange;

            return null;
        }

        protected internal VariableEntry FindVariable(String itemName)
        {
            foreach (VariableEntry var in VariableEntries)
            {
                if (var.Name == itemName)
                    return var;
            }
            return null;
        }

        protected internal Register GetRegister(Exchange exchange, RegisterSettings settings)
        {
            String itemName = settings.Content;

            if (itemName != "")
            {
                VariableEntry var = FindVariable(itemName);

                if (var != null)
                {
                    if (var.GetType() == typeof(VariableEntry_Numeric))
                        return new RegisterNumber(exchange, settings, ((VariableEntry_Numeric)var).ExportValueDelegate, ((VariableEntry_Numeric)var).ImportValueDelegate);
                    else if (var.GetType() == typeof(VariableEntry_String))
                        return new RegisterString(exchange, settings, ((VariableEntry_String)var).ExportValueDelegate, ((VariableEntry_String)var).ImportValueDelegate);
                    else if (var.GetType() == typeof(VariableEntry_Bytes))
                        return new RegisterBytes(exchange, settings, ((VariableEntry_Bytes)var).ExportValueDelegate, ((VariableEntry_Bytes)var).ImportValueDelegate);
                }
                // some devices do not need external exposure of the Register entries - eg CC128
                // LogMessage("Device.GetRegister - Cannot find 'Content': " + itemName, LogEntryType.Trace);
            }

            RegisterSettings.RegisterValueType type = settings.Type;

            if (type == RegisterSettings.RegisterValueType.rv_bytes)
                return new RegisterBytes(exchange, settings, null);
            if (type == RegisterSettings.RegisterValueType.rv_string)
                return new RegisterString(exchange, settings, null);

            return new RegisterNumber(exchange, settings, null);
        }

        protected internal Register FindRegister(String exchangeType, String exchangeName, String itemName)
        {
            foreach (Exchange exchange in DeviceExchanges)
            {
                if (exchangeType != "" && exchangeType != exchange.Type)
                    continue;
                if (exchangeName != "" && exchangeName != exchange.Name)
                    continue;
                foreach (Register item in exchange.ExchangeAllRegisters)
                {
                    if (itemName == item.Name)
                        return item;
                }
            }
            return null;
        }

        protected void UpdateDynamicDataMap()
        {
            // extract list of data item positions from the format message response
            Exchange dynamicFormatExchange = null;
            Exchange dynamicDataExchange = null;
            foreach (Exchange exchange in DeviceExchanges)
            {
                if (exchange.Name == "%GetDynamicFormat")
                    dynamicFormatExchange = exchange;
                else if (exchange.Name == "%GetDynamicData")
                    dynamicDataExchange = exchange;
            }

            if (dynamicFormatExchange == null || dynamicDataExchange == null)
                return;

            ByteVar deviceDataMap = (ByteVar)Params.Protocol.GetSessionVariable("%Payload_2", dynamicFormatExchange.Conversation);
            if (deviceDataMap == null)
                return;

            ExchangeMessage dataExchangeMessage = null;
            foreach (ExchangeMessage exchangeMsg in dynamicDataExchange.ExchangeMessages)
                if (exchangeMsg.Name == "ReceiveData")
                    dataExchangeMessage = exchangeMsg;

            if (dataExchangeMessage == null || dataExchangeMessage.DynamicDataMap == null)
                return;

            byte[] mapBytes = deviceDataMap.GetBytes();
            // assign positions to candidate items
            for (int i = 0; i < mapBytes.Length; i++)
                dataExchangeMessage.DynamicDataMap.SetItemPosition((Int16)mapBytes[i], (UInt16)(i * 2));

            // build list of expected data items
            dataExchangeMessage.DynamicDataMap.BuildItemArray();
            // build list of data item registers and associate with the dynamic data retrieval message            
            dynamicDataExchange.LoadDynamicRegisters(dataExchangeMessage);
        }

        protected virtual void LoadVariables()
        {
            VariableEntry var;

            var = new VariableEntry_String("Model", SetModel);
            VariableEntries.Add(var);
            var = new VariableEntry_String("Manufacturer", SetManufacturer);
            VariableEntries.Add(var);
            var = new VariableEntry_String("SerialNo", SetSerialNo, GetSerialNo);
            VariableEntries.Add(var);
            var = new VariableEntry_Numeric("ErrorCode", SetErrorCode);
            VariableEntries.Add(var);
            var = new VariableEntry_Bytes("AlarmRegisters", SetAlarmRegisters);
            VariableEntries.Add(var);
            var = new VariableEntry_Bytes("ErrorRegisters", SetErrorRegisters);
            VariableEntries.Add(var);
        }

        public virtual void ClearAttributes()
        {
            ErrorCode = null;
            AlarmRegisters = new byte[2];
            ErrorRegisters = new byte[2];
            HaveErrorRegisters = false;
        }

        public void LogAlarm(String type, DateTime alarmTime, IDeviceManager deviceManager)
        {
            Register alarmFlag = null;
            foreach (Exchange exchange in DeviceExchanges)
            {
                if (exchange.Type == type)
                {
                    foreach (Register item in exchange.ExchangeAllRegisters)
                        if (item.IsAlarmFlag)
                            alarmFlag = item;
                }
            }
            if (alarmFlag == null)
                return;
            string alarm = alarmFlag.ValueString;
            foreach (Exchange exchange in DeviceExchanges)
            {
                if (exchange.Type == type)
                {
                    foreach (Register item in exchange.ExchangeAllRegisters)
                        if (item.IsAlarmDetail)
                            deviceManager.ErrorLogger.LogMessage("alarm", ((int)Address).ToString(), alarmTime, alarm, 2, item.GetRegisterDataBytes());
                }
            }
        }

        public void LogError(String type, DateTime errorTime, IDeviceManager deviceManager)
        {
            Register errorFlag = null;
            foreach (Exchange exchange in DeviceExchanges)
            {
                if (exchange.Type == type)
                {
                    foreach (Register item in exchange.ExchangeAllRegisters)
                        if (item.IsErrorFlag)
                            errorFlag = item;
                }
            }
            if (errorFlag == null)
                return;
            string error = errorFlag.ValueString;
            foreach (Exchange exchange in DeviceExchanges)
            {
                if (exchange.Type == type)
                {
                    foreach (Register item in exchange.ExchangeAllRegisters)
                        if (item.IsErrorDetail)
                            deviceManager.ErrorLogger.LogMessage("error", ((int)Address).ToString(), errorTime, error, 2, item.GetRegisterDataBytes());
                }
            }
        }

        public virtual bool ExtractExchangeType(String type, bool mandatory, bool dbWrite, bool continueOnFailure, ref bool alarmFound, ref bool errorFound)
        {
            if (FaultDetected)
                return false;

            bool res = false;
            alarmFound = false;
            errorFound = false;
            try
            {                
                res = LoadExchangeType(type, mandatory, dbWrite, continueOnFailure, ref alarmFound, ref errorFound);
                if (!res)
                    return false;

                res = ExecuteAlgorithmType(type, mandatory, dbWrite, continueOnFailure, ref alarmFound, ref errorFound);
                if (!res)
                    return false;

                
            }
            catch (Exception e)
            {
                LogMessage("DeviceAlgorithm.ExtractExchangeType - Type: " + type + " - Exception: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }
            return res;
        }


        private bool DynamicMapUpdated = false;
        public virtual bool ExtractIdentity()
        {
            bool alarmFound = false;
            bool errorFound = false;

            try
            {
                Options.Clear();
                bool res = ExtractExchangeType("Identity", false, false, false, ref alarmFound, ref errorFound);
                if (res && !DynamicMapUpdated)
                {
                    // only do this after successful Identity extract
                    // can only do this once - prevent multiple dynamic map entries in table
                    UpdateDynamicDataMap();
                    DynamicMapUpdated = true;
                }
                return res;
            }
            catch (Exception e)
            {
                LogMessage("DeviceAlgorithm.ExtractIdentity - Exception: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }
        }

        public virtual bool ExtractReading(bool dbWrite, ref bool alarmFound, ref bool errorFound)
        {
            alarmFound = false;
            errorFound = false;
            bool res = false;
            if (FaultDetected)
                return res;

            String stage = "LoadExchangeType";
            try
            {
                res = LoadExchangeType("Reading", true, dbWrite, false, ref alarmFound, ref errorFound);
                if (!res)
                    return false;

                stage = "Execute";
                Options.Execute(this, true, dbWrite, false, ref alarmFound, ref errorFound);

                // Execute optional Reading Algorithms
                {
                    bool _alarmFound = false;
                    bool _errorFound = false;
                    res = ExecuteAlgorithmType("Reading", false, dbWrite, false, ref _alarmFound, ref _errorFound);
                    alarmFound &= _alarmFound;
                    errorFound &= _errorFound;
                    if (!res)
                        return false;
                }

                stage = "Errors";
                if (alarmFound)
                {
                    bool _alarmFound = false;
                    bool _errorFound = false;
                    // Execute optional Alarm Log Algorithms
                    res = ExecuteAlgorithmType("AlarmLog", false, dbWrite, false, ref _alarmFound, ref _errorFound);
                    if (!res)
                        return false;
                }

                if (errorFound)
                {
                    bool _alarmFound = false;
                    bool _errorFound = false;
                    // Execute optional Error Log Algorithms
                    res = ExecuteAlgorithmType("ErrorLog", false, dbWrite, false, ref _alarmFound, ref _errorFound);
                    if (!res)
                        return false;
                }
            }
            catch (Exception e)
            {
                LogMessage("DeviceAlgorithm.ExtractReading - Stage: " + stage + " - Exception: " + e.Message, LogEntryType.ErrorMessage);
                return false;
            }

            return res;
        }
    }
}
