﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Xml;
using System.Text;
using PVSettings;
using MackayFisher.Utilities;
using Conversations;
using PVBCInterfaces;

namespace Algorithms
{
    public class ExchangeMessage
    {
        public String Name;
        public List<Register> MessageItems;
        public byte[] Payload;
        public ByteVar PayloadByteVar;
        public UInt32 PayloadSize;
        public ByteVar PayloadSizeByteVar;
        public Message Message;
        public MessageType MessageType;
        public PVSettings.DynamicDataMap DynamicDataMap;
    }

    public abstract class Exchange
    {
        // A Modbus Exchange represents a collection of modbus registers available in a single Modbus command request
        public List<Register> ExchangeAllRegisters = null;  // All registers in the exchange - top level and inside messages
        public List<Register> ExchangeLevelRegisters = null; // Exchange level registers only - inside messages not included 
        public List<ExchangeMessage> ExchangeMessages = null;  // All messages defined in the exchange

        public DeviceAlgorithm Device;
        
        //public ICommunicationDeviceManager DeviceManager;
        public ExchangeSettings ExchangeSettings;

        public String Name { get; private set; }
        public String Type { get; private set; }

        public Conversation Conversation { get; private set; }


        public bool AlarmFound { get; protected set; }
        public bool ErrorFound { get; protected set; }
        public bool OnDbWriteOnly { get; protected set; }
        public int Base { get; private set; }

        public byte[] RegisterData;

        public Protocol Protocol;
        public ProtocolSettings.ProtocolType ProtocolType { get; private set; }

        protected Int16 retryCount;

        public bool Optional { get; private set; }

        protected void LogMessage(String message, LogEntryType logEntryType)
        {
            GlobalSettings.LogMessage("Exchange", message, logEntryType);
        }

        public Exchange(DeviceAlgorithm device, ExchangeSettings exchangeSettings, Protocol protocol)
        {
            AlarmFound = false;
            ErrorFound = false;

            Device = device;
            //DeviceManager = Device.DeviceManager;
            
            //DeviceManager = Device.Manager;
            ExchangeSettings = exchangeSettings;
            retryCount = ExchangeSettings.TimeoutRetries;
            Optional = ExchangeSettings.Optional;
            Protocol = protocol;
            ProtocolType = Protocol.Type;
            Conversation = Protocol.GetConversation(ExchangeSettings.Conversation);
            Name = ExchangeSettings.Name;
            Type = ExchangeSettings.Type;
            OnDbWriteOnly = ExchangeSettings.OnDbWriteOnly;
            LoadRegisters();
            int? temp = ExchangeSettings.Base;
            Base = temp.HasValue ? temp.Value : -1;
        }

        protected abstract void LoadRegisters();

        public virtual bool SendExchange(bool continueOnFailure)
        {
            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("SendExchange - Starting - Name: " + Name + " - Type: " + Type, LogEntryType.Trace);

            bool res = false;
            res = SendExchange_Special(continueOnFailure);

            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("SendExchange - Complete - Name: " + Name + " - Result: " + (res ? "true" : "false"), LogEntryType.Trace);
            return res;
        }

        public bool GetExchange(bool continueOnFailure, bool dbWrite, OptionList options)
        {
            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("GetExchange - Starting - Name: " + Name + " - Type: " + Type, LogEntryType.Trace);
            bool res = false;
            AlarmFound = false;
            ErrorFound = false;

            res = GetExchange_Special(continueOnFailure, dbWrite, options);

            if (res)
            {
                foreach (Register item in ExchangeAllRegisters)
                {
                    if (item.IsErrorFlag)
                    {
                        string error = item.ErrorStatus;
                        ErrorFound |= (error != "OK");
                    }
                    if (item.IsAlarmFlag)
                    {
                        string alarm = item.AlarmStatus;
                        AlarmFound |= (alarm != "OK");
                    }
                }
            }

            if (ErrorFound)
            {
                bool _alarmFound = false;
                bool _errorFound = false;
                Device.ExecuteAlgorithmType("ErrorLog", false, dbWrite, continueOnFailure, ref _alarmFound, ref _errorFound);
                AlarmFound &= _alarmFound;
                ErrorFound &= _errorFound;
            }
            if (AlarmFound)
            {
                bool _alarmFound = false;
                bool _errorFound = false;
                Device.ExecuteAlgorithmType("AlarmLog", false, dbWrite, continueOnFailure, ref _alarmFound, ref _errorFound);
                AlarmFound &= _alarmFound;
                ErrorFound &= _errorFound;
            }

            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("GetExchange - Complete - Name: " + Name + " - Result: " + (res ? "true" : "false"), LogEntryType.Trace);
            return res;
        }

        protected ExchangeMessage GetExchangeMessage(String messageName)
        {
            foreach (ExchangeMessage msg in ExchangeMessages)
                if (msg.Name == messageName)
                    return msg;

            ExchangeMessage exchangeMsg = new ExchangeMessage();
            exchangeMsg.Name = messageName;
            exchangeMsg.Message = Protocol.GetMessage(Conversation.Name, exchangeMsg.Name);
            if (exchangeMsg.Message == null)
            {
                LogMessage("NonModbus LoadRegisters - Cannot find message: " + exchangeMsg.Name, LogEntryType.ErrorMessage);
                exchangeMsg.MessageType = MessageType.Unknown;
                exchangeMsg.PayloadByteVar = null;
                exchangeMsg.PayloadSizeByteVar = null;
            }
            else
            {
                exchangeMsg.MessageType = exchangeMsg.Message.MessageType;
                exchangeMsg.PayloadByteVar = (ByteVar)exchangeMsg.Message.GetVariable("%Payload_");
                exchangeMsg.PayloadSizeByteVar = (ByteVar)exchangeMsg.Message.GetVariable("%PayloadSize_");
            }

            exchangeMsg.MessageItems = new List<Register>();
            exchangeMsg.PayloadSize = 0;
            exchangeMsg.Payload = null;

            ExchangeMessages.Add(exchangeMsg);
            return exchangeMsg;
        }

        public void LoadDynamicRegisters(ExchangeMessage exchangeMsgData)
        {
            ExchangeMessage exchangeMsgDataSize = GetExchangeMessage("ReceiveDataSize");

            XmlDocument doc = new XmlDocument();
            UInt16 size = 0;

            foreach (DynamicDataMap.MapItem item in exchangeMsgData.DynamicDataMap.Items)
            {
                Register registerItem = Device.GetRegister(this, item.templateRegisterSettings);

                if (registerItem == null)
                    LogMessage("LoadDynamicRegisters - Cannot find content: " + item.templateRegisterSettings.Content, LogEntryType.ErrorMessage);
                else
                {
                    registerItem.PayloadPosition = item.Position;
                    UInt16 minSize = (UInt16)(item.Position.Value + registerItem.CurrentSize);
                    if (size < minSize)
                        size = minSize;

                    ExchangeAllRegisters.Add(registerItem);
                    exchangeMsgData.MessageItems.Add(registerItem);
                }
            }

            exchangeMsgData.PayloadSize = size;
            if (exchangeMsgDataSize.PayloadSizeByteVar != null)
            {
                UInt32 sizeSize = (UInt32)exchangeMsgDataSize.PayloadSizeByteVar.Length;
                if (sizeSize != 1 && sizeSize != 2 && sizeSize != 4)
                {
                    LogMessage("LoadDynamicRegisters - Payload size must be 1, 2 or 4 bytes", LogEntryType.ErrorMessage);
                }
            }
            exchangeMsgData.Payload = new byte[exchangeMsgData.PayloadSize];
            exchangeMsgData.Payload.Initialize();
        }

        protected abstract bool SendExchange_Special(bool continueOnFailure);

        protected abstract bool GetExchange_Special(bool ContinueOnFailure, bool dbWrite, OptionList options);
    }

    public class Exchange_Modbus : Exchange
    {
        public UInt16? Registers { get; private set; }
        public UInt16? FirstRegister { get; private set; }
        public UInt16? LastRegister { get; private set; }

        public byte? CommandId { get; private set; }
        bool IsKLNEModbus;

        public Exchange_Modbus(DeviceAlgorithm device, ExchangeSettings exchangeSettings, Protocol protocol)
            : base(device, exchangeSettings, protocol)
        {
            CommandId = ExchangeSettings.CommandId;
            IsKLNEModbus = protocol.ProtocolSettings.Name == "KLNEModbus";
        }

        protected bool RetrieveDeviceRegisters(bool continueOnFailure)
        {
            LogMessage("RetrieveDeviceRegisters - Exchange: " + Name + " - Type: " + Type, LogEntryType.Trace);
            UInt16 firstRegister = (UInt16)(Base + FirstRegister.Value);
            if (IsKLNEModbus)
            {
                byte[] bytes = EndianConverter.GetBCDFromDecimal(Device.Address, 6, 0, false);
                Device.InverterAddress.SetBytes(ref bytes, 0, 6);
            }
            else
                Device.InverterAddress.SetByte((byte)Device.Address);

            Device.ModbusCommand.SetByte(CommandId.Value);
            Device.RegisterCount.SetBytes(Registers.Value);
            Device.FirstModbusAddress.SetBytes(firstRegister);

            int count = 0;
            bool timeOut;
            bool result;
            do
            {
                result = Protocol.DoConversation(Conversation.Name, out timeOut, continueOnFailure);
            }
            while (!result && timeOut && count++ < retryCount);

            if (!result)
            {
                LogMessage("RetrieveDeviceRegisters - Error in '" + Conversation + "' conversation", LogEntryType.Trace);
                RegisterData = new byte[2];
                return false;
            }

            RegisterData = Device.DeviceData.GetBytes();

            return true;
        }

        protected override void LoadRegisters()
        {
            bool isFirst = true;
            ExchangeAllRegisters = new List<Register>();

            Registers = null;
            FirstRegister = null;
            LastRegister = null;

            foreach (RegisterSettings register in ExchangeSettings.RegisterList)
            {
                Register registerItem = Device.GetRegister(this, register);

                if (registerItem != null)
                {
                    UInt16? id = register.Id;
                    ExchangeAllRegisters.Add(registerItem);
                    if (id.HasValue)
                    {
                        if (!FirstRegister.HasValue)
                        {
                            FirstRegister = 0;
                            LastRegister = 0;
                        }
                        if (FirstRegister.Value > id.Value || isFirst)
                        {
                            FirstRegister = id.Value;
                            isFirst = false;
                        }
                        UInt16 endRegister = (UInt16)(id.Value + register.RegisterCount - 1);
                        if (LastRegister < endRegister)
                            LastRegister = endRegister;
                    }
                }
            }

            if (FirstRegister.HasValue)
                Registers = (UInt16)((LastRegister - FirstRegister) + 1);
        }

        protected void PrepareRegisterData()
        {
            UInt16 sendSize;

            if (Registers.HasValue)
                sendSize = (UInt16)(Registers.Value * 2);  // modbus is always all response or all query, not mixed in one exchange
            else
                sendSize = 0;

            RegisterData = new byte[sendSize];
            RegisterData.Initialize();

            foreach (Register item in ExchangeAllRegisters)
            {
                if (item.Binding == null)
                {
                    item.LoadRegisterFromExternalValue();
                    item.StoreItemValue(ref RegisterData);
                }
            }
        }

        protected bool UpdateDeviceRegisters(bool continueOnFailure)
        {
            LogMessage("UpdateDeviceRegisters - Exchange: " + Name + " - Type: " + Type, LogEntryType.Trace);
            PrepareRegisterData();
            UInt16 firstRegister = (UInt16)(Base + FirstRegister.Value);
            if (IsKLNEModbus)
            {
                byte[] bytes = EndianConverter.GetBCDFromDecimal(Device.Address, 6, 0, false);
                Device.InverterAddress.SetBytes(ref bytes, 0, 6);
            }
            else
                Device.InverterAddress.SetByte((byte)Device.Address);

            Device.RegisterCount.SetBytes(Registers.Value);
            Device.FirstModbusAddress.SetBytes(firstRegister);
            byte size = (byte)(Registers.Value * 2);
            Device.DeviceDataValueSize.SetByte(size);
            Device.DeviceDataValue.SetBytes(ref RegisterData, 0, size);

            int count = 0;
            bool timeOut;
            bool result;
            do
            {
                result = Protocol.DoConversation(Conversation.Name, out timeOut, continueOnFailure);
            }
            while (!result && timeOut && count++ < retryCount);

            if (!result)
            {
                LogMessage("UpdateDeviceRegisters - Error in '" + Conversation + "' conversation", LogEntryType.ErrorMessage);
                return false;
            }

            return true;
        }

        protected override bool SendExchange_Special(bool continueOnFailure)
        {
            return UpdateDeviceRegisters(continueOnFailure);
        }

        protected override bool GetExchange_Special(bool ContinueOnFailure, bool dbWrite, OptionList options)
        {
            bool res = RetrieveDeviceRegisters(ContinueOnFailure);
            if (res)
                foreach (Register item in ExchangeAllRegisters)
                    if (item.Binding == null)
                        item.GetItemValue(ref RegisterData, options);
            return res;
        }
    }

    public abstract class Exchange_NonModbus : Exchange
    {
        public Exchange_NonModbus(DeviceAlgorithm device, ExchangeSettings exchangeSettings, Protocol protocol)
            : base(device, exchangeSettings, protocol)
        {
        }

        private UInt16 LoadRegisterList(ObservableCollection<RegisterSettings> registers, List<Register> list)
        {
            UInt16 size = 0;
            foreach (RegisterSettings register in registers)
            {
                Register registerItem = Device.GetRegister(this, register);
                if (registerItem == null)
                    LogMessage("LoadRegisters - Cannot find content: " + register.Content, LogEntryType.ErrorMessage);
                else
                {
                    if (registerItem.BindingName == "")
                    {
                        if (registerItem.PayloadPosition.HasValue)  // items with position specified in config
                        {
                            UInt16 minSize = (UInt16)(registerItem.PayloadPosition.Value + registerItem.CurrentSize);
                            if (size < minSize)
                                size = minSize;
                        }
                        else  // items with position determined by order in configuration
                        {
                            registerItem.PayloadPosition = size;
                            size += registerItem.CurrentSize;
                        }
                    }
                    ExchangeAllRegisters.Add(registerItem);
                    if (list != null)
                        list.Add(registerItem);
                }
            }
            return size;
        }

        protected override void LoadRegisters()
        {
            ExchangeAllRegisters = new List<Register>();
            ExchangeLevelRegisters = new List<Register>();
            ExchangeMessages = new List<ExchangeMessage>();

            // Load exchange level registers
            LoadRegisterList(ExchangeSettings.RegisterList, ExchangeLevelRegisters);

            // Load registers defined at message scope level
            foreach (ExchangeMessageSettings msg in ExchangeSettings.MessageList)
            {
                ExchangeMessage exchangeMsg = GetExchangeMessage(msg.Name);

                exchangeMsg.PayloadSize = LoadRegisterList(msg.RegisterList, exchangeMsg.MessageItems);
                if (exchangeMsg.PayloadSizeByteVar != null)
                {
                    UInt32 sizeSize = (UInt32)exchangeMsg.PayloadSizeByteVar.Length;
                    if (sizeSize != 1 && sizeSize != 2 && sizeSize != 4)
                    {
                        LogMessage("NonModbus LoadRegisters - Payload size must be 1, 2 or 4 bytes", LogEntryType.ErrorMessage);
                    }
                }
                exchangeMsg.Payload = new byte[exchangeMsg.PayloadSize];
                exchangeMsg.Payload.Initialize();
                exchangeMsg.DynamicDataMap = msg.DynamicDataMap;
            }
        }

        protected void SetupProtocolBindings()
        {
            foreach (Register item in ExchangeAllRegisters)
            {
                if (!item.BoundToSend && !item.BoundToRead && !item.BoundToFind)
                    continue;
                Type type = item.Binding.GetType();

                if (item.HasExternalInputValueBinding)
                    item.LoadRegisterFromExternalValue();

                if (type == typeof(StringVar))               
                    ((StringVar)item.Binding).Value = item.ValueString;                
                else if (type == typeof(ByteVar))
                {
                    byte[] bytes = item.ValueBytes;
                    ((ByteVar)item.Binding).SetBytes(ref bytes, 0, bytes.Length);
                }
            }
        }

        protected void PrepareRegisterData()
        {
            foreach (ExchangeMessage msg in ExchangeMessages)
            {
                if (msg.MessageType == MessageType.Send || msg.MessageType == MessageType.Find || msg.MessageType == MessageType.Read)
                {
                    msg.Payload.Initialize();
                    foreach (Register item in msg.MessageItems)
                    {
                        if (item.MappedToRegisterData)
                        {
                            item.LoadRegisterFromExternalValue();   // pulls external values into the register
                            item.StoreItemValue(ref msg.Payload);   // and places it into the payload
                        }
                    }
                    if (msg.PayloadSizeByteVar != null)
                    {
                        UInt32 sizeSize = (UInt32)msg.PayloadSizeByteVar.Length;
                        if (sizeSize == 1)
                            msg.PayloadSizeByteVar.SetByte((byte)msg.PayloadSize);
                        else if (sizeSize == 2)
                            msg.PayloadSizeByteVar.SetBytes((UInt16)msg.PayloadSize);
                        else if (sizeSize == 4)
                            msg.PayloadSizeByteVar.SetBytes((UInt32)msg.PayloadSize);
                    }
                    if (msg.PayloadByteVar != null)
                        msg.PayloadByteVar.SetBytes(ref msg.Payload, 0, (int)msg.PayloadSize);
                }
            }
        }

        protected bool SendRequest()
        {
            LogMessage("SendRequest - Exchange: " + Name + " - Type: " + Type, LogEntryType.Trace);

            // SetupProtocolBindings must be first as it pulls in external values for all registers where required
            SetupProtocolBindings();
            PrepareRegisterData();

            int count = 0;
            bool timeOut;
            bool result;
            do
            {
                result = Protocol.DoConversation(Conversation.Name, out timeOut);
            }
            while (!result && timeOut && count++ < retryCount);

            if (!result)
            {
                LogMessage("SendRequest - Error in '" + Conversation + "' conversation", LogEntryType.Trace);
                return false;
            }

            return true;
        }

        protected bool DoQueryResponse(bool continueOnFailure, OptionList options)
        {
            LogMessage("DoQueryResponse - Exchange: " + Name + " - Type: " + Type, LogEntryType.Trace);

            // SetupProtocolBindings must be first as it pulls in external values for all registers where required
            SetupProtocolBindings();
            PrepareRegisterData();

            int count = 0;
            bool timeOut;
            bool result;
            do
            {
                result = Protocol.DoConversation(Conversation.Name, out timeOut, continueOnFailure, Optional);
            }
            while (!result && !Optional && timeOut && count++ < retryCount);

            if (!result)
            {
                RegisterData = new byte[2];
                if (Optional || continueOnFailure && timeOut)
                    return false;
                LogMessage("DoQueryResponse - Failure in '" + Conversation + "' conversation", LogEntryType.Trace);                
                return false;
            }

            foreach (ExchangeMessage msg in ExchangeMessages)
                if ((msg.MessageType == MessageType.Extract || msg.MessageType == MessageType.ExtractDynamic ) && msg.PayloadByteVar != null)
                    msg.Payload = msg.PayloadByteVar.GetBytes();

            foreach (Register item in ExchangeAllRegisters)
            {
                if (!item.BoundToExtract)
                    continue;

                Type type = item.Binding.GetType();

                // extract value from bound protocol variables
                if (type == typeof(StringVar))
                    item.ValueString = ((StringVar)item.Binding).Value;
                else if (type == typeof(ByteVar))
                    item.SetValueBytes(options, ((ByteVar)item.Binding).GetBytes());
                else if (type == typeof(DynamicByteVar))
                    item.SetValueBytes(options, ((DynamicByteVar)item.Binding).GetBytes());
                // push extracted value back to external variable
                item.LoadContentVariable();
            }

            foreach (ExchangeMessage msg in ExchangeMessages)
                if (msg.MessageType == MessageType.Extract || msg.MessageType == MessageType.ExtractDynamic)
                    foreach (Register item in msg.MessageItems)
                        if (!item.BoundToExtract)
                            item.GetItemValue(ref msg.Payload, options);   // if no binding, extract value from payload, push back to external variable

            if (GlobalSettings.SystemServices.LogTrace)
                LogMessage("DoQueryResponse - Success", LogEntryType.Trace);

            return true;
        }
    }

    public class Exchange_RequestResponse : Exchange_NonModbus
    {
        public Exchange_RequestResponse(DeviceAlgorithm device, ExchangeSettings exchangeSettings, Protocol protocol)
            : base(device, exchangeSettings, protocol)
        {
        }

        protected override bool SendExchange_Special(bool continueOnFailure)
        {
            return SendRequest();
        }

        protected override bool GetExchange_Special(bool ContinueOnFailure, bool dbWrite, OptionList options)
        {
            return DoQueryResponse(ContinueOnFailure, options);
        }
    }

    public class Exchange_Phoenixtec : Exchange_NonModbus
    {
        public Exchange_Phoenixtec(DeviceAlgorithm device, ExchangeSettings exchangeSettings, Protocol protocol)
            : base(device, exchangeSettings, protocol)
        {
        }

        protected override bool SendExchange_Special(bool continueOnFailure)
        {
            return SendRequest();
        }

        protected override bool GetExchange_Special(bool ContinueOnFailure, bool dbWrite, OptionList options)
        {
            return DoQueryResponse(ContinueOnFailure, options);
        }
    }
}
