﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Algorithms
{
    /*
     *      Algorithms.Exchange      Conversations.Conversation
     *      Algorithms.ExchangeMessage     Conversations.Message
     * Device definitions contain element declarations. 
     * These elements are either directly inside an exchange definition or nested inside a message definition that is itself inside an exchange definition.
     * If an element declaration has a value attribute, the element is a literal. It carries a fixed value that can be bound to a Conversation.Variable
    */
    public abstract class VariableEntry
    {
        public String Name { get; private set; }

        public VariableEntry(String name)
        {
            Name = name;
        }
    }

    public class VariableEntry_Numeric : VariableEntry
    {
        public VariableEntry_Numeric(String name, SetNumberValueDelegate exportValueDelegate, GetNumberValueDelegate importValueDelegate = null)
            : base(name)
        {
            ExportValueDelegate = exportValueDelegate;
            ImportValueDelegate = importValueDelegate;
        }

        public SetNumberValueDelegate ExportValueDelegate;
        public GetNumberValueDelegate ImportValueDelegate;
    }

    public class VariableEntry_String : VariableEntry
    {
        public VariableEntry_String(String name, SetStringValueDelegate exportValueDelegate, GetStringValueDelegate importValueDelegate = null)
            : base(name)
        {
            ExportValueDelegate = exportValueDelegate;
            ImportValueDelegate = importValueDelegate;
        }

        public SetStringValueDelegate ExportValueDelegate;
        public GetStringValueDelegate ImportValueDelegate;
    }

    public class VariableEntry_Bytes : VariableEntry
    {
        public VariableEntry_Bytes(String name, SetBytesValueDelegate exportValueDelegate, GetBytesValueDelegate importValueDelegate = null)
            : base(name)
        {
            ExportValueDelegate = exportValueDelegate;
            ImportValueDelegate = importValueDelegate;
        }

        public SetBytesValueDelegate ExportValueDelegate;
        public GetBytesValueDelegate ImportValueDelegate;
    }
}
