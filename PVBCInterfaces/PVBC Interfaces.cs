﻿/*
* Copyright (c) 2012 Dennis Mackay-Fisher
*
* This file is part of PV Bean Counter
* 
* PV Bean Counter is free software: you can redistribute it and/or 
* modify it under the terms of the GNU General Public License version 3 or later 
* as published by the Free Software Foundation.
* 
* PV Bean Counter is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with PV Bean Counter.
* If not, see <http://www.gnu.org/licenses/>.
*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace PVBCInterfaces
{
    public enum LogEntryType
    {
        StatusChange,
        ErrorMessage,
        Information,
        Format,
        MessageFormat,
        Trace,
        DetailTrace,
        MessageContent,
        Database,
        Event
    }

    public interface IThreadControl
    {
        bool TraceEnabled { get; }
    }

    public interface IUtilityLog
    {
        bool EnableThreadTrace { get; set; }
        bool ThreadTraceEnabled { get; set; }
        bool LogStatus { get; set; }
        bool LogFormat { get; set; }
        bool LogInformation { get; set; }
        bool LogError { get; set; }
        bool LogTrace { get; set; }
        bool LogDetailTrace { get; set; }
        bool LogMessageContent { get; set; }
        bool LogDatabase { get; set; }
        bool LogEvent { get; set; }

        void LogMessage(string component, string message, LogEntryType logEntryType);

        void GetDatabaseMutex();
        void ReleaseDatabaseMutex();
    }


    public interface IEvents
    {
        void EmitEventTypes(bool updatedEvents);
        void NewStatusEvent(string statusType, string statusText);
        void ScanForEvents();
        ManualResetEvent PVEventReadyEvent { get; }
        int MaxConsolidationDepth { get; set; }
    }
}

namespace Conversations
{
    public enum MessageType
    {
        // first value must start at 0
        Read = 0,
        Find,
        Extract,
        ExtractDynamic,
        Send,
        Unknown,   // used when config error condition exists - cannot locate message definition
        ValueCount // leave ValueCount as last value - it supplies the number of values in the enum
    }

}
